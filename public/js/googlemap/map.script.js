var goMap = {
    type: "cluster",
    container : "goMap",
    current: 0,
    shows: 0,
    data: [],
    options: {},
    cluster: {},
    markers: {},
    map: {},
    user_location : 0,
    setMap: function() {
        var lat_lng;

        if ( (typeof this.options.map.center != 'undefined' && typeof this.data[0] == 'undefined') || 1 == this.user_location ) {
            lat_lng = this.options.map.center;
        } else {
            lat_lng = new google.maps.LatLng(this.data[0]['lat'], this.data[0]['lng']);
        }
        this.options.map.center = lat_lng;


        this.map = new google.maps.Map(document.getElementById(this.container), this.options.map);
    },
    setMarkers: function() {
        var options = {};
        if (typeof this.options.marker != 'undefined') {
            options = this.options.marker;
        }

        if (typeof options.infobox != 'undefined' && typeof InfoBox != 'undefined') {
            var infoBox = new InfoBox(options.infobox);
        }

        if (typeof options.infowindow != 'undefined') {
            var infoWindow = new google.maps.InfoWindow();
        }

        var markers = [];
        $(this.data).each(function(i, loc) {

            if ("cluster" == this.type) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(loc['lat'], loc['lng']),
                    icon: options.icon
                });

            } else {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(loc['lat'], loc['lng']),
                    map: goMap.map,
                    icon: options.icon
                });


            }

            if (typeof infoWindow != 'undefined') {

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var iwContent = "";
                        if (typeof options.infowindow.type != 'undefined') {
                            if ("contact" == options.infowindow.type) {
                                iwContent += "<a href='tel:" + loc['phone'].replace(/ /g, "-") + "'>Call Us</a>";
                                if ("1" == loc['mobile_setup']) {
                                    iwContent += "<br /><a href='" + loc['website'] + "'>View Site</a>";
                                }
                            }
                        }
                        infoWindow.setContent("<div style='width:60px;'>" + iwContent + "</div>");
                        infoWindow.open(this.map, marker);
                    }
                })(marker, i));
            }
            if (typeof infoBox != 'undefined') {
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var loc_content = [];

                        $.ajax({
                            url:base_url+"/propertyData/" + loc['id'],
                            type:'GET',
                            async: false,
                            cache: false,
                            dataType: 'json',
                            success: function(data){
                                loc_content = data;
                            }
                        });

                        console.log(loc_content);

                        var boxContent = $("<div/>");

                        var boxImage = $("<div/>")
                            .addClass('boxImage')
                            .css({
                                "background-image": " url(" + loc_content['image'] + ")",
                                "background-size": "100% 100%"
                            });

                        var boxDetail = $("<div/>").addClass("boxDetail");

                        if ("" != loc_content['price']) {
                            var detailPrice = $("<p/>")
                                .addClass("detailPrice")
                                .html(loc_content['price']);

                            boxDetail.append(detailPrice);
                        }
                        if ("" != loc_content['suburb']) {
                            var detailSuburb = $("<p/>")
                                .addClass("detailSuburb")
                                .html(loc_content['suburb']);

                            boxDetail.append(detailSuburb);
                        }
                        if ("" != loc_content['address']) {
                            var detailAddress = $("<p/>")
                                .addClass("detailAddress")
                                .html(loc_content['address']);

                            boxDetail.append(detailAddress);
                        }
                        if ("" != loc_content['beds'] || "" != loc_content['baths'] ||  "" != loc_content['cars']) {
                            var detailOption = $("<p/>")
                                .addClass("detailOption");

                            if ("" != loc_content['beds']) {
                                var optionBeds =  $("<span/>").html(loc_content['beds']);
                                detailOption.append(optionBeds);
                                detailOption.append($("<img/>").attr('src', bed_img));
                            }
                            if ("" != loc_content['baths']) {
                                var optionBaths =  $("<span/>").html(loc_content['baths']);
                                detailOption.append(optionBaths);
                                detailOption.append($("<img/>").attr('src', bath_img));
                            }
                            if ("" != loc_content['cars']) {
                                var optionCars =  $("<span/>").html(loc_content['cars']);
                                detailOption.append(optionCars);
                                detailOption.append($("<img/>").attr('src', car_img));
                            }

                            boxDetail.append(detailOption);
                        }
                        if ("" != loc_content['link']) {
                            var boxLink = $("<div/>")
                                .addClass("boxLink");

                            var linkHref = $("<a/>")
                                .attr("href", loc_content['link'])
                                .html("View Details")
                                .attr('target', '_blank');

                            boxLink.append(linkHref);
                        }

                        boxContent.append(boxImage);
                        boxContent.append(boxDetail);
                        boxContent.append(boxLink);

                        infoBox.setContent(boxContent.wrap("<div/>").parent().html());

                        infoBox.open(this.map, marker);
                    }
                })(marker, i));

                google.maps.event.addListener(goMap.map, 'click', function (){
                    infoBox.close();
                });
                google.maps.event.addListener(goMap.map, 'zoom_changed', function (){
                    infoBox.close();
                });
            }



            markers.push(marker);
        });

        this.markers = markers;
    },
    setCluster: function() {
        var options = {};

        if (typeof this.options.cluster != 'undefined') {
            options = this.options.cluster;
        }

        var markerClusterOptions = {};

        if (typeof options.style != 'undefined') {
            markerClusterOptions = {
                styles: [options.style]
            };
        }
        if (typeof MarkerClusterer != 'undefined') {
            this.cluster = new MarkerClusterer(this.map, this.markers, markerClusterOptions);
        }

        if (typeof options.event != 'undefined') {
            if (typeof options.event.mouseover != 'undefined') {
                google.maps.event.addListener(this.cluster,'mouseover',function(c){
                    c.clusterIcon_.div_.firstChild.src=options.event.mouseover.icon;
                });
            }
            if (typeof options.event.mouseout != 'undefined') {
                google.maps.event.addListener(this.cluster,'mouseout',function(c){
                    c.clusterIcon_.div_.firstChild.src=options.event.mouseout.icon;
                });
            }
        }
    },
    init : function (_opts) {
        if (typeof _opts.container != 'undefined') {
            this.container = _opts.container;
        }
        if (typeof _opts.data != 'undefined') {
            this.data      = _opts.data;
            this.current   = (this.data).length;
        }
        if (typeof _opts.options != 'undefined') {
            this.options   = _opts.options;
        }
        if (typeof _opts.type != 'undefined') {
            this.type   = _opts.type;
        }
    },
    render : function () {

        this.setMap();
        this.setMarkers();
        if ("cluster" == this.type) {
            this.setCluster();
        }

        this.shows = this.current;
    },
    mergeData: function (_data) {
        var newData = [];
        var i = 0;

        $(this.data).each(function(key, val) {
            newData[i] = val;
            i++;
        });

        $(_data).each(function(key, val) {
            newData[i] = val;
            i++;
        });

        this.data = newData;
        this.current = (this.data).length;
    }

};
