goMap.init({
    container:"gMap",
    options: {
        map: {
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: new google.maps.LatLng("-25", "134")
        },
        marker:{
            icon:"/img/icon-map-pin.png",
            infobox: {
                boxClass: 'infoBox'
                ,maxWidth: 0
                ,pixelOffset: new google.maps.Size(-170, -40)
                ,zIndex: null
                ,closeBoxURL: ""
                ,infoBoxClearance: new google.maps.Size(20, 20)
                ,pane: "floatPane"
                ,enableEventPropagation: false
                ,alignBottom: true
            }
        },
        cluster:{
            event: {
                mouseover: {
                    icon: "/img/icon-map-circle2.png"
                },
                mouseout: {
                    icon: "/img/icon-map-circle.png"
                }
            },
            style:{
                textColor: "#ffffff",
                fontFamily: "Arial",
                textSize: 14,
                height: 56,
                width: 60,
                url: "/img/icon-map-circle.png"
            }
        }
    }
});
var tmpDataForm = $('#ListingResultsForm').serializeArray();

$("#btn-view-map").click(function(){
    $('#view-map').addClass('active');
    goMap.data = [];
    goMap.render();

    ajaxMap(1);
});

var ajaxMap = function (i) {
    var dataForm = tmpDataForm;

    for (var item in dataForm) {
        if (dataForm[item].name == 'pageno') {
            dataForm[item].value = i;
            break;
        }
    }
    dataForm = $.param(dataForm);

    $.ajax({
        url: "/listings/mapData/residential",
        type:'GET',
        data: dataForm,
        async: true,
        cache: false,
        dataType: 'json',
        success: function(data){
            goMap.mergeData(data.map_data);
            goMap.render();
        }
    });
}