
    $(document).ready(function(){
        $('#subscribe').on('submit',function(e){
            e.preventDefault(); // this prevents the form from submitting
            $.ajax({
                url: site_url+'/subscriber',
                type: "POST",
                data: {'email':$('#sub-email').val(), '_token': token, 'subscribed':1},
                dataType: 'JSON',
                beforeSend: function(){
                    $('.sub-success').text('');
                    $('.sub-error').text('');
                    $('#spin').show();
                },
                success: function (data) {
                    if (data.status == 'success'){
                        $('.sub-success').text(data.message);
                        $('#spin').hide();
                        $('#sub-email').val('');
                        bootbox.alert("Thankyou!! You have been successfully added to our subscriber list.");
                    } else {
                        $('.sub-error').text(data.error);
                        $('#spin').hide();
                    }
                }
            });
        });
    });






$(document).on("keypress", '#search-form', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});



