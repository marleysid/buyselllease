//function for input mask
function inputmask_phonenumber(phone_number_new,id){
	var phone_number = phone_number_new.replace(/\s+/g, '');
    var initialchar = phone_number.substring(0,4);
    var inititialchar_sec = phone_number.substring(0,2);
    var mask_two = "99 9999 9999";
    var mask_four = "9999 999 999";
    $(id).unmask();
    if(initialchar == 1800 || initialchar == 1300 || inititialchar_sec == 04){
        
        $(id).mask(mask_four);
        //console.log('1st');
    }else if(inititialchar_sec == 02 || inititialchar_sec == 03 || inititialchar_sec == 07){
        $(id).unmask(mask_two);
        $(id).mask(mask_two);
        //console.log('2nd');
    }else{
    	if(phone_number != ""){
     	   $(id).mask(mask_four);
    	}else{
	        $(id).unmask(mask_four);
	    }	
        //console.log('last');
    }
}


$().ready(function() {
       
        // validate signup form on keyup and submit
        
        $("#professional-signup-form").validate({
            ignore: [],
            rules: {
                first_name: "required",
                last_name: "required",
                password: {
                    required: true,
                    minlength: 5
                },
                comfirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                street_address: "required",
                logo: "required",
                business_name: "required",
                category: "required",
                agree: "required",
                business_position: "required",
                work_phone_number: "required",
                link: {
                    required:true,
                    url: true
                },
                description: "required",
            },
            messages: {
                first_name: "Please enter your firstname",
                last_name: "Please enter your lastname",                
                password: {
                    required: "Enter a password so you may edit your details in the future",
                    minlength: "Your password must be at least 5 characters long"
                },
                comfirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                street_address: "Please enter street address",
                suburb:"Select from drop down screen to make sure you pick the right suburb in the right state",
                email: "Please enter a valid email address",
                category: "Please select your industry type from drop down",
                business_name: "Please enter name of your business",
                agree: "Please accept our policy",
                business_position: "Please enter your position in the business",
                work_phone_number: "Please enter office or work phone number",
                link: "Please enter your website address for consumers to be  directed to, it should be started from http://",
                description: "Please enter description of what your business does",
                "region[]": "Please select one of the above states."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "agree" )
                    error.insertAfter(".agreecon");
                else
                    error.insertAfter(element);
                
                if(element.attr("name")=="region[]")
                    error.insertAfter(".lasterr");

            }
            
        });
//Agent sign up form validation
    var tagCheckRE = new RegExp("(\\w+)(\\s+)(\\w+)");
    $.validator.addMethod("tagcheck", function(value, element) { 
        return tagCheckRE.test(value);
    }, "At least two words.");
    //validation with suburb at least one comma
    // $.validator.addMethod("addressValidation", function( value, element ) {
    //     var regex = new RegExp("^.*\, ?[a-zA-Z]*");
    //     console.log(value);
    //         if ( !regex.test(value) ) {
    //             return false;
    //         }
    //         return true;
    // });

    $("#agent-signup-form").validate({
        ignore: [],
            rules: {
                name: {
                    required: true,
                    tagcheck: true
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                street_address: "required",
                logo: "required",
                address: "required",
                suburb:"required",
                agree: "required",
                postcode: "required",
                phone: "required",
                link: {
                    required:true,
                    url:true
                },
                description: "required",
                trading: "required",
                staffURL: {
                    required:true,
                    url:true
                },
                crm: "required"
            },
            messages: {
                name: "Please enter your name in order (Firstname Lastname)",
                // last_name: "Please enter your lastname",                
                password: {
                    required: "Please provide a password, Enter a password so you may edit your details in the future",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                street_address: "Please enter street address.",
                suburb:"Select from drop down screen to make sure you pick the right suburb in the right state.",
                
                email: "Please enter a valid email address.",
                address: "Please enter office address.",
                agree: "Please accept our policy.",
                postcode: "Please enter your office postcode.",
                phone: "Please enter office or work phone number.",
                link: "Please enter your website address for company, it should be started from http://",
                description: "Please enter description of what your business does.",
                trading: "Please enter company trading name.",
                staffURL: "Please enter your website address for meet the team page, it should be started from http://",
                crm: "Please enter who is the company that uploads your property."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "agree" )
                    error.insertAfter(".agreecon");
                else
                    error.insertAfter(element);
            }
    });



    $("#update-search").validate({
        ignore: [],
        rules : {
            suburb: {
                required: true
            } 
        },

        messages: {
            suburb: "Suburb cannot be empty. Please select suburb"
        }
    });
});

