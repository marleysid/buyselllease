/**
 * Created by sharmila timsina on 9/25/2015.
 */

$('#advertisement-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: site_url+'/admin/advertisement/getAdvanceFilterData'
});
