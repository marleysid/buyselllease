<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Agent;

class AgentTableSeeder extends Seeder {

    public function run()
    {
        Agent::create([
            'name'                  => 'Highlands',
            'trading_name'          => 'Highland Property',
            'import_id'             => 'HIGHLAND',
            'address'               => '25 Kingsway Cronulla NSW',
            'abn'                   => '',
            'acn'                   => '139 860 487',
            'listing_base_url'      => 'http://www.highlandproperty.com.au/',
            'listing_staff_url'     => 'http://www.highlandproperty.com.au/about/our-team.php',
            'logo_url'              => 'http://highlandproperty.com.au/img/logo.png',
            'primary_colour'        => '#717174',
            'ftp_home_directory'    => '/home/agentbox',
        ]);

        Agent::create([
            'name'                  => 'Belle Properties',
            'trading_name'          => 'Highland Property',
            'import_id'             => 'HIGHLAND',
            'address'               => 'Shop 1, 25-31 Perouse Road, Randwick NSW 2031',
            'abn'                   => '13 386 207 397',
            'acn'                   => '',
            'listing_base_url'      => 'http://www.belleproperty.com/',
            'listing_staff_url'     => 'http://www.belleproperty.com/about/our-team.php',
            'logo_url'              => 'http://www.belleproperty.com/img/main-logo.png',
            'primary_colour'        => '#fff',
            'ftp_home_directory'    => '/home/agentbox',
        ]);
    }

}
