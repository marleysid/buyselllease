<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHomedirToAgents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// add ftp home directory attribute
        Schema::table('Agent', function($table) {
            $table->string('ftp_home_directory', 255);
        });

    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// remove ftp home directory attribute
        Schema::table('Agent', function($table) {
            $table->dropColumn('ftp_home_directory');
        });
	}

}
