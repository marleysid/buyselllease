<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyPayload extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('property_data', function(Blueprint $table)
        {
            $table->integer('import_payload_id', false, true)->after('property_id');
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('property_data', function(Blueprint $table)
        {
            $table->dropColumn('import_payload_id');
            $table->dropSoftDeletes();
        });
	}

}
