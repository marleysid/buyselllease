<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ImportPayloadSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('import_payloads', function(Blueprint $table) {
            // identifiers
            $table->increments('id')->unsigned();

            // data
            $table->mediumText('payload');
            $table->dateTime('processed');

            // timestamps
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('import_payloads');
    }
}
