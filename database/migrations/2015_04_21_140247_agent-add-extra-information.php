<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgentAddExtraInformation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('agents', function(Blueprint $table) {
            $table->string('trading_name', 255)->after('name');
            $table->string('address', 255)->after('trading_name');
            $table->string('abn', 255)->after('address');
            $table->string('acn', 255)->after('abn');
            $table->string('listing_staff_url', 255)->after('listing_base_url');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('agents', function(Blueprint $table)
        {
            $table->dropColumn('trading_name');
            $table->dropColumn('address');
            $table->dropColumn('abn');
            $table->dropColumn('acn');
            $table->dropColumn('listing_staff_url');
        });
	}

}
