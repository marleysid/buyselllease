<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PropertySchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('Agent', function($table) {
            // identifiers
            $table->increments('id')->unsigned();

            // data
            $table->string('name', '255');
            $table->string('logo_url', '255');
            $table->string('listing_base_url', '255');
            $table->string('primary_colour', '8');

            // timestamps
            $table->softDeletes();
            $table->timestamps();

            // indexes
        });

        Schema::create('Property', function($table) {
            // identifiers
            $table->increments('id')->unsigned();
            $table->integer('agent_id')->unsigned();

            // data
            $table->string('property_code', 255);
            $table->string('postcode', 255);
            $table->string('suburb', 255);
            $table->string('geo_location', 255);

            // timestamps
            $table->timestamps();
            $table->softDeletes();

            // indexes (each main field as this is the 'engine room' for search)

            // keys
            $table->foreign('agent_id')
                ->references('id')->on('Agent')
                ->onDelete('cascade');

        });

        Schema::create('PropertyData', function($table) {
            // identifiers
            $table->increments('id')->unsigned();
            $table->integer('property_id')->unsigned();

            $table->text('payload');

            // keys
            $table->foreign('property_id')
                    ->references('id')->on('Property')
                    ->onDelete('cascade');
        });

        Schema::create('SearchProfile', function($table) {
            // identifiers
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();

            // data
            $table->text('profile'); // json encoded string

            // timestamps
            $table->softDeletes();
            $table->timestamps();

            // keys
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::create('PropertyView', function($table) {
            // identifiers
            $table->integer('user_id')->unsigned();
            $table->integer('property_id')->unsigned();

            // data
            $table->string('interaction'); // website/email/other
            $table->timestamp('seen_on');

            // keys
            $table->primary(array('user_id', 'property_id'));
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('property_id')
                ->references('id')->on('Property')
                ->onDelete('cascade');
        });

        Schema::create('FavouriteProperties', function($table) {
            // identifiers
            $table->integer('user_id')->unsigned();
            $table->integer('property_id')->unsigned();

            // data
            $table->string('interaction'); // website/email/other
            $table->timestamp('added_on');

            // keys
            $table->primary(array('user_id', 'property_id'));
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('property_id')
                ->references('id')->on('Property')
                ->onDelete('cascade');
        });

        Schema::create('BlockedProperties', function($table) {
            // identifiers
            $table->integer('user_id')->unsigned();
            $table->integer('property_id')->unsigned();

            // data
            $table->string('reason'); // reason for block
            $table->timestamp('blocked_on');

            // keys
            $table->primary(array('user_id', 'property_id'));
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('property_id')
                ->references('id')->on('Property')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('Agent');
        Schema::drop('Property');
        Schema::drop('PropertyData');
        Schema::drop('SearchProfile');
        Schema::drop('PropertyView');
        Schema::drop('FavouriteProperties');
        Schema::drop('BlockedProperties');
    }
}
