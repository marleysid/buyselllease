<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProperty extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('properties', function(Blueprint $table)
        {
            $table->string('property_type')->after('property_code');
            $table->integer('price', false, true)->after('property_type');
            $table->string('land_area')->after('price');
            $table->string('category')->after('land_area');
            $table->string('parking')->after('suburb');
            $table->string('bedrooms')->after('parking');
            $table->string('bathrooms')->after('bedrooms');
            $table->string('address')->after('bathrooms');
            $table->string('lat')->after('address');
            $table->string('lng')->after('lat');
            $table->dateTime('source_updated')->after('lng');
            $table->dropColumn('geo_location');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('properties', function(Blueprint $table)
        {
            $table->dropColumn(array(
                'property_type',
                'price',
                'land_area',
                'category',
                'parking',
                'bedrooms',
                'bathrooms',
                'address',
                'lat',
                'lng',
                'source_updated'
            ));
            $table->string('geo_location');
        });
	}

}
