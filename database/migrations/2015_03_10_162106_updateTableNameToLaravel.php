<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTableNameToLaravel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Agent', function(Blueprint $table)
		{
			$table->rename('agents');
		});

        Schema::table('Property', function(Blueprint $table)
        {
            $table->rename('properties');
        });

        Schema::table('PropertyData', function(Blueprint $table)
        {
            $table->rename('property_data');
        });

        Schema::table('SearchProfile', function(Blueprint $table)
        {
            $table->rename('search_profiles');
        });

    }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('agents', function(Blueprint $table)
        {
            $table->rename('Agent');
        });

        Schema::table('properties', function(Blueprint $table)
        {
            $table->rename('Property');
        });

        Schema::table('property_data', function(Blueprint $table)
        {
            $table->rename('PropertyData');
        });

        Schema::table('search_profiles', function(Blueprint $table)
        {
            $table->rename('SearchProfile');
        });

	}

}
