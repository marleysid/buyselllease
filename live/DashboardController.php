<?php namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Professionals;
use App\Models\Property;
use App\Models\PropertyHelper;
use App\Repositories\AgentRepository;
use App\Repositories\ProfessionalRepository;
use App\Repositories\PropertyRepository;
use SH;
use App\Models\SearchProfile;
use App\Models\User;
use Auth;
use DB;
use Flash;
use GH;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Image;
use Jenky\LaravelPlupload\Exception;
use Mail;

class DashboardController extends Controller {

	public function listAll() {
		$properties = Property::all();
		$data = array(
			'properties' => $properties,
			'search_profile' => json_decode(SH::getSearchProfileFromSession()),
		);

		$properties->shuffle();

		return view('dashboard.list', $data);
	}

	public function search(Request $request, $search_profile_id = NULL) {
		/*----------------------------------------
			        |
			            IF   SEARCH PROFILE ID EXIST  IT  SEARCH FOR  THAT  PROFILE AND PUT IT TO SESSION ELSE
			        |
			        --------------------------------------------
		*/

		if (!is_null($search_profile_id) && $search_profile_id != 'professionals') {
			$searchprofle = SearchProfile::where('id', $search_profile_id)->first();
			$sessionSearchProfile_array = json_decode($searchprofle->profile, true);
			SH::saveArrayToSessionSearchProfile($sessionSearchProfile_array);
			$sessionSearchProfile = SH::getSearchProfileFromSession();

		} else {
            if(!empty($request))
			   SH::saveRequestToSessionSearchProfile($request);

			$sessionSearchProfile = SH::getSearchProfileFromSession();
		}






		$is_empty_searchprofile=json_decode($sessionSearchProfile,true);

		if ( empty($is_empty_searchprofile)) {
			Flash::error('You have not  provided  sufficient  filters for  property search.');
			return  redirect('/');
		}
		$agreementType = SH::getSearchProfileValue('agreement-type');





		if (Auth::user()) {

			$searchProfile = SearchProfile::firstOrNew(array('user_id' => Auth::user()->id));
			$searchProfile->user_id = Auth::user()->id;
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();
			/* $searchProfile = SearchProfile::create([
				                'user_id' => Auth::user()->id,
				                'profile' => $sessionSearchProfile,
			*/
		} else {
			$searchProfile = SearchProfile::firstOrNew(array('ip' => $request->getClientIp()));
			$searchProfile->user_id = 0;
			$searchProfile->ip = $request->getClientIp();
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();
			/* $searchProfile = SearchProfile::firstorCreate([
				                'id' => 0,
				                'user_id' => 0,
				                'profile' => $sessionSearchProfile,
			*/
		}
		
		if (Input::get('sort') && Input::get('sort') == 'favourite') {
			if(!Auth::check() || (Auth::check() && Auth::user()->role =='1')){
				Flash::warning("Sorry!!  You must be logged in to use this feature.");
	            return \Redirect::to('auth/login');
			}
		}


		$properties = PropertyRepository::findAllBySearchProfile($searchProfile);

		$searchProfileData = json_decode($searchProfile->profile, true);
		
		if ($agreementType == 'sell') {

			$agents = AgentRepository::findAllBySearchProfile($searchProfile); //@todo further work required at data capture
			// $agents = Agent::get();
			/* $data = array(
	                'agents' => $agents,

*/
			$searchprofileid = $searchProfile->id;
			$searchProfile = $searchProfileData;
			$agents->setPath('search');
			return view('dashboard.agent-list', compact('agents', 'searchprofileid','searchProfile'));
		}



		if($agreementType == 'lease' ) {
			if(isset($searchProfileData['tenant'])){

			
				if($searchProfileData['tenant'] =='landlord') {


					$agents = AgentRepository::findAllBySearchProfile($searchProfile); //@todo further work required at data capture
					//dd($agents);
					$searchprofileid = $searchProfile->id;
					$searchProfile = $searchProfileData;

					$agents->setPath('search');

					return view('dashboard.agent-list', compact('agents', 'searchprofileid','searchProfile'));
				}
			}
		}




		$data = array(
			'searchprofileid' => $searchProfile->id,
			'properties' => $properties,
			'searchProfile' => $searchProfileData,
			'listingType' => $searchProfileData['agreement-type'] == 'buy' ? 'sale' : 'lease',
			'priceMultiplier' => $searchProfileData['agreement-type'] == 'buy' ? 100000 : 1,
		);
		//dd(DB::getQueryLog());
		if (!Input::get('sort')) {
			$properties->shuffle();
		}

		$properties->setPath('search');
		//dd(DB::getQueryLog());
		
		return view('dashboard.list', $data);
	}

	public function Professionals(Request $request, $search_profile_id = NULL) {

		if (!is_null($search_profile_id) && $search_profile_id != 'professionals') {
			$searchprofle = SearchProfile::where('id', $search_profile_id)->first();
			$sessionSearchProfile_array = json_decode($searchprofle->profile, true);
			SH::saveArrayToSessionSearchProfile($sessionSearchProfile_array);
			$sessionSearchProfile = SH::getSearchProfileFromSession();

		} else {
			SH::saveRequestToSessionSearchProfile($request);
			$sessionSearchProfile = SH::getSearchProfileFromSession();

		}
		$sessionSearchProfile = SH::getSearchProfileFromSession();
		$agreementType = SH::getSearchProfileValue('agreement-type');


		if (Auth::user()) {

			$searchProfile = SearchProfile::firstOrNew(array('user_id' => Auth::user()->id));
			$searchProfile->user_id = Auth::user()->id;
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();
			/* $searchProfile = SearchProfile::create([
				                 'user_id' => Auth::user()->id,
				                 'profile' => $sessionSearchProfile,
			*/
		} else {
			$searchProfile = SearchProfile::firstOrNew(array('ip' => $request->getClientIp()));
			$searchProfile->user_id = 0;
			$searchProfile->ip = $request->getClientIp();
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();
			/* $searchProfile = SearchProfile::firstorCreate([
				                 'id' => 0,
				                 'user_id' => 0,
				                 'profile' => $sessionSearchProfile,
			*/
		}
		/*$searchProfile = SearchProfile::firstOrCreate([
			            'user_id' => 0,
			            'profile' => $sessionSearchProfile,
		*/

		$searchprofileid = $searchProfile->id;

		$professionals = ProfessionalRepository::findAllBySearchProfile($searchProfile);
		$searchProfile = json_decode($searchProfile->profile, true);

		if ($agreementType == 'sell') {
			$redirect_url_text = 'See Agents';
		} else if($agreementType =='lease') {
			if($searchProfile['tenant'] =='landlord') {
				$redirect_url_text = 'See Agents';
			} else {
				$redirect_url_text = 'See Properties';
			}
		} else {
			$redirect_url_text = 'See Properties';
		}
		$professionals->shuffle();
		$professionals->setPath('professionals');
		return View('dashboard.professional-list', compact('professionals', 'redirect_url_text', 'searchprofileid', 'searchProfile'));
	}

	/*
		     * ------------------------------------------------------------------------------
		     * verifying user email after signup
		     * ------------------------------------------------------------------------------
	*/
	public function confirm($code) {
		if (!$code) {
			throw new InvalidConfirmationCodeException;
		}

		$user = User::whereconfirmation($code)->first();

		if (!$user) {
			throw new InvalidConfirmationCodeException;
		}

		$user->status = 1;
		$user->confirmation = null;
		$user->save();

		return Redirect::back()
			->withMessage('Sucess !! Email verified successfully');
	}

	/*
		     * ------------------------------------------------------------------------------------
		     * ALLOWING USER TO CREATE PASSWORD
		     * -------------------------------------------------------------------------------------
	*/
	public function createPassword(Request $request) {
		$v = validator::make($request->all(), [
			'password' => 'required',
			'confirmpassword' => 'required | same:password',
		]);

		if ($v->fails()) {
			return redirect::back()
				->withErrors($v->messages());
		} else {

			//update user row with new password
			try {
				DB::beginTransaction();
				DB::table('users')
					->where('id', Auth::user()->id)
					->update(['password' => Hash::make(Input::get('password'))]);

			} catch (\PDOException $e) {
				DB::rollback();
				return redirect::back()
					->with('error', 'There was some problem creating a password. Please try later');
			}
			DB::commit();
			Auth::logout();
			return redirect::to('auth/login')
				->with('message', 'Please relogin with the password');
		}
	}

	/*
		     * ------------------------------------------------------------------------------------
		     * ALLOWING USER TO CHANGE PASSWORD
		     * -------------------------------------------------------------------------------------
	*/

	public function changePasswordView() {
		if (Auth::user()->password != '') {
			return view('auth.changepassword');
		} else {
			return view('auth.createpassword');
		}
	}

	public function changePassword(Request $request) {
		$data = $request->all();
		$rules = [
			'oldpassword' => 'required',
			'newpassword' => 'required',
			'confirmnewpassword' => 'required | same:newpassword',
		];

		$messages = [
			'oldpassword.required' => 'You must enter your old password',
			'newpassword.required' => 'You must enter your new password',
			'confirmnewpassword.required' => 'You must enter your confirm password',
			'same' => 'New password and confirm password must match',
		];

		$v = Validator::make($data, $rules, $messages);

		if ($v->fails()) {
			return redirect::back()
				->withErrors($v->messages());
		} else {

			$oldPassword = Hash::check(Input::get('oldpassword'), Auth::user()->password);

			if ($oldPassword) {
				/*
					                 * ------------------------
					                 * old password and new password matched
					                 * now check whether old password and new password are same or not
					                 * ------------------------
				*/

				$confirmNewPassword = Hash::make(Input::get('newpassword'));
				$newPassword = Hash::check(Input::get('oldpassword'), $confirmNewPassword);

				/*
					                 * -----------------------------------------------------------
					                 *give error message if old password and new password is same
					                 * -----------------------------------------------------------------
				*/

				if ($newPassword) {
					return redirect::back()
						->with('error', 'Oops !! Old Password and New password should be different');
				} else {
					//starting the transaction
					DB::beginTransaction();
					try {
						DB::table('users')
							->where('id', '=', Auth::user()->id)
							->update(['password' => $confirmNewPassword]);

					} catch (\Exception $e) {
						DB::rollback();
						return redirect::back()
							->with('error', 'This operation couldnot be performed at this time. Please try later');
					}
				}

				DB::commit();
				//send email to user mentioning about changed password
				$data = ['email' => Auth::user()->email];

				$signupText = DB::table('emailtemplates')
					->where('type', '=', 'changepassword')
					->first();
				$contents = $signupText->text;

				$content = [
					'content' => $contents,
					'email' => Auth::user()->email,
				];

				Mail::send('emails.notify', $content, function ($message) use ($content) {
					$message->to(Auth::user()->email)->subject('buyselllease.com.au-Password Changed');
					$message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
				});

				return redirect::back()
					->with('message', 'Password Updated successfully');

			} else {
				/*
					                * ------------------------
					                * user didnot entered correct old password..redirect back
					                * ------------------------
				*/
				return redirect::back()
					->with('error', 'You didnot enter your old password correctly');
			}

		}

	}

	public function get_property_details($id) {
		$property = Property::where('property_id', $id)->first();
		return \Response::make($property);
	}

	/*
		     * ---------------------------------------------------------------------------------------------------
		     * UPDATING THE PROFESSIONAL INFORMATION ##GET REQUEST
		     * ---------------------------------------------------------------------------------------------------
	*/
	public function updateProfessional() {
		try {
			$selectedProfessions = DB::table('professions')->get();

			$professionalDetails = DB::table('users')
				->join('professionals', 'users.id', '=', 'professionals.user_id')
				->where('professionals.user_id', '=', Auth::user()->id)
				->where('users.id', '=', Auth::user()->id)
				->first();

		} catch (\Exception $e) {
			return redirect::back()
				->with('error', 'Oops!! There was some problem. Try later');
		}

		return view('dashboard.profile')->with(array(
			'profession' => $selectedProfessions,
			'details' => $professionalDetails,
		));
	}

	public function getMap(Request $request) {
		$sessionSearchProfile = SH::getSearchProfileFromSession();
		$agreementType = SH::getSearchProfileValue('agreement-type');
		if ($agreementType == 'sell') {
			$redirect_url_text = 'See Agents';
		} else {
			$redirect_url_text = 'See Properties';
		}

		if (Auth::user()) {
			$searchProfile = SearchProfile::firstOrNew(array('user_id' => Auth::user()->id));
			$searchProfile->user_id = Auth::user()->id;
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();

		} else {
			$searchProfile = SearchProfile::firstOrNew(array('ip' => $request->getClientIp()));
			$searchProfile->user_id = 0;
			$searchProfile->ip = $request->getClientIp();
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();

		}


		$searchprofileid = $searchProfile->id;

		$properties = PropertyRepository::findAllBySearchProfileMap($searchProfile);

		$map_data = [];
		if (!empty($properties)) {
			foreach ($properties as $property) {
				$prof_data = [];
				$prof_data['lat'] = $property->lat;
				$prof_data['lng'] = $property->lng;
				$prof_data['id'] = $property->id;
				array_push($map_data, $prof_data);
			}
		}

		return json_encode(['map_data' => $map_data]);
	}

	public function getMapProfessional(Request $request) {
		$sessionSearchProfile = SH::getSearchProfileFromSession();
		$agreementType = SH::getSearchProfileValue('agreement-type');
		if ($agreementType == 'sell') {
			$redirect_url_text = 'See Agents';
		} else {
			$redirect_url_text = 'See Properties';
		}

		if (Auth::user()) {
			$searchProfile = SearchProfile::firstOrNew(array('user_id' => Auth::user()->id));
			$searchProfile->user_id = Auth::user()->id;
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();

		} else {
			$searchProfile = SearchProfile::firstOrNew(array('ip' => $request->getClientIp()));
			$searchProfile->user_id = 0;
			$searchProfile->ip = $request->getClientIp();
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();

		}


		$searchprofileid = $searchProfile->id;

		$professionals = ProfessionalRepository::findAllBySearchProfileMap($searchProfile);






		$map_data = [];
		if (!empty($professionals)) {
			foreach ($professionals as $professional) {
				$prof_data = [];
				$prof_data['lat'] = $professional->lat;
				$prof_data['lng'] = $professional->lng;
				$prof_data['id'] = $professional->user_id;
				array_push($map_data, $prof_data);
			}
		}

		return json_encode(['map_data' => $map_data]);
	}

	public function getMapAgent(Request $request) {
		$sessionSearchProfile = SH::getSearchProfileFromSession();
		$agreementType = SH::getSearchProfileValue('agreement-type');

		if ($agreementType == 'sell') {
			$redirect_url_text = 'See Agents';
		} else {
			$redirect_url_text = 'See Properties';
		}

		if (Auth::user()) {
			$searchProfile = SearchProfile::firstOrNew(array('user_id' => Auth::user()->id));
			$searchProfile->user_id = Auth::user()->id;
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();

		} else {
			$searchProfile = SearchProfile::firstOrNew(array('ip' => $request->getClientIp()));
			$searchProfile->user_id = 0;
			$searchProfile->ip = $request->getClientIp();
			$searchProfile->profile = $sessionSearchProfile;
			$searchProfile->save();

		}


		$searchprofileid = $searchProfile->id;

		$agents = AgentRepository::findAllBySearchProfileMap($searchProfile);

		$map_data = [];
		if (!empty($agents)) {
			foreach ($agents as $agents) {
				$prof_data = [];
				$prof_data['lat'] = $agents->lat;
				$prof_data['lng'] = $agents->lng;
				$prof_data['id'] = $agents->id;
				array_push($map_data, $prof_data);
			}
		}



		return json_encode(['map_data' => $map_data]);
	}

	public function propertyData($id) {

		$property = Property::where('id', $id)->first();

		$detail_array = [];
		if (!empty($property)) {

			$detail_array['image'] = PropertyHelper::getData($property, 'img_m');
			$detail_array['link'] = PropertyHelper::getURL($property);
			$detail_array['beds'] = $property->bedrooms;
			$detail_array['baths'] = $property->bathrooms;
			$detail_array['cars'] = $property->parking;
			$detail_array['address'] = $property->address;
			$detail_array['suburb'] = $property->suburb;
			$detail_array['price'] = $property->price;
			$detail_array['description'] = GH::limit_character(PropertyHelper::getData($property, 'description'), 25);

		}

		return \Response::make($detail_array);
	}

	public function professionalData($id) {

		$professional = Professionals::where('user_id', $id)->first();

		$prof_array = [];
		if (!empty($professional)) {

			$prof_array['link'] = GH::pritify_url($professional->url);
			$prof_array['image'] = !empty($professional->logo) && file_exists(public_path('uploads/professionals').'/'.$professional->logo)?asset('uploads/professionals').'/'.$professional->logo:asset('uploads/professionals').'/'.'noimage.png';
			$prof_array['address'] = $professional->street_address;
			$prof_array['suburb'] = $professional->suburb;
		}

		return \Response::make($prof_array);
	}

	public function agentData($id) {

		$agent = Agent::where('id', $id)->first();

		$prof_array = [];
		if (!empty($agent)) {

			$prof_array['link'] = GH::pritify_url($agent->listing_base_url);
			$prof_array['logo'] = GH::pritify_url($agent->logo_url);
			$prof_array['address'] = $agent->address;
			$prof_array['suburb'] = $agent->suburb;
		}

		return \Response::make($prof_array);
	}
	

	/*
		     * ------------------------------------------------------------------------
		     * PROFESSIONAL PROFILE UPDATE
		     * ------------------------------------------------------------------------
	*/
	public function confirmProfessionalUpdate(Request $request) {
		//update the professional profile
		$professionalValidation = validator::make($request->all(), [
			'username' => 'required',
			'firstname' => 'required',
			'lastname' => 'required',
			'streetaddress' => 'required',
			'suburb' => 'required',
			'business' => 'required',
			'company' => 'required',
			'position' => 'required',
			'mobile' => 'required',
			'category' => 'required',
			'address' => 'required',
			'description' => 'required',

		]);

		if ($professionalValidation->fails()) {
			return redirect::back()
				->withErrors($professionalValidation->messages())
				->withInput();
		} else {
			//save the updated data into database
			$userData = ['name' => Input::get('username')];

			$professionalData = [
				'firstname' => Input::get('firstname'),
				'lastname' => Input::get('lastname'),
				'street_address' => Input::get('streetaddress'),
				'suburb' => Input::get('suburb'),
				'business_name' => Input::get('business'),
				'company_name' => Input::get('company'),
				'business_position' => Input::get('position'),
				'contact' => Input::get('work'),
				'mobile_no' => Input::get('mobile'),
				'profession' => Input::get('category'),
				'url' => Input::get('address'),
				'description' => Input::get('description'),
			];

			if (Input::file('userfile')) {

				$image = Input::file('userfile');

				$filename = time() . $image->getClientOriginalName();
				$path = public_path('uploads/professionals/' . $filename);
				Image::make($image->getRealPath())->save($path);
				$professionalData['logo'] = $filename;
			}
			try {
				DB::beginTransaction();

				DB::table('users')
					->where('id', '=', Auth::user()->id)
					->update($userData);

				DB::table('professionals')
					->where('user_id', '=', Auth::user()->id)
					->update($professionalData);
				DB::commit();

			} catch (Exception $e) {
				DB::rollback();

				return redirect::back()
					->with('error', 'Oops!! There was some problem updating the information. Please try later');
			}

			return redirect::back()
				->with('message', 'Professional information has been updated successfully');
		}
	}

	public function storeNotitication(Request $request) {
		if ($request->ajax()) {
			$notification = $request->on;
			$user_id = Auth::user()->id;
			$user = User::find($user_id);
			$user->notification = $notification;
			$user->save();
			if ($notification == 1) {
				Flash::success('Notification services is  actived.');

			} else {

				Flash::success('Notification services is  deactived.');
			}

			return \Response::make(['success' => 1]);

		}

	}


	/*
	 |Display the property detail
	 | Property details will be displayed according to the property code
	 */
    public function propertyDetail($code)
    {

        try {
           $propertyDetails = Property::where('property_code', $code)->first();
           
           $otherDetails = DB::table('properties as p')
           					->join('agents as a', 'a.id', '=', 'p.agent_id')
           					->where('property_code', '=', $code)
           					->first();
        	
       	

			$images =PropertyHelper::getImages($propertyDetails);

			$inspectionTimes = PropertyHelper::getInspections($propertyDetails);

			return view('dashboard.property-details')
				->with(array(
					'propertyDetails' => $propertyDetails,
					'images'		  => $images,
					'otherDetails'	  => $otherDetails,
					'inspectionsTimes' =>$inspectionTimes
				));

        } catch(\PDOException $e){
            Flash::error('Property couldnot be found');
        }


		//return view('dashboard.property-details', compact('propertyDetails','images'));



    }



	public function printDetail($code)
	{

		try {
			$propertyDetails = Property::where('property_code', $code)->first();

			$otherDetails = DB::table('properties as p')
				->join('agents as a', 'a.id', '=', 'p.agent_id')
				->where('property_code', '=', $code)
				->first();



			$images =PropertyHelper::getImages($propertyDetails);

			$inspectionTimes = PropertyHelper::getInspections($propertyDetails);

			return view('dashboard.print')
				->with(array(
					'propertyDetails' => $propertyDetails,
					'images'		  => $images,
					'otherDetails'	  => $otherDetails,
					'inspectionsTimes' =>$inspectionTimes
				));

		} catch(\PDOException $e){
			Flash::error('Property couldnot be found');
		}


		//return view('dashboard.property-details', compact('propertyDetails','images'));



	}

}
