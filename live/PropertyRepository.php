<?php namespace App\Repositories;

use GCH;
use App\Models\Property;
use SH;
use App\Models\SearchProfile;
use DB;
use Input;

class PropertyRepository {

	public static function findAllBySearchProfile(SearchProfile $searchProfile)
	{

		$searchProfileArray = json_decode($searchProfile->profile, true);

		if (isset($searchProfileArray['suburb'])) {
			$address = $searchProfileArray['suburb'];
		} else {
			$address = '';
		}


		$property = Property::query();
		//$property->with('favouriteproperties');
		$whereClauses = [];


		foreach ($searchProfileArray as $key => $value) {
			if (empty($value)) {
				continue;
			} // skip any empty values from the search form

			switch ($key) {
				case 'suburb':

					// @todo inc_surround == all properties for the time being
					if (empty($searchProfileArray['inc-surrounding'])) {
						$property->select('*')->where('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');

					} else {
						// Uses  address to get   its  lat lng  of   submitted  subub

						if (empty($searchProfileArray['cover_disatance'])) {
							$distance = 5;
						} else {
							$distance = $searchProfileArray['cover_disatance'];
						}


						$latlng = GCH::geolocate_address($address);
						if ($latlng) {
							$lat = $latlng['lat'];
							$lng = $latlng['lng'];
							$property->distance($lat, $lng, $distance, 'KM');
						}

					}
					break;
				case 'price-range':
				        if(isset($searchProfileArray['mode'])){
					$maxIndex = explode(',', $value)[1];

					list($minValue, $maxValue) = explode(',', SH::getCalculatedRange(
						$searchProfileArray['agreement-type'],
						$searchProfileArray['mode'],
						$value
					));
					array_push($whereClauses, "price >= '{$minValue}'");
					//$property->where('price', '>=', $minValue);

					// no upper limit if range is maxed out
					if ($maxIndex != 200) {
						array_push($whereClauses, "price <= '{$maxValue}'");
						//$property->where('price', '<=', $maxValue);
					}
					}
					break;
				case 'agreement-type':
					array_push($whereClauses, "property_type = '{$value}'");
					//$property->where('property_type', '=', $value);
					break;
				case 'bedrooms':
					array_push($whereClauses, "bedrooms >= '{$value}'");
					//$property->where('bedrooms', '>=', $value);
					break;
				case 'bathrooms':
					array_push($whereClauses, "bathrooms >= '{$value}'");
					//$property->where('bathrooms', '>=', $value);
					break;
				case 'parking':
					array_push($whereClauses, "parking >= '{$value}'");
					//$property->where('parking', '>=', $value);
					break;
				case 'mode': // property type				
					if (isset($searchProfileArray[$value . '-action'])) {
						$propertyType = $searchProfileArray[$value . '-action'];
						//dd($propertyType);
						if ($propertyType) {
							array_push($whereClauses, "category = '{$propertyType}'");
						}
					}
					//$property->where('category', '=', $propertyType);
					// @todo combine this into any groupings ie. Unit/Apartment
					break;
				case 'land-area':
					array_push($whereClauses, "land_area >= '{$value}'");
					//$property->where('land_area', '>=', $value);
					break;


			}
		}
		$property->whereRaw(implode($whereClauses, " AND "));	
		//$property->distinct('property.proprety_code');

		if (Input::get('sort') && Input::get('sort') == 'favourite') {
		      
			$property->favourite();
			
		}




		if(Input::get('sort') && Input::get('order')){
			if(Input::get('sort') == 'favourite') {
				if (Input::get('order'))
					$property->orderBy('user_id', Input::get('order'));
				else
					$property->orderBy('user_id', 'asc');
			} else {
				if (Input::get('order'))
					$property->orderBy(Input::get('sort'), Input::get('order'));
				else
					$property->orderBy(Input::get('sort'), 'asc');
			}
		}

		$property->groupBy('property_code');


		return $property->paginate(20);

	}


	public static function findAllBySearchProfileMap(SearchProfile $searchProfile){

	$searchProfileArray = json_decode($searchProfile->profile, true);

		if (isset($searchProfileArray['suburb'])) {
			$address = $searchProfileArray['suburb'];
		} else {
			$address = '';
		}


		$property = Property::query();
		//$property->with('favouriteproperties');
		$whereClauses = [];


		foreach ($searchProfileArray as $key => $value) {
			if (empty($value)) {
				continue;
			} // skip any empty values from the search form

			switch ($key) {
				case 'suburb':

					// @todo inc_surround == all properties for the time being
					if (empty($searchProfileArray['inc-surrounding'])) {
						$property->select('*')->where('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');

					} else {
						// Uses  address to get   its  lat lng  of   submitted  subub

						if (empty($searchProfileArray['cover_disatance'])) {
							$distance = 5;
						} else {
							$distance = $searchProfileArray['cover_disatance'];
						}


						$latlng = GCH::geolocate_address($address);
						if ($latlng) {
							$lat = $latlng['lat'];
							$lng = $latlng['lng'];
							$property->distance($lat, $lng, $distance, 'KM');
						}

					}
					break;
				case 'price-range':
				        if(isset($searchProfileArray['mode'])){
					$maxIndex = explode(',', $value)[1];

					list($minValue, $maxValue) = explode(',', SH::getCalculatedRange(
						$searchProfileArray['agreement-type'],
						$searchProfileArray['mode'],
						$value
					));
					array_push($whereClauses, "price >= '{$minValue}'");
					//$property->where('price', '>=', $minValue);

					// no upper limit if range is maxed out
					if ($maxIndex != 200) {
						array_push($whereClauses, "price <= '{$maxValue}'");
						//$property->where('price', '<=', $maxValue);
					}
					}
					break;
				case 'agreement-type':
					array_push($whereClauses, "property_type = '{$value}'");
					//$property->where('property_type', '=', $value);
					break;
				case 'bedrooms':
					array_push($whereClauses, "bedrooms >= '{$value}'");
					//$property->where('bedrooms', '>=', $value);
					break;
				case 'bathrooms':
					array_push($whereClauses, "bathrooms >= '{$value}'");
					//$property->where('bathrooms', '>=', $value);
					break;
				case 'parking':
					array_push($whereClauses, "parking >= '{$value}'");
					//$property->where('parking', '>=', $value);
					break;
				case 'mode': // property type
					if (isset($searchProfileArray[$value . '-action'])) {

						$propertyType = $searchProfileArray[$value . '-action'];
						if ($propertyType) {
							array_push($whereClauses, "category = '{$propertyType}'");
						}
					}
					//$property->where('category', '=', $propertyType);
					// @todo combine this into any groupings ie. Unit/Apartment
					break;
				case 'land-area':
					array_push($whereClauses, "land_area >= '{$value}'");
					//$property->where('land_area', '>=', $value);
					break;


			}
		}
		$property->whereRaw(implode($whereClauses, " AND "));


		if (Input::get('sort') && Input::get('sort') == 'favourite') {
			$property->favourite();

		}




		if(Input::get('sort') && Input::get('order')){
			if(Input::get('sort') == 'favourite') {
				if (Input::get('order'))
					$property->orderBy('user_id', Input::get('order'));
				else
					$property->orderBy('user_id', 'asc');
			} else {
				if (Input::get('order'))
					$property->orderBy(Input::get('sort'), Input::get('order'));
				else
					$property->orderBy(Input::get('sort'), 'asc');
			}
		}




		return $property->get();
	}


	public static function   findAllFavouriteProperties($user_id){


		$property = Property::query();
		/*$whereClauses = [];

		foreach ($searchProfileArray as $key => $value) {
			if (empty($value)) {
				continue;
			} // skip any empty values from the search form

			switch ($key) {
				case 'suburb':

					// @todo inc_surround == all properties for the time being
					if (empty($searchProfileArray['inc-surrounding'])) {
						$property->select('*')->where('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');
					} else {
						// Uses  address to get   its  lat lng  of   submitted  subub

						if (empty($searchProfileArray['cover_disatance'])) {
							$distance = 5;
						} else {
							$distance = $searchProfileArray['cover_disatance'];
						}



						$latlng =  GCH::geolocate_address($address);
						if($latlng) {
							$lat = $latlng['lat'];
							$lng = $latlng['lng'];
							$property->distance($lat, $lng, $distance, 'KM');
						}
					}
					break;
				case 'price-range':
					$maxIndex = explode(',', $value)[1];

					list($minValue, $maxValue) = explode(',', SH::getCalculatedRange(
						$searchProfileArray['agreement-type'],
						$searchProfileArray['mode'],
						$value
					));
					array_push($whereClauses, "price >= '{$minValue}'");
					//$property->where('price', '>=', $minValue);

					// no upper limit if range is maxed out
					if ($maxIndex != 200) {
						array_push($whereClauses, "price <= '{$maxValue}'");
						//$property->where('price', '<=', $maxValue);
					}
					break;
				case 'agreement-type':
					array_push($whereClauses, "property_type = '{$value}'");
					//$property->where('property_type', '=', $value);
					break;
				case 'bedrooms':
					array_push($whereClauses, "bedrooms >= '{$value}'");
					//$property->where('bedrooms', '>=', $value);
					break;
				case 'bathrooms':
					array_push($whereClauses, "bathrooms >= '{$value}'");
					//$property->where('bathrooms', '>=', $value);
					break;
				case 'parking':
					array_push($whereClauses, "parking >= '{$value}'");
					//$property->where('parking', '>=', $value);
					break;
				case 'mode': // property type
					if (isset($searchProfileArray[$value . '-action'])) {

						$propertyType = $searchProfileArray[$value . '-action'];
						if ($propertyType) {
							array_push($whereClauses, "category = '{$propertyType}'");
						}
					}
					//$property->where('category', '=', $propertyType);
					// @todo combine this into any groupings ie. Unit/Apartment
					break;
				case 'land-area':
					array_push($whereClauses, "land_area >= '{$value}'");
					//$property->where('land_area', '>=', $value);
					break;


			}
		}
		$property->whereRaw(implode($whereClauses, " AND "));*/

			$property->join('favouriteproperties', function ($join) use($user_id) {
				$join->on('favouriteproperties.property_id', '=', 'properties.property_code')
				     ->where('favouriteproperties.user_id', '=', $user_id);

			});

		if(Input::get('sort') && Input::get('order')){
			if(Input::get('sort') == 'favourite') {
				if (Input::get('order'))
					$property->orderBy('user_id', Input::get('order'));
				else
					$property->orderBy('user_id', 'asc');
			} else {
				if (Input::get('order'))
					$property->orderBy(Input::get('sort'), Input::get('order'));
				else
					$property->orderBy(Input::get('sort'), 'asc');
			}
		}


		return $property->paginate(20);
	}


	public static function findAllByFavouriteMap($user_id){
		$property = Property::query();

		$property->join('favouriteproperties', function ($join) use($user_id) {
			$join->on('favouriteproperties.property_id', '=', 'properties.property_code')
				->where('favouriteproperties.user_id', '=', $user_id);

		});

		if(Input::get('sort') && Input::get('order')){
			if(Input::get('order'))
				$property->orderBy(Input::get('sort'),Input::get('order'));
			else
				$property->orderBy(Input::get('sort'),'asc');
		}


		return $property->get();
	}
}