<?php
return array(
// set your paypal credential
    //'client_id' => env('PAYMENT_MODE')=='live'?env('PAYMENT_CLIENT_ID_LIVE'):env('PAYMENT_CLIENT_ID'),
    'client_id' => '%client_id%',
   // 'secret' => env('PAYMENT_MODE')=='live'?env('PAYMENT_SECRET_ID_LIVE'):env('PAYMENT_SECRET_ID'),
    'secret' =>'%secret_id%' ,
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => '%mode%',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 10000,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => env('PAYMENT_MODE')=='live'?env('PAYPAL_LOG_LIVE'):env('PAYPAL_LOG'),
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);