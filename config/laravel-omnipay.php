<?php

return [

    // The default gateway to use
    'default' => 'eway',

    // Add in each gateway here
    'gateways' => [
        'paypal' => [
            'driver' => 'PayPal_Express',
            'options' => [
                'solutionType' => '',
                'landingPage' => '',
                'headerImageUrl' => ''
            ]
        ],
		'eway' => [
            'driver' => 'Eway_RapidShared',
            'options' => [
                'solutionType' => '',
                'landingPage' => '',
                'headerImageUrl' =>''
            ]
        ]

	]

];