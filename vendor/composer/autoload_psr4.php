<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Yaro\\SocShare\\' => array($vendorDir . '/yaro/soc-share/src'),
    'XdgBaseDir\\' => array($vendorDir . '/dnoegel/php-xdg-base-dir/src'),
    'Symfony\\Polyfill\\Util\\' => array($vendorDir . '/symfony/polyfill-util'),
    'Symfony\\Polyfill\\Php56\\' => array($vendorDir . '/symfony/polyfill-php56'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'SuperClosure\\' => array($vendorDir . '/jeremeamia/SuperClosure/src'),
    'Stringy\\' => array($vendorDir . '/danielstjules/stringy/src'),
    'Spatie\\Newsletter\\' => array($vendorDir . '/spatie/laravel-newsletter/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Omnipay\\Eway\\' => array($vendorDir . '/omnipay/eway/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'League\\OAuth1\\' => array($vendorDir . '/league/oauth1-client/src'),
    'League\\Fractal\\' => array($vendorDir . '/league/fractal/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'Laravel\\Socialite\\' => array($vendorDir . '/laravel/socialite/src'),
    'Jenky\\LaravelPlupload\\' => array($vendorDir . '/jenky/laravel-plupload/src'),
    'Intervention\\Image\\' => array($vendorDir . '/intervention/image/src/Intervention/Image'),
    'Illuminate\\Html\\' => array($vendorDir . '/illuminate/html'),
    'Illuminate\\' => array($vendorDir . '/laravel/framework/src/Illuminate'),
    'GuzzleHttp\\Stream\\' => array($vendorDir . '/guzzlehttp/streams/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Collective\\Html\\' => array($vendorDir . '/laravelcollective/html/src'),
    'ClassPreloader\\' => array($vendorDir . '/classpreloader/classpreloader/src'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'App\\Repositories\\' => array($baseDir . '/app/Repositories'),
    'App\\Hydrators\\' => array($baseDir . '/app/Hydrators'),
    'App\\' => array($baseDir . '/app'),
);
