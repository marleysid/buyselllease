<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Property extends Model {

    use SoftDeletes;
    const DISTANCE_UNIT_KILOMETERS = 111.045;
    const DISTANCE_UNIT_MILES      = 69.0;
    protected $fillable = array('property_code','property_type','price','land_area','category');

    public function userBlocks() {
        return $this->belongsToMany('App\Models\User', 'BlockedProperties');
    }



    public function userViews() {
        return $this->belongsToMany('App\Models\User', 'PropertyView');
    }


    public function userFavourites(){
        return $this->belongsToMany('App\Models\User','favouriteproperties','user_id','property_id');
    }



    public function agent() {
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function propertyData() {
        return $this->hasMany('App\Models\PropertyData', 'property_id');
    }

    public function Scopefavourite($query){

        $query->leftJoin('favouriteproperties', function ($leftJoin) {
            $leftJoin->on('properties.property_code', '=', 'favouriteproperties.property_id')
                ->where('favouriteproperties.user_id', '=', \Auth::user()->id);
        });


    }





    /**
     * @param $query
     * @param $lat* @param $lng
     * @param $radius numeric
     * @param $units string|['K', 'M']
     */
    public function scopedistance($query, $lat, $lng, $radius = 10, $units = 'K')
    {

        if($lat == '0' && $lng =='0'){
            $subselect = clone $query;
            $subselect
                ->select(DB::raw('properties.*'));

            $query
                ->from(DB::raw('(' . $subselect->toSql() . ') as properties'));


        } else {
            $distanceUnit = $this->distanceUnit($units);


            $haversine = sprintf('properties.*,(%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(lat)) * COS(RADIANS(%f - lng)) + SIN(RADIANS(%f)) * SIN(RADIANS(lat))))) AS distance',
                $distanceUnit,
                $lat,
                $lng,
                $lat
            );

            $subselect = clone $query;
            $subselect
                ->select(DB::raw($haversine));

            // Optimize the query, see details here:
            // http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

            $latDistance = $radius / $distanceUnit;
            $latNorthBoundary = $lat - $latDistance;
            $latSouthBoundary = $lat + $latDistance;
            $subselect->whereRaw(sprintf("lat BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

            $lngDistance = $radius / ($distanceUnit * cos(deg2rad($lat)));
            $lngEastBoundary = $lng - $lngDistance;
            $lngWestBoundary = $lng + $lngDistance;
            $subselect->whereRaw(sprintf("lng BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));

            $query
                ->from(DB::raw('(' . $subselect->toSql() . ') as properties'))
                ->where('distance', '<=', $radius);
        }
    }


    

    /**
     * @param $units
     */
    private function distanceUnit($units = 'K')
    {
        if ($units == 'K' || $units=='KM') {
            return static::DISTANCE_UNIT_KILOMETERS;
        } elseif ($units == 'M') {
            return static::DISTANCE_UNIT_MILES;
        }
    }



}
