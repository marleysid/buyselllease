<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class SearchProfile extends Model
{
    use SoftDeletes;

    protected $table = 'search_profiles';

    protected $fillable = ['user_id', 'profile'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
