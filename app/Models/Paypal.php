<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model {

    protected  $table ='payments';

    protected  $fillable = ['client_id','secret_id','mode'];

}
