<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

	protected $table = 'region_city';

	public $timestamps = false;

	public function getRegion()
	{
		return $this->belongsTo('App\Models\Region', 'region_id');
	}

}
