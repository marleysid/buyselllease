<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deletedat extends Model {

	protected $table = 'deleted_users';

    public $timestamps = false;

}
