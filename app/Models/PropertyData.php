<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyData extends Model {

    use SoftDeletes;

    protected $table = 'property_data';

	protected $fillable = ['property_id', 'import_payload_id'];

    public $timestamps = false;

    public function property()
    {
        return $this->belongsTo('App\Models\Property', 'property_id');
    }

    public function importPayload()
    {
        return $this->belongsTo('App\Models\ImportPayload', 'import_payload_id');
    }
}
