<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyDailyUpdates extends Model {

    protected $table='property_daily_updates';

    protected $fillable = ['property_id', 'agent_id', 'status','source_updated','web_hoster','xmlfile','entry_type','property_location','agent_office_location','agent_name'];


}
