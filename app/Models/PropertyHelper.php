<?php namespace App\Models;


class PropertyHelper {

    public static function getData(Property $property, $dataIndex) {
        if($property->propertyData()->first()) {
            //dd($property->propertyData()->first());
            //dd(json_decode($property->propertyData()->first()->payload, true));
            
            if (isset(json_decode($property->propertyData()->first()->payload, true)[$dataIndex]))
                return json_decode($property->propertyData()->first()->payload, true)[$dataIndex];
            else
                return false;
        } else {
            return false;
        }
    }

    public static function getURL(Property $property) {
        if(!$property->agent()->first()){
             return url('dashboard/property/detail/'.$property->property_code);
        }

        $importId = $property->agent()->first()->import_id;
        
        //propertyy detail page logic
        if($importId == "HIGHLAND" || $importId == "BELLERANDWICK" || $importId == "110" || $importId == "39" || $importId == "BD1054")
        {
            if ($property->agent()->first()->listing_base_url){
               return $property->agent()->first()->listing_base_url. $property->property_code;
           }else {
               return url('dashboard/property/detail/'.$property->property_code);
           }

       }else{
            return url('dashboard/property/detail/'.$property->property_code);
       }


       // if ($importId == 'COASTLINEAGENCY' || $importId == '24036' || $importId == '26231' || $importId == '26662' || $importId == 'AG819' || $importId == '27770'){
       //      return url('dashboard/property/detail/'.$property->property_code);
       // } else {
       //     if ($property->agent()->first()->listing_base_url)
       //         return $property->agent()->first()->listing_base_url. $property->property_code;
       //     else {
       //         return url('dashboard/property/detail/'.$property->property_code);
       //     }
       // }
    }


    public  static  function getImages(Property $property){

        $images_array = [];
        $alphas = range('a', 'z');

        foreach($alphas as $key=>$value){
            $img = self::getData($property,'img_'.$value);
            if(!empty($img)) {
                $images_array['img_'.$value] = self::getData($property,'img_'.$value);
            }


        }

        $item = $images_array['img_m'];
        unset($images_array[$key]);
        $images_array = array('img_m' => $item) + $images_array;

        $final_array = [];
        foreach($images_array as $img) {
            $final_array[] = $img;
        }
        return $final_array;

    }


    public static function  getInspections(Property $property) {

       $payload =  json_decode($property->propertyData()->first()->payload,true);

       $inspection_array = [];

        $alphas = range('0', '100');

        foreach($alphas as $key=>$value){
            if(isset($payload['inspectionTimes.inspection_'.$value])) {
                $inspec = self::getData($property, 'inspectionTimes.inspection_' . $value);

                if (!empty($inspec)) {

                    $inspection_array['inspection_' . $value] =  self::getData($property, 'inspectionTimes.inspection_' . $value);
                }
            }
        };
        return $inspection_array;


    }



}
