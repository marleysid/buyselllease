<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agentpricing extends Model {

	protected $table = 'agentpricing';

	public $timestamps = false;

}
