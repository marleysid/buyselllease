<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model {


    protected $table='favouriteproperties';

    protected  $fillable = ['user_id','property_id','interaction'];


    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'propertyfavourites');
    }




}
