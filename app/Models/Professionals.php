<?php namespace App\Models;

use App\Models\City;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Professionals extends Model
{

    protected $table = 'professionals';

    protected $fillable = ['firstname', 'lastname', 'avatar', 'user_id', 'profession', 'logo', 'url', 'suburb', 'payment_status', 'contact', 'description', 'street_address', 'mobile_no', 'lat', 'lng', 'business_name', 'company_name', 'business_position', 'expired_at', 'type'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Profession', 'profession', 'id');
    }

  
    public function scopedistance($query, $lat, $lng, $radius, $units)
    {

        $radius = $radius ? $radius : 0;

        if ($units == 'KM') {
            $distanceUnit = 111.045;
        } else {
            $distanceUnit = 69.0;
        }

        $getProfessionalAroundRegion = City::all();

        foreach ($getProfessionalAroundRegion as $getAll) {

            $lat1 = (float) $lat;
            $lon1 = (float) $lng;

            $lat2 = (float) $getAll->lat;
            $lon2 = (float) $getAll->lon;

            $theta    = $lon1 - $lon2;
            $distance = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) +
                (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
                cos(deg2rad($theta)));
            $distance = acos($distance);
            $distance = rad2deg($distance);
            $distance = $distance * 60 * 1.1515;
            $distance = $distance * 1.609344;
            if ((float) $distance <= (float) $radius) {
                $formatted[] = $getAll->region_id;
            }

        }

        if (!empty($formatted)) {
            Session::put('regionwise', $formatted);
        }

        /*
        |-----------------------------------------------------------------------------------------------------------------------
        |
        | getting the region id respection to search
        | each region id is associated with the professionals. Now we should display all those professionals in the list
        | who has a type of region and associated with the region that we are getting in the below array.
        | @all region id will come from the region_city table by analysing the current search stats of the user
        |
        |-------------------------------------------------------------------------------------------------------------------------
         */
        if (!empty($formatted)) {
            $getOnlyRegionId = array_values(array_unique($formatted));
        }

        //  dd($getOnlyRegionId,false);

        $all = Professionals::where(['type' => 'region'])->get();
        /*
        | now unserialize the lat value.
        | check against the region id that we fetched eariler.
        | if the region_id is in the lat value then that professional should get displayed in the list.
         */
        if (count($all) != "0") {
            foreach ($all as $prof) {
                $professionalId       = $prof->id;
                $prittyLat[$prof->id] = unserialize($prof->lat);
            }
        }

        //   dd($prittyLat);
        if (!empty($prittyLat) && !empty($getOnlyRegionId)) {
            $regions = array();
            foreach ($prittyLat as $k => $v) {
                //   dd($v1);
                foreach ($v as $k1 => $v1) {
                    // dd($v1);

                    if (in_array($v1, $getOnlyRegionId)) {
                        $regions[] = $k;
                    }
                }

            }
        }

        if (!empty($regions)){
              Session::put('userwise', $regions);
        }
        //dd($regions);
        /*
        |---------------------------------------------------
        | ends here
        |---------------------------------------------------
         */

        $haversine = sprintf('professionals.*, ROUND(%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(lat)) * COS(RADIANS(%f - lng)) + SIN(RADIANS(%f)) * SIN(RADIANS(lat)))),2) AS distance', $distanceUnit, $lat, $lng, $lat);

        $subselect = clone $query;
        $subselect->selectRaw(DB::raw($haversine)); // Optimize haversine query: http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

        $latDistance      = $radius / $distanceUnit;
        $latNorthBoundary = $lat - $latDistance;
        $latSouthBoundary = $lat + $latDistance;
        $subselect->whereRaw(sprintf("lat BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

        $lngDistance     = $radius / ($distanceUnit * cos(deg2rad($lat)));
        $lngEastBoundary = $lng - $lngDistance;
        $lngWestBoundary = $lng + $lngDistance;
        $subselect->whereRaw(sprintf("lng BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));
        $subselect->orWhereRaw("type = 'nationwide'");

        if (!empty($regions)) {
            foreach ($regions as $reg) {
                $subselect->orWhereRaw("professionals.id = '$reg'");
            };
        }

        $query
            ->from(DB::raw('(' . $subselect->toSql() . ') as professionals'));
    }

    public function scopeActive($query)
    {
        $subselect = clone $query;
        $subselect->selectRaw(DB::raw('active')->leftJoin('users', 'users.id=professionals.user_id'));

        $query->sql
            ->from(DB::raw('(' . $subselect->toSql()));

    }

    public function getRegion()
    {
        return $this->hasOne('App\Models\Region', 'lat');
    }

}
