<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Agent extends Model {

	use SoftDeletes;
	protected $table = 'agents';

	protected $guarded = ['id'];

	public function properties() {
		return $this->hasMany('App\Models\Property');
	}

	/*public function users() {
		        return $this->belongsToMany('users', 'AgentUsers');
	*/

	public function user() {
		return $this->belongsTo('App\Models\User', 'user_id');
	}

	public function scopedistance($query, $lat, $lng, $radius, $units) {
		$radius = $radius ? $radius : 500;

		if ($units == 'KM') {
			$distanceUnit = 111.045;
		} else {
			$distanceUnit = 69.0;
		}

		$haversine = sprintf('agents.*, ROUND(%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(lat)) * COS(RADIANS(%f - lng)) + SIN(RADIANS(%f)) * SIN(RADIANS(lat)))),2) AS distance', $distanceUnit, $lat, $lng, $lat);

		$subselect = clone $query;
		$subselect->selectRaw(DB::raw($haversine)); // Optimize haversine query: http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

		$latDistance = $radius / $distanceUnit;
		$latNorthBoundary = $lat - $latDistance;
		$latSouthBoundary = $lat + $latDistance;
		$subselect->whereRaw(sprintf("lat BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

		$lngDistance = $radius / ($distanceUnit * cos(deg2rad($lat)));
		$lngEastBoundary = $lng - $lngDistance;
		$lngWestBoundary = $lng + $lngDistance;
		$subselect->whereRaw(sprintf("lng BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));

		$query
			->from(DB::raw('(' . $subselect->toSql() . ') as agents'))
			->where('distance', '<=', $radius);
			
	}

	public function scopeActive($query) {
		$subselect = clone $query;
		$subselect->selectRaw(DB::raw('active')->leftJoin('users', 'users.id=agents.user_id'));

		$query->sql
			->from(DB::raw('(' . $subselect->toSql()));

	}
}
