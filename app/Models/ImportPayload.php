<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportPayload extends Model {

    protected $fillable = ['payload','filename','webhoster'];

    public function scopeUnprocessed($query) {
        return $query->whereNull('processed');
    }
}
