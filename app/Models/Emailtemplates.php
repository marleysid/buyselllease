<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emailtemplates extends Model {

	protected $table = 'emailtemplates';

    public $timestamps = false;

}
