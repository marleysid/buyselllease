<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'confirmation', 'remember_token'];




    public function searchProfiles() {
        return $this->hasMany('App\Models\SearchProfile', 'user_id');
    }
	public function professionals() {
		return  $this->hasOne('App\Models\Professinals','user_id');
	}

	public function agents() {
		return  $this->hasOne('App\Models\Agent','user_id');
	}

	public function favourites() {
		return $this->belongsToMany('App\Models\Favourite', 'propertyfavourites');
	}



}
