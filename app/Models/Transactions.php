<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model {

    protected  $table ='transactions';

    protected  $fillable = ['details','created_at','user_id'];

}
