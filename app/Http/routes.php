<?php

/**
 * Public Area
 */
include 'routes/helpers_routes.php';
include 'routes/faq_routes.php';
include 'routes/public_auth_routes.php';
include 'routes/public_user_protected_routes.php';
include 'routes/user_logged_in_routes.php';
include 'routes/admin_routes.php';
include 'routes/dashboard_routes.php';
include 'routes/signup_routes.php';
include 'routes/agent_routes.php';

Route::get('/', ['as' => 'home.index',
	'uses' => 'HomeController@index']);

Route::get('/howitworks', ['as' => 'home.howitworks',
	'uses' => 'HomeController@howItWorks']);

Route::get('/aboutus', ['as' => 'home.aboutus',
	'uses' => 'HomeController@aboutUs']);

Route::get('/contactus', ['as' => 'home.contactus',
	'uses' => 'HomeController@contactUs']);

Route::get('/direct-relationships', ['as' => 'home.directrelationships',
	'uses' => 'HomeController@directRelationships']);

Route::get('/get-instant-alerts', ['as' => 'home.getinstantalerts',
	'uses' => 'HomeController@getInstantAlerts']);

Route::get('/the-focus-is-on-you', ['as' => 'home.thefocusisonyou',
	'uses' => 'HomeController@theFocusIsOnYou']);

Route::get('/cost-effective', ['as' => 'home.costeffective',
	'uses' => 'HomeController@costEffective']);

Route::get('/sales-and-leasing', ['as' => 'home.salesandleasing',
	'uses' => 'HomeController@salesAndLeasing']);

Route::get('/find-services', ['as' => 'home.findservices',
	'uses' => 'HomeController@findServices']);

Route::get('/faq', ['as' => 'home.faq',
	'uses' => 'HomeController@faq']);

Route::get('/terms-of-use', ['as' => 'home.termsofuse',
	'uses' => 'HomeController@termsOfUse']);

Route::get('/terms-subscribers', ['as' => 'home.termssubscribers', 'uses' => function () {
	return View::make('home.termssubscribers');
}]);

Route::get('/privacy-policy', ['as' => 'home.privacypolicy', 'uses' => 'HomeController@privacyPolicy']);

Route::post('subscriber', [
	'uses' => 'HomeController@postSubscribe',
]);

Route::post('dashboard/advance/search',['as'=>'advance-search','uses'=>'SearchController@advanceSearch']);


Route::get('sendemail', 'CronController@sendReminder');


Route::get('professionalrenew/getdata/{id}', 'CronController@updateData');







