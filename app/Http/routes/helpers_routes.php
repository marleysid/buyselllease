<?php
/**
 * Created by PhpStorm.
 * User: ebpearls
 * Date: 11/19/2015
 * Time: 9:02 AM
 */

/*
to  run command  from routes
Route::get('<name>', function () {
Artisan::call('<artisan:comman>');
});
 */

Route::get('/runcron', ['as' => 'cron', 'uses' => 'CronController@index']);

Route::post('uploadImage', ['as' => 'uploadImage', 'uses' => 'MediaController@upload']);
Route::delete('deleteImage/{image}', ['as' => 'delete', 'uses' => 'MediaController@deleteImage']);

Route::post('uploadAgentLogo', ['as' => 'uploadAgentLogo', 'uses' => 'MediaController@uploadAgentLogo']);
Route::delete('deleteImageAgentLogo/{image}', ['as' => 'deleteAgentLogo', 'uses' => 'MediaController@deleteImageAgentLogo']);

Route::post('uploadAgentProfile', ['as' => 'uploadAgentProfile', 'uses' => 'MediaController@uploadAgentProfile']);
Route::delete('deleteImageAgentprofile/{image}', ['as' => 'deleteImageAgentprofile', 'uses' => 'MediaController@deleteImageAgentprofile']);

Route::get('send-test', 'SignupController@send_mail_test');
/*
 * for  export controller*/
Route::get('getCSVLink', ['as' => 'export_getCSVLink', 'uses' => 'ExportController@getCompareXmlDB']);
Route::get('createInvoice', ['as' => 'createInvoice', 'uses' => 'SignupController@CreateAndSendInovice']);
Route::get('dailyupdate', ['as' => 'dailyupdate', 'uses' => 'CronController@createSendDailyCsvUpdates']);
Route::get('print/property-detail/{code}', ['as' => 'print.details', 'uses' => 'DashboardController@printDetail']);

Route::post('search/get-more-fields',['as'=>'get-more-fields','uses'=>'SearchController@getMoreFields']);
Route::post('search/get-sell-more-option',['as'=>'get-sell-more-option','uses'=>'SearchController@getMoreSellSearchFields']);
Route::post('search/get-tenant-landlord-search-options',['as'=>'get-tenant-landlord-search-options','uses'=>'SearchController@getTenantSearchOptions']);