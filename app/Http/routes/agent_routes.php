<?php

Route::get('/agent-info', ['as' => 'home.agent-info',
	'uses' => 'HomeController@agentInfo']);

Route::get('/agent-signup', ['as' => 'home.agent.signup',
	'uses' => 'SignupController@agentSignUpForm']);

Route::get('/agent-signup', ['as' => 'home.agent-signup',
	'uses' => 'SignupController@agentSignUpForm']);



// Add this route for checkout or submit form to pass the item into paypal
Route::post('agent-signup', array('as' => 'agent-payment','uses' => 'SignupController@postPaymentAgent'));

Route::get('agent-signup/getdata', array('uses' => 'SignupController@getAgentData'));

// this is after make the payment, PayPal redirect back to your site
Route::get('eway/payment/success/agent/{id}',['as'=>'eway/payment/success/agent','uses'=>'SignupController@ewaySuccessAgent']);
/*Route::get('agent-payment-status', array('as' => 'agent.payment.status','uses' => 'SignupController@getPaymentStatus'));*/