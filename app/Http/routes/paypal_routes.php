<?php

Route::get('professionals-signup', ['as' => 'professionals-signup', 'uses' => 'PaypalController@professionalSignup']);

// Add this route for checkout or submit form to pass the item into paypal
Route::post('professionals-signup', array(
	'as' => 'payment',
	'uses' => 'PaypalController@postPayment',
));
// this is after make the payment, PayPal redirect back to your site
Route::get('payment/status', array(
	'as' => 'payment.status',
	'uses' => 'PaypalController@getPaymentStatus',
));