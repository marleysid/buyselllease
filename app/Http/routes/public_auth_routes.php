<?php
/**
 * Created by PhpStorm.
 * User: ebpearls
 * Date: 11/19/2015
 * Time: 9:00 AM
 */



/**
 * |--------------------------------------------------------------------------
 * | Auth components inc. social login helpers
 * |--------------------------------------------------------------------------
 */
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'search' => 'SearchController',

]);


Route::get('login', ['as' => 'login',
    'uses' => 'Auth\AuthController@getLogin']);

Route::get('logout', ['as' => 'logout',
    'uses' => 'Auth\AuthController@getLogout']);

Route::get('register', ['as' => 'register',
    'uses' => 'Auth\AuthController@getRegister']);

Route::get('facebookLogin', ['as' => 'facebook.login',
    'uses' => 'SocialController@facebookLoginRedirect']);

Route::get('facebookLoginCallback', ['as' => 'facebook.login.callback',
    'uses' => 'SocialController@facebookLoginCallback']);

Route::get('googlePlusLogin', ['as' => 'google-plus.login',
    'uses' => 'SocialController@googlePlusLoginRedirect']);

Route::get('googlePlusCallback', ['as' => 'google-plus.login.callback',
    'uses' => 'SocialController@googlePlusLoginCallback']);

//payments routes
Route::get('pay', 'SignupController@postPayment');
Route::get('payment-success/{id}', 'SignupController@getSuccessPayment');
Route::get('agentpayment-success/{id}', 'HomeController@getSuccessPayment');
Route::get('cancel-order', 'SignupController@cancelOrder');