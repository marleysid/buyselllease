<?php
/**
 * Created by PhpStorm.
 * User: ebpearls
 * Date: 11/16/2015
 * Time: 10:59 AM
 */

/**
 * |--------------------------------------------------------------------------
 * | Auth components  for admin
 * |--------------------------------------------------------------------------
 */

Route::group(['namespace' => 'Admin'], function () {
    Route::group(['prefix' => 'admin', 'middleware' => ['admin.guest']], function () {

        get('/', ['uses' => 'AuthController@getLogin']);
        get('login', ['uses' => 'AuthController@getLogin']);
        post('login', ['uses' => 'AuthController@postLogin']);
    });

    Route::get('admin/logout', ['as' => 'admin.logout', 'uses' => 'AuthController@getLogout']);

});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth.admin']], function () {

    Route::group(['prefix' => 'dashboard'], function () {
        get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

    });

    Route::group(['prefix' => 'users'], function () {
        get('/', ['as' => 'admin.users', 'uses' => 'UserController@index']);
        get('', ['as' => 'admin.users', 'uses' => 'UserController@index']);
        get('/getAdvanceFilterData', ['as' => 'admin.advancedatafilter', 'uses' => 'UserController@getAdvanceFilterData']);
        get('/create', ['as' => 'admin.users.create', 'uses' => 'UserController@create']);
        post('/store', ['as' => 'admin.users.store', 'uses' => 'UserController@store']);
        get('/show/{id}', ['as' => 'admin.users.show', 'uses' => 'UserController@show']);
        get('/edit/{id}', ['as' => 'admin.users.edit', 'uses' => 'UserController@edit']);
        post('/update/{id}', ['as' => 'admin.users.update', 'uses' => 'UserController@update']);
        get('/destroy/{id}', ['as' => 'admin.users.destroy', 'uses' => 'UserController@destroy']);

        /*
         * -------------------------------------------------------------------------
         * ACTIVATING AND SUSPENDING THE USER #GET
         * -------------------------------------------------------------------------
         */
        Route::get('activate/{id}', [
            'uses' => 'UserController@activateUser',
        ]);

        Route::get('suspend/{id}', [
            'uses' => 'UserController@suspendUser',
        ]);

    });

    /*
     * ---------------------------------------------------------------------------
     * ######PROMOTIONAL CODE #GET AND #POST ROUTE#########
     * DISPLAYING PROMOTIONAL CODE LIST TO THE ADMIN ##GET
     * ---------------------------------------------------------------------------
     */

    Route::get('promotionalcodelist', [
        'as'   => 'admin.codelist',
        'uses' => 'PromotionController@index',
    ]);

    Route::get('createnewpromocode', [
        'as'   => 'admin.createpromo',
        'uses' => 'PromotionController@create',
    ]);

    Route::post('storepromocode', [
        'as'   => 'admin.storepromo',
        'uses' => 'PromotionController@store',
    ]);

    Route::get('deletepromo/{id}', [
        'as'   => 'admin.deletepromo',
        'uses' => 'PromotionController@destroy',
    ]);

    /*
     * --------------------------------------------------------------------------
     * PROMOTIONAL CODE ROUTE ENDS HERE
     * -------------------------------------------------------------------------
     */

    Route::group(['prefix' => 'payment'], function () {
        get('/', ['as' => 'admin.payment', 'uses' => 'PaymentController@create']);
        post('/save', ['as' => 'admin.payment.save', 'uses' => 'PaymentController@save']);

    });

    Route::group(['prefix' => 'advertisement'], function () {
        get('/', ['as' => 'admin.advertisement', 'uses' => 'AdvertisementController@index']);
        get('index', ['as' => 'admin.advertisementindex', 'uses' => 'AdvertisementController@index']);
        get('/getAdvanceFilterData', ['as' => 'admin.advanceadvertisementdatafilter', 'uses' => 'AdvertisementController@getAdvanceFilterData']);

        get('/create', ['as' => 'admin.advertisement.create', 'uses' => 'AdvertisementController@create']);
        post('/store', ['as' => 'admin.advertisement.store', 'uses' => 'AdvertisementController@store']);
        get('/show/{id}', ['as' => 'admin.advertisement.show', 'uses' => 'AdvertisementController@show']);
        get('/edit/{id}', ['as' => 'admin.advertisement.edit', 'uses' => 'AdvertisementController@edit']);
        post('/update/{id}', ['as' => 'admin.advertisement.update', 'uses' => 'AdvertisementController@update']);
        get('/destroy/{id}', ['as' => 'admin.advertisement.destroy', 'uses' => 'AdvertisementController@destroy']);

        Route::get('activate/{id}', [
            'uses' => 'AdvertisementController@activateAd',
        ]);

        Route::get('suspend/{id}', [
            'uses' => 'AdvertisementController@suspendAd',
        ]);

    });

    Route::group(['prefix' => 'professionals'], function () {
        get('/', ['as' => 'admin.professionals', 'uses' => 'ProfessionalsController@index']);

    });

    Route::group(['prefix' => 'Agenttools'], function () {
        get('/', ['as' => 'admin.agenttools', 'uses' => 'AgenttoolsController@index']);
        get('/create', ['as' => 'admin.agenttools.create', 'uses' => 'AgenttoolsController@create']);
        post('/store', ['as' => 'admin.agenttools.store', 'uses' => 'AgenttoolsController@store']);
        get('/getAdvanceFilterData', ['as' => 'admin.advancedatafilter', 'uses' => 'AgenttoolsController@getAdvanceFilterData']);

    });

    /*
    |---------------------------------------------------
    |acticvating and suspending agents
    |----------------------------------------------------
     */
    Route::get('agent/activate/{id}', [
        'uses' => 'AgenttoolsController@activateAgent',
    ]);

    Route::get('agent/suspend/{id}', [
        'uses' => 'AgenttoolsController@suspendAgent',
    ]);

    /*
    |------------------------------------------------------------
    |viewing, editing and deleting the agent
    |-------------------------------------------------------------
     */
    Route::get('agent/show/{id}', [
        'uses' => 'AgenttoolsController@show',
    ]);

    Route::get('agent/edit/{id}', [
        'uses' => 'AgenttoolsController@edit',
    ]);

    Route::post('agent/update/{id}', [
        'uses' => 'AgenttoolsController@update',
    ]);

    Route::get('agent/destroy/{id}', [
        'uses' => 'AgenttoolsController@destroy',
    ]);

    Route::get('emailtemplate', [
        'uses' => 'TemplateController@showList',
    ]);

    Route::get('templateedit/{id}', [
        'uses' => 'TemplateController@editTemplate',
    ]);

    Route::post('updatetemplate/{id}', [
        'uses' => 'TemplateController@confirmUpdate',
    ]);

    /*
    |GET and POST Routes for agent pricing
    |INCLUDES both #GET and #POST route
     */
    Route::get('agentpricing', [
        'uses' => 'AgentPricingController@addPricing',
    ]);

    Route::get('createpricing', [
        'uses' => 'AgentPricingController@createPricing',
    ]);

    Route::post('storepricing', [
        'uses' => 'AgentPricingController@storePricing',
    ]);

    Route::get('editprice/{id}', [
        'uses' => 'AgentPricingController@editPricing',
    ]);

    Route::post('updatepricing/{id}', [
        'uses' => 'AgentPricingController@updatePricing',
    ]);

    /*
    | city management routes starts here
     */
    Route::get('citylist', [
        'uses' => 'CityController@index',
    ]);


    Route::get('createnewcity', [
        'uses'  => 'CityController@create',
        ]);
    Route::get('createnewcity/{id}', [
        'uses'  => 'CityController@create',
        ]);

    Route::post('storecity', [
        'uses'  => 'CityController@store',
        ]);

    Route::get('city/edit/{id}', [
        'uses'  =>  'CityController@edit'
        ]);

    Route::post('updatecity/{id}', [
          'uses'    => 'CityController@update'
        ]);


    Route::get('city/delete/{id}', [
            'uses'  => 'CityController@delete'
        ]);


    Route::get('region/edit/{id}', [
            'uses' => 'CityController@regionEdit'
        ]);
    Route::get('region/delete/{id}', [
            'uses' => 'CityController@regionDelete'
        ]);
    Route::post('region/update/{id}', [
            'uses'  => 'CityController@updateRegion'
        ]);
   

});


