<?php
// this is after make the payment, PayPal redirect back to your site
/*Route::get('payment/status', array(
	'as' => 'payment.status',
	'uses' => 'PaypalController@getPaymentStatus',
));*/
Route::get('professionals-signup', ['as' => 'professionals-signup', 'uses' => 'SignupController@professionalSignup']);


Route::get('professionals-signup/getdata', ['uses' => 'SignupController@getAll']);
// Add this route for checkout or submit form to pass the item into paypal
Route::post('professionals-signup', array('as' => 'payment','uses' => 'SignupController@postPaymentProfessional'));
Route::get('eway/payment/success/professional/{id}',['as'=>'eway/payment/success/professional','uses'=>'SignupController@ewaySuccessProfessional']);

Route::get('eway/payment/cancel/{id}',['as'=>'payment/cancel','uses'=>'SignupController@ewayCancel']);

Route::get('getcities', [
	'uses' => 'SignupController@fetchCity',
	]);

