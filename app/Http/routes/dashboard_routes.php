<?php
/**
 * Created by PhpStorm.
 * User: ebpearls
 * Date: 11/19/2015
 * Time: 9:05 AM
 */

/**
 * |--------------------------------------------------------------------------
 * | Dashboard
 * |--------------------------------------------------------------------------
 */
Route::get('dashboard/search', ['as' => 'dashboard.search',
	'uses' => 'DashboardController@search']);

Route::get('dashboard/search/property-profiles/{id?}', ['as' => 'dashboard.search.profiles',
	'uses' => 'DashboardController@search']);

Route::get('dashboard/searches/property-profiles/{id?}', ['as' => 'dashboard.search.profiles',
	'uses' => 'DashboardController@search']);

Route::get('dashboard/search/professionals/{id?}', ['as' => 'dashboard.professionals',
	'uses' => 'DashboardController@professionals']);

Route::get('dashboard/favourites/{id?}', ['as' => 'dashboard.favourites',
	'uses' => 'FavouriteController@index']);

Route::get('get-map-data-favourite', ['as' => 'dashboard.getMap.favourite',
	'uses' => 'FavouriteController@getMapFavourite']);

Route::get('dashboard/searches', ['as' => 'dashboard.searches',
	'uses' => 'DashboardController@search']);

Route::get('dashboard/listall', ['as' => 'dashboard.listAll',
	'uses' => 'DashboardController@listAll']);

Route::get('get-map-data/{searchid}', ['as' => 'dashboard.getMap',
	'uses' => 'DashboardController@getMap']);

Route::get('dashboard/listall-map', ['as' => 'dashboard.listAllMap',
	'uses' => function () {
		return View::make('dashboard.googlemap');
	}]);

Route::get('get-property-details/{id}', ['as' => 'dashboard.get_property_details',
	'uses' => 'DashboardController@get_property_details']);

Route::get('profile/addFavorite/{code}', ['as' => 'profile.add-favourite',
	'uses' =>'FavouriteController@addFavourite']);

Route::get('profile/removeFavorite/{code}', ['as' => 'profile.remove-favourite',
	'uses' =>'FavouriteController@removeFavourite']);

Route::get('dashboard/listall', ['as' => 'dashboard.listAll',
	'uses' => 'DashboardController@listAll']);

Route::get('get-map-data/{searchid}', ['as' => 'dashboard.getMap',
	'uses' => 'DashboardController@getMap']);

Route::get('get-map-data-professional/{searchid}', ['as' => 'dashboard.getMap.professional',
	'uses' => 'DashboardController@getMapProfessional']);

Route::get('get-map-data-agent/{searchid}', ['as' => 'dashboard.getMap.agent',
	'uses' => 'DashboardController@getMapAgent']);

Route::get('propertyData/{searchid}', ['as' => 'dashboard.propertyData',
	'uses' => 'DashboardController@propertyData']);

Route::get('professionalData/{searchid}', ['as' => 'dashboard.professionalData',
	'uses' => 'DashboardController@professionalData']);

Route::get('agentData/{searchid}', ['as' => 'dashboard.agentData',
	'uses' => 'DashboardController@agentData']);

Route::get('dashboard/listall-map', ['as' => 'dashboard.listAllMap',
	'uses' => 'DashboardController@list_all_test_map']);

/*
 * property detail routes
 */
Route::get('dashboard/property/detail/{code}', [
	'uses'	=> 'DashboardController@propertyDetail'
]);