<?php
/**
 * Created by PhpStorm.
 * User: ebpearls
 * Date: 11/19/2015
 * Time: 9:05 AM
 */


/**
 * |--------------------------------------------------------------------------
 * | Content Pages
 * |--------------------------------------------------------------------------
 */
Route::get('/faq/customers', ['as' => 'faq.customers',
    'uses' => function () {
        return View::make('home.faq.customers');
    }]);

Route::get('/faq/agents', ['as' => 'faq.agents',
    'uses' => function () {
        return View::make('home.faq.agents');
    }]);

Route::get('/faq/services', ['as' => 'faq.services',
    'uses' => function () {
        return View::make('home.faq.services');
    }]);

Route::get('/faq/marketing', ['as' => 'faq.marketing',
    'uses' => function () {
        return View::make('home.faq.marketing');
    }]);