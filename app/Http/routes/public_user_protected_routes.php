<?php


/*
 * ---------------------------------------------------------------------------------
 * PROTECTED ROUTES FOR USER
 * ---------------------------------------------------------------------------------
 */
Route::group(['middleware' => ['auth', 'public']], function () {

    /*
     Route::get('dashboard/profile', [
         'as' => 'dashboard.profile',
         'uses' => function () {
             return View::make('dashboard.profile');
         }]);
    */
    Route::get('dashboard/profile', [
        'as' => 'dashboard.profile',
        'uses' => 'DashboardController@updateProfessional'
    ]);

    //----------------------editing the profile of the user ##POST route------------------------//
    Route::post('professional/editprofile', [
        'uses'   =>  'DashboardController@confirmProfessionalUpdate'
    ]);

    Route::post('dashboard/store/notification', [
        'uses'   =>  'DashboardController@storeNotitication'
    ]);


    Route::post('createpassword', [
       'uses'   => 'DashboardController@createPassword'
    ]);

    Route::get('user/changepassword', [
       'uses'   => 'DashboardController@changePasswordView'
    ]);

    Route::post('changepassword', [
        'as'     => 'changepassword',
        'uses'    => 'DashboardController@changePassword'
    ]);

});


/*
 * ---------------------------------------------------------------------------------
 * ENDS HERE
 * ---------------------------------------------------------------------------------
 */



Route::get('register/verify/{code}', [
    'as' => 'confirmation_path',
    'uses' => 'DashboardController@confirm'
]);
