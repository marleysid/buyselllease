<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfessionalRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */

	public function rules()
	{
		return [
			//'user_name' => 'required|string:50',
			'first_name' => 'required|string:50',
			'last_name' => 'required|string:50',
			'email' => 'required|email|unique:users,email',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
			'link' => ['required','regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
			//'work_phone_number' => 'required',
			//'mobile_number' => 'required',
			'street_address' => 'required',
			'business_name' => 'required',
			'work_phone_number'	=> 'required',
			//'company_name' => 'required',
			'business_position' => 'required',
			//'suburb' => 'required',
            //'nationwide' => 'required_without:suburb',
			'description' => 'required',
			'agree' => 'required',
		];
	}

}
