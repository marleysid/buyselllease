<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Files;
use Storage;
use App\Agent;
use App\REA_XML;
use Queue;
use File;
use Log;
use App\Hydrators\PropertyHydrator;
use View;


class ExportController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public  function getCompareXmlDB()
	{

		self::flushAndAdd();

		// query
		$query = DB::table('property_temp as pt');
		$query->selectRaw('*,pt.agent_id as  xml_import_id');
		$query->join('properties as p', 'p.property_code', '=', 'pt.property_id', 'left');
		$data = $query->get(); // fetched data

		// passing the columns which I want from the result set. Useful when we have not selected required fields
		$arrColumns = array('temp_id', 'agent_id','xml_import_id', 'property_id', 'property_code','id', 'web_hoster', 'filename', 'status',  'property_type','price','land_area','category','postcode','suburb','parking','bedrooms','bathrooms','address','lat','lng','source_updated','created_at','updated_at','deleted_at');

		// define the first row which will come as the first row in the csv
		$arrFirstRow = array('Temp ID', 'Agent ID', 'Xml Agent_ID ','Xml Unique id','Property Code', 'Property Table Id',  'Web Hoster', 'Xml File', 'Status','Property Type','Price','Land Area','Category','PostCode','Suburb','Parking','Bedrooms','Bathrooms','Address','Lat','Lng','Source Update','Created AT','Update AT','Deleted AT');

		// building the options array
		$options = array(
			'columns' => $arrColumns,
			'firstRow' => $arrFirstRow,
		);

		$filename = 'comparision_'.date('Y-m-d').'_'.time();

	 return  Files::convertToCSV($data, $options,$filename );


	}

	public static function  flushAndAdd(){
		$agents = Agent::where('ftp_home_directory', '!=', '')->distinct()->get(['ftp_home_directory']);

		if (empty($agents)) {
			throw new \Exception('Agents not found');
		}
		$xml =[];
		$counter = 0;

		foreach ($agents as $agent) {

			$processedDirectory = base_path() . $agent->ftp_home_directory . '/processed';


			if (!File::isDirectory($processedDirectory)) {
				throw new \Exception('Directory not found: [' . $processedDirectory . ']');
			}

			foreach (File::files($processedDirectory) as $processed) {

				$file_detail = pathinfo($processed);
				$filename = $file_detail['filename'];
				$prossed_file = File::get($processed);
				// verify file is properties list XMLc
				if ($prossed_file == "") {
					continue;
				}

				$rea_xml = new REA_XML();
				$xml_document = $rea_xml->parse_xml($prossed_file);

				foreach ($xml_document as $type => $import_properties) {

					foreach ($import_properties as $import_property) {

						$property_content = [];
						$property_content['web_hoster'] = $agent->ftp_home_directory;
						$property_content['type'] = $type;
						$property_content['filename'] =$filename;
						// flatten xml for easier processing

						$import_property = array_dot($import_property);
						$final = array_merge($property_content,$import_property);
						array_push($xml,$final);
						$counter++;

					}
				}

			}
		}



		// group   with   webhosters

		$agentbox =[];
		$total_property = count($xml);



		foreach($xml as $key=>$value) {

			$property_array = [];
			$property_array['web_hoster'] = $value['web_hoster'];
			$property_array['agent_id'] = $value['agentID'];
			$property_array ['property_id'] = $value['uniqueID'];
			$property_array['address'] =  self::buildAddress($value);
			$property_array['status']= $value['status'];
			$property_array['filename']= $value['filename'];
			array_push($agentbox,$property_array);

		}

		$truncate =  DB::table('property_temp')->truncate();
		$insert_temp =   DB::table('property_temp')->insert($agentbox);
		if($insert_temp)
			return true;
		else
			return false;

	}


	public static function buildAddress(array $rea_xml) {

		return implode(', ', array(
			(empty($rea_xml['address.subNumber']) ? '' : $rea_xml['address.subNumber'] . '/') . $rea_xml['address.streetNumber'] . ' ' . $rea_xml['address.street'],
			$rea_xml['address.suburb'],
			$rea_xml['address.postcode'],
			$rea_xml['address.country']
		));
	}



}
