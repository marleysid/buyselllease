<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\Emailtemplates;

class TemplateController extends Controller {

	public function showList()
    {
        $all = Emailtemplates::all();
        return view('admin.template.list')
            ->with(array(
                'template' => $all
            ));
    }


    public function editTemplate($id)
    {
        $findTemplate = Emailtemplates::find($id);

        return view('admin.template.edit')
            ->with(array(
               'temp' => $findTemplate
            ));
    }


    public function confirmUpdate(Request $request, $id)
    {
        $v = validator::make($request->all(), [
           'text'   => 'required'
        ]);

        if ($v->fails()){
            return redirect::back()
                ->withErrors($v->messages());
        } else {

            DB::table('emailtemplates')
                ->where('id', '=', $id)
                ->update(['text' => htmlspecialchars(Input::get('text'))]);

            return redirect::to('admin/emailtemplate')
                ->with('message', 'Email Template edited successfully');
        }
    }

}
