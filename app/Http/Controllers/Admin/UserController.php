<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Deletedat;
use App\Models\Region;
use App\Models\User;
use App\Models\Professionals;
use App\Models\Profession;
use Auth;
use Datatables;
use GCH;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function _construct()
    {

    }

    public function index()
    {

        return view('admin.users.list');
    }

    public function getAdvanceFilterData()
    {
        /*
         *-------------------------------------------------------------
         *getting the list of the user of type general and professional
         *-------------------------------------------------------------
         */

        $users = User::select(['id', 'name', 'email', 'role', 'active', 'created_at'])
            ->where('id', '!=', Auth::user()->id)
            ->where('role', '!=', '1')
            ->where('role', '!=', '2')
            ->orderBy('created_at', 'desc');

        return Datatables::of($users)
            ->addColumn('Change Status', '@if ($active)
                                                <a href="<?=url("admin/users/suspend")."/".$id?>" class="btn btn-default btn-sm" onclick="return active()" >Suspend User</a>
                                            @else
                                                <a href="<?=url("admin/users/activate")."/".$id?>" class="btn btn-default btn-sm" onclick="return active()">Activate User</a>
                                            @endif')
            ->addColumn('operations', '@if ($role == 3)
                   <a href="<?= url("admin/users/show")."/".$id?>" class="btn btn-default btn-sm" >View</a>
                    <a href="<?php echo  url("admin/users/edit/")."/".$id ;?>" class="btn btn-default btn-sm">Edit</a>
                    @endif
                     <a href="<?php echo  url("admin/users/destroy/")."/".$id ;?>" class="btn btn-default btn-sm" onclick="return active()">Delete</a>

                ')

            ->editColumn('active', '@if($active)
                                Active
                            @else
                                Suspended
                            @endif')
            ->editColumn('role', '@if($role == 2)
                                Agent
                            @elseif ($role == 3)
                                Professional
                            @else
                                General User
                            @endif')
            ->make();
        /*return Datatables::of($users)->make();*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $userDetail = DB::table('users as u')
            ->join('professionals as p', 'u.id', '=', 'p.user_id')
            ->leftjoin('professions as pr', 'p.profession', '=', 'pr.id')
            ->leftjoin('region as reg', 'p.lat', '=', 'reg.id')
            ->select('p.firstname', 'p.lastname', 'u.email', 'p.logo', 'p.street_address', 'p.suburb', 'p.business_name',
                'p.company_name', 'p.business_position', 'p.type', 'p.lat', 'p.contact', 'p.mobile_no', 'pr.title', 'u.created_at', 'p.description', 'reg.name as regionname')
            ->where('u.id', '=', $id)
            ->first();

        return view('admin.users.view')
            ->with(array(
                'users' => $userDetail,
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $region     = Region::orderBy('name','asc')->get();
        $userDetail = DB::table('users as u')
            ->join('professionals as p', 'u.id', '=', 'p.user_id')
            ->leftJoin('professions as pr', 'p.profession', '=', 'pr.id')
            ->select('p.firstname', 'p.lastname', 'u.email', 'u.id as userid', 'p.logo', 'p.street_address', 'p.suburb', 'p.business_name',
                'p.company_name', 'p.url', 'p.lat', 'p.lng', 'p.description', 'p.business_position', 'p.mobile_no', 'p.contact', 'pr.id', 'pr.title', 'p.type', 'u.created_at')
            ->where('u.id', '=', $id)
            ->first();
            
         if ($userDetail->type == 'region'){
                $inArrayId = unserialize($userDetail->lat);
            } else {
                $inArrayId = array();
            }

    
        $profession = Profession::all();

      
        return view('admin.users.edit')
            ->with(array(
                'users'  => $userDetail,
                'pro'    => $profession,
                'region' => $region,
                'col'    => $inArrayId
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $v = validator::make($request->all(), [
            'firstname'         => 'required',
            'lastname'          => 'required',
            'email'             => 'required',
            'street_address'    => 'required',
            'business_name'     => 'required',
            'business_position' => 'required',
            //'company_name'      =>  'required',
            //'category'          =>  'required',
            'contact'           => 'required',
            'url'               => 'required',
            'description'       => 'required',
        ]);

        if ($v->fails()) {
            return redirect::back()
                ->withErrors($v->messages())
                ->withInput();
        } else {
            /*
             * prepare the data to update
             */

            //check whether the user has selected region or suburb or nationwide
            if ($request->searchtype == 'suburb') {
                $suburb = $request->suburb;
                $geomety = GCH::geolocate_address($request->suburb);
                $lat = $geomety['lat'];
                $lng = $geomety['lng'];
                $type = 'suburb';

            } elseif ($request->searchtype == 'region') {

                if (is_null($request->region)){
                     return redirect::back()
                            ->with('error', 'Please select at lease one state');
                }
                $lat = serialize($request->region);
                $lng = serialize($request->region);
                $suburb = '';
                $type = 'region';

            } elseif ($request->searchtype == 'nationwide') {
                $lat = 0;
                $lng = 0;
                $suburb = '';
                $type = 'nationwide';
            }

            $professional = [
                'firstname'         => Input::get('firstname'),
                'lastname'          => Input::get('lastname'),
                'street_address'    => Input::get('street_address'),
                'suburb'            => $suburb,
                'lat'               => $lat,
                'lng'               => $lng,
                'business_name'     => Input::get('business_name'),
                'business_position' => Input::get('business_position'),
                'company_name'      => Input::get('company_name'),
                'profession'        => Input::get('category'),
                'mobile_no'         => Input::get('mobile'),
                'contact'           => Input::get('contact'),
                'url'               => Input::get('url'),
                'description'       => Input::get('description'),
                'type'              => $type,

            ];

            if ($request->file('logo')) {

                $currentImage = DB::table('professionals')
                    ->where('user_id', $id)->first();

                \File::delete('uploads/professionals/' . $currentImage->logo);

                $destination = 'uploads/professionals/';
                $extension   = \Input::file('logo')->getClientOriginalExtension(); //getting extension of uploaded image
                $fileName    = time() . '.' . $extension; //renaming image
                Input::file('logo')->move($destination, $fileName);
                $professional['logo'] = $fileName;
            }

            try {
                DB::beginTransaction();

                DB::table('professionals')
                    ->where('user_id', $id)
                    ->update($professional);

                $user        = User::find($id);
                $user->name  = $request->firstname;
                $user->email = $request->email;
                $user->save();

            } catch (\Exception $e) {
                DB::rollback();
                dd($e);
                return redirect::back()
                    ->with('error', 'There was problem updating the information. Please try later');
            }
            DB::commit();
            return redirect::back()
                ->with('message', 'User Information updated successfully');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $usertype = DB::table('users')
            ->where('id', '=', $id)
            ->first();

        if ($usertype->role == '4') {
            DB::table('users')
                ->where('id', $id)
                ->delete();
            return redirect::back()
                ->with('error', 'User removed successfully from the list');
        } else {

            try {
                DB::beginTransaction();
                $getUser = DB::table('users as u')
                    ->join('professionals as p', 'p.user_id', '=', 'u.id')
                    ->where('u.id', '=', $id)
                    ->select('u.name', 'u.email', 'p.business_name', 'p.company_name', 'p.suburb', 'p.contact', 'p.mobile_no')
                    ->first();

                $deleted                = new Deletedat;
                $deleted->user_id       = $id;
                $deleted->name          = $getUser->name;
                $deleted->email         = $getUser->email;
                $deleted->business_name = $getUser->business_name;
                $deleted->company_name  = $getUser->company_name;
                $deleted->suburb        = $getUser->suburb;
                $deleted->contact       = $getUser->contact;
                $deleted->type          = 'pro';
                $deleted->mobile        = $getUser->mobile_no;
                $deleted->deleted_at    = date('YmdHis');

                $deleted->save();

                DB::table('users')
                    ->where('id', '=', $id)
                    ->delete();

                DB::table('professionals')
                    ->where('user_id', '=', $id)
                    ->delete();

            } catch (\PDOException $e) {
                DB::rollback();
                return redirect::back()
                    ->with('error', 'There was problem deleting the information. Please try later');

            }
        }
        DB::commit();
        return redirect::back()
            ->with('error', 'User removed successfully from the list');
    }

    /*
     * ------------------------------------------------------------------
     * ACTIVATING THE USER STATUS #GET ROUTE
     * ------------------------------------------------------------------
     */
    public function activateUser($id)
    {
        try {
            //success
            DB::beginTransaction();
            DB::table('users')
                ->where('id', '=', $id)
                ->update(['active' => '1']);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect::back()
                ->with('error', 'There was a problem activating the user. Please try later');
        }
        return redirect::back()
            ->with('message', 'User with id ' . $id . ' activated successfully');
    }

    /*
     * ------------------------------------------------------------------
     * SUSPENDING THE USER STATUS #GET ROUTE
     * ------------------------------------------------------------------
     */
    public function suspendUser($id)
    {
        try {
            //success
            DB::beginTransaction();
            DB::table('users')
                ->where('id', '=', $id)
                ->update(['active' => '0']);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            return redirect::back()
                ->with('error', 'There was a problem activating the user. Please try later');
        }
        return redirect::back()
            ->with('message', 'User with id ' . $id . ' suspended successfully');
    }

}
