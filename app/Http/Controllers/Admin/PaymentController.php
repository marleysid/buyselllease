<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Crypt;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class PaymentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function create() {
		$payment_old = Payment::First();

		$payment = new stdClass();
		if(!empty($payment_old)){
			$payment->id=$payment_old->id;
			$payment->appkey=Crypt::decrypt($payment_old->appkey);
			$payment->apppassword=Crypt::decrypt($payment_old->apppassword);
			$payment->mode=$payment_old->mode;
		}

		return view('admin.payment.create', compact('payment'));
	}

	public function save(Request $request) {
		$validator = Validator::make($request->all(), [
			'appkey' => 'required',
			'apppassword' => 'required',
			'mode' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('admin/payment')->withErrors($validator);
		} else {



			$payment = Payment::FirstOrCreate(['id' => '1']);
			$payment->appkey = Crypt::encrypt($request->appkey);
			$payment->apppassword = Crypt::encrypt($request->apppassword);
			$payment->mode = $request->mode;
			$payment->save();


			Flash::success('Payment Details saved successfully.');
			return redirect('admin/payment');

		}
	}

}