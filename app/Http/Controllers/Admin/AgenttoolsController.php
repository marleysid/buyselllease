<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Deletedat;
use App\Models\User;
use Datatables;
use DB;
use GCH;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Validator;

class AgenttoolsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.agent.list');
    }

    public function getAdvanceFilterData()
    {

        $agentList = DB::table('users as u')
            ->join('agents as a', 'u.id', '=', 'a.user_id')
            ->where('u.role', '=', '2')
            ->select('u.id', 'u.name', 'u.email', 'a.import_id', 'a.trading_name', 'a.address', 'a.trading_name', 'u.status', 'u.active');

        return Datatables::of($agentList)
            ->editColumn('status', '@if($active == 1)
                                Active
                            @else
                                Suspended
                            @endif')
            ->editColumn('active', '@if ($active == 1)
                                        <a href="<?=url("admin/agent/suspend")."/".$id?>" class="btn btn-default btn-sm" onclick="return suspend()" >Suspend User</a>
                                            @else
                                        <a href="<?=url("admin/agent/activate")."/".$id?>" class="btn btn-default btn-sm" onclick="return activate()">Activate User</a>
                                    @endif')
            ->addColumn('operations',
                '
                    <a href="<?php echo  url("admin/agent/show/")."/".$id ;?>" class="btn btn-default btn-sm">View</a>
                    <a href="<?php echo  url("admin/agent/edit/")."/".$id ;?>" class="btn btn-default btn-sm">Edit</a>
                    <a  href="<?php echo  url("admin/agent/destroy")."/".$id ;?>" class="btn btn-default btn-sm" onclick="return deleteThis()">Delete</a>
                ')
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $price      = DB::table('agentpricing')->get();
        $web_hoster = DB::table('webhoster')->get();
        return view('admin.agent.add', compact('price', 'web_hoster'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $agentVaidation = validator::make($request->all(), [

            'agent_name'        => 'required',
            //'company_name' => 'required',
            'trading_name'      => 'required',
            //'abn' => 'required',
            //'acn' => 'required',
            'email'             => 'required|email|unique:users',
            'suburb'            => 'required',
            'company_website'   => ['required', 'regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'meet_the_team_url' => ['required', 'regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'description'       => 'required',
            'address'           => 'required',
            'postcode'          => 'required',
            //'office_type' => 'required',
            //'profile_image' =>  ['required_without:profile_upload','regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'logo'              => ['required_without:logo_upload', 'regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'logo_upload'       => 'required_without:logo',
            //'profile_upload' => 'required_without:profile_image',
            //'import_id' => 'required',
            //'mobile' => 'required ',
            'phone'             => 'required',
            //'crm_provider' => 'required',
            // 'industry' => 'required',
            //'service_provided' => 'required',
            'web_hoster'        => 'required',
            'password'          => 'required',
            'confirm_password'  => 'required | same:password',
        ]);

        if ($agentVaidation->fails()) {
            return redirect::back()
                ->withErrors($agentVaidation->messages())
                ->withInput(Input::except('password', 'confirm_password'));
        } else {

            $geomety = GCH::geolocate_address($request->suburb);

            $lat = $geomety['lat'];
            $lng = $geomety['lng'];

            /*
            |-----------------------------------------------------------------------
            |send user data both to the users table as well as to agent table
            |proceed to the paypal payment
            |after sucecss, redirect back to the site and save datas to the database
            |-----------------------------------------------------------------------
             */

            $user           = new User;
            $user->name     = $request->agent_name;
            $user->email    = $request->email;
            $user->password = \Hash::make($request->password);
            $user->role     = env('AGENT_USER', '2');
            $user->active   = 1;

            $user->save();

            $userDetails = [
                'name'  => $request->agent_name,
                'email' => $request->email,
            ];

            /*
            |------------------------------------------------------------
            |Adding new agent
            |saving other agent details to the database
            |also pass the user id here
            |-------------------------------------------------------------
             */

            if ($request->hasFile('logo_upload')) {

                $destination1 = 'uploads/agents/logo';
                $extension1   = Input::file('logo_upload')->getClientOriginalExtension(); //getting extension of uploaded image
                $fileName1    = rand(11111, 99999) . time() . '.' . $extension1; //renaming image
                Input::file('logo_upload')->move($destination1, $fileName1);
                $logo = asset($destination1 . '/' . $fileName1);
            } else {
                $logo = $request->logo;
            }

            if ($request->hasFile('profile_upload')) {

                $destination = 'uploads/agents/profile';
                $extension   = Input::file('profile_upload')->getClientOriginalExtension(); //getting extension of uploaded image
                $fileName    = rand(11111, 99999) . time() . '.' . $extension; //renaming image
                Input::file('profile_upload')->move($destination, $fileName);
                $profile = asset($destination . '/' . $fileName);
            } else {
                $profile = $request->profile_image;
            }

            //dd($request);

            $agent                     = new Agent;
            $agent->user_id            = $user->id;
            $agent->name               = $request->company_name;
            $agent->trading_name       = $request->trading_name;
            $agent->address            = $request->address;
            $agent->suburb             = $request->suburb;
            $agent->lat                = $lat;
            $agent->lng                = $lng;
            $agent->abn                = $request->abn;
            $agent->acn                = $request->acn;
            $agent->mobile             = $request->mobile;
            $agent->phone              = $request->phone;
            $agent->import_id          = $request->import_id;
            $agent->listing_base_url   = $request->company_website;
            $agent->listing_staff_url  = $request->meet_the_team_url;
            $agent->description        = $request->description;
            $agent->logo_url           = $logo;
            $agent->profile_image      = $profile;
            $agent->postcode           = $request->postcode;
            $agent->payment_status     = 1;
            $agent->primary_colour     = $request->primary_color;
            //$agent->offices            = $request->office_type;
            $agent->ftp_home_directory = $request->web_hoster;
            $agent->expired_at         = date('Y-m-d H:i:s', strtotime('+1 years'));
            $agent->save();

            $signupText = DB::table('emailtemplates')
                ->where('type', '=', 'agentregistration')
                ->first();
            $contents = $signupText->text;

            $data = array('content' => $contents, 'email' => $user->email, 'name' => $request->agent_name, 'trading' => $request->trading, 'link' => $request->company_website, 'staffURL' => $request->meet_the_team_url, 'address' => $request->address, 'postcode' => $request->postcode, 'phone' => $request->phone, 'crm' => $request->import_id);
             // Mail  to  admin result
            //dd($data);
            \Mail::send('emails.agent-registration', $data, function ($message) use ($data) {
            //
            $username = $data['name'];
            // $message->to(env('BSL_SALES_EMAIL'), env('BSL_SALES_NAME'));
            $message->to('accounts@buyselllease.com.au', 'Neaf');
            $message->bcc('erpushparaj23@gmail.com', 'Puspa');
            $message->subject('Agent Registration Submission - ' . $username);
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            });

            $toMail = $request->email;
            $toName = $request->agent_name;
            //mail to  user
            $content = [
                'content' => $contents,
                'toName'  => $toName,
            ];

            \Mail::send('emails.agent-registration-user', $data, function ($message) use ($data) {
                //
                $message->to($data['email'], $data['name']);
                $message->bcc('puspa@ebpearls.com', 'Puspa');
                $message->subject('Agent Registrated Successfully');
                $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            });

            return redirect::to('admin/Agenttools')
                ->with('message', 'Agent Added successfully');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $getAgent = DB::table('users as u')
                ->join('agents as a', 'u.id', '=', 'a.user_id')
                ->where('u.id', '=', $id)
                ->select('*')
                ->first();

        } catch (\Exception $e) {
            return redirect::back()
                ->with('error', 'We are unable to display the information of this user form now. Please try later');
        }

        $getAgent = Agent::where(['user_id' => $id])->first();
        return view('admin.agent.show')
            ->with(array(
                'agent' => $getAgent,
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        try {

            $price      = DB::table('agentpricing')->get();
            $web_hoster = DB::table('webhoster')->get();
            $getAgent   = DB::table('users as u')
                ->join('agents as a', 'u.id', '=', 'a.user_id')
                ->where('u.id', '=', $id)
                ->select(
                    'u.id as user_id',
                    'u.name as agent_name',
                    'u.email',
                    'a.*',
                    'a.name as company_name'
                )
                ->first();

        } catch (\Exception $e) {
            return redirect::back()
                ->with('error', 'We are unable to display the information of this user form now. Please try later');
        }
        return view('admin.agent.edit')
            ->with(array(
                'users'      => $getAgent,
                'price'      => $price,
                'web_hoster' => $web_hoster,
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $v = validator::make($request->all(), [

            'agent_name'        => 'required',
            //'company_name' => 'required',
            'trading_name'      => 'required',
            //'abn' => 'required',
            //'acn' => 'required',
            'email'             => 'required|email|unique:users,email,' . $id,
            'suburb'            => 'required',
            'company_website'   => ['required', 'regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'meet_the_team_url' => ['required', 'regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'description'       => 'required',
            'address'           => 'required',
            'postcode'          => 'required',
            //'office_type' => 'required',
            //'profile_image' =>  ['required_without:profile_upload','regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'logo'              => ['required_without:logo_upload', 'regex:/(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?/'],
            'logo_upload'       => 'required_without:logo',
            //'profile_upload' => 'required_without:profile_image',
            //'import_id' => 'required',
            //'mobile' => 'required ',
            'phone'             => 'required',
            //'crm_provider' => 'required',
            // 'industry' => 'required',
            //'service_provided' => 'required',
            'web_hoster'        => 'required',
        ]);
        if ($v->fails()) {
            return redirect::back()
                ->withErrors($v->messages())
                ->withInput();
        } else {
            $geomety = GCH::geolocate_address($request->suburb);

            $lat = $geomety['lat'];
            $lng = $geomety['lng'];

            $agent = Agent::where(['user_id' => $id])->first();
            /*$updateValues = [
            'name'              =>    Input::get('name'),
            'trading_name'      =>    Input::get('trading_name'),
            'import_id'         =>    Input::get('import_id'),
            'suburb'            =>    Input::get('suburb'),
            'abn'               =>    Input::get('abn'),
            'acn'               =>    Input::get('acn'),
            'mobile'            =>    Input::get('mobile'),
            'phone'             =>    Input::get('phone'),
            'listing_base_url'  =>    Input::get('listing_base_url'),
            'listing_staff_url' =>    Input::get('listing_staff_url'),
            'postcode'          =>    Input::get('postcode'),
            'ftp_home_directory'=>    Input::get('ftp_home_directory')
            ];*/

            if ($request->hasFile('logo_upload')) {

                $destination1 = 'uploads/agents/logo';

                $extension1 = Input::file('logo_upload')->getClientOriginalExtension(); //getting extension of uploaded image
                $fileName1  = rand(11111, 99999) . time() . '.' . $extension1; //renaming image
                Input::file('logo_upload')->move($destination1, $fileName1);
                $logo = asset($destination1 . '/' . $fileName1);
            } else {
                $logo = $request->logo;
            }

            if ($request->hasFile('profile_upload')) {

                $destination = 'uploads/agents/profile';
                $extension   = Input::file('profile_upload')->getClientOriginalExtension(); //getting extension of uploaded image
                $fileName    = rand(11111, 99999) . time() . '.' . $extension; //renaming image
                Input::file('profile_upload')->move($destination, $fileName);
                $profile = asset($destination . '/' . $fileName);
            } else {
                $profile = $request->profile_image;
            }

            $agent->name               = $request->company_name;
            $agent->trading_name       = $request->trading_name;
            $agent->address            = $request->address;
            $agent->suburb             = $request->suburb;
            $agent->lat                = $lat;
            $agent->lng                = $lng;
            $agent->abn                = $request->abn;
            $agent->acn                = $request->acn;
            $agent->mobile             = $request->mobile;
            $agent->phone              = $request->phone;
            $agent->import_id          = $request->import_id;
            $agent->listing_base_url   = $request->company_website;
            $agent->listing_staff_url  = $request->meet_the_team_url;
            $agent->description        = $request->description;
            $agent->logo_url           = $logo;
            $agent->profile_image      = $profile;
            $agent->postcode           = $request->postcode;
            $agent->primary_colour     = $request->primary_color;
            $agent->offices            = $request->office_type;
            $agent->ftp_home_directory = $request->web_hoster;
            $agent->save();

            $user        = User::find($agent->user_id);
            $user->name  = $request->agent_name;
            $user->email = $request->email;
            $user->save();

            /* try{
            DB::beginTransaction();
            DB::table('agents')
            ->where('user_id', '=', $id)
            ->update($updateValues);
            } catch(\PDOException $e){
            DB::rollback();
            return redirect::back()
            ->with('error', 'Information couldnot be updated at this time. Please try later');
            }*/
            /*DB::commit();*/
            return redirect::back()
                ->with('message', 'Information updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $agent = Agent::where('user_id', $id)->first();
        $agent_user_id = $agent->id;

        $countRow = DB::table('properties')
                            ->where('agent_id', $agent_user_id)
                            ->count();



        if ($countRow != "0") {
            return redirect::back()
                ->with('error', 'Cannot remove. There are properties associated with this agent');
        } else {

            try {
                DB::beginTransaction();

                $getUser = DB::table('users as u')
                    ->join('agents as a', 'u.id', '=', 'a.user_id')
                    ->where('u.id', '=', $id)
                    ->select('a.name', 'u.email', 'a.trading_name', 'a.suburb', 'a.phone', 'a.mobile')
                    ->first();

                $deleted                = new Deletedat;
                $deleted->user_id       = $id;
                $deleted->name          = $getUser->name;
                $deleted->email         = $getUser->email;
                $deleted->business_name = $getUser->trading_name;
                $deleted->company_name  = "";
                $deleted->suburb        = $getUser->suburb;
                $deleted->contact       = $getUser->phone;
                $deleted->type          = 'agent';
                $deleted->mobile        = $getUser->mobile;
                $deleted->deleted_at    = date('YmdHis');

                $deleted->save();

                /*
                |----------------------------------------------------------------------
                |prepare all the datas that need to be inserted to deleted_users table
                |-------------------------------------------------------------------------
                 */

                DB::table('agents')
                    ->where('user_id', '=', $id)
                    ->delete();

                DB::table('users')
                    ->where('id', '=', $id)
                    ->delete();

            } catch (\PDOException $e) {
                DB::rollback();
                return redirect::back()
                    ->with('error', 'Couldnot perform this operation now. Please try later');
            }
        }
        DB::commit();
        return redirect::back()
            ->with('message', 'Agent Removed successfully');
    }

    public function activateAgent($id)
    {
        try {
            DB::table('users')
                ->where('id', '=', $id)
                ->update(['active' => '1']);
        } catch (\Exception $e) {
            return redirect::back()
                ->with('error', 'Couldnot perform this operation now. Please try later');
        }
        return redirect::back()
            ->with('message', 'User activated successfully');
    }

    public function suspendAgent($id)
    {
        try {
            DB::table('users')
                ->where('id', '=', $id)
                ->update(['active' => '0']);
        } catch (\Exception $e) {
            return redirect::back()
                ->with('error', 'Couldnot perform this operation now. Please try later');
        }
        return redirect::back()
            ->with('message', 'User suspended successfully');
    }

}
