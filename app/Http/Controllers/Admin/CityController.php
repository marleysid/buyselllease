<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Region;
use GCH;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class CityController extends Controller
{

    public function index()
    {
        //$city = City::all();
        $region = Region::all();
        return view('admin.city.list')->with('region', $region);
    }

    public function create($id = null)
    {
        if($id != null){
            $selected_id = $id;
        }else{
            $selected_id = "";
        }

        $region = Region::all();
        
        return view('admin.city.create')->with('region', $region)->with('selected',$selected_id);
    }

    public function store(Request $request)
    {

/*      if ($request->newregion != ""){
            $v = validator::make($request->all(), [
                'newregion' => 'required|unique:region,name',
            ]);
        }

        if ($v->fails()) {
            return redirect::back()
                ->withErrors($v->messages())
                ->withInput();
        } else {*/

            if ($request->newregion != ""){
                //first add parent region
                $reg           = new Region;
                $reg->name = $request->newregion;
                $reg->save();
            }


            if ($request->newregion != ""){
                $idOfRegion = $reg->id;
            } else {
                $idOfRegion = $request->region;
            }
            //now add the suburbs falling under that region

      
            foreach (($request->name) as $req) {
                $geomety = GCH::geolocate_address($req);
                $lat     = $geomety['lat'];
                $lng     = $geomety['lng'];

                $city            = new City;
                $city->region_id = $idOfRegion;
                $city->name      = $req;
                $city->lat       = $lat;
                $city->lon       = $lng;
                $city->save();

            }

            return redirect::to('admin/citylist')
                ->with('message', 'Suburb Added successfully');
        //}
    }

    /*
    | editing the city
    |
     */
    public function edit($id)
    {
        $region = Region::all();
        $city   = City::find($id);
        return view('admin.city.edit')
            ->with(array(
                'city'   => $city,
                'region' => $region,
            ));
    }

    /*
    | updating the city
     */
    public function update(Request $request, $id)
    {

        $v = validator::make($request->all(), [
            'name' => 'required|unique:region_city,name,' . $id,
        ]);

        if ($v->fails()) {
            return redirect::back()
                ->withErrors($v->messages());
        } else {
            $geomety = GCH::geolocate_address($request->name);
            $lat     = $geomety['lat'];
            $lng     = $geomety['lng'];

            $city            = City::find($id);
            $city->region_id = $request->region;
            $city->name      = $request->name;
            $city->lat       = $lat;
            $city->lon       = $lng;
            $city->save();

            return redirect::to('admin/citylist')
                ->with('message', 'Suburb Updated successfully');

        }
    }

    public function delete($id)
    {
        $city = City::find($id);
        $city->delete();
        return redirect::to('admin/citylist')
            ->with('message', 'Suburb Deleted successfully');
    }

    public function regionEdit($id)
    {
        $region = Region::find($id);
        return view('admin.city.regionedit')
            ->with(array(
                'region' => $region,
            ));
    }

    public function updateRegion(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|unique:region,name,' . $id,
        ]);

        if ($v->fails()) {
            return redirect::back()
                ->withErrors($v->messages());
        } else {
            $region       = Region::find($id);
            $region->name = $request->name;
            $region->save();

             return redirect::to('admin/citylist')
                ->with('message', 'Region Updated successfully');
        }
    }
    public function regionDelete($id){
        $region = Region::find($id);
        $region->delete();
        return redirect::to('admin/citylist')
            ->with('message', 'Region Deleted successfully');
    }

}
