<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Input;
use Redirect;
use App\Models\Agentpricing;
use DB;

use Illuminate\Http\Request;

class AgentPricingController extends Controller {

	public function addPricing()
	{
		$allPrices = DB::table('agentpricing')->get();
		return view('admin.pricing.list')
					->with(array(
						'price'	=> $allPrices
					));
	}

	
	public function createPricing()
	{
		return view('admin.pricing.create');
	}


	public function storePricing(Request $request)
	{
		$v = validator::make($request->all(), [
				 'office'	=>	'required|unique:agentpricing,office',
				 'price'	=>	'required | numeric'
			]);

		if ($v->fails()){
			return redirect::back()
				->withErrors($v->messages());
		} else {
			/*
			|validation has been passed including unique validation
			|now save the datas in the database
			*/
			$price = new Agentpricing;
			$price->office = Input::get('office');
			$price->price  = Input::get('price');
			$price->status = 'active';

			$price->save();

			return redirect::back()
					->with('message', 'New Pricing has been created sucessfully');
		}
	}


	public function editPricing($id)
	{
		$pricing = AgentPricing::findorFail($id);
		return view('admin.pricing.editprice')
					->with(array(
						'editprice' => $pricing
					));
	}


	public function updatePricing(Request $request, $id)
	{

		$v = validator::make($request->all(), [
				 'office'	=>	'required|unique:agentpricing,office,' . $id,
				 'price'	=>	'required | numeric'
			]);

		if ($v->fails()){
			return redirect::back()
				->withErrors($v->messages());
		} else {
			try{
				DB::beginTransaction();
				DB::table('agentpricing')
						->where('id', '=', $id)
						->update(['price' => Input::get('price')]);
			} catch (\Exception $e) {
				DB::rollback();
				return redirect::back()
					->with('error', 'Sorry!! we couldnot update the value at this time');
			}
			DB::commit();
			return redirect::to('admin/agentpricing')
					->with('message', 'Price updated successfully');

		}

	}

}
