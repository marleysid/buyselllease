<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Paypal;
use Crypt;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class PaypalController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function create() {
		$payment_old = Paypal::First();

		$payment = new stdClass();
		if(!empty($payment_old)){
			$payment->id=$payment_old->id;
			$payment->client_id=Crypt::decrypt($payment_old->client_id);
			$payment->secret_id=Crypt::decrypt($payment_old->secret_id);
			$payment->mode=$payment_old->mode;
		}

		return view('admin.payment.create', compact('payment'));
	}

	public function save(Request $request) {
		$validator = Validator::make($request->all(), [
			'client_id' => 'required',
			'secret_id' => 'required',
			'mode' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('admin/payment')->withErrors($validator);
		} else {

			//$payment = Payment::findOrNew('1');
			$payment = Paypal::firstOrNew(['id'=>'1']);
			$payment->client_id = Crypt::encrypt($request->client_id);
			$payment->secret_id = Crypt::encrypt($request->secret_id);
			$payment->mode = $request->mode;
			$payment->save();
			
			
			Flash::success('Payment Details saved successfully.');
			return redirect('admin/payment');

		}
	}

}