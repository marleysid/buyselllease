<?php namespace App\Http\Controllers\Admin;

/*use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;*/

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User as User;
use Flash;


class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;
    protected $redirectTo;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

    }


    public function getLogin()
    {
        return view('admin.login');
    }





    public function postLogin(Request $request, User $user)
    {

        $validator = Validator::make($request->all(), array('name' => 'required', 'password' => 'required'));
        if ($validator->fails()) {

            return redirect()->to('admin/login')->withErrors($validator)->withInput();
        }

        $credentials = $request->only('name', 'password');
        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            if($this->auth->user()->role =='1' && $this->auth->user()->status =='1'){
                //Flash::overlay('You are now a Laracasts member!');
                return redirect()->to('admin/users')->with('message', 'successfully logged in');
            } else {
                Flash::error('Authentication Failed');
                $this->auth->logout();
                redirect()->to('admin/login');
            }

        }
        Flash::error('These credentials do not match our records.');
        return redirect()->to('admin/login')->withInput($request->only('email', 'remember'));
    }


    /**
     * Log the user out of the application.
     *
     * @Get("auth/logout")
     *
     * @return Response
     */
    public function getLogout()
    {

        $this->auth->logout();

        return redirect('admin/login');
    }




}
