<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Advertisement;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\AdminController;
use Datatables;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;


class AdvertisementController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.advertisement.list');
	}


	/**
	 *  ajax  datatable
     */
	public function getAdvanceFilterData()
	{
		$ads = Advertisement::select(['id','title','link','status','created_at']);

		return Datatables::of($ads)
				->addColumn('Change Status','@if($status == 1)
                               <a href="<?=url("admin/advertisement/suspend/")."/".$id;?>" class="btn btn-default btn-sm" onclick="suspend()">Suspend</a>
                            @else
                                 <a href="<?=url("admin/advertisement/activate"). "/".$id;?>" class="btn btn-default btn-sm" onclick="activate()">Activate</a>
                            @endif')
			->addColumn('operations','<a   href="<?php echo  url("admin/advertisement/edit/")."/".$id ;?>" class="btn btn-default btn-sm">Edit</a>
                    <a  href="<?php echo  url("admin/advertisement/destroy")."/".$id ;?>" class="btn btn-default btn-sm" onclick="deleteThis()">Delete</a>
                ')
			->editColumn('status','@if($status == 1)
                                Active
                            @else
                                Suspended
                            @endif')
			->make();
		return Datatables::of($ads)->make();
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$action =['form' => 'add','button' => 'Save'];
		return view('admin.advertisement.form',compact('action'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = validator::make($request->all(), [
           'title'          =>  'required',
            'description'   =>  'required',
            'userfile'      =>  'required|mimes:jpg,jpeg,png,svg,gif',
            'link'          =>  'required',
            'status'        =>  'required'
        ]);

        if ($v->fails()) {
            return redirect::back()
                ->withErrors($v->messages());
        } else {
           /*
            * ---------------------------------------
            * specifying destination path for the image
            * ---------------------------------------
            */
            $destination = 'uploads/admin/advertisement';
            $extension = Input::file('userfile')->getClientOriginalExtension();  //getting extension of uploaded image
            $fileName = rand(11111, 99999).date('Ymdhis').'.' .$extension;           //renaming image
            Input::file('userfile')->move($destination, $fileName);             //moving file to destination

            //saving all the datas to database
            $ad = new Advertisement;
            $ad->title          = Input::get('title');
            $ad->description    = Input::get('description');
            $ad->image          = $fileName;
            $ad->link           =  Input::get('link');
            $ad->status         = Input::get('status');

            $ad->save();

            return redirect::to('admin/advertisement')
                ->with('message', 'New advertisement uploaded successfully');


        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return view('admin.advertisement.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
            $ad = DB::table('advertisements')
                    ->where('id', '=', $id)
                    ->first();
        } catch(\PDOException $e) {
            return redirect::back()
                ->with('error', 'Sorry!! The ad you have requested is not available now. Please try later');

        }
        return view('admin.advertisement.edit')
            ->with(array(
               'editad' => $ad
            ));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

        if (Input::file('userfile')){
            $v = validator::make($request->all(), [
                'title'          =>  'required',
                'description'   =>  'required',
                'userfile'      =>  'required|mimes:jpg,jpeg,png,svg,gif',
                'link'          =>  'required',
                'status'        =>  'required'
            ]);

            if ($v->fails()){
                return redirect::back()
                    ->withErrors($v->messages());
            } else {

                $currentImage = DB::table('advertisements')
                    ->where('id', '=', $id)
                    ->first();

                try {
                    File::delete('uploads/admin/advertisement/' . $currentImage->image);

                    /*
                    * ---------------------------------------
                    * specifying destination path for the image
                    * ---------------------------------------
                    */
                    $destination = 'uploads/admin/advertisement';
                    $extension = Input::file('userfile')->getClientOriginalExtension();  //getting extension of uploaded image
                    $fileName = time().'.' .$extension;           //renaming image
                    Input::file('userfile')->move($destination, $fileName);             //moving file to destination

                   $updateArray = [
                       'title'          => Input::get('title'),
                       'description'    => Input::get('description'),
                       'image'          => $fileName,
                       'link'           => Input::get('link'),
                       'status'         => Input::get('status')
                   ];

                    DB::table('advertisements')
                        ->where('id', '=', $id)
                        ->update($updateArray);

                } catch (\Exception $e) {

                    return redirect::back()
                        ->with('error', 'There was some problem. Please try later');

                }

                return redirect::back()
                    ->with('message', 'Advertisement edited successfully');
            }

        } else {
            $v = validator::make($request->all(), [
                'title'          =>  'required',
                'description'   =>  'required',
                'link'          =>  'required',
                'status'        =>  'required'
            ]);

            if ($v->fails()){
                return redirect::back()
                    ->withErrors($v->messages());
            } else {

                $updateArray = [
                    'title'          => Input::get('title'),
                    'description'    => Input::get('description'),
                    'link'           => Input::get('link'),
                    'status'         => Input::get('status')
                ];

                DB::table('advertisements')
                    ->where('id', '=', $id)
                    ->update($updateArray);

                return redirect::back()
                    ->with('message', 'Advertisement edited successfully');
            }
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        try {
            DB::beginTransaction();

            $getImage = DB::table('advertisements')
                        ->where('id', '=', $id)
                        ->first();
            File::delete('uploads/admin/advertisement/' . $getImage->image);

            DB::table('advertisements')
                ->where('id', '=', $id)
                ->delete();


        } catch (\PDOException $e){
            DB::rollback();
            return redirect::back()
                ->with('error', "cannot delete this ad. Please try later");
        }
        DB::commit();

        return redirect::back()
            ->with('message', 'Advertisement deleted successfully');
	}



    public function activateAd($id)
    {
        try {
            $suspendAd = DB::table('advertisements')
                ->where('id', '=', $id)
                ->update(['status' => '1']);
        } catch (\PDOException $e) {
            return redirect::back()
                ->with('error', 'Cannot activate this ad now. Please try later');
        }

        return redirect::back()
            ->with('message', 'Advertisement activated successfully');
    }



    public function suspendAd($id)
    {
        try {
            $suspendAd = DB::table('advertisements')
                ->where('id', '=', $id)
                ->update(['status' => '2']);
        } catch (\PDOException $e) {
            return redirect::back()
                ->with('error', 'Cannot suspend this ad now. Please try later');
        }

        return redirect::back()
            ->with('message', 'Advertisement suspended successfully');
    }

}
