<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Promotion;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PromotionController extends Controller {

	/**
	 * Display a listing of the promotion codes.
	 *
	 * @return Response
	 */

	public function index() {
		$getCodes = DB::table('promotionalcode')->get();

		return view('admin.promotion.list')
			->with(array(
				'code' => $getCodes,
			));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('admin.promotion.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		if (Input::get('percentage') == "") {
			$v = validator::make($request->all(), [
				'code' => 'required',
				'type' => 'required',
			]);
			$percentage = 100;

		} else {
			$v = validator::make($request->all(), [
				'code' => 'required',
				'type' => 'required',
				'percentage' => 'required|numeric',
			]);
			$percentage = Input::get('percentage');

		}

		if ($v->fails()) {
			return redirect::back()
				->withErrors($v->messages());
		} else {

			date_default_timezone_set('Asia/Kathmandu');

			$promo = new Promotion;
			$promo->code = Input::get('code');
			$promo->type = Input::get('type');
			$promo->percentage = $percentage;
			$promo->created_at = date('Y-m-d H:i:s');

			$promo->save();

			return redirect::route('admin.codelist')
				->with('message', 'Promo code created successfully');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		try {
			DB::table('promotionalcode')
				->where('id', '=', $id)
				->delete();
		} catch (\PDOException $e) {
			return redirect::route('admin.codelist')
				->with('error', 'Promo Code could not be deleted. Please try later');
		}

		return redirect::route('admin.codelist')
			->with('message', 'Promo Code deleted successfully');
	}

}
