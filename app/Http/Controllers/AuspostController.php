<?php namespace App\Http\Controllers;

use App\Helpers\AuspostAPIHelper;
use Illuminate\Http\Request;

class AuspostController extends Controller {

    /**
     *
     */
    public function __construct()
    {

	}

    public function suburb_search(Request $request)
	{
        return AuspostAPIHelper::suburb_search($request);
	}
}
