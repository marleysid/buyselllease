<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use View;
use Mail;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agent;
use App\Models\Payment;
use DB;
use App\Professionals;
use Crypt;
use App\Models\Transactions;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\Advertisement;
use App\Models\Subscriber;
use GCH;
use Spatie\Newsletter\MailChimp\Newsletter;
use App\Models\User;


class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{

        $ads = DB::table('advertisements')
                ->where('status', '=', '1')
                ->take(4)
                ->get();

        return view('home.home')
            ->with(array(
                'deals' => $ads
            ));
	}

    public function howItWorks() {
        return View::make('home.howitworks');
    }

    public function aboutUs() {
        return View::make('home.aboutus');

    }
	public function directrelationships() {
		return View::make('home.directrelationships');
	}

	public function getInstantAlerts() {
		return View::make('home.getinstantalerts');
	}

	public function theFocusIsOnYou() {
		return View::make('home.thefocusisonyou');
	}

    public function contactUs(Request $request) {

        if($request->query()) {
            $this->validate($request, [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'address' => 'required',
                'msg' => 'required',
            ]);

            Mail::send('emails.contactus', $request->all(), function($message) use ($request)
            {
                $message->to(env('BSL_ADMIN_EMAIL'), env('BSL_ADMIN_NAME'))->subject('Contact Us Submission - ' . $request->get('name'));
                $message->bcc('john@cdwsydney.com.au', 'John Bonato');
                $message->bcc('steven@cdwsydney.com.au', 'Steven Spencer');
                $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            });

            Session::flash('flash_message', 'Your message has been sent. Thank You.');
        }

        return View::make('home.contactus');
    }

    public function costEffective() {
        return View::make('home.costeffective');
    }

    public function salesAndLeasing() {
        return View::make('home.salesandleasing');
    }

    public function findServices() {
        return View::make('home.findservices');
    }

	public function faq() {
		return View::make('home.faq');
	}

    public function termsOfUse() {
        return View::make('home.termsofuse');
    }

    public function privacyPolicy() {
        return View::make('home.privacypolicy');
    }

    public function agentInfo() {
        return View::make('home.agent-info');
    }

    public function postSubscribe(Request $request)
    {

        if($request->ajax()) {
            $getAll = Input::all();
            $v = validator::make($request->all(), [
               'email' => 'required|email|unique:subscriber,email'
            ]);

            if ($v->fails()){
                return \Response::make(['status' => 'failed', 'error' => $v->messages()->first('email')]);

            } else {
                \Newsletter::subscribe(Input::get('email'));
                $sub = new Subscriber;
                $sub->email         = $request->email;
                $sub->subscribed    = 1;
                $sub->save();
                return \Response::make(['status' => 'success', 'message' => 'You have been successfully added to our subscriber list']);
            }
        }


    }
}

