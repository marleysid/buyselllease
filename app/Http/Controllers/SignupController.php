<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfessionalRequest;
use App\Models\Agent;
use App\Models\City;
use App\Models\Professionals;
use App\Models\Promotion;
use App\Models\Region;
use App\Models\Transactions;
use App\Models\User;
use DB;
use Flash;
use GCH;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;
use SH;

class SignupController extends Controller
{

    protected $apikey;
    protected $password;
    protected $test_mode;

    /**
     *
     *
     * @return Response
     */
    public function agentSignUpForm()
    {
        $pricing = DB::table('agentpricing')->get();
        return view('home.agent-signup')->with(array('price' => $pricing));
    }

    /**
     *
     *
     * @return Response
     */
    public function professionalSignup()
    {
            $signupText = DB::table('emailtemplates')
            ->where('type', '=', 'professionalregistration')
            ->first();

            $txt = $signupText->text;

           /* echo htmlspecialchars_decode($txt);

            die();*/
          

        $professions = SH::getCategoryOptions(true);
        $region      = Region::orderBy('name', 'asc')->get();
        return view('signup.form')->with(array(
            'professions' => $professions,
            'region'      => $region,
        ));
    }

    public function postPaymentAgent(Request $request)
    {

        $agentVaidation = validator::make($request->all(), [
            'name'             => 'required',
            'trading'          => 'required',
            'email'            => 'required|email|unique:users',
            'suburb'           => 'required',
            'link'             => 'required',
            'staffURL'         => 'required',
            'address'          => 'required',
            'postcode'         => 'required',
            'description'      => 'required',
            'phone'            => 'required',
            'crm'              => 'required',
            'password'         => 'required',
            'confirm_password' => 'required | same:password',
            'agree'            => 'required',
        ]);

        if ($agentVaidation->fails()) {
            return redirect::back()
                ->withErrors($agentVaidation->messages())
                ->withInput(Input::except('password', 'confirm_password'));
        } else {
            $geomety = GCH::geolocate_address($request->suburb);

            $lat = $geomety['lat'];
            $lng = $geomety['lng'];

            /*
            |-----------------------------------------------------------------------
            |send user data both to the users table as well as to agent table
            |proceed to the paypal payment
            |after sucecss, redirect back to the site and save datas to the database
            |-----------------------------------------------------------------------
             */

            $user           = new User;
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = \Hash::make($request->password);
            $user->role     = env('AGENT_USER', '2');
            $user->active   = 0;

            $user->save();

            $userDetails = [
                'name'  => $request->name,
                'email' => $request->email,
            ];

            Session::put('user_id_current', $user->id);

            /*
            |------------------------------------------------------------
            |Adding new agent
            |saving other agent details to the database
            |also pass the user id here
            |-------------------------------------------------------------
             */

            if (Input::has("my_uploader_id_agent_logo_files")) {
                $logo = asset('uploads/agents/logo') . '/' . $request->my_uploader_id_agent_logo_files[0];
            } else {
                $logo = '';
            }

            if (Input::has("my_uploader_id_agent_profile_files")) {
                $profile = asset('uploads/agents/profile') . '/' . $request->my_uploader_id_agent_profile_files[0];
            } else {
                $profile = '';
            }

            if ($request->promocode != "") {
                $code = Promotion::where(['code' => $request->promocode])->first();
                if (count($code) == '1') {
                    $findType = $code->type;
                    if ($findType == 'discount') {
                        $price = 5500 - (($code->percentage) / 100) * 5500;
                    } else if ($findType == 'free') {
                        $price = 0;
                    }
                } else {
                    Flash::error('Invalid promotional Code');
                    return redirect::back()->withInput();
                }
            } else {
                $price = '5500.00';
            }

            $agent                    = new Agent;
            $agent->user_id           = $user->id;
            $agent->name              = $request->company;
            $agent->trading_name      = $request->trading;
            $agent->address           = $request->address;
            $agent->suburb            = $request->suburb;
            $agent->lat               = $lat;
            $agent->lng               = $lng;
            $agent->abn               = $request->abn;
            $agent->acn               = $request->acn;
            $agent->mobile            = $request->mobile;
            $agent->phone             = $request->phone;
            $agent->import_id         = $request->importid;
            $agent->listing_base_url  = $request->link;
            $agent->listing_staff_url = $request->staffURL;
            $agent->description       = $request->description;
            $agent->logo_url          = $logo;
            $agent->profile_image     = $profile;
            $agent->postcode          = $request->postcode;
            $agent->payment_status    = 1;

            $agent->expired_at = date('Y-m-d H:i:s', strtotime('+1 years'));
            $agent->save();

            $formattedName = $request->name;
            $all           = explode(" ", $formattedName);

            if (count($all) == '1') {
                $agentFirstName = $all['0'];
                $agentLastName  = "Smith";
            } else {
                $agentFirstName = $all['0'];
                $agentLastName  = $all['1'];
            }

            if ($price != "0") {
                $firstName        = $agentFirstName;
                $lastName         = $agentLastName;
                $PaymentAmount    = $price;
                $CompanyName      = $request->company;
                $paymentReference = str_random(8);
                $redirect         = url('agent-signup/getdata');

                $queryString = "FirstName=" . $firstName . "&LastName=" . $lastName . "&PaymentReference=" . $paymentReference . "&PaymentAmount=" . $PaymentAmount . "&CompanyName=" . $CompanyName . "&RedirectURL=" . $redirect;

                //pass userid to the getAll function

                return redirect::to('https://buy-sell-lease.pay.ezidebit.com.au/?' . $queryString);
            } else {
                $findAgent                 = Agent::where('user_id', Session::get('user_id_current'))->first();
                $findAgent->payment_status = 1;
                $findAgent->save();

                self::sendEmailAgent(Session::get('user_id_current'));
                Session::forget('user_id_current');
                Flash::success('Signup successful. You will hear us from shortly');
                return redirect::to('agent-signup');
            }

        }

    }

    public function SaveUserAndMailAgent($request)
    {
        $firstName        = $request->company;
        $lastName         = $request->trading;
        $PaymentAmount    = '5500.00';
        $CompanyName      = $request->company;
        $paymentReference = str_random(8);
        $redirect         = url('agent-signup/data'); //'http://localhost:8000/agent-signup/getdata';

        $queryString = "FirstName=" . $firstName . "&LastName=" . $lastName . "&PaymentReference=" . $paymentReference . "&PaymentAmount=" . $PaymentAmount . "&CompanyName=" . $CompanyName . "&RedirectURL=" . $redirect;

        //pass userid to the getAll function

        return redirect::to('https://buy-sell-lease.pay.ezidebit.com.au/?' . $queryString);

        $request = (object) ($request);

    }

    public function getAgentData()
    {
        $response = $_GET;

        if (!empty($response)) {
            if ($response['ResultCode'] == '0') {
                $intoJson             = (serialize($response));
                $transaction          = new Transactions;
                $transaction->user_id = Session::get('user_id_current');
                $transaction->details = $intoJson;

                $transaction->save();

                /*
                | now also update the professional payment status to 1
                 */
                $findAgent                 = Agent::where('user_id', Session::get('user_id_current'))->first();
                $findAgent->payment_status = 1;
                $findAgent->save();

                self::sendEmailAgent(Session::get('user_id_current'));
                Session::forget('user_id_current');
                Flash::success('Signup successful. You will hear us from shortly');
                return redirect::to('agent-signup');
            } else {
                $findAgent = Agent::where('user_id', Session::get('user_id_current'))->first();
                if ($findAgent) {
                    $findAgent->delete();
                }
                $users = User::find(Session::get('user_id_current'));
                if ($users) {
                    $users->delete();
                }
            }
        }
        $findAgent = Agent::where('user_id', Session::get('user_id_current'))->first();
        if ($findAgent) {
            $findAgent->delete();
        }
        $users = User::find(Session::get('user_id_current'));
        if ($users) {
            $users->delete();
        }
        Flash::error('Signup Unsuccessful. There was problem processing your payment. Please Try later');
        return redirect::to('agent-signup');
    }

    public function postPaymentProfessional(ProfessionalRequest $request)
    {

        /*
        | first creating the user credentials to login
        | and putting the user id in session
         */
        if ($request->suburb != '') {
            $geomety = GCH::geolocate_address($request->suburb);
            $lat     = $geomety['lat'];
            $lng     = $geomety['lng'];
            $type    = 'suburb';
        } else if ($request->region != "") {

            $region = $request->region;
            $lat    = serialize($request->region);
            $lng    = serialize($request->region);
            $type   = 'region';

        } else {
            $geomety = GCH::geolocate_address($request->street_address);
            $lat     = '0'; //$geomety['lat'];
            $lng     = '0'; //$geomety['lng'];
            $type    = 'nationwide';
        }

        if ($request->promocode != "") {
            $code = Promotion::where(['code' => $request->promocode])->first();
            if (count($code) == '1') {
                $findType = $code->type;
                if ($findType == 'discount') {
                    $price = 2200 - (($code->percentage) / 100) * 2200;
                } else if ($findType == 'free') {
                    $price = 0;
                }
            } else {
                Flash::error('Invalid promotional Code');
                return redirect::back()->withInput();
            }
        } else {
            $price = '2200.00';
        }

        $user = User::create([
            'name'     => $request->first_name,
            'email'    => $request->email,
            'password' => \Hash::make($request->password),
            'role'     => env('PROFESSIONAL_USER', '3'),
            'active'   => '0',
        ]);

        Session::put('user_id_current', $user->id);

        $professional = Professionals::create([
            'firstname'         => $request->first_name,
            'lastname'          => $request->last_name,
            'user_id'           => $user->id,
            'profession'        => $request->category,
            'suburb'            => $request->suburb,
            'url'               => $request->link,
            'contact'           => $request->work_phone_number,
            'logo'              => $request->my_uploader_id_files[0],
            'description'       => $request->description,
            'lat'               => $lat,
            'lng'               => $lng,
            'street_address'    => $request->street_address,
            'business_name'     => $request->business_name,
            'company_name'      => $request->company_name,
            'mobile_no'         => $request->mobile_number,
            'business_position' => $request->business_position,
            'payment_status'    => 0,
            'expired_at'        => date('Y-m-d H:i:s', strtotime('+1 years')),
            'type'              => $type,
        ]);

        if ($price != '0') {
            $firstName         = $request->first_name;
            $lastName          = $request->last_name;
            $PaymentAmount     = $price;
            $CompanyName       = $request->company_name;
            $EmailAddress      = $request->email;
            $MobilePhoneNumber = $request->mobile_number;
            $paymentReference  = str_random(8);
            $redirect          = url('professionals-signup/getdata');

            $queryString = "FirstName=" . $firstName . "&LastName=" . $lastName . "&PaymentReference=" . $paymentReference . "&PaymentAmount=" . $PaymentAmount . "&EmailAddress=" . $EmailAddress . "&CompanyName=" . $CompanyName . "&RedirectURL=" . $redirect;

            return redirect::to('https://buy-sell-lease.pay.ezidebit.com.au/?' . $queryString);

        } else {
            $findProfessional                 = Professionals::where('user_id', Session::get('user_id_current'))->first();
            $findProfessional->payment_status = 1;
            $findProfessional->save();

            self::sendEmail(Session::get('user_id_current'));
            Session::forget('user_id_current');
            Flash::success('Signup successful. You will hear us from shortly');
            return redirect::to('professionals-signup');
        }

    }

    public function getAll()
    {
        $response = $_GET;

        if (!empty($response)) {
            if ($response['ResultCode'] == '0') {
                $intoJson             = (serialize($response));
                $transaction          = new Transactions;
                $transaction->user_id = Session::get('user_id_current');
                $transaction->details = $intoJson;

                $transaction->save();

                /*
                | now also update the professional payment status to 1
                 */
                $findProfessional                 = Professionals::where('user_id', Session::get('user_id_current'))->first();
                $findProfessional->payment_status = 1;
                $findProfessional->save();

                self::sendEmail(Session::get('user_id_current'));
                Session::forget('user_id_current');
                Flash::success('Signup successful. You will hear us from shortly');
                return redirect::to('professionals-signup');

            } else {
                //error message then handle it
                $findPro = Professionals::where('user_id', Session::get('user_id_current'))->first();
                $findPro->delete();
                $users = User::find(Session::get('user_id_current'));
                $users->delete();
                Flash::error('Signup Unsuccessful. There was problem processing your payment. Please Try later');
                return redirect::to('professionals-signup');
            }
            //Flash::error('Signup Unsuccessful. There was problem processing your payment. Please Try later');
            //return redirect::to('professionals-signup');

        }
    }

    public function sendEmail($id)
    {
        $request    = Professionals::where('user_id', $id)->first();
        $user_email = User::find($id);

        $signupText = DB::table('emailtemplates')
            ->where('type', '=', 'professionalregistration')
            ->first();

        $contents = htmlspecialchars_decode($signupText->text);

        $profession_id = $request->profession;

        $data = array('content' => $contents, 'email' => $user_email->email, 'name' => $request->firstname . ' ' . $request->lastname, 'business_name' => $request->business_name, 'business_type' => $profession_id);

        Mail::send('emails.professional-registration', $data, function ($message) use ($data) {

            $username = $data['name'];
            //$message->to(env('BSL_SALES_EMAIL'), env('BSL_SALES_NAME'));

            $message->to('accounts@buyselllease.com.au', 'Neaf');
            $message->bcc('bsiddhartha25@gmail.com', 'Sid');
            $message->bcc('erpushparaj23@gmail.com', 'Pusp');
            $message->subject('Professional Registration Submission - ' . $username);
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
        });

        $toMail = $user_email->email;
        $toName = $request->name;

        //\Mail::send('emails.professional-registration-user', $content, function ($message) use ($content, $toMail, $toName) {
        Mail::send('emails.professional-registration-user', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name']);
            $message->bcc('erpushparaj23@gmail.com', 'Pradeep');
            $message->subject('Professional Registration Successfully');
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
        });
    }

    public function sendEmailAgent($id)
    {
        $request      = User::find($id);
        $agent_detail = Agent::where('user_id', $id)->first();

        $signupText = DB::table('emailtemplates')
            ->where('type', '=', 'agentregistration')
            ->first();

        $contents = htmlspecialchars_decode($signupText->text);

        $data = array('content' => $contents, 'email' => $request->email, 'name' => $agent_detail->name, 'trading' => $agent_detail->trading, 'link' => $agent_detail->listing_base_url, 'staffURL' => $agent_detail->listing_staff_url, 'address' => $agent_detail->address, 'postcode' => $agent_detail->postcode, 'phone' => $agent_detail->phone, 'crm' => $agent_detail->import_id);

        Mail::send('emails.agent-registration', $data, function ($message) use ($data) {

            $username = $data['name'];
            //$message->to(env('BSL_SALES_EMAIL'), env('BSL_SALES_NAME'));

            $message->to('accounts@buyselllease.com.au', 'Neaf');
            $message->bcc('erpushparaj23@gmail.com', 'Puspa');
            $message->subject('Agent Registration Submission - ' . $username);
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
        });

        $toMail = $request->email;
        $toName = $request->name;

        //\Mail::send('emails.professional-registration-user', $content, function ($message) use ($content, $toMail, $toName) {
        Mail::send('emails.agent-registration-user', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name']);
            $message->bcc('erpushparaj23@gmail.com', 'Pradeep');
            $message->subject('Agent Registration Successfully');
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
        });
    }

    public function fetchCity()
    {
        $findRegion = $_GET['region'];
        $city       = City::where('region_id', $findRegion)->get();

        $select = "<select class='form-control' name='selectedcity' id='selectedcity'>";
        foreach ($city as $c) {
            $select .= "<option value='" . $c->id . "'>" . $c->name . "</option>";
        }

        $select .= "</select>";

        return \Response::json($select);
    }

}
