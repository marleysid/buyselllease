<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Hydrators\PropertyHydrator;
use App\Libraries\REA_XML;
use App\Models\Agent;
use App\Models\ImportPayload;
use App\Models\Professionals;
use App\Models\Property;
use App\Models\PropertyDailyUpdates;
use App\Models\PropertyData;
use DB;
use File;
use Files;
use Mail;
use Storage;
use ZipArchive;

class CronController extends Controller
{

    /**
     * This is  cron controller  that will be automated by system  on every  1 in  live
     */
    public function index()
    {
        $this->unzipfiles();

        $this->moveFileFromRootToIncoming();

        $this->importpayload();

        $this->processProperty();
        //self::improvedPropertiesStatus();

    }

    public function unzipfiles(){
       
        $dir    = base_path().'/e2ljbslfile/incoming/incoming';
        
        
        $files1 = array_diff(scandir($dir),array('..', '.'));
        
        //Count are there any files or not
        if(count($files1)>0){
            foreach ($files1 as $key => $value) {
                //check pathinfo
                $file_parts = pathinfo($value);
                
                //get extension
                if(array_key_exists('extension',$file_parts)){
                    if(strtolower($file_parts['extension']) == "zip"){
                        //create an instance of ZipArchive Class
                        $zip = new ZipArchive();
                        //open the file that you want to unzip.
                        //NOTE: give the correct path. In this example zip file is in the same folder
                        $zipped = $zip->open($dir."/".$value);
                        // get the absolute path to $file, where the files has to be unzipped
                        $path = pathinfo(realpath($dir), PATHINFO_DIRNAME);
                        
                        //check if it is actually a Zip file
                        if ($zipped) {
                         
                        //if yes then extract it to the said folder
                        $extract = $zip->extractTo($path);
                        //if unzipped succesfully then show the success message
                           
                        //close the zip
                        $zip->close();  
                        }
                        copy($dir."/".$value, $dir.'/backup/'.$value);
                        @unlink($dir."/".$value);
                    }
                }
            }
        }
    }
    /*
     * Check  if  there is any  file in   root of  webhoster folder
     * if  yes check if  it is xml file
     * then move  file to   incoming folder  for  further process
     * */

    public function moveFileFromRootToIncoming()
    {

        $agents = Agent::where('ftp_home_directory', '!=', '')->distinct()->get(['ftp_home_directory']);
        if (!empty($agents)) {

            $total_agents = count($agents);

            $excuted = 0;
            foreach ($agents as $agent) {
                $webhoster         = base_path() . $agent->ftp_home_directory;
                $incomingDirectory = base_path() . $agent->ftp_home_directory . '/incoming';
                if (!File::isDirectory($webhoster)) {
                    continue; // @todo implement alert system to track these failures
                    // throw new \Exception('Directory not found: [' . $incomingDirectory . ']');
                } else {
                    foreach (File::files($webhoster) as $xml) {
                        $extension = File::extension($xml);
                        if ($extension == 'xml') {
                            File::move($xml, $incomingDirectory . '/' . basename($xml));
                        }
                    }
                }

            }

        }

    }

    /**
     * this function  imports  exml files  send to our ftp  by webhosters and  validate it
     * if  tis is valid  it  moved to  the  processed  folder
     * and if it is  rejected  or  invalid it  moves to  he  rejected  folder and
     *  every   imporeted exml  details are save to   import_payload table in  db
     * @throws \Exception
     */
    public function importpayload()
    {
        $agents = Agent::where('ftp_home_directory', '!=', '')->distinct()->get(['ftp_home_directory']);

        if (!empty($agents)) {

            $total_agents = count($agents);

            $excuted = 0;
            foreach ($agents as $agent) {
                $incomingDirectory  = base_path() . $agent->ftp_home_directory . '/incoming';
                $rejectDirectory    = base_path() . $agent->ftp_home_directory . '/rejected';
                $processedDirectory = base_path() . $agent->ftp_home_directory . '/processed';
                $webhoster          = $agent->ftp_home_directory;

                if (!File::isDirectory($incomingDirectory)) {
                    continue; // @todo implement alert system to track these failures
                    // throw new \Exception('Directory not found: [' . $incomingDirectory . ']');
                }

                foreach (File::files($incomingDirectory) as $incomingFile) {

                    $payload     = File::get($incomingFile);
                    $f           = File::size($incomingFile);
                    $file_detail = pathinfo($incomingFile);
                    $filename    = $file_detail['filename'];
                    // verify file is properties list XML
                    if ($f <= 0) {

                        // your file is not empty
                        File::move($incomingFile, $rejectDirectory . '/' . basename($incomingFile));
                        continue;
                    } else {

                        $document = new \SimpleXMLElement($payload);

                        if ($document->getName() != 'propertyList') {
                            File::move($incomingFile, $rejectDirectory . '/' . basename($incomingFile));
                            throw new \Exception('File does not contain XML with propertyList root element');
                        }

                        // save file to db
                        ImportPayload::create(array(
                            'payload'   => $payload,
                            'filename'  => $filename,
                            'webhoster' => $webhoster,
                        ));

                        // move file to processed folder
                        File::move($incomingFile, $processedDirectory . '/' . basename($incomingFile));
                    }
                }

            }
        }

    }

    /**
     * This function is  responsible for  processing  the imported   xml file    saved in  import payload table
     *  it only  process those file  which not processed  previously
     * already processed  can be identified by  time  in processed field
     * mail will be send to admin   after  successful   process
     * @throws \Exception
     */
    public function processProperty()
    {
        foreach (ImportPayload::Unprocessed()->get() as $import_payload) {

            // @todo: I'm not happy with this library, look to replace it, prefer to access all value directly, not a subset!
            $rea_xml = new REA_XML();
            //dd($rea_xml);
            $xml_document = $rea_xml->parse_xml($import_payload->payload);

            $filename          = $import_payload->filename;
            $webhoster_initial = str_replace('/', '', $import_payload->webhoster);
            $webhoster         = strtoupper($webhoster_initial);
            if (empty($xml_document)) {
                //throw new \Exception('invalid xml provided');
                continue;
            }

            foreach ($xml_document as $type => $import_properties) {
                foreach ($import_properties as $import_property) {
                    // flatten xml for easier processing
                    $import_property = array_dot($import_property);
                    //dd($import_property);
                    $property = PropertyHydrator::hydrateFromREAXMLArray($type, $import_property);

                    if ($property == '') {
                        continue;
                    }
                    //  skip if   property is already sold(useful when   cron stop for some reasons and resumes after some days)
                    $check_sold = Property::where('property_code', $import_property['uniqueID'])->withTrashed()->first();
                    if (!empty($check_sold)) {
                        continue;
                    }

                    $property_test = Property::where('property_code', $import_property['uniqueID'])->first();
                    if (empty($property_test->property_code)) {
                        $propery_entry = 'NEW';
                    } else {
                        $propery_entry = 'UPDATED';
                    }
                    // save the property
                    if ($import_property['status'] == 'current') {
                        $property->save();
                    }

                    if (!empty($property->id)) {
                        // soft delete existing data the xml payload
                        PropertyData::where('property_id', '=', $property->id)->delete();
                        $property_data = new PropertyData(array(
                            'property_id'       => $property->id,
                            'import_payload_id' => $import_payload->id,
                        ));

                        $property_data->payload = json_encode($import_property);
                        $property_data->save();
                    }

                    $agent = Agent::find($import_property['agentID']);
                    if ($agent) {
                        $agent_location = $agent->suburb;
                    } else {
                        $agent_location = '';
                    }

                    //add to   daily update table for daily reporting
                    PropertyDailyUpdates::create([
                        'web_hoster'            => strtoupper(str_replace('/', '', $webhoster)),
                        'xmlfile'               => $filename,
                        'agent_id'              => $import_property['agentID'],
                        'property_id'           => $import_property['uniqueID'],
                        'source_updated'        => $import_property['modTime'],
                        'status'                => strtoupper($import_property['status']),
                        'entry_type'            => $propery_entry,
                        'agent_name'            => $import_property['listingAgent.name'],
                        'property_location'     => PropertyHydrator::buildAddress($import_property),
                        'agent_office_location' => $agent_location,

                    ]);

                    /* Mail::send('emails.propertyUpdate', ['property' => $property], function ($message) use ($property, $import_property) {
                $message->to(env('BSL_ADMIN_EMAIL'), env('BSL_ADMIN_NAME'))
                ->subject(($property->trashed() ? 'DELETED' : 'ADDED/UPDATED') . " - {$import_property['agentID']} - {$property->property_code}, " . ucfirst($property->property_type) . " in {$property->suburb}");
                //$message->bcc(explode(',', env('MONITOR_EMAIL')));
                $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
                });*/
                }
            }

            $import_payload->processed = date('Y-m-d H:i:s');
            $import_payload->save();
        }
    }

    /**
     *distinguish  into  new  , updated , status
     * create csv file for  each
     * use this function to  save csv file . convertToCSVAndSave in  storage file/csv  folder
     * flush  PropertyDailyUpdates
     * and  send   email to admin with  attaching three csv
     */
    public function createSendDailyCsvUpdates()
    {

        self::getSoldProperties();
        self::getCurrentProperties();
        self::getNewProperties();
        self::getUpdatedProperties();

        DB::reconnect();

        $truncate     = DB::table('property_daily_updates')->truncate();
        $csvDirectory = storage_path('csv') . '/' . date('Y-m-d');

        Mail::send('emails.dailyUpdate', [], function ($message) use ($csvDirectory) {
            $message->to(env('BSL_ADMIN_EMAIL'), env('BSL_ADMIN_NAME'))
                ->subject("Daily Property Update");
            $message->bcc('psquaretop@gmail.com', 'pradeep');
            $message->bcc('puspa@ebpearls.com', 'Naresh');
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            foreach (File::files($csvDirectory) as $csvfile) {
                $message->attach($csvfile);
            }

        });

    }

    public static function getSoldProperties()
    {
        DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('status', '!=', 'current')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster', 'xmlfile', 'agent_id', 'agent_name', 'agent_office_location', 'property_id', 'property_location', 'source_updated', 'entry_type', 'status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ', 'Agent Name', 'Agent Office Location', 'Property ID', 'Property Location', 'Source Updated Date', 'NEW/UPDATED', 'Status');
        // building the options array
        $options = array(
            'columns'  => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'SOLD_' . date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options, $filename, date('Y-m-d'));
    }

    public static function getCurrentProperties()
    {
        DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('status', 'current')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster', 'xmlfile', 'agent_id', 'agent_name', 'agent_office_location', 'property_id', 'property_location', 'source_updated', 'entry_type', 'status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ', 'Agent Name', 'Agent Office Location', 'Property ID', 'Property Location', 'Source Updated Date', 'NEW/UPDATED', 'Status');
        // building the options array
        $options = array(
            'columns'  => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'CURRENT_' . date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options, $filename, date('Y-m-d'));
    }

    public static function getNewProperties()
    {
        DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('entry_type', 'NEW')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster', 'xmlfile', 'agent_id', 'agent_name', 'agent_office_location', 'property_id', 'property_location', 'source_updated', 'entry_type', 'status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ', 'Agent Name', 'Agent Office Location', 'Property ID', 'Property Location', 'Source Updated Date', 'NEW/UPDATED', 'Status');
        // building the options array
        $options = array(
            'columns'  => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'NEW_' . date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options, $filename, date('Y-m-d'));
    }

    public static function getUpdatedProperties()
    {
        DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('entry_type', 'UPDATED')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster', 'xmlfile', 'agent_id', 'agent_name', 'agent_office_location', 'property_id', 'property_location', 'source_updated', 'entry_type', 'status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ', 'Agent Name', 'Agent Office Location', 'Property ID', 'Property Location', 'Source Updated Date', 'NEW/UPDATED', 'Status');
        // building the options array
        $options = array(
            'columns'  => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'UPDATED_' . date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options, $filename, date('Y-m-d'));
    }
    public static function improvedPropertiesStatus()
    {

        $currentProperties = Property::get();
        if (!empty($currentProperties)) {
            foreach ($currentProperties as $properties) {

                $check_sold = Property::where('property_code', $properties->property_code)->withTrashed()->first();

                if (!empty($check_sold)) {
                    Property::where('property_code', '=', $properties->property_code)->where('id', '!=', $check_sold->id)->delete();
                }

            }
        }
    }

    /*
    | sending a reminder email to the professional whose subscriptions are about to end
     */
    public function sendReminder()
    {
        $professionals = \DB::table('professionals')->get();
        $profes        = [];
        foreach ($professionals as $pro) {
            $exp   = $pro->expired_at;
            $cre   = $pro->created_at;
            $date1 = date_create($exp);
            $date2 = date_create($cre);
            $diff  = date_diff($date2, $date1);

            if ($diff->days <= 3 && $diff->days > 1) {
                array_push($profes, $pro->id);
            } else if ($diff->days = 0) {
                $profe                 = Professionals::find($pro->id);
                $profe->payment_status = 0;
                $profe->save();
            }

        }
        $this->sendMail($profes);
    }

    public function sendMail($profes)
    {

        $findAll = Professionals::whereIn('id', $profes)->get();

        foreach ($findAll as $all) {

            $firstName        = $all->firstname;
            $lastName         = $all->lastname;
            $PaymentAmount    = '2200.00';
            $CompanyName      = $all->company_name;
            $paymentReference = str_random(8);
            $redirect         = url('professionalrenew/getdata/' . $all->id);

            $queryString = "FirstName=" . $firstName . "&LastName=" . $lastName . "&PaymentReference=" . $paymentReference . "&PaymentAmount=" . $PaymentAmount . "&CompanyName=" . $CompanyName . "&RedirectURL=" . $redirect;

            $link = 'https://buy-sell-lease.pay.demo.ezidebit.com.au/?' . $queryString;

            $data = ['email' => $all->user->email, 'string' => $queryString, 'name' => $all->firstname, 'link' => $link];
            Mail::send('emails.reminder', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->bcc('bsiddhartha25@gmail.com', 'sid');
                $message->bcc('accounts@buyselllease.com.au', 'Neaf');
                $message->subject('Subscription renew reminder');
                $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            });
        }
    }

    public function updateData($id)
    {
        if ($_GET['ResultCode'] == 0) {

            $renew = Professionals::find($id);

            $date1 = date_create($renew->expired_at);
            $date2 = date_create($renew->created_at);
            $diff  = date_diff($date2, $date1);

            if ($diff->days > 4){
                \Flash::error('Opps!!! some error occured');
                return \Redirect::to('professionals-signup');
            }

            //add the date by one year
            $renew             = Professionals::find($id);
            $currDate          = date('Y-m-d H:i:s');
            $afterOneYear      = date(strtotime('+1 year'), strtotime($currDate));
            $renew->expired_at = date('Y-m-d H:i:s', $afterOneYear);
            $renew->save();

            \Flash::success('Professional renewed successfully');
            return \Redirect::to('professionals-signup');
        }
    }

}
