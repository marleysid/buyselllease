<?php namespace App\Http\Controllers;

use GCH;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfessionalRequest;
use App\Models\Professionals;
use Illuminate\Support\Facades\Request;
use SH;
use App\Models\Transactions;
use App\Models\User;
use DB;
use Flash;
use Crypt;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Models\Paypal;

//custome add
use PayPal\Api\Amount;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Presentation;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\WebProfile;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;





class PaypalController extends Controller
{
    private $_api_context;

    public function __construct()
    {


        $paypal_details = Paypal::First();

        if ($paypal_details) {
            $client_id = Crypt::decrypt($paypal_details->client_id);
            $secret_id = Crypt::decrypt($paypal_details->secret_id);
            $mode = $paypal_details->mode;
        } else {
            Flash::error('Some error occur, sorry for inconvenient');
            return redirect::back();
        }


        $paypal_conf = \Config::get('paypal');
        if ($paypal_details) {
            $paypal_conf['client_id'] = $client_id;
            $paypal_conf['secret'] = $secret_id;
            $paypal_conf['settings']['mode'] = $mode;

        }
// setup PayPal api context
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));

        $this->_api_context->setConfig($paypal_conf['settings']);


    }

    /**
     *
     *
     * @return Response
     */
    public function professionalSignup()
    {

        $professions = SH::getCategoryOptions(true);
        return view('signup.form', compact('professions'));
    }

    public function postPaymentPaypal(ProfessionalRequest $request)
    {

        $promo = Input::get('promocode');

        if ($promo != "") {
            $findCode = DB::table('promotionalcode')
                ->where('code', '=', $promo)
                ->first();
            if (is_null($findCode)) {
                Flash::error('The entered promotional code is invalid');
                return redirect::back();

            } else {
                $promoType = $findCode->type;
            }


            if ($promoType = 'discount') {
                $discountPercentage = $findCode->percentage;
            } elseif ($promoType = 'free') {
                $discountPercentage = 100;
            } else {
                $discountPercentage = 0;
            }
        } else {
            $discountPercentage = 0;
            $promo = NULL;
        }

        if ($discountPercentage == '100') {
            self::SaveUserAndMail($request->all());
            Flash::success('Signup successful, You will hear from us shortly.');
            return Redirect::back();
        }

        $initial = 2000 - (($discountPercentage / 100) * 2000);
        $stingAmt = number_format(floatval($initial), 2, '.', '');
        $percent = (0.1 * $initial);
        $finalPercent = number_format(floatval($percent), 2);
        $amount_total = $stingAmt + $finalPercent;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $item_1->setName('Annual Marketing Subscription')// item name
        ->setCurrency('AUD')
            ->setQuantity(1)
            ->setPrice($stingAmt); // unit price

        $item_2 = new Item();
        $item_2->setName('10% GST')
            ->setCurrency('AUD')
            ->setQuantity(1)
            ->setPrice($finalPercent);
        // add item to list

        $item_list = new ItemList();
        $item_list->setItems(array($item_1, $item_2));
        $amount = new Amount();
        $amount->setCurrency('AUD')
            ->setTotal($amount_total);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('payment.status'))// Specify return URL
        ->setCancelUrl(\URL::route('payment.status'));

        $presentation = new Presentation;
        $presentation->setLogoImage(asset('img/logo-sm.png'))
            //	A label that overrides the business name in the PayPal account on the PayPal pages.
            ->setBrandName("Paypal! Paypal");
        //  Locale of pages displayed by PayPal payment experience.
        $inputFields = new InputFields();
        // Enables the buyer to enter a note to the merchant on the PayPal page during checkout.
        $inputFields->setAllowNote(false)
            // Determines whether or not PayPal displays shipping address fields on the experience pages. Allowed values: 0, 1, or 2. When set to 0, PayPal displays the shipping address on the PayPal pages. When set to 1, PayPal does not display shipping address fields whatsoever. When set to 2, if you do not pass the shipping address, PayPal obtains it from the buyer’s account profile. For digital goods, this field is required, and you must set it to 1.
            ->setNoShipping(1)
            // Determines whether or not the PayPal pages should display the shipping address and not the shipping address on file with PayPal for this buyer. Displaying the PayPal street address on file does not allow the buyer to edit that address. Allowed values: 0 or 1. When set to 0, the PayPal pages should not display the shipping address. When set to 1, the PayPal pages should display the shipping address.
            ->setAddressOverride(0);

        $webProfile = new WebProfile();
        $webProfile->setName("Buyselllease.com.au" . uniqid())
            // Parameters for style and presentation.
            ->setPresentation($presentation)
            // Parameters for input field customization.
            ->setInputFields($inputFields);


        try {
            $createProfileResponse = $webProfile->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException  $ex) {

            if (\Config::get('app.debug')) {
                // echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);

                Flash::error('Signup Unsuccessful. Some error occur, sorry for inconvenient.Please try again after some time');
                return redirect::back()->withInput();
            } else {
                //die('Some error occur, sorry for inconvenient');
                Flash::error('Signup Unsuccessful. Some error occur, sorry for inconvenient.Please try again after some time');
                return redirect::back()->withInput();
            }
        }

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction))
            ->setExperienceProfileId($createProfileResponse->getId());


        try {
            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PayPalConnectionException  $ex) {

            if (\Config::get('app.debug')) {
                // echo "Exception: " . $ex->getMessage() . PHP_EOL;
                //$err_data = json_decode($ex->getData(), true);

                Flash::error('Signup Unsuccessful. Some error occur, sorry for inconvenient.Please try again after some time');
                return redirect::back()->withInput();
            } else {
                //die('Some error occur, sorry for inconvenient');
                Flash::error('Signup Unsuccessful. Some error occur, sorry for inconvenient.Please try again after some time');
                return redirect::back()->withInput();
            }
        }


        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        // add payment ID to session

        Session::put('paypal_payment_id', $payment->getId());

        Session::put('signupDetails', $request->all());
        if (isset($redirect_url)) {
            // redirect to paypal
            return Redirect::away($redirect_url);
        }
        Flash::error('Unknown error occurred');
        return Redirect::route('professionals-signup');

    }

    public function getPaymentStatus()
    {
        // Get the payment ID before session clear
        $payment_id = \Input::get('paymentId');
        $request = Session::get('signupDetails');


        // clear the session payment ID
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            Flash::error('Payment failed');
            return Redirect::route('professionals-signup');

        }
        $payment = Payment::get($payment_id, $this->_api_context);
        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site

        $execution = new PaymentExecution();
        $execution->setPayerId(\Input::get('PayerID'));
        //Execute the payment
        $result = $payment->execute($execution, $this->_api_context);
        // echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
        if ($result->getState() == 'approved') {
            // payment made
            self::SaveUserAndMail($request);
            self::SavePaymentDetails($payment_id, $this->_api_context);
            Flash::success('Payment and Signup successful, You will hear from us shortly.');
            return Redirect::route('professionals-signup');

        }
        Flash::error('Payment failed');
        return Redirect::route('professionals-signup');

    }

    public function SaveUserAndMail($request)
    {
        $request = (object)($request);
        $geomety = GCH::geolocate_address($request->suburb);
        $lat = $geomety['lat'];
        $lng = $geomety['lng'];

        $user = User::create([
            'name' => $request->user_name,
            'email' => $request->email,
            'password' => \Hash::make($request->password),
            'role' => env('PROFESSIONAL_USER', '3'),
            'active' => '0',
        ]);

        Session::put('user_id_current', $user->id);

        if (isset($request->my_uploader_id_files)) {
            $image = $request->my_uploader_id_files[0];
        } else {
            $image = NULL;
        }

        $professional = Professionals::create([
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'user_id' => $user->id,
            'profession' => $request->category,
            'suburb' => $request->suburb,
            'url' => $request->link,
            'contact' => $request->work_phone_number,
            'logo' => $image,
            'description' => $request->description,
            'lat' => $lat,
            'lng' => $lng,
            'street_address' => $request->street_address,
            'business_name' => $request->business_name,
            'company_name' => $request->company_name,
            'mobile_no' => $request->mobile_number,
            'business_position' => $request->business_position,
            'payment_status' => 1,
            'expired_at' => date('Y-m-d H:i:s', strtotime('+1 years')),
        ]);

        $signupText = DB::table('emailtemplates')
            ->where('type', '=', 'professionalregistration')
            ->first();
        $contents = $signupText->text;

        // Mail  to  admin result
        \Mail::send('emails.professional-registration', (array)$request, function ($message) use ($request) {

            $username = $request->user_name;
            $message->to(env('BSL_SALES_EMAIL'), env('BSL_SALES_NAME'));
            $message->bcc('psquaretop@gmail.com', 'Pradeep');
            $message->subject('Professional Registration Submission - ' . $username);
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
        });

        $toMail = $request->email;
        $toName = $request->first_name;
        //mail to  user
        $content = [
            'content' => $contents,
            'toName' => $toName,
        ];

        \Mail::send('emails.professional-registration-user', $content, function ($message) use ($content, $toMail, $toName) {
            //
            $message->to($toMail, $toName);
            $message->bcc('psquaretop@gmail.com', 'Pradeep');
            $message->subject('Professional Registration Successfully');
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
        });
    }

    public function SavePaymentDetails($payment_id, $apicontext)
    {
        $user_id = Session::get('user_id_current');
        Session::forget('user_id_current');
        $payment_details = Payment::get($payment_id, $apicontext);
        Transactions::create([
            'user_id' => $user_id,
            'details' => serialize($payment_details),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }





}
