<?php namespace App\Http\Controllers;

use App\Models\Profession;
use App\Models\Professionals;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use SH;
use Illuminate\Http\Request;
use View;
use Session;
use App\Models\User;
use App\Repositories\ProfessionalRepository;
use App\Models\SearchProfile;

use Flash;

/**
 * This controller handles calls which are used to populate and manipulate the search criteria being entered
 *
 * Class SearchController
 * @package App\Http\Controllers
 */
class SearchController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     */
	public function getBuystep1(Request $request)
	{
        SH::clearSearchProfile();
        $viewData = [];
        SH::saveRequestToSessionSearchProfile($request);

        // @todo put this into an object -> assoc. array could contain anything
        $viewData['transition_params'] = [
            'totalSteps'    => 2,
            'currentStep'   => 1,
            'prevStep'      => 'start',
            'nextStep'      => 'buystep2',
            'isFinal'       => false,
        ];
        $viewData['residentialActionOptions'] = SH::getBuyStep1ResidentialActionOptions();
        $viewData['commercialActionOptions'] = SH::getBuyStep1CommercialActionOptions();
        $viewData['categoryOptions'] = SH::getCategoryOptions();

        return View::make('search.partials.buy-1', $viewData);
	}

    /**
     * @param Request $request
     * @return mixed
     */
    public function getBuystep2(Request $request)
    {
        $viewData = [];


        $validator = Validator::make($request->all(), [
            'residential-action'    => 'required_without_all:commercial-action,category-action',
            'commercial-action'     => 'required_without_all:residential-action,category-action',
            'category-action'     => 'required_without_all:commercial-action,residential-action',
            'suburb'                => 'required_with:residential-action,commercial-action',
        ], [
            'residential-action.required_without_all'   => 'Please choose a Residential property description.',
            'commercial-action.required_without_all'    => 'Please choose a Commercial property description.',
            'category-action.required_without_all'    => 'Please choose a Category property description.',
            'suburb.required_with'                       => 'Please enter a suburb to search.'
        ]);



        if($validator->passes()) {
            $mode = SH::getStep2Mode($request);
            SH::saveArrayToSessionSearchProfile(['mode' => $mode]);

            SH::saveRequestToSessionSearchProfile($request);

            // @todo put this into an object -> assoc. array could contain anything
            $viewData['transition_params'] = [
                'totalSteps' => 2,
                'currentStep' => 2,
                'prevStep' => 'buystep1',
                'nextStep' => 'search',
                'isFinal' => false,
            ];

            $viewData['mode'] = $mode;

            return View::make('search.partials.buy-2', $viewData)->withErrors($validator->messages());
        }

        // Step 1 validation failed, reload Step one with errors and previous input
        $request->flash();

        // @todo put this into an object -> assoc. array could contain anything
        $viewData['transition_params'] = [
            'totalSteps'    => 2,
            'currentStep'   => 1,
            'prevStep'      => 'start',
            'nextStep'      => 'buystep2',
            'isFinal'       => false,
        ];
        $viewData['residentialActionOptions'] = SH::getBuyStep1ResidentialActionOptions();
        $viewData['commercialActionOptions'] = SH::getBuyStep1CommercialActionOptions();
        $viewData['categoryOptions'] = SH::getCategoryOptions();

        return View::make('search.partials.buy-1', $viewData)->withErrors($validator->messages());
    }

//    /**
//     * @param Request $request
//     * @return mixed
//     */
//    public function getBuystep3(Request $request)
//    {
//        $viewData = [];
//
//        SH::saveRequestToSessionSearchProfile($request);
//
//        $viewData['transition_params'] = [
//            'totalSteps'    => 3,
//            'currentStep'   => 3,
//            'nextStep'      => 'search',
//            'isFinal'       => false,
//        ];
//
//        return View::make('search.partials.buy-3', $viewData);
//    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getSellstep1(Request $request)
    {
        SH::clearSearchProfile();
        $viewData = [];

        SH::saveRequestToSessionSearchProfile($request);

        // @todo put this into an object -> assoc. array could contain anything
        $viewData['transition_params'] = [
            'totalSteps'    => 2,
            'currentStep'   => 1,
            'prevStep'      => 'start',
            'nextStep'      => 'sellstep2',
            'isFinal'       => false,
        ];
        $viewData['actionOptions'] = SH::getSellStep1ActionOptions();
        $viewData['categoryOptions'] = SH::getCategoryOptions(); //only add if needed

        return View::make('search.partials.sell-1', $viewData);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getSellstep2(Request $request)
    {
        $viewData = [];

        $validator = Validator::make($request->all(), [
            'action' => 'required_without_all:category-action',
            'category-action'     => 'required_without_all:action',
            'suburb' => 'required_with_all:action',
        ]);



        if($validator->passes()) {

            SH::saveRequestToSessionSearchProfile($request);

            // @todo put this into an object -> assoc. array could contain anything
            $viewData['transition_params'] = [
                'totalSteps'    => 2,
                'currentStep'   => 2,
                'prevStep'      => 'sellstep1',
                'nextStep'      => 'search',
                'isFinal'       => true,
            ];

            $viewData['mode'] = SH::getSellStep2Mode($request->get('action'));
            $viewData['typeOptions'] = SH::getSellStep2TypeOptions($viewData['mode']);
            $viewData['categoryOptions'] = SH::getCategoryOptions(); //only add if needed
            if ($request->has('action')) {
                return View::make('search.partials.sell-2', $viewData)->withErrors($validator->messages());
            } else {
                SH::saveRequestToSessionSearchProfile($request);

                return '<h2>Loading</h2>';
            }

        }

        // Step 1 validation failed, reload Step one with errors and previous input
        $request->flash();

        // @todo put this into an object -> assoc. array could contain anything
        $viewData['transition_params'] = [
            'totalSteps'    => 2,
            'currentStep'   => 1,
            'prevStep'      => 'start',
            'nextStep'      => 'sellstep2',
            'isFinal'       => false,
        ];
        $viewData['actionOptions'] = SH::getSellStep1ActionOptions();
        $viewData['categoryOptions'] = SH::getCategoryOptions(); //only add if needed

        return View::make('search.partials.sell-1', $viewData)->withErrors($validator->messages());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getStart(Request $request)
    {
        return View::make('search.partials.start', []);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getLeasestep1(Request $request)
    {
        SH::clearSearchProfile();
        $viewData = [];
        SH::saveRequestToSessionSearchProfile($request);

        // @todo put this into an object -> assoc. array could contain anything
        $viewData['transition_params'] = [
            'totalSteps'    => 2,
            'currentStep'   => 1,
            'prevStep'      => 'start',
            'nextStep'      => 'leasestep2',
            'isFinal'       => false,
        ];
        $viewData['actionOptions'] = SH::getLandlordLeaseStep1ActionOptions();

        $viewData['residentialActionOptions'] = SH::getLeaseStep1ResidentialActionOptions();
        $viewData['commercialActionOptions'] = SH::getLeaseStep1CommercialActionOptions();
        $viewData['categoryOptions'] = SH::getCategoryOptions();

        return View::make('search.partials.lease-1', $viewData);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getLeasestep2(Request $request)
    {
        $viewData = [];


        if($request->tenant =='landlord') {
            $validator = Validator::make($request->all(),[
                'action' => 'required_without_all:category-action',
                'category-action' => 'required_without_all:action',
                'suburb' => 'required_with:residential-action,commercial-action',
            ],[
                'action.required_without_all' => 'Please choose a property type.',
                'category-action.required_without_all' => 'Please choose a Category property description.',
                'suburb.required_with' => 'Please enter a suburb to search.'
            ]);
        } else {

            $validator = Validator::make($request->all(), [
                'residential-action' => 'required_without_all:commercial-action,category-action',
                'commercial-action' => 'required_without_all:residential-action,category-action',
                'category-action' => 'required_without_all:commercial-action,residential-action',
                'suburb' => 'required_with:residential-action,commercial-action',
            ], [
                'residential-action.required_without_all' => 'Please choose a Residential property description.',
                'commercial-action.required_without_all' => 'Please choose a Commercial property description.',
                'category-action.required_without_all' => 'Please choose a Category property description.',
                'suburb.required_with' => 'Please enter a suburb to search.'
            ]);
        }

        if($validator->passes()) {
            $mode = SH::getStep2Mode($request);

            SH::saveArrayToSessionSearchProfile(['mode' => $mode]);

            SH::saveRequestToSessionSearchProfile($request);

            // @todo put this into an object -> assoc. array could contain anything
            if ($request->tenant == 'landlord'){
                $viewData['transition_params'] = [
                    'totalSteps'    => 2,
                    'currentStep'   => 2,
                    'prevStep'      => 'leasestep1',
                    'nextStep'      => 'search',
                    'isFinal'       => true,
                ];
                $viewData['mode'] = SH::getSellStep2Mode($request->get('action'));

                $viewData['typeOptions'] = SH::getSellStep2TypeOptions($viewData['mode']);
                $viewData['categoryOptions'] = SH::getCategoryOptions(); //only add if needed

                return View::make('search.partials.sell-2', $viewData)->withErrors($validator->messages());
        } else {
                $viewData['transition_params'] = [
                    'totalSteps' => 2,
                    'currentStep' => 2,
                    'prevStep' => 'leasestep1',
                    'nextStep' => 'search',
                    'isFinal' => false,
                ];


                $viewData['mode'] = $mode;

                return View::make('search.partials.lease-2', $viewData)->withErrors($validator->messages());
            }
        }

        // Step 1 validation failed, reload Step one with errors and previous input
        $request->flash();

        // @todo put this into an object -> assoc. array could contain anything
        $viewData['transition_params'] = [
            'totalSteps'    => 2,
            'currentStep'   => 1,
            'prevStep'      => 'start',
            'nextStep'      => 'leasestep2',
            'isFinal'       => false,
        ];
        $viewData['actionOptions'] = SH::getLandlordLeaseStep1ActionOptions();
        $viewData['residentialActionOptions'] = SH::getLeaseStep1ResidentialActionOptions();
        $viewData['commercialActionOptions'] = SH::getLeaseStep1CommercialActionOptions();
        $viewData['categoryOptions'] = SH::getCategoryOptions();

        return View::make('search.partials.lease-1', $viewData)->withErrors($validator->messages());
    }

    public function getSearch(Request $request)
    {

        SH::saveRequestToSessionSearchProfile($request);

        return '<h2><input type="hidden" id="only_professional" value="'.$request->only_professional.'"/>Loading</h2>';
    }


    public function getMoreFields(Request $request)
    {
        if ($request->ajax()) {
            $search_type = $request->agreement_type;

            if (\Auth::user()) {

                $searchProfile = SearchProfile::where(array('user_id' => \Auth::user()->id))->first();

            } else {
                $searchProfile = SearchProfile::where(array('ip' => $request->getClientIp()))->first();

            }


            $searchProfileData = json_decode($searchProfile->profile, true);
            $mode = isset($searchProfileData['commercial-action']) ? 'commercial' : 'residential';

            switch ($search_type) {
                case 'buy':
                    $residentialActionOptions = SH::getBuyStep1ResidentialActionOptions();
                    $commercialActionOptions = SH::getBuyStep1CommercialActionOptions();
                    $categoryOptions = SH::getCategoryOptions();
                    return view::make('search.detailsearch.buy-search-more', compact('searchProfileData', 'residentialActionOptions', 'commercialActionOptions', 'categoryOptions', 'mode'));
                    break;

                case 'sell':
                    $actionOptions = SH::getSellStep1ActionOptions();
                    $categoryOptions = SH::getCategoryOptions(); //only add if needed
                    return view::make('search.detailsearch.sell-search-more', compact('searchProfileData', 'actionOptions', 'categoryOptions', 'mode'));
                    break;

                case 'lease':
                    $residentialActionOptions = SH::getBuyStep1ResidentialActionOptions();
                    $commercialActionOptions = SH::getBuyStep1CommercialActionOptions();
                    $categoryOptions = SH::getCategoryOptions();
                    return view::make('search.detailsearch.lease-search-more', compact('searchProfileData', 'residentialActionOptions', 'commercialActionOptions', 'categoryOptions', 'mode'));
                    break;

            }

        }
    }


    public function getMoreSellSearchFields(Request $request){
        if ($request->ajax()) {
            if (\Auth::user()) {

                $searchProfile = SearchProfile::where(array('user_id' => \Auth::user()->id))->first();

            } else {
                $searchProfile = SearchProfile::where(array('ip' => $request->getClientIp()))->first();

            }

            $searchProfileData = json_decode($searchProfile->profile, true);

            $property_type = $request->ptype;
            $mode = \SH::getSellStep2Mode($property_type);
            $typeOptions = \SH::getSellStep2TypeOptions($mode);

            return view::make('search.detailsearch.sell-search-more-options', compact('typeOptions','mode','searchProfileData'));


        }
    }


    public function getTenantSearchOptions(Request $request){
        if ($request->ajax()) {

            $tenant_landlord = $request->tenant_landlrd;

            if (\Auth::user()) {

                $searchProfile = SearchProfile::where(array('user_id' => \Auth::user()->id))->first();

            } else {
                $searchProfile = SearchProfile::where(array('ip' => $request->getClientIp()))->first();

            }


            $searchProfileData = json_decode($searchProfile->profile, true);
            $mode = isset($searchProfileData['commercial-action']) ? 'commercial' : 'residential';
            switch ($tenant_landlord){
                case 'tenant':
                    $residentialActionOptions = SH::getBuyStep1ResidentialActionOptions();
                    $commercialActionOptions = SH::getBuyStep1CommercialActionOptions();
                    return view::make('search.detailsearch.lease-tenant-search-options', compact('searchProfileData', 'residentialActionOptions', 'commercialActionOptions', 'mode'));
                    break;
                case 'landlord':
                    $actionOptions = SH::getLandlordLeaseStep1ActionOptions();
                    return view::make('search.detailsearch.lease-landlord-search-options', compact('searchProfileData', 'actionOptions',  'mode'));
                    break;

            }

        }
    }


        public function advanceSearch(Request $request) {


            SH::clearSearchProfile();
            SH::saveRequestToSessionSearchProfile($request);
            $sessionSearchProfile = SH::getSearchProfileFromSession();

            $is_empty_searchprofile=json_decode($sessionSearchProfile,true);

            if ( empty($is_empty_searchprofile)) {
                Flash::error('You have not  provided  sufficient  filters for  property search.');
                return  redirect('/');
            }




            if (\Auth::user()) {

                $searchProfile = SearchProfile::firstOrNew(array('user_id' => \Auth::user()->id));
                $searchProfile->user_id = \Auth::user()->id;
                $searchProfile->profile = $sessionSearchProfile;
                $searchProfile->save();

            } else {
                $searchProfile = SearchProfile::firstOrNew(array('ip' => $request->getClientIp()));
                $searchProfile->user_id = 0;
                $searchProfile->ip = $request->getClientIp();
                $searchProfile->profile = $sessionSearchProfile;
                $searchProfile->save();

            }


           return \Redirect::to('dashboard/search');
        }






}

