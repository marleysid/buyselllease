<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Image;
use Input;

class MediaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function upload()
	{

		return \Plupload::file('file', function($file)
		{
			// Store the uploaded file
			$file->move(public_path('uploads/professionals'), time().$file->getClientOriginalName());

			// This will be included in JSON response result
			return [
				'success' => true,
				'message' => 'Upload successful.',
				'id' => time().$file->getClientOriginalName(),
				'url' => asset('uploads/professionals').'/'.time().$file->getClientOriginalName(),
				'deleteUrl' => url('deleteImage').'/'.time().$file->getClientOriginalName()
			];
		});





	}


	public function uploadAgentLogo()
	{

		return \Plupload::file('file', function($file)
		{
			// Store the uploaded file
			$file->move(public_path('uploads/agents/logo'), time().$file->getClientOriginalName());

			// This will be included in JSON response result
			return [
				'success' => true,
				'message' => 'Upload successful.',
				'id' => time().$file->getClientOriginalName(),
				'url' => asset('uploads/agents/logo').'/'.time().$file->getClientOriginalName(),
				'deleteUrl' => url('deleteImageAgentLogo').'/'.time().$file->getClientOriginalName()
			];
		});





	}


	public function uploadAgentProfile()
	{

		return \Plupload::file('file', function($file)
		{
			// Store the uploaded file
			$file->move(public_path('uploads/agents/profile'), time().$file->getClientOriginalName());

			// This will be included in JSON response result
			return [
				'success' => true,
				'message' => 'Upload successful.',
				'id' => time().$file->getClientOriginalName(),
				'url' => asset('uploads/agents/profile').'/'.time().$file->getClientOriginalName(),
				'deleteUrl' => url('deleteImageAgentprofile').'/'.time().$file->getClientOriginalName()
			];
		});





	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function deleteImage($image)
	{
		@unlink(public_path('uploads/professionals').$image);
		return [
			'success' => true,
			'message' => 'image removed'
			];
	}


	public function deleteImageAgentLogo($image)
	{
		@unlink(public_path('uploads/agents/logo').$image);
		return [
			'success' => true,
			'message' => 'image removed'
		];
	}


	public function deleteImageAgentprofile($image)
	{
		@unlink(public_path('uploads/agents/profile').$image);
		return [
			'success' => true,
			'message' => 'image removed'
		];
	}




}
