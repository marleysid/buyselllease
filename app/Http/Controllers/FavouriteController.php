<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Favourite;
use App\Repositories\PropertyRepository;
use Input;

class FavouriteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id='')
	{

		if($id != '')
			$user_id = $id;
		else
			$user_id = \Auth::user()->id;
		$favouriteproperties = PropertyRepository::findAllFavouriteProperties($user_id);

		if (!Input::get('sort')) {
			$favouriteproperties->shuffle();
		}

		$favouriteproperties->setPath('dashboard/favourites');

		return view('dashboard.favourite', compact('favouriteproperties'));

	}


	public function  getMapFavourite($id=''){

		if($id != '')
			$user_id = $id;
		else
			$user_id = \Auth::user()->id;

			$properties = PropertyRepository::findAllByFavouriteMap($user_id);

			$map_data = [];
			if (!empty($properties)) {
				foreach ($properties as $property) {
					$prof_data = [];
					$prof_data['lat'] = $property->lat;
					$prof_data['lng'] = $property->lng;
					$prof_data['id'] = $property->id;
					array_push($map_data, $prof_data);
				}
			}

			return json_encode(['map_data' => $map_data]);
		}




	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addFavourite($property_code)
	{

		$favourite = Favourite::create(['user_id'=>\Auth::user()->id,'property_id'=>$property_code,'interaction'=>'favourite property']);
		if($favourite){
			return \Response::make(['status'=>'success','msg'=>'Successfully  added to favourite list']);
		} else {
			return \Response::make(['status'=>'success','msg'=>'Sorry Something went wrong']);
		}
	}


	public function removeFavourite($property_code)
	{
		//$favourite = Favourite::where(['user_id'=>\Auth::user()->id,'property_id'=>$property_code])->first();

		$delete = \DB::table('favouriteproperties')->where('user_id', '=', \Auth::user()->id)->where('property_id','=',$property_code)->delete();

		if($delete){
			return \Response::make(['status'=>'success','msg'=>'Successfully  removed to favourite list']);
		} else {
			return \Response::make(['status'=>'success','msg'=>'Sorry Something went wrong']);
		}
	}

}
