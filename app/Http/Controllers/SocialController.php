<?php namespace App\Http\Controllers;

use App\Hydrators\UserHydrator;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Auth;
use App\Models\User;
use DB;
use URL;
use Redirect;

class SocialController extends Controller {

    /**
     *
     */
    public function __construct()
    {

    }

    public function facebookLoginRedirect()
    {
        // authenticate user
        return Socialite::with('facebook')->redirect();

    }

    public function facebookLoginCallback(Request $request)
    {
        /**
         * If permission denied
         */
        if($request->has('error')) {

            return  Redirect::to('/auth/login')->withErrors('Unable to establish permission from Facebook! Please try again.');
        }

        $userData = Socialite::with('facebook')->user();
        if(!property_exists($userData, 'id')) {
            throw new Exception('Facebook return id not found.');
        }

        // Retrieve existing user and update
        $user = User::where('email', $userData->email)->first();
        if ($user){
            try {
                DB::beginTransaction();
                DB::table('users')
                    ->where('email', '=', $user->email)
                    ->update(['facebook_id' => $userData->id]);
                DB::commit();
            } catch(\PDOException $e) {
                DB::rollback();
                return Redirect::back()
                    ->with('error', 'Opps!! There was problem processing request. Please try later');
            }
        } else {
            $user = UserRepository::findByFacebookIdorNew($userData->id);
            UserHydrator::hydrateFromSocialiteTwoFacebookUser($user, $userData);
        }

        $user->save();

        // Login and redirect
        Auth::loginUsingId($user->id);

        return Redirect::intended('/');
    }

    public function googlePlusLoginRedirect()
    {
        // authenticate user
        return Socialite::with('google')->redirect();

    }

    public function googlePlusLoginCallback(Request $request)
    {
        /**
         * If permission denied
         */
        if($request->has('error')) {
            return Redirect::to('/auth/login')->withErrors('Unable to establish permission from Google Plus! Please try again.');
        }

        $userData = Socialite::with('google')->user();
        if(!property_exists($userData, 'id')) {
            throw new Exception('Google Plus return id not found.');
        }

        // Retrieve existing user and update
        $user = User::where('email', $userData->email)->first();
        if ($user){
            try {
                DB::beginTransaction();
                DB::table('users')
                    ->where('email', '=', $user->email)
                    ->update(['googleplus_id' => $userData->id]);
                DB::commit();
            } catch(\PDOException $e) {
                DB::rollback();
                return Redirect::back()
                    ->with('error', 'Opps!! There was problem processing request. Please try later');
            }
        } else {
            $user = UserRepository::findByGooglePlusIdOrNew($userData->id);
            UserHydrator::hydrateFromSocialiteTwoGooglePlusUser($user, $userData);
        }
        $user->save();

        // Login and redirect
        Auth::loginUsingId($user->id);

        return Redirect::intended('/');
    }
}
