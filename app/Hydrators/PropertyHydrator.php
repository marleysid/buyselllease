<?php namespace App\Hydrators;

use App\Models\Agent;
use App\Models\Property;
use App\Models\User;
use GCH;
use Uuid;

class PropertyHydrator
{

    /**
     * @param $type
     * @param array $rea_xml
     * @return Property
     * @throws \Exception
     *
     * Residential - Under Residential as All
     * Rental  - Under Residential
     * Land -  Already in the list in Residential
     * Rural - Already in the list in Residential
     * Commercial - Under Commercial as All
     * CommercialLand - Under Commercial  as Land/Development
     * Business - Under Commercial as Offices


     */
    public static function hydrateFromREAXMLArray($type, array $rea_xml)
    {

        switch ($type) {
            case 'rental':
                return PropertyHydrator::processREAXMLRentalArray($rea_xml);
                break;
            case 'residential':
                return PropertyHydrator::processREAXMLResidentialArray($rea_xml);
                break;
            case 'commercial':
                return PropertyHydrator::processREAXMLCommercialArray($rea_xml);
                break;
            case 'commercialLand':
                return PropertyHydrator::processREAXMLCommercialLandArray($rea_xml);
                break;
            case 'land':
                return PropertyHydrator::processREAXMLLandArray($rea_xml);
                break;
            case 'rural':
                return PropertyHydrator::processREAXMLRuralArray($rea_xml);
                break;
            case 'business':
                return PropertyHydrator::processREAXMLBusinessArray($rea_xml);
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * @param array $rea_xml
     * @return static
     * @throws \Exception
     */
    public static function processREAXMLRentalArray(array $rea_xml)
    {
        $errors = array();

        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',
            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }

        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
        
        $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        usleep(250000); // to ensure we do not go over the 5 requests per second limit of the google API

        $latlng = GCH::geolocate_address($address);

        $cat = $rea_xml['category'];

        if ($cat == "Unit" || $cat == "Flat" || $cat == "ServicedApartment" || $cat == "Duplex"){
            $category = "Apartment";
        } else if ($cat == "Semi-detached" || $cat == "Terrace"){
            $category = "House";
        } else if($cat == "AcreageSemi-rural"){
            $category = "Rural/Farm";
        } else {
            $category = $rea_xml['category'];
        }

    


        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = 'lease';
        $property->address        = $address;
        $property->category       = $category;
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];

        switch ($rea_xml['rent_period']) {
            case 'month':
            case 'monthly':
                $property->price = (int) $rea_xml['rent'] / 4.5;
                break;
            default:
                $property->price = $rea_xml['rent'];
        }

        return $property;
    }

    /*
     * @param array $rea_xml
     * @return static
     * @throws Exception
     */
    public static function processREAXMLResidentialArray(array $rea_xml)
    {
        $errors = array();

        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',
            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }

        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
       
        $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        $latlng = GCH::geolocate_address($address);

        if ($cat == "Unit" || $cat == "Flat" || $cat == "ServicedApartment" || $cat == "Duplex"){
            $category = "Apartment";
        } else if ($cat == "Semi-detached" || $cat == "terrace"){
            $category = "House";
        } else if($cat == "AcreageSemi-rural"){
            $category = "Rural/Farm";
        } else {
            $category = $rea_xml['category'];
        }


        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = 'buy';
        $property->address        = $address;
        $property->category       = $category;
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];
        $property->price          = $rea_xml['price'];

        return $property;
    }

    public static function processREAXMLCommercialArray(array $rea_xml)
    {
        $errors = array();

        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',
            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }
        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
        $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        $latlng = GCH::geolocate_address($address);

        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = $rea_xml['commercialListingType'];
        $property->address        = $address;
        $property->category       = $rea_xml['commercialCategory'];
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];
        $property->price          = $rea_xml['price'];

        return $property;
    }

    public static function processREAXMLCommercialLandArray(array $rea_xml)
    {
        $errors = array();

        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',
            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }
        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
        $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        $latlng = GCH::geolocate_address($address);

        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = $rea_xml['commercialListingType'];
        $property->address        = $address;
        $property->category       = $rea_xml['commercialCategory'];
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];
        $property->price          = $rea_xml['price'];

        return $property;
    }

    public static function processREAXMLLandArray(array $rea_xml)
    {
        $errors = array();

        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',
            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }
        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
        $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        $latlng = GCH::geolocate_address($address);

        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = 'buy';
        $property->address        = $address;
        $property->category       = 'Land';
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];
        $property->price          = $rea_xml['price'];
        return $property;
    }

    public static function processREAXMLRuralArray(array $rea_xml)
    {
        $errors = array();

        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',

            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }
        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
         $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        $latlng = GCH::geolocate_address($address);

        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = 'buy';
        $property->address        = $address;
        $property->category       = 'Rural/Farm';
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];
        $property->price          = $rea_xml['price'];

        return $property;
    }

    public static function processREAXMLBusinessArray(array $rea_xml)
    {
        $errors = array();
        // 2 stage validation, core and the required (for current listings)
        $core_fields = array(
            'uniqueID',
            'status',
        );

        foreach ($core_fields as $required_field) {
            if (empty($rea_xml[$required_field])) {
                array_push($errors, 'Core field missing: ' . $required_field);
            }
        }

        if ($rea_xml['status'] == 'current') {
            $required_fields = array(
                'agentID',
                'uniqueID',
                'modTime',
                'status',

            );

            foreach ($required_fields as $required_field) {
                if (empty($rea_xml[$required_field])) {
                    array_push($errors, 'Required field missing: ' . $required_field);
                }
            }

            if (!empty($errors)) {
                throw new \Exception(implode("\n", $errors));
            }
        }

        $property = Property::firstOrNew(array('property_code' => $rea_xml['uniqueID']));

        if ($rea_xml['status'] != 'current') {
            $property->delete();
            return $property;
        }
        if (!isset($rea_xml['listingAgent.telephone'])){
           $rea_xml['listingAgent.telephone'] = "";
        }
        $agent = PropertyHydrator::createAgent($rea_xml['agentID'], $rea_xml['listingAgent.email'], $rea_xml['listingAgent.name'], $rea_xml['listingAgent.telephone'], $rea_xml['address.suburb'], $rea_xml['address.street']);

        $address = PropertyHydrator::buildAddress($rea_xml);

        $parking = PropertyHydrator::calculateParking($rea_xml);

        $latlng = GCH::geolocate_address($address);

        $property->agent_id       = $agent->id;
        $property->property_code  = $rea_xml['uniqueID'];
        $property->property_type  = $rea_xml['commercialListingType'];
        $property->address        = $address;
        $property->category       = 'Offices';
        $property->postcode       = $rea_xml['address.postcode'];
        $property->suburb         = $rea_xml['address.suburb'];
        $property->parking        = $parking;
        $property->bedrooms       = $rea_xml['features.bedrooms'];
        $property->bathrooms      = $rea_xml['features.bathrooms'];
        $property->source_updated = $rea_xml['modTime'];
        $property->lat            = $latlng['lat'];
        $property->lng            = $latlng['lng'];
        $property->price          = $rea_xml['price'];

        return $property;
    }

    public static function createAgent($agentId = '', $email, $name, $phone, $suburb, $street)
    {

        $uuid         = Uuid::generate();
        $latlng       = GCH::geolocate_address($suburb . ',Australia');
        $lat          = $latlng['lat'];
        $lng          = $latlng['lng'];
        $unique_email = 'buyselllease@' . $uuid;
        $agent        = Agent::where(array('import_id' => $agentId))->first();

        if (empty($agent)) {
            $user = User::Create(['email' => $email, 'role' => 2, 'status' => '1', 'active' => '1']);
            Agent::Create(array('user_id' => $user->id, 'import_id' => $agentId, 'name' => $name, 'trading_name' => $name, 'address' => $street, 'phone' => $phone, 'suburb' => $suburb, 'lat' => $lat, 'lng' => $lng));
        }

        $agent_final = Agent::where('import_id', '=', $agentId)->firstOrFail();

        return $agent_final;

    }

    public static function buildAddress(array $rea_xml)
    {
        return implode(', ', array(
            (empty($rea_xml['address.subNumber']) ? '' : $rea_xml['address.subNumber'] . '/') . $rea_xml['address.streetNumber'] . ' ' . $rea_xml['address.street'],
            $rea_xml['address.suburb'],
            $rea_xml['address.postcode'],
            $rea_xml['address.country'],
        ));
    }

    public static function calculateParking(array $rea_xml)
    {
        return array_sum(array(
            empty($rea_xml['features.openSpaces']) ? 0 : $rea_xml['features.openSpaces'],
            empty($rea_xml['features.carports']) ? 0 : $rea_xml['features.carports'],
            empty($rea_xml['features.garages']) ? 0 : $rea_xml['features.garages'],
        ));
    }
}
