<?php namespace App\Hydrators;

use App\Models\User;
use Laravel\Socialite\Two\User as SocialiteUser;

class UserHydrator {

    public static function hydrateFromSocialiteTwoFacebookUser(User $user, SocialiteUser $socialiteUser) {
        return $user->fill([
            'name'              => $socialiteUser->getName(),
            'email'             => $socialiteUser->getEmail(),
            'facebook_id'       => $socialiteUser->getId(),
            'facebook_avatar'   => $socialiteUser->getAvatar()
        ]);
    }

    public static function hydrateFromSocialiteTwoGooglePlusUser(User $user, SocialiteUser $socialiteUser) {
        return $user->fill([
            'name'              => $socialiteUser->getName(),
            'email'             => $socialiteUser->getEmail(),
            'googleplus_id'       => $socialiteUser->getId(),
            'googleplus_avatar'   => $socialiteUser->getAvatar()
        ]);
    }
}
