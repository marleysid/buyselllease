<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Flash;
use Illuminate\Support\Facades\Redirect;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
        if ($e instanceof TokenMismatchException)
        {
            Flash::error('Oops!! Something went wrong. Please try again');
            return redirect::back();
        }


         if ($request->route()->getAction()["controller"] == "App\Http\Controllers\CronController@index") {

            //dd((array)$e);

            $errors = [
                'error' => $e->getMessage(),
                'line'  => $e->getline(),
            ];
            //dd($errors);
            \Mail::send('emails.cronerror', ['errors' => $errors], function ($message) use ($errors) {
                $message->to('puspa@ebpearls.com','Puspa');
                $message->subject("cronfailure");
                $message->bcc('psquaretop@gmail.com', 'pradeep');
                $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            });

           
        }

		return parent::render($request, $e);
	}

}
