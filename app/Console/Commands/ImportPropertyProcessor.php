<?php namespace App\Console\Commands;

use App\Property;
use Illuminate\Console\Command;
use App\Agent;
use App\ImportPayload;
use App\Hydrators\PropertyHydrator;
use App\Libraries\REA_XML;
use App\PropertyData;
use Mail;
use Queue;
use File;
use DB;
use App\PropertyDailyUpdates;



class ImportPropertyProcessor extends Command {

    protected $name = 'bsl:process-incoming-payloads';

    protected $description = 'Scan imported payload and process any new items';

    public function handle()
    {
        DB::reconnect();
        foreach (ImportPayload::Unprocessed()->get() as $import_payload) {

            // @todo: I'm not happy with this library, look to replace it, prefer to access all value directly, not a subset!
            $rea_xml = new REA_XML();
            $xml_document = $rea_xml->parse_xml($import_payload->payload);

            $filename = $import_payload->filename;
            $webhoster = strtoupper(str_replace('/','',$import_payload->webhoster));
            if (empty($xml_document)) {
                throw new \Exception('invalid xml provided');
            }

            foreach ($xml_document as $type => $import_properties) {
                foreach ($import_properties as $import_property) {
                    // flatten xml for easier processing
                    $import_property = array_dot($import_property);
                    $property = PropertyHydrator::hydrateFromREAXMLArray($type, $import_property);

                    if ($property == '') {
                        continue;
                    }
                    $property_test = Property::where('property_code', $import_property['uniqueID'])->first();
                    if (empty($property_test->property_code)) {
                        $property_entry = 'NEW';
                    } else {
                        $property_entry = 'UPDATED';
                    }
                    // save the property
                    if ($import_property['status'] == 'current') {
                        $property->save();
                    }

                    if (!empty($property->id)) {
                        // soft delete existing data the xml payload
                        PropertyData::where('property_id', '=', $property->id)->delete();
                        $property_data = new PropertyData(array(
                            'property_id' => $property->id,
                            'import_payload_id' => $import_payload->id
                        ));

                        $property_data->payload = json_encode($import_property);
                        $property_data->save();
                    }


                    //add to   daily update table for daily reporting
                    PropertyDailyUpdates::create([
                        'web_hoster' => strtoupper(str_replace('/','',$webhoster)),
                        'xmlfile' => $filename,
                        'agent_id' => $import_property['agentID'],
                        'property_id' => $import_property['uniqueID'],
                        'source_updated' => $import_property['modTime'],
                        'status' => $import_property['status'],
                        'entry_type' => $property_entry
                    ]);


                    /* Mail::send('emails.propertyUpdate', ['property' => $property], function ($message) use ($property, $import_property) {
                         $message->to(env('BSL_ADMIN_EMAIL'), env('BSL_ADMIN_NAME'))
                             ->subject(($property->trashed() ? 'DELETED' : 'ADDED/UPDATED') . " - {$import_property['agentID']} - {$property->property_code}, " . ucfirst($property->property_type) . " in {$property->suburb}");
                         //$message->bcc(explode(',', env('MONITOR_EMAIL')));
                         $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
                     });*/
                }
            }

            $import_payload->processed = date('Y-m-d H:i:s');
            $import_payload->save();
        }
    }

}
