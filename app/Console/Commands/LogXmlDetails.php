<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\REA_XML;
use App\Agent;
use Queue;
use File;
use DB;
use Log;
use App\Hydrators\PropertyHydrator;
use View;
use Mail;

class LogXmlDetails extends Command
{

    protected $name = 'bsl:maintain-log-for-xml';

    protected $description = 'Scan processed files and  mantain log of  each file details';

    public function handle()
    {

        DB::reconnect();
        $agents = Agent::where('ftp_home_directory', '!=', '')->distinct()->get(['ftp_home_directory']);

        if (empty($agents)) {
            throw new \Exception('Agents not found');
        }
        $xml = [];
        $counter = 0;

        foreach ($agents as $agent) {

            $processedDirectory = base_path() . $agent->ftp_home_directory . '/processed';

            if (!File::isDirectory($processedDirectory)) {
                throw new \Exception('Directory not found: [' . $processedDirectory . ']');
            }

            foreach (File::files($processedDirectory) as $processed) {

                $file_detail = pathinfo($processed);
                $filename = $file_detail['filename'];
                $prossed_file = File::get($processed);
                // verify file is properties list XMLc
                if ($prossed_file == "") {
                    continue;
                }

                $rea_xml = new REA_XML();
                $xml_document = $rea_xml->parse_xml($prossed_file);
                foreach ($xml_document as $type => $import_properties) {

                    foreach ($import_properties as $import_property) {
                        $property_content = [];
                        $property_content['web_hoster'] = $agent->ftp_home_directory;
                        $property_content['type'] = $type;
                        $property_content['filename'] = $filename;
                        // flatten xml for easier processing

                        $import_property = array_dot($import_property);
                        $final = array_merge($property_content, $import_property);
                        array_push($xml, $final);
                        $counter++;
                    }
                }

            }
        }


        // group   with   webhosters

        $agentbox = [];
        $total_property = count($xml);
        foreach ($xml as $key => $value) {
            $agentbox[$value['agentID']][$value['web_hoster']][$value['uniqueID']][] = ['proprty_id' => $value['uniqueID'], 'modified_time' => $value['modTime'], 'status' => $value['status'], 'filename' => $value['filename']];

        }


        $html = View::make('txt.txt', compact('agentbox'));

        File::put(storage_path() . '/propertybuyselllease.txt', $html);
        $txtpath = storage_path() . '/propertybuyselllease.txt';

        Mail::send('emails.dailyUpdate', [], function ($message) use ($txtpath) {
            $message->to(env('BSL_ADMIN_EMAIL'), env('BSL_ADMIN_NAME'))
                ->subject("test");
            $message->bcc('psquaretop@gmail.com','pradeep');
            $message->bcc('naresh.yadav@ebpearls.com','Naresh');
            $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
            $message->attach($txtpath);


        });

    }

    /*public function orderTxt(Order $order) {

    }*/


}