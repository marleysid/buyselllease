<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Agent;
use App\ImportPayload;
use Queue;
use File;
use DB;



class ImportPayloadProcessor extends Command {

    protected $name = 'bsl:scan-incoming-xml';

    protected $description = 'Scan FTP folders and process REAXML files';

    public function handle()
    {
        $agents = Agent::where('ftp_home_directory', '!=', '')->distinct()->get(['ftp_home_directory']);

        if (!empty($agents)) {

            $total_agents = count($agents);

            $excuted = 0;
            foreach ($agents as $agent) {
                $incomingDirectory = base_path() . $agent->ftp_home_directory . '/incoming';
                $rejectDirectory = base_path() . $agent->ftp_home_directory . '/rejected';
                $processedDirectory = base_path() . $agent->ftp_home_directory . '/processed';
                $webhoster =  $agent->ftp_home_directory;

                if (!File::isDirectory($incomingDirectory)) {
                    //continue; // @todo implement alert system to track these failures
                    throw new \Exception('Directory not found: [' . $incomingDirectory . ']');
                }

                foreach (File::files($incomingDirectory) as $incomingFile) {

                    $payload = File::get($incomingFile);
                    $f = File::size($incomingFile);
                    $file_detail = pathinfo($incomingFile);
                    $filename = $file_detail['filename'];
                    // verify file is properties list XML
                    if ($f <= 0) {

                        // your file is not empty
                        File::move($incomingFile, $rejectDirectory . '/' . basename($incomingFile));
                        continue;
                    } else {

                        $document = new \SimpleXMLElement($payload);

                        if ($document->getName() != 'propertyList') {
                            File::move($incomingFile, $rejectDirectory . '/' . basename($incomingFile));
                            throw new \Exception('File does not contain XML with propertyList root element');
                        }

                        // save file to db
                        ImportPayload::create(array(
                            'payload' => $payload,
                            'filename' => $filename,
                            'webhoster' => $webhoster
                        ));

                        // move file to processed folder
                        File::move($incomingFile, $processedDirectory . '/' . basename($incomingFile));
                    }
                }

            }
        }



    }
}
