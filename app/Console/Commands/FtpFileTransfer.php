<?php
/**
 * Created by PhpStorm.
 * User: ebpearls
 * Date: 11/9/2015
 * Time: 2:21 PM
 */

namespace App\Console\Commands;


class FtpFileTransfer {

    protected $name = 'bsl:fileTransfer-ci';

    protected $description = 'Get file  from   live to  local';

    public function handle()
    {
        $server = 'ftp.you-server.com'; //address of ftp server (leave out ftp://)
        $ftp_user_name = 'serName'; // Username
        $ftp_user_pass = 'asswordHere'; // Password
        $source = '/home//public_html/filename.ext';
        $dest = '/public_html/filename.ext';
        $mode='FTP_BINARY';
        $connection = ftp_connect($server);

        $login = ftp_login($connection, $ftp_user_name, $ftp_user_pass);

        if (!$connection || !$login) { die('Connection attempt failed!'); }

        $upload = ftp_put($connection, $dest, $source, FTP_BINARY);

        if (!$upload) { echo 'FTP upload failed!'; }

        ftp_close($connection);
        echo 'one';

    }

}