<?php namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Agent;
use App\ImportPayload;
use File;
use DB;
use App\Hydrators\PropertyHydrator;
use App\Libraries\REA_XML;
use App\PropertyData;
use App\Property;
use Mail;
use App\PropertyDailyUpdates;
use Files;
use Storage;

class CreateDailyReport extends Command
{

    protected $name = 'bsl:Daily-Report';

    protected $description = 'Create Daily report for  property import';
    /**
     *distinguish  into  new  , updated , status
     * create csv file for  each
     * use this function to  save csv file . convertToCSVAndSave in  storage file/csv  folder
     * flush  PropertyDailyUpdates
     * and  send   email to admin with  attaching three csv
     */
    public function handle()
    {
        self::getSoldProperties();
        self::getCurrentProperties();
        self::getNewProperties();
        self::getUpdatedProperties();

        DB::reconnect();
        $truncate =  DB::table('property_daily_updates')->truncate();
        $csvDirectory = storage_path('csv').'/'.date('Y-m-d');


        Mail::send('emails.dailyUpdate', [], function ($message) use ($csvDirectory) {
                       $message->to(env('BSL_ADMIN_EMAIL'), env('BSL_ADMIN_NAME'))
                           ->subject("test");
                       $message->bcc('psquaretop@gmail.com','pradeep');
                       $message->bcc('naresh.yadav@ebpearls.com','Naresh');
                       $message->from(env('BSL_NOREPLY_EMAIL'), env('BSL_NOREPLY_NAME'));
                     foreach (File::files($csvDirectory) as $csvfile) {
                            $message->attach($csvfile);
                    }

        });

       /* foreach (File::files($csvDirectory) as $csvfile) {
            if (File::exists($csvfile)) {
                File::delete($csvfile);
            }
        }*/





    }

        public static function getSoldProperties(){
            DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('status','!=','current')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster','xmlfile','agent_id', 'agent_name','agent_office_location','property_id','property_location','source_updated','entry_type','status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ','Agent Name','Agent Office Location','Property ID','Property Location', 'Source Updated Date','NEW/UPDATED','Status');
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'SOLD_'.date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options,$filename,date('Y-m-d'));
    }


        public static function  getCurrentProperties(){
            DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('status','current')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster','xmlfile','agent_id', 'agent_name','agent_office_location','property_id','property_location','source_updated', 'entry_type','status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ','Agent Name','Agent Office Location','Property ID','Property Location', 'Source Updated Date','NEW/UPDATED','Status');
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'CURRENT_'.date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options,$filename ,date('Y-m-d'));
    }


        public  static function getNewProperties(){
            DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('entry_type','NEW')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster','xmlfile','agent_id', 'agent_name','agent_office_location','property_id','property_location','source_updated', 'entry_type','status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ','Agent Name','Agent Office Location','Property ID','Property Location', 'Source Updated Date','NEW/UPDATED','Status');
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'NEW_'.date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options,$filename,date('Y-m-d') );
    }



        public static function getUpdatedProperties(){
            DB::reconnect();
        // query
        $daily_updates = PropertyDailyUpdates::where('entry_type','UPDATED')->get();
        // passing the columns which I want from the result set. Useful when we have not selected required fields
        $arrColumns = array('web_hoster','xmlfile','agent_id', 'agent_name','agent_office_location','property_id','property_location','source_updated', 'entry_type','status');
        // define the first row which will come as the first row in the csv
        $arrFirstRow = array('Webhoster', 'Xmlfile', 'Agent ID ','Agent Name','Agent Office Location','Property ID','Property Location', 'Source Updated Date','NEW/UPDATED','Status');
        // building the options array
        $options = array(
            'columns' => $arrColumns,
            'firstRow' => $arrFirstRow,
        );
        $filename = 'UPDATED_'.date('Y-m-d');

        Files::convertToCSVAndSave($daily_updates, $options,$filename,date('Y-m-d') );
    }


}