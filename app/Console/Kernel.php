<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
        'App\Console\Commands\ImportPayloadProcessor',
        'App\Console\Commands\ImportPropertyProcessor',
        'App\Console\Commands\LogXmlDetails',
        'App\Console\Commands\CompareXmlWithDb',
        'App\Console\Commands\CreateDailyReport'
    ];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
        $schedule->command('bsl:scan-incoming-xml')
            ->everyFiveMinutes()
            ->withoutOverlapping();

        $schedule->command('bsl:process-incoming-payloads')
            ->everyFiveMinutes()
            ->withoutOverlapping();

        $schedule->command('bsl:maintain-log-for-xml')
            ->everyFiveMinutes()
            ->withoutOverlapping();


        $schedule->command('bsl:Daily-Report')
            ->dailyAt('23:57')
            ->withoutOverlapping();

        $schedule->command(' bsl:maintain-log-for-xml')
            ->dailyAt('23:57')
            ->withoutOverlapping();


	}

}
