<?php namespace App\Repositories;

use GCH;
use App\Models\Agent;
use App\Models\SearchProfile;
use DB;

class AgentRepository {

	public static function findAllBySearchProfile(SearchProfile $searchProfile) {
		$searchProfileArray = json_decode($searchProfile->profile, true);
		//dd($searchProfileArray );
		if (isset($searchProfileArray['suburb'])) {
			$address = $searchProfileArray['suburb'];
		} else {
			$address = '';
		}

		$agent = Agent::query();

		if(isset($searchProfileArray['inc-surrounding'])) {
			$latlng = GCH::geolocate_address($address);
			if ($latlng) {
				$lat = $latlng['lat'];
				$lng = $latlng['lng'];

			} else {
				$lat = '0';
				$lng = '0';

			}
		} else {
			$lat = '0';
			$lng = '0';
		}

		if (!isset($searchProfileArray['cover_distance'])) {
			$distance = 0;
		} else {
			$distance = $searchProfileArray['cover_distance'];
			$agent->distance($lat, $lng, $distance, 'KM');
		}

		
		
		//dd($searchProfileArray);
		$whereClauses = [];
		foreach ($searchProfileArray as $key => $value) {
			if (empty($value)) { // skip any empty values from the search form
				continue;
			}

			switch ($key) {
				case 'suburb':
					if(!isset($searchProfileArray['inc-surrounding'])) {
						//dd($value);
						$agent->where('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');
						//dd($agent);
					}
					break;	
			}
		}
		//$agent->whereRaw(implode($whereClauses));
		
		return $agent->paginate(10);
		
		// $whereClauses = [];
		// $profile = json_decode($searchProfile->profile,true);
		// $suburb =explode(',', $profile['suburb']);
		// return Agent::where('suburb', 'LIKE', '%' . $suburb[0] . '%')->paginate(20);
	}



	public static function findAllBySearchProfileMap(SearchProfile $searchProfile) {
		/*$profile = json_decode($searchProfile->profile,true);
		$suburb =explode(',', $profile['suburb']);
		return Agent::where('suburb', 'LIKE', '%' . $suburb[0] . '%')->get();*/
		$searchProfileArray = json_decode($searchProfile->profile, true);
		//dd($searchProfileArray );
		if (isset($searchProfileArray['suburb'])) {
			$address = $searchProfileArray['suburb'];
		} else {
			$address = '';
		}

		$agent = Agent::query();

		if(isset($searchProfileArray['inc-surrounding'])) {
			$latlng = GCH::geolocate_address($address);
			if ($latlng) {
				$lat = $latlng['lat'];
				$lng = $latlng['lng'];

			} else {
				$lat = '0';
				$lng = '0';

			}
		} else {
			$lat = '0';
			$lng = '0';
		}

		if (!isset($searchProfileArray['cover_distance'])) {
			$distance = 0;
		} else {
			$distance = $searchProfileArray['cover_distance'];
			$agent->distance($lat, $lng, $distance, 'KM');
		}

		
		
		//dd($searchProfileArray);
		$whereClauses = [];
		foreach ($searchProfileArray as $key => $value) {
			if (empty($value)) { // skip any empty values from the search form
				continue;
			}

			switch ($key) {
				case 'suburb':
					if(!isset($searchProfileArray['inc-surrounding'])) {
						//dd($value);
						$agent->where('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');
						//dd($agent);
					}
					break;	
			}
		}

		return $agent->get();

	}

}
