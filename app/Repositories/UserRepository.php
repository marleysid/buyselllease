<?php namespace App\Repositories;

use App\Models\User;

class UserRepository {

    /**
     * @param $facebook_id
     * @return static
     */
    public static function findByFacebookIdOrNew($facebook_id)
    {
        return User::firstOrNew([
            'facebook_id'   => $facebook_id,
            'role'          => 4,
            'status'        => 1,
            'active'        => 1
        ]);
    }

    /**
     * @param $facebook_id
     * @return static
     */
    public static function findByGooglePlusIdOrNew($googleplus_id)
    {
        return User::firstOrNew([
            'googleplus_id' => $googleplus_id,
            'role'          => 4,
            'status'        => 1,
            'active'        => 1
        ]);
    }

    public static function updateByGooglePlusIdOrNew($googleplus_id)
    {
        return User::updateOrCreate([
            'googleplus_id'  => $googleplus_id
        ]);
    }

}
