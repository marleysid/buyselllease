<?php namespace App\Repositories;

use App\Models\City;
use App\Models\Professionals;
use App\Models\SearchProfile;
use GCH;

class ProfessionalRepository
{

    public static function findAllBySearchProfile(SearchProfile $searchProfile)
    {

        $professions        = [];
        $lat                = '';
        $lng                = '';
        $searchProfileArray = json_decode($searchProfile->profile, true);

        $professionals = Professionals::query();

        foreach (json_decode($searchProfile->profile) as $key => $value) {

            if (empty($value)) {
                continue;
            } // skip any empty values from the search form

            switch ($key) {
                case 'suburb':
                    if (empty($searchProfileArray['inc-surrounding'])) {

                         
                            
                        $nlatlng = GCH::geolocate_address($value);

                        if ($nlatlng) {

                            $lat = $nlatlng['lat'];
                            $lng = $nlatlng['lng'];
                            $distance = 1;

                            $professionals->distance($lat, $lng, $distance, 'KM')->orderBy('distance', 'asc');
                            // $professionals->orWhere('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');
                        } 

                        

                    } else {

                        if (empty($searchProfileArray['cover_distance'])) {
                            $distance = 0;
                        } else {
                            $distance = $searchProfileArray['cover_distance'];
                        }

                        $latlng = GCH::geolocate_address($value);

                        if ($latlng) {

                            $lat = $latlng['lat'];
                            $lng = $latlng['lng'];

                            $professionals->distance($lat, $lng, $distance, 'KM')->orderBy('distance', 'asc');
                            //$professionals->regiondistance();
                            //$professionals->orWhere('nationwide','=','1');
                        }

                    }
                    break;
                case 'category-action':

                    if (!empty($value)) {
                        foreach ($value as $key1 => $val) {
                            array_push($professions, $val);
                        }

                    }

                    break;
            }
        }

        if (!empty($professions)) {
            return $professionals->whereIn('profession', $professions)->join('users', function ($join) {
                $join->on('users.id', '=', 'professionals.user_id')
                    ->where('users.active', '=', 1);
            })->paginate(20);
        } else {
            return $professionals->join('users', function ($join) {
                $join->on('users.id', '=', 'professionals.user_id')
                    ->where('users.active', '=', 1);
            })->paginate(20);
        }

    }

    public static function findAllBySearchProfileMap(SearchProfile $searchProfile)
    {

        $professions        = [];
        $lat                = '';
        $lng                = '';
        $searchProfileArray = json_decode($searchProfile->profile, true);
        $professionals      = Professionals::query();

        foreach (json_decode($searchProfile->profile) as $key => $value) {

            if (empty($value)) {
                continue;
            } // skip any empty values from the search form

            switch ($key) {
                case 'suburb':
                    if (empty($searchProfileArray['inc-surrounding'])) {
                        $professionals->select('professionals.*')->where('suburb', 'LIKE', '%' . explode(',', $value)[0] . '%');
                    } else {

                        if (empty($searchProfileArray['cover_distance'])) {
                            $distance = 5;
                        } else {
                            $distance = $searchProfileArray['cover_distance'];
                        }

                        $latlng = GCH::geolocate_address($value);
                        if ($latlng) {
                            $lat = $latlng['lat'];
                            $lng = $latlng['lng'];
                            $professionals->distance($lat, $lng, $distance, 'KM')->orderBy('distance', 'asc');
                          
                        }

                    }
                    break;

                case 'category-action':

                    if (!empty($value)) {
                        foreach ($value as $key1 => $val) {
                            array_push($professions, $val);
                        }

                    }

                    break;

            }

        }

        if (!empty($professions)) {
            return $professionals->whereIn('profession', $professions)->join('users', function ($join) {
                $join->on('users.id', '=', 'professionals.user_id')
                    ->where('users.active', '=', 1);
            })->get();
        } else {
            return $professionals->join('users', function ($join) {
                $join->on('users.id', '=', 'professionals.user_id')
                    ->where('users.active', '=', 1);
            })->get();
        }

    }

}
