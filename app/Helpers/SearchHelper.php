<?php namespace App\Helpers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use App\Models\Profession;
use Input;
use SH;


class SearchHelper {

    /**
     * @return mixed
     */
    public static function getSearchProfileFromSession() {
        if(Session::has('search-profile')) {
            return Session::get('search-profile');

        }
        return json_encode([]);
    }

    /**
     * check is search key exists
     *
     * @param $key
     * @return bool
     */
    public static function hasSearchProfileValue($key) {
        if(SH::getSearchProfileFromSession() !='[]') {
            return property_exists(
                json_decode(SH::getSearchProfileFromSession()),
                $key
            );
        } else {
            return false;

        }
    }

    /**
     * Check is search key exists, if so return it, otherwise return default
     *
     * @param $key
     * @param string $default
     * @return string
     */
    public static function getSearchProfileValue($key, $default = '') {
        if(!SH::hasSearchProfileValue($key)) {
            return $default;
        }
        return json_decode(SH::getSearchProfileFromSession())->$key;
    }

    /**
     * @param Request $request
     */
    public static function saveRequestToSessionSearchProfile(Request $request) {

        if (!Session::has('search-profile')) {
            Session::set('search-profile', json_encode([]));
        }
        // merge the current request with the existing session profile
        Session::put('search-profile',
            json_encode(
                array_merge(
                    $request->all(),
                   json_decode(
                       SH::getSearchProfileFromSession(), true
                   )

                )
            )
        );
    }

    public static function saveArrayToSessionSearchProfile($searchValues) {
        if(!is_array($searchValues)) {
            throw new Exception('Parameter type mismatch, Array expected [' . gettype($searchValues) . ']');
        }

        // merge the current request with the existing session profile
        Session::put('search-profile',
            json_encode(
                array_merge(
                    $searchValues,
                    json_decode(
                        SH::getSearchProfileFromSession(), true
                    )
                )
            )
        );
    }

    public static function clearSearchProfile() {
        Session::forget('search-profile');
    }

    public static function getResidentialActionOptions() {
        return [
            'House'                 => 'House',
            'Apartment'             => 'Apartment',
            'Unit'                  => 'Unit',
            'Townhouse'             => 'Townhouse',
            'Villa'                 => 'Villa',
            'Land'                  => 'Land',
            'Rural'                 => 'Rural/Farm',
            'Flat'                  => 'Flat',
            'Studio'                => 'Warehouse',
            'DuplexSemi-detached'   => 'DuplexSemi-detached',
            'Alpine'                => 'Alpine',
            'AcreageSemi-rural'     => 'AcreageSemi-rural',
            'BlockOfUnits'          => 'BlockOfUnits',
            'Terrace'               => 'Terrace',
            'Retirement'            => 'Retirement',
            'ServicedApartment'     => 'ServicedApartment',
            'Other'                 => 'Other'
        ];

    }

    public static function getCommercialActionOptions() {
        return [
            'Offices'               => 'Offices',
            'Retail'                => 'Retail',
            'Industrial/Warehouse'  => 'Industrial/Warehouse',
            'Showrooms/Bulky Goods' => 'Showrooms/Bulky Goods',
            'Land/Development'      => 'Land/Development',
            'Hotel/Leisure'         => 'Hotel/Leisure',
            'Medical/Consulting'    => 'Medical/Consulting',
            'Commercial Farming'    => 'Commercial Farming',
        ];

    }
    public static function getResidentialActionOptionsLease(){
        return [
            'House'                 => 'House',
            'Apartment/Townhouse/Villa'             => 'Apartment/Townhouse/Villa',
            //'Unit'                  => 'Unit',
            //'Townhouse/Villa'             => 'Townhouse/Villa',
            //'Villa'                 => 'Villa',
            'Land'                  => 'Land',
            'Rural'                 => 'Rural/Farm',
            //'Flat'                  => 'Flat',
            'Retirement Living'                => 'Retirement Living',
            //'DuplexSemi-detached'   => 'DuplexSemi-detached',
            //'Alpine'                => 'Alpine',
            //'AcreageSemi-rural'     => 'AcreageSemi-rural',
            //'BlockOfUnits'          => 'BlockOfUnits',
            //'Terrace'               => 'Terrace',
            //'Retirement'            => 'Retirement',
            //'ServicedApartment'     => 'ServicedApartment',
            //'Other'                 => 'Other'
        ];
    }

    // public static function getCommercialActionOptions() {
    //     return [
    //         'Offices'               => 'Commercial Offices',
    //         'Retail'                => 'Retail',
    //         'Residential Investments' => 'Residential Investments',
    //         'Industrial/Warehouse'  => 'Industrial/Warehouse',
    //         'Showrooms/Bulky Goods' => 'Showrooms/Bulky Goods',
    //         'Land/Development'      => 'Land/Development Sites',
    //         'Hotel/Leisure'         => 'Hotel/Leisure',
    //         'Medical/Consulting'    => 'Medical/Consulting',
    //         'Commercial Farming'    => 'Commercial Farming',
    //         'BlockOfUnits'          => 'BlockOfUnits'
    //     ];

    // }

    public static  function getCategoryOptions($edit_first = false) {
        $categories = Profession::where('status','=','1')->get();
        if($edit_first)
            $categories_array =['' => 'YOUR INDUSTRY'];
        else
            $categories_array =[];


        if(!empty($categories)){
            foreach($categories as $cat){

                $categories_array  = array_add($categories_array, $cat->id, $cat->title);
            }
        }
       return $categories_array;
    }

    public static function getBuyStep1ResidentialActionOptions() {
        return array_merge(
            ['I WOULD LIKE TO BUY...' => 'I WOULD LIKE TO BUY (Residential)...'],
            SH::getResidentialActionOptions()
        );
    }

    public static function getBuyStep1CommercialActionOptions() {
        return array_merge(
            ['I WOULD LIKE TO BUY...' => 'I WOULD LIKE TO BUY (Commercial)...'],
            SH::getCommercialActionOptions()
        );
    }

    public static function getLeaseStep1ResidentialActionOptions() {
        return array_merge(
            ['I WOULD LIKE TO LEASE...' => 'I WOULD LIKE TO LEASE (Residential)...'],
            SH::getResidentialActionOptions()
        );
    }

    public static function getLeaseStep1CommercialActionOptions() {
        return array_merge(
            ['I WOULD LIKE TO LEASE...' => 'I WOULD LIKE TO LEASE (Commercial)...'],
            SH::getCommercialActionOptions()
        );
    }

    // public static function getLandlordLeaseStep1ActionOptions() {
    //     return [
    //         'I WOULD LIKE TO LEASE...' => 'I WOULD LIKE TO LEASE...',
    //         'My home' => 'My home',
    //         'My holiday property' => 'My holiday property',
    //         'My residential investment property' => 'My residential investment property',
    //         //'A rural property' => 'A rural property',
    //         //'A commercial space' => 'A commercial space',
    //         'My retail/commercial space' => 'My retail/commercial space',
    //         'My industrial property' => 'My industrial property',
    //         //'A commercial investment property' => 'A commercial investment property',
    //         'My development site' => 'My development site',
    //         'My rural property' => 'My rural property',
    //     ];
    // }
     public static function getLandlordLeaseStep1ActionOptions() {
        return [
            'I WOULD LIKE TO SELL...' => 'I WOULD LIKE TO LEASE...',
            'My home' => 'My home',
            'A holiday property' => 'A holiday property',
            'A residential investment property' => 'A residential investment property',
            'A rural property' => 'A rural property',
            'A commercial space' => 'A commercial space',
            'A retail space' => 'A retail space',
            'An industrial property' => 'An industrial property',
            'A commercial investment property' => 'A commercial investment property',
            'A development site' => 'A development site',
        ];
    }
    public static function getStep2Mode(Request $request)
    {
        return $request->has('residential-action') ? 'residential' : 'commercial';
    }

    public static function getBuyStep2TypeOptions($mode) {
        switch($mode) {
            case 'residential':
                return [
                    '' => 'Type of Property',
                    'House' => 'House',
                    'Townhouse/Villa' => 'Townhouse/Villa',
                    'Apartment' => 'Apartment',
                    'New Apartment' => 'New Apartment',
                    'New Townhouse' => 'New Townhouse',
                    'Garage' => 'Garage',
                    'Retirement Living' => 'Retirement Living',
                    'Rural' => 'Rural',
                    'Block of apartments' => 'Block of apartments'
                ];
            case 'commercial':
                return [
                    '' => 'Type of Property',
                    'Commercial office space' => 'Commercial office space',
                    'Retail space' => 'Retail space',
                    'Industrial premises' => 'Industrial premises',
                    'Commercial/mixed use building' => 'Commercial/mixed use building',
                    'Industrial Building' => 'Industrial Building',
                    'Showroom/bulky goods' => 'Showroom/bulky goods',
                    'Hotel/Leisure' => 'Hotel/Leisure',
                    'Medical/Consulting' => 'Medical/Consulting',
                    'Commercial farming' => 'Commercial farming',
                    'Other' => 'Other',
                    'Development Site' => 'Development Site'
                ];
        }
        return [];
    }

    // public static function getSellStep1ActionOptions() {
    //     return [
    //         'I WOULD LIKE TO SELL...' => 'I WOULD LIKE TO SELL...',
    //         'My home' => 'My home',
    //         'My holiday property' => 'My Holiday Property',
    //         'My residential investment property' => 'My Residential Investment Property',
    //         //'My commercial space' => 'My commercial space',
    //         'My retail/commercial property' => 'My Retail/Commercial Property',
    //         'My industrial property' => 'My Industrial Property',
    //         //'My commercial investment property' => 'My commercial investment property',
    //         'My development site' => 'My Development Site',
    //         'My rural property' => 'My Rural Property',
    //     ];
    // }

public static function getSellStep1ActionOptions() {
        return [
            'I WOULD LIKE TO SELL...' => 'I WOULD LIKE TO SELL...',
            'My home' => 'My home',
            'A holiday property' => 'A holiday property',
            'A residential investment property' => 'A residential investment property',
            'A rural property' => 'A rural property',
            'A commercial space' => 'A commercial space',
            'A retail space' => 'A retail space',
            'An industrial property' => 'An industrial property',
            'A commercial investment property' => 'A commercial investment property',
            'A development site' => 'A development site',
        ];
    }



    public static function getSellStep2Mode($action)
    {
        switch ($action) {
            case 'My home':
            case 'A holiday property':
            case 'A residential investment property':
            case 'A rural property':
                return 1;
            case 'A commercial space':
            case 'A retail space':
            case 'An industrial property':
            case 'A commercial investment property':
                return 2;
            case 'A development site':
                return 3;
        }
    }

    public static function getSellStep2TypeOptions($mode) {
        switch($mode) {
            case 1:
                return [
                    '' => 'Type of Property',
                    'House' => 'House',
                    'Townhouse/Villa' => 'Townhouse/Villa',
                    'Apartment' => 'Apartment',
                    'Garage' => 'Garage',
                    'Retirement Living' => 'Retirement Living',
                    'Rural' => 'Rural',
                    'Block of apartments' => 'Block of apartments'
                ];
            case 2:
                return [
                    '' => 'Type of Property',
                    'Commercial office space' => 'Commercial office space',
                    'Retail space' => 'Retail space',
                    'Industrial premises' => 'Industrial premises',
                    'Commercial/mixed use building' => 'Commercial/mixed use building',
                    'Industrial Building' => 'Industrial Building',
                    'Showroom/bulky goods' => 'Showroom/bulky goods',
                    'Hotel/Leisure' => 'Hotel/Leisure',
                    'Medical/Consulting' => 'Medical/Consulting',
                    'Commercial farming' => 'Commercial farming',
                    'Other' => 'Other'
                ];
            case 3:
                return [
                    '' => 'Type of Property',
                    'Development Site' => 'Development Site'
                ];
        }
        return [];
    }

    /**
     * @param $agreementType
     * @param $mode
     * @param $sliderValue
     * @return string
     */
    public static function getCalculatedRange($agreementType, $mode, $sliderValue) {

        $maxSteps = 200;
        $lowMultiplier = SH::getRangeMultiplier($agreementType, $mode, 'low');
        $highMultiplier = SH::getRangeMultiplier($agreementType, $mode, 'high');

        list($startIndex, $stopIndex) = explode(',', $sliderValue);

        if($startIndex <= $maxSteps/2) {
            $startValue = $startIndex * $lowMultiplier;
        } else {
            $startValue = ($maxSteps/2) * $lowMultiplier + ($startIndex - $maxSteps/2) * $highMultiplier;
        }

        if($stopIndex <= $maxSteps/2) {
            $stopValue = $stopIndex * $lowMultiplier;
        } else {
            $stopValue = ($maxSteps/2) * $lowMultiplier + ($stopIndex - $maxSteps/2) * $highMultiplier;
        }

  return implode(',', [$startValue, $stopValue]);

    }

    /**
     * @param $agreementType (buy/sell/lease)
     * @param $mode (residential/commercial)
     * @param $scale (high/low)
     */
    public static function getRangeMultiplier($agreementType, $mode, $scale) {
        $multipliers = [
            'low' => [
                'residential' => [
                    'buy'   => 10000,
                    'lease' => 10,
                    'sell'  => 10000
                ],
                'commercial' => [
                    'buy'   => 100000,
                    'lease' => 250,
                    'sell'  => 100000
                ],
            ],
            'high' => [
                'residential' => [
                    'buy'   => 90000,
                    'lease' => 990,
                    'sell'  => 90000
                ],
                'commercial' => [
                    'buy'   => 100000,
                    'lease' => 750,
                    'sell'  => 100000
                ],
            ]
        ];

    return $multipliers[$scale][$mode][$agreementType];
    }



    public static function getImage($filename)
    {

        return asset('uploads/professionals').'/'.$filename;
    }

    public static function getImageService($serviceType){
        switch ($serviceType){
            case 'depreciation_report' :
                return asset('professions'). '/' .'depreciation_report.jpg';
                break;
            case 'conveyancing' :
                return asset('professions'). '/'.'conveyancing.png';
                break;
            case 'finance_broker' :
                return asset('professions'). '/' . 'property_financial_brokerage_services.jpg';
                break;
            case 'insurance_broler' :
                return asset('professions'). '/' . 'property_landlord_insurance_services.jpg';
                break;
            case 'valuer' :
                return asset('professions'). '/' .'valuation_services.jpg';
                break;
            case 'surveyor' :
                return asset('professions'). '/' . 'surveying.jpg';
                break;
            case 'buyer_agent' :
                return asset('professions'). '/' . 'buyers_agent.jpg';
                break;
            case 'pest_report' :
                return asset('professions'). '/' . 'pest_report_and_control_services.jpg';
                break;
            case 'building_report' :
                return asset('professions'). '/' . 'building_inspection_and_report_services.jpg';
                break;
            case 'carpet_cleaners' :
                return asset('professions'). '/' . 'carpet_cleaning.jpg';
                break;
            case 'painter' :
                return asset('professions'). '/' . 'painting_service.jpg';
                break;
            case 'removalists' :
                return asset('professions'). '/' . 'removal_storage.jpg';
                break;
            case 'locksmiths' :
                return asset('professions'). '/' . 'lock_smith.jpg';
                break;
            case 'gardening' :
                return asset('professions'). '/' . 'gardening_and_landscaping_services.jpg';
                break;
            case 'rubbish' :
                return asset('professions'). '/' . 'rubbish_removal.jpg';
                break;
            case 'utility' :
                return asset('professions'). '/' . 'utility_connection.jpg';
                break;
            case 'streaming_home_entertaitment' :
                return asset('professions'). '/' . 'home_entertainment.jpg';
                break;
            case 'owner_builder' :
                return asset('professions'). '/' . 'owner_builder_permits.jpg';
                break;


        }
    }
}
