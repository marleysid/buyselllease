<?php namespace App\Helpers;

use App\Models\Agent;
use App\Models\Favourite;
use App\Models\Profession;
use App\Models\Region;
use DB;
use Input;

class GeneralHelper
{

    public static function limit_word($string, $limit_to)
    {
        if (self::count_words($string) >= $limit_to) {
            $words = explode(" ", $string);
            return implode(" ", array_splice($words, 0, $limit_to)) . '...';
        } else {
            return $string;
        }

    }

    public static function count_words($string)
    {
        return str_word_count($string);
    }

    public static function limit_character($string, $limit)
    {
        if (strlen($string) > $limit) {
            return substr($string, 0, $limit) . '...';
        } else {
            return $string;
        }

    }

    public static function db_proprty_status($property_code)
    {
        $property = DB::table('properties')->where('property_code', 'LIKE', $property_code)->first();
        if (empty($property)) {
            return 'Not Inserted in DB';
        } else if (is_null($property->deleted_at)) {
            return 'Current Active';
        } else {
            return 'Not showing  because  it may be  sold or  withdrawn or offmarket or leased';
        }

    }

    public static function get_sort_urls($base_url, $sort = null, $order = null, $page = null)
    {
        if (!is_null($page)) {
            if (!is_null($sort)) {

                if (!is_null($order)) {
                    if (Input::get('sort') == $sort) {
                        if ($order == 'asc') {
                            $order_reverse = 'dsc';
                        } else {
                            $order_reverse = 'asc';
                        }
                    } else {
                        $order_reverse = 'asc';
                    }

                    $url = $base_url . '?page=' . $page . '&&sort=' . $sort . '&&order=' . $order_reverse;

                } else {
                    $url = $base_url . '?page=' . $page . '&&sort=' . $sort . '&&order=asc';
                }

            } else {
                $url = $base_url . '?page=' . $page . '&&sort=' . $sort . '&&order=asc';
            }

        } else {

            if (!is_null($sort)) {

                if (!is_null($order)) {
                    if (Input::get('sort') == $sort) {
                        if ($order == 'asc') {
                            $order_reverse = 'dsc';
                        } else {
                            $order_reverse = 'asc';
                        }
                    } else {
                        $order_reverse = 'asc';
                    }

                    $url = $base_url . '?sort=' . $sort . '&&order=' . $order_reverse;

                } else {
                    $url = $base_url . '?sort=' . $sort . '&&order=asc';
                }

            } else {
                $url = $base_url . '?sort=' . $sort . '&&order=asc';
            }

        }

        return $url;

    }

    public static function pritify_url($str = '')
    {

        if ($str === 'http://' or $str === '') {
            return '';
        }
        $url = parse_url($str);
        if (!$url or !isset($url['scheme'])) {
            return 'http://' . $str;
        }
        return $str;

    }

    public static function activeSidebar($url)
    {

        if (\URL::full() == $url) {
            return 'active-menu';
        } else {
            return '';
        }
    }

    public static function rootUrl($url)
    {
        if (!empty($url)) {
            $urlParts = preg_split('#(?<!/)/(?!/)#', self::pritify_url($url), 2);
            return $urlParts[0] != '' ? $urlParts[0] . '/' : '';
        } else {
            return '#';
        }
    }

    public static function isFavourite($propertyCode)
    {

        $user_id = \Auth::user()->id;

        $favourite = Favourite::where(['user_id' => $user_id, 'property_id' => $propertyCode])->first();
        if ($favourite) {
            return 1;
        } else {
            return 0;
        }

    }

    public static function removeCommaFromLastOfString($str)
    {

        return rtrim($str, ', ');

    }

    public static function getProfessionName($ids = [])
    {
        if (!empty($ids)) {
            try {
                $findProfessions = DB::table('professions')
                    ->select('title')
                    ->whereIn('id', $ids)
                    ->get();

                $allProfession = [];

                if (!empty($findProfessions)) {
                    foreach ($findProfessions as $pro) {
                        array_push($allProfession, $pro->title);

                    }
                    return join(', ', $allProfession);
                } else {
                    $findProfessions = 'All';
                }

            } catch (\Exception $e) {
                $findProfessions = 'All';
            }
        } else {
            $findProfessions = 'All';
        }
        return $findProfessions;
    }

    public static function ql($raw = false, $last = false)
    {
        $log = DB::getQueryLog();

        if ($raw) {
            foreach ($log as $key => $query) {
                $log[$key] = vsprintf(str_replace('?', "'%s'", $query['query']), $query['bindings']);
            }
        }

        return ($last) ? end($log) : $log;
    }

    public static function contact_format($phone, $mob = false)
    {

        $phone               = preg_replace("/[^0-9]/", "", $phone);
        $phone_valid_initals = ['02', '03', '07'];
        $phoneInitials       = substr($phone, 0, 2);

        if (!in_array($phoneInitials, $phone_valid_initals)) {
// return  in  format xx-xxxx-xxxx

            if (strlen($phone) == 10 || strlen($phone) > 10) {

                return preg_replace("/([0-9]{4})([0-9]{3})([0-9]{3})/", "$1 $2 $3", $phone);
            } else {
                return preg_replace("/([0-9]{4})([0-9]{4})/", "$1 $2", $phone);
            }
        } else {
// return  in  format xxxx-xxx-xxx

            if (strlen($phone) == 10 || strlen($phone) > 10) {
                return preg_replace("/([0-9]{2})([0-9]{4})([0-9]{4})/", "$1 $2 $3", $phone);
            } else {
                return preg_replace("/([0-9]{4})([0-9]{4})/", "$1 $2", $phone);
            }

        }

    }

    public static function getCatName($id)
    {
        $findIt = Profession::find($id);
        return $findIt->title;
    }

    public static function getTelephone($id)
    {
        $agent = Agent::find($id);
        if ($agent->phone != "") {
            return $agent->phone;
        } else {
            return "N/A";
        }
    }


    public static function getRegionName($id)
    {
        $allId  = unserialize($id);
        $getName = Region::whereIn('id', $allId)->get();

        foreach($getName as $regName){
            $allName[] = $regName->name;
        }
        

        return $allName;
    }

}
