<?php namespace App\Helpers;

use Guzzle\Http\Client;
use Symfony\Component\HttpFoundation\Request;

class AuspostAPIHelper {

    public static function suburb_search(Request $request) {
        $client = new Client('https://auspost.com.au');
        $request = $client->get('/api/postcode/search.json?q=' . $request['term'], ['auth-key'  => env('AUSPOST_AUTHKEY') ]);

        $response = $request->send()->json();

        $returnArray = [];
        foreach($response['localities']['locality'] as $locality) {
            if($locality['category'] == 'Delivery Area') {
                array_push($returnArray, $locality['location']);
            }
        }

        return json_encode($returnArray);
    }

}
