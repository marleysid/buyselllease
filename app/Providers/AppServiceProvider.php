<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);

		$this->app['db']->enableQueryLog();

		/*
		|--------------------------------------------------------------------------
		| Extend blade so we can define a variable
		| <code>
		| @define $variable = "whatever"
		| </code>
		|--------------------------------------------------------------------------
		*/

		\Blade::extend(function($value) {
			return preg_replace('/\@define(.+)/', '<?php ${1}; ?>', $value);
		});
	}

}
