<div class="row grey-background-holder">

    @if($agent->profile_image != '')
        <div class="col-md-3 col-sm-12"><div class="img-wrap-holder row imagelist" style="background: url('{{$agent->profile_image}}')">    <!--  <img src="{{$agent->profile_image}}"  alt="Photo of {{$agent->name}}" class="img-responsive"> --> </div></div>
    @else
        <div class="col-md-3 col-sm-12"><div class="img-wrap-holder row imagelist" style="background: url('{{asset('uploads/noimage.png')}}')">     <!-- <img src="{{asset('uploads/noimage.png')}}"  alt="Photo of {{$agent->name}}" class="img-responsive"> --></div></div>
    @endif
        
        <div class="col-md-5 col-sm-12 col-xs-12">
            <div class="detail-holder">
                <h4>{!! GH::limit_word($agent->trading_name,4) !!} <small></small>
                </h4><span class="location-line"><span class="glyphicon glyphicon-map-marker"> </span> {!! $agent->suburb!!}</span><p>{!! GH::limit_word($agent->description,50) !!}</p></div></div>

        <div class="col-md-2 col-sm-12 col-xs-12">
            <div class="logo-area-holder">

                @if($agent->logo_url != '')
                <img src="{{$agent->logo_url}}"  alt="Logo of {{$agent->name}}" class="img-responsive">
                    @else
                    <img src="{{asset('uploads/noimage.png')}}"  alt="Logo of {{$agent->name}}" class="img-responsive">
                    @endif
            </div>
        </div>
        <div class="col-md-2 col-sm-12 col-xs-12 agents-location-row">
            <div class="business-detail-wrapper-holder view-detail-cls-wrap-agents-detail">

                <p><i class="fa fa-phone"></i>&nbsp; {{GH::contact_format($agent->phone)}}</p>
                <a role="button" target="_new" href="{{$agent->listing_staff_url}}" class="btn btn-primary">VIEW AGENTS</a>
        </div>
            </div>
</div>



