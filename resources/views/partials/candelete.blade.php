@extends('layouts.master')

@section('header-style')
<link rel="stylesheet" href="{{{ asset('/css/master.css') }}}" type="text/css" />
<link rel="stylesheet" href="{{{ asset('/css/console.css') }}}" type="text/css" />
@overwrite

@section('content-body')
    <body>
    <div id="content-subheader">
        <div class="row title-row">
            <div class=col-xs-1">
                @yield('content-back');
            </div>
            <div class="col-xs-4">
                @yield('content-title','<h2 class="title">Administration Console</h2>')
            </div>
        </div>
    </div>
    </body>