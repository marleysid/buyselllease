<style>
    .withloading_img {
        background: black url(../img/loading.gif) center center;
        background-repeat: no-repeat;
        overflow: hidden;
    }
</style>
<div class="row main-search-box-holder">
    <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12">
        <div class="img-wrap-holder">
            <img alt="Photo of {{ $property->address }}" class="withloading_img img-responsive img-full-widht-wrap" height="180" src="{{ \App\Models\PropertyHelper::getData($property, 'img_m') }}" width="270"/>
        </div>
    </div>
    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
        <div class="business-deatil-wrapper-holder-search">
            <h4>
                {{ GH::limit_character( \App\Models\PropertyHelper::getData($property, 'headline'),40) }}
            </h4>
            <span>
                <span class="glyphicon glyphicon-map-marker">
                </span>
                {{GH::removeCommaFromLastOfString($property->address) }}
            </span>
            <p>
                {{ GH::limit_character( \App\Models\PropertyHelper::getData($property, 'description'),140) }}
            </p>
            @if(\App\Models\PropertyHelper::getData($property, 'priceView') != "")
            <span>
                <span class="glyphicon glyphicon-tag">
                </span>
                {{ \App\Models\PropertyHelper::getData($property, 'priceView') }}
            </span>
            @endif

            @if(Auth::check() && Auth::user()->role !='1')
            <div class="view-item">
                <span class="inline faworite-ico">
                    <a class="{{GH::isFavourite($property->property_code)?'remove-favourite-property':'add-favourite-property'}}" href="javascript:;" pcode="{{$property->property_code}}">
                        <span class="glyphicon glyphicon-heart-full glyphicon-heart-empty {{ GH::isFavourite($property->property_code)=='1' ? 'make-red' : ' heart-deafult-color' }}">
                        </span>
                    </a>
                </span>
            </div>
            @endif
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <div class="search-business-logo-holder">
            <img alt="{!! isset($property->agent()->first()->name)?$property->agent()->first()->name:'' !!} Logo" class="img-responsive" height="180" src="{!!isset( $property->agent()->first()->logo_url)?$property->agent()->first()->logo_url:''!!}" width="180">
            </img>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <div class="search-detai-wrap">
            <p>
                <span class="glyphicon glyphicon-user">
                </span>
                {{ \App\Models\PropertyHelper::getData($property, 'listingAgent.name') }}
            </p>
            <p>
                <span class="glyphicon glyphicon-earphone">
                </span>
                <?php
                $telephone = \App\Models\PropertyHelper::getData($property, 'listingAgent.telephone') ;
                if($telephone != ""){
                    echo GH::contact_format($telephone);
                } else {
                    $tel = GH::getTelephone($property->agent_id);
                    echo GH::contact_format($tel);
                }
                ?>
            </p>
            <a class="view-poperty-button" href="{{ \App\Models\PropertyHelper::getURL($property) }}" target="_blank">
                <span class="glyphicon glyphicon-new-window">
                </span>
                view Property
            </a>
        </div>
    </div>
    <div class="properties-box-holder-wrap">
        <div class="bg-wrap-list-holder-sort-list">
            <img alt="" src="{{ asset('img/bg-back-list-type.png') }}"/>
        </div>
        <div>
            <span class="inline prop-num">
                {{ $property->bedrooms }}
            </span>
            <span class="inline prop-ico">
                <img alt="Bedrooms Icon" class="img-responsive" src="{{ asset('img/icon-bed.png') }}"/>
            </span>
        </div>
        <div class="">
            <span class="inline prop-num">
                {{ $property->bathrooms }}
            </span>
            <span class="inline prop-ico">
                <img alt="Bathrooms Icon" class="img-responsive" src="{{ asset('img/icon-bath.png') }}"/>
            </span>
        </div>
        <div class="">
            <span class="inline prop-num">
                {{ $property->parking }}
            </span>
            <span class="inline prop-ico">
                <img alt="Parking Icon" class="img-responsive" src="{{ asset('img/icon-car.png') }}"/>
            </span>
        </div>
    </div>
</div>
