@foreach($deals as $dea)
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
     @if(file_exists(public_path('uploads/admin/advertisement/' . $dea->image)))
       <a href="{{ 'http://'.$dea->link }}" target="_blank">

           <img src="{{ asset('uploads/admin/advertisement/' . $dea->image) }}"  alt="Buy Sell Lease" class="img-thumbnail img-responsive" style="width: 260px; height: 177px;">
       </a>
       @endif
    </div>
@endforeach
