
<div class="collapse navbar-collapse" id="navbar">
    <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ route('home.howitworks') }}">How it works</a></li>
        <li><a href="{{ route('home.aboutus') }}">About us</a></li>
        <li><a href="{{ route('home.contactus') }}">Contact</a></li>


<!--start of if-->
        @if (Auth::check())
            @if(Auth::user()->role != '1'  )
                <li>
                    <a href= "@if (Auth::user()->role == '3')
                    {{route('dashboard.profile') }}
                    @else
                    {{ URL::to('user/changepassword') }}
                    @endif
                            ">
                        Edit Profile
                    </a>
                </li>

                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
            @else
                <li><a href="{{ url('/auth/login') }}">Login</a></li>
            @endif
        @else

            <li><a href="{{ url('/auth/login') }}">Login</a></li>
            @endif



        <li class="social fb-btn"><a  target="_blank" href="https://www.facebook.com/buyselllease.au"><img src="{{ asset('/img/icon-fb.png') }}" alt="Facebook"></a></li>
        <li class="social"><a target="_blank" href="https://twitter.com/buyselllease_au"><img src="{{ asset('/img/icon-tweeter.png') }}" alt="Twitter"></a></li>
        <li class="social"><a target="_blank" href="https://plus.google.com/u/1/+BuysellleaseAu/about"><img src="{{ asset('/img/icon-g-plus.png') }}" alt="Google +"></a></li>
    </ul>
</div>

