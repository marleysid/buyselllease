<div class="sub-navbar navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
@if (Auth::check())
        <li><a href="{{ route('home.index') }}">New search</a></li>
        <li><a href="{{ route('dashboard.favourites') }}">Favourites</a></li>
        <li><a href="{{ route('dashboard.searches') }}">My searches</a></li>
            @if (Auth::user()->role == '3') <li><a href="{{ route('dashboard.profile') }}">My account</a></li> @endif
        <li><a href="{{ URL::to('user/changepassword')  }}">Change Password</a></li>
@endif
    </ul>
</div>
