<div class="row item-row" class="item-list-sec">
    <div class="col-lg-8 col-md-8 col-sm-12 desc-row">
        <div class="pic-slider">

            <img src={{ \App\Models\PropertyHelper::getData($property, 'img_m') }}  height="180" width="270" alt="Photo of {{ $property->address }}" class="img-responsive">
        </div>
        <div class="details-item">
            <h4>{{ \App\Models\PropertyHelper::getData($property, 'headline') }}</h4>


            <span class="location-line"><span class="glyphicon glyphicon-map-marker"> </span> {{ $property->address }}</span>
            <div class="properties-box">
                <span class="inline prop-num">{{ $property->bedrooms }}</span>
                <span class="inline prop-ico"><img src="{{ asset('img/icon-bed.png') }}" alt="Bedrooms Icon" class="img-responsive"></span>
                <span class="inline prop-num">{{ $property->bathrooms }}</span>
                <span class="inline prop-ico"><img src="{{ asset('img/icon-bath.png') }}" alt="Bathrooms Icon" class="img-responsive"></span>
                <span class="inline prop-num">{{ $property->parking }}</span>
                <span class="inline prop-ico"><img src="{{ asset('img/icon-car.png') }}" alt="Parking Icon" class="img-responsive"></span>
            </div>
            <p>{{ \App\Models\PropertyHelper::getData($property, 'description') }}</p>
            <div class="guide-item">
                <span class="inline tag-ico"><span class="glyphicon glyphicon-tag"></span></span>
                <span class="inline guide-txt">{{ \App\Models\PropertyHelper::getData($property, 'priceView') }}</span>
            </div>
            <div class="view-item">
                <span class="inline faworite-ico"><a href="{{ Route('profile.add-favourite', array($property->property_code) ) }}" class=""><span class="glyphicon {{ \App\Models\PropertyHelper::isFavourite($property) ? 'glyphicon-heart-full' : 'glyphicon-heart-empty' }}"></span></a></span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 location-row">
        <div class="img-gmap">
            <div class="seller-info">
                <span class="info-row"><span class="glyphicon glyphicon-user"></span> {{ \App\Models\PropertyHelper::getData($property, 'listingAgent.name') }}</span>
                <span class="info-row"><span class="glyphicon glyphicon-earphone"></span> {{ \App\Models\PropertyHelper::getData($property, 'listingAgent.telephone') }}</span>
            </div>
            <div class="inline"><a class="btn btn-primary" href="{{ \App\Models\PropertyHelper::getURL($property) }}" target="_new" role="button"><span class="glyphicon glyphicon-new-window"></span> VIEW PROPERTY</a></div>
        </div>
        <div class="company-data">
            <div class="logo-box">
                <img src="{{ $property->agent()->first()->logo_url }}" alt="{{ $property->agent()->first()->name }} Logo" height="180" width="180" class="img-responsive">
            </div>
        </div>
    </div>
</div>
