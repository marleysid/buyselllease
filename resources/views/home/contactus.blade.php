@extends('layouts.default.master')
@section('body-class', 'contact-body')
@section('content-body')

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">
            </div>

            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 contact-form">
                <h1>Contact Us</h1>
                @if ( Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                
                <div class="col-md-12">
                    <div class="default-transparent">
                        <form role="form">
                            <div class="form-group">
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <input type="text" class="form-control" name="name" placeholder="Name">
                                             <i class="glyphicon glyphicon-user"></i>
                                     </span>
                                </div>
                                 <span class="error">{!!$errors->first('name')!!}</span>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                         <i class="glyphicon glyphicon-envelope"></i>
                                     </span>
                                </div>
                                 <span class="error">{!!$errors->first('email')!!}</span>
                            </div>

                            <div class="form-group ">
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <input type="text" class="form-control" id="phone" name="phone" placeholder="Your phone number">
                                         <i class="glyphicon glyphicon-phone-alt"></i>
                                     </span>
                                </div>
                               <span class="error">{!!$errors->first('phone')!!}</span>
                            </div>

                            <div class="form-group ">
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <input type="text" class="form-control" id="address" name="address" placeholder="Your address">
                                         <i class="glyphicon glyphicon-globe"></i>
                                     </span>
                                </div>
                                <span class="error">{!!$errors->first('address')!!}</span>
                            </div>

                            <div class="form-group ">
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <textarea name="msg" class="form-control " rows="4" cols="78" placeholder="Enter your message here"></textarea>
                                         <i class="glyphicon glyphicon-pencil"></i>
                                     </span>
                                </div>
                               <span class="error">{!!$errors->first('msg')!!}</span>
                            </div>
                            <div class="btn-row">
                                <button type="submit" id="send-btn" class="btn btn-primary">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12 address-box">
                    <div class="default-transparent">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3>SALES</h3>
                                <a href="mailto:sales@buyselllease.com.au">sales@buyselllease.com.au</a>

                                <p>Tel 02 8960-7277</p>
                            </div>
                            <div class="col-sm-6">
                                <h3>ADMIN</h3>
                                <a href="mailto:admin@buyselllease.com.au">admin@buyselllease.com.au</a>
                                <p>Tel 02 8960-7277<br><br></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
@stop

