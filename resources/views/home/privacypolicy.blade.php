@extends('layouts.default.master')

@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>Privacy Policy</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <p>
                <h4>Preamble</h4>

                <p>Buy Sell Lease, BSL ( “Our”, “we” or “us”) value and appreciate your use of the Services of BSL,
                    and we are committed in protecting your personal information and respect your Privacy.</p>

                <p>Our Privacy Policy is for all Users, Real Estate Agents and Other Real Estate Professionals who access and use our services.</p>

                <p>We are bound by the National Privacy Principles in the Privacy Act 1988 (Cth) with regard to the handling of personal information, that relates to you
                    under that Act.</p>

                <p>We may, at any time, review and update our Privacy policy, in accordance with any new or changes in law,
                    changes to technology and/or our operations.
                    All personal information held by us will be governed by us in accordance with our Privacy Policy as displayed on our website, at the time.</p>

                <h4>Personal Information</h4>

                <p>In providing a service to you, BSL collect information to create a direct synergy and relationship between Users,
                    Real Estate Agents and Other Real Estate Professionals for the purpose of dealing in the buying, selling and/or leasing of real estate.</P>

                <p>All information is information that you enter within the Search Criteria or for the purpose of using any of the services of BSL.</p>

                <h4>Credit Card Information</h4>

                <p>BSL provide a free service to Users. Users should not provide any Credit details to any Service Provider such as a Real Estate Agent or Other Real
                    Estate Professional,
                    in the context of using the services of BSL.</p>

                <p>In respect of Real Estate Agents and Other Real Estate Professionals, BSL do not record complete credit card details after any transaction,
                    nor do we share any credit card information with any third party.</p>

                <h4>Use of Information</h4>

                <p>We usually collect information relating to your name, address, email address and telephone number(s) as well as details
                    relating to your enquiry or service regarding real estate services. </p>

                <p>We do not collect sensitive information and personal information,
                    however we may collect general information for the purposes of industry related statistics and/or improving our service,
                    although this information will relate to real estate and property and not of your personal information.
                    If, for any reason, we wish to collect your sensitive information for publication or sharing with a third party, we will seek your consent for it.</p>

                <p>Information that you place or share through BSL is for the purpose of real estate services, and as such,
                    any information that you create and make willing and able to share, is provided through your direct synergy from content and information from your
                    website,
                    and not for the purpose of our Website or Material.</p>

                <p>We will not share your information for the use of any third party for any purpose, without your consent.</p>
                <h4>Usage Logs</h4>

                <p>When you use BSL, your computer communicates a variety of information to computer servers, including the existence of cookies,
                    your Internet Protocol address, and information about your browser program. We also create a record of what information you are requesting.</p>


            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <h4>Calls</h4>

                <p>Calls to BSL or to third parties through BSL may be recorded or monitored for quality assurance and customer service purposes.
                    If a call is recorded between you and a third party, it will be deemed private information of both you and the third party,
                    and may be released to either of you.</p>

                <h4>Security</h4>

                <p>Personal information will be processed and stored by BSL in databases hosted in Australia.
                    BSL has taken reasonable steps to protect the information you share with us, including,
                    but not limited to, setup of processes, equipment and software to avoid unauthorised access or disclosure of this information.</p>

                <p>However, you acknowledge that the security of online transactions and the security of communications sent by electronic means or post,
                    is not and cannot be guaranteed. BSL will not accept responsibility for misuse or loss of, or unauthorised access to,
                    your personal information where the security of information is not within our control.</p>

                <p>If you suspect any misuse or loss of, or unauthorised access to, your personal information, please let us know immediately.</p>

                <h4>Third Party Web Sites</h4>

                <p>Through the services of BSL, you will link to the websites of real estate agents and other real estate professionals.
                    This will involve the distribution of your search criteria directly to these real estate services.
                    BSL’s Privacy Policy does not extend to these third party and external Web Sites.
                    These third party Web sites may collect personal information about users on those Web sites and third parties.
                    Please refer to the Privacy Policies of these third parties.</p>

                <h4>Public Information</h4>

                Any information posted on our Facebook, Twitter, Blogs, and any other form of social media, by you, becomes public information.
                BSL cannot guarantee the security of the content displayed or disseminated on any public forum.</P>

                <h4>Cookies</h4>

                <p>Cookies are bits of electronic information that can be transferred to a computer to uniquely identify the computer’s browser.
                    When you use BSL, we may place one or more cookies on your computer. Cookies help us improve the quality of our services to you,
                    by tracking usage patterns, or storing information that you may wish to re-visit and re-use.
                    You may, at any time, adjust the settings on your browser to refuse the cookies, however, if you disable cookies, many of the features of BSL, may
                    not work correctly.

                <h4> Merger or sale</h4>

                <p>If and when BSL is involved in a merger, acquisition or any form of sale of some or all of its business, personal information may be transferred
                    along with the business.</p>


                <h4>Email Management</h4>

                <p>You may receive e-mail from BSL for different reasons. You will be given the option to unsubscribe from e-newsletters and other marketing or
                    promotional material, by clicking on the unsubscribe link at the base of the email message

                    Contact BSL

                    If you wish to access your information or if you have any questions about this Privacy policy, or the Privacy practices of BSL, please email us at
                    privacy@buyselllease.com.au. We will ask you to verify your identity. We may charge you a fee to cover the costs of meeting your request.


                </p><br/><br/>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

            </div>
        </div>
    </div>
@stop
