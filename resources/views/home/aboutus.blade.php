@extends('layouts.default.master')
@section('body-class', 'about-us-body')
@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>About us</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <h3>Buy Sell Lease has been created as a result of our Managing Director’s 20 year career within the real estate industry,
                    as well as personal experiences searching for property, people and real estate professionals.</h3>
                <p>&nbsp;</p>

                <p>Technology has improved our ability to be informed and our ability to save time. <br />
                    Buy Sell Lease goes further than any other real estate
                    portal in delivering instant results directly from the webpages of the real estate industry.</p>
                <p>Marketing is an essential part of real estate. We, at Buy Sell Lease, have and continue to create,
                    the most cost efficient, time efficient and direct form of real estate marketing for both consumers and the real estate industry.</p>
                <p>&nbsp;</p>

                <h4>Why we are different?</h4>

                <p>Buy Sell Lease is not a directory of property. Our process of finding property, real estate agents and other real estate professionals,
                    is orchestrated in a most efficient and timely manner, saving you valuable time and resources in informing you of the latest in property listings,
                    people in real estate, real estate services as well as other real estate professionals. In a few short minutes, you can register to use Buy Sell Lease and open your direct line of communication with every aspect of real estate. Whether you are looking to buy or lease a property, whether it is residential, commercial, industrial, rural, retail, investment, development site,
                    sell or lease a property, and/or require the services of other real estate professionals such as legals, finance, insurance, valuations, surveyors,
                    pest inspectors, building inspectors and the like, Buy Sell Lease connects you with property, people and services, within minutes.</p>

                <p>Your returned searches are stored in your profile. Click on a result and you are immediately entering the website of the business offering the property and/or service you are seeking.</p>

                <p>As you enter in a business website, you are ensured that you are viewing the most indepth content relevant to your search,
                    displaying all photo’s, video’s, floorplans, personnel profiles and detailed services.</p>
                <p></p>Essentially, your search tells us what you are seeking, we find it and deliver it directly to you – in minutes,
                allowing you to make informed decisions and saving you considerable and valuable time.<br /></p>

                </p>

            </div>
            <div class="pict-col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <img class="img-responsive" src="{{ asset('/img/logo-keys.jpg') }}" width="" 100%" >
                <h2>We believe that we have<br />
                    created ONE portal for all<br />
                    of your real estate needs.</h2>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bgs-about.jpg') }}" width="" 100%" ></section>
@stop
