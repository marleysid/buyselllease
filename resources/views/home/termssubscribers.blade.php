@extends('layouts.default.master')
@section('body-class', 'subscribersterms-body')
@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>Subscribers Service Agreement</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 sidebar">
                <ul>
                    <li><a href="terms-of-use">USERS</a></li>
                    <li class="active"><i class="glyphicon glyphicon-arrow-right"></i><a href="terms-subscribers">SUBSCRIBERS</a></li>
                </ul>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 subscribers-content">
                <h2>Terms & Conditions </h2>
                <h4>Preamble</h4>
                <p>
                    The Terms & Conditions herein and also included in other Supporting Agreements relate to the Use of Buy Sell Lease’s Website, App and Service (BSL)
                    by a Real Estate Agent, and Other Real Estate Professional (YOU) with the consent of the Real Estate Agency &/Or Other Employer/Contractor of Other
                    Real Estate Professional (Agency) that they contract to or are employed by. By entering into this Agreement, you agree to be bound by our
                    Terms & Conditions and other Supporting Agreements. Definitions are located at the end of these Terms & Conditions.</p>
                <p>
                <div class="decimal-list">
                    <ol>
                        <li><h3>Parties to the Agreement</h3>
                            <ol>
                                <li>
                                    <h5>These Terms & Conditions create a legally binding Agreement and obligation between:</h5>
                                    <ul class="sublist">
                                        <li>Buy Sell Lease (BSL)</li>
                                        <li>YOU (As an Individual or Company)</li>
                                        <li>AGENCY (that employs or contracts YOU)</li>
                                    </ul>
                                </li>
                            </ol>
                        </li>
                        <li><h3>User Terms & Conditions also apply</h3>
                            <ol>
                                <li>
                                    <h5>These Terms and Conditions are to be read in conjunction with the “User Terms & Conditions” ,
                                        that apply for the User Use of Buy Sell Lease Synergy, together with the Privacy Policy of BSL.
                                        Acceptance of these Terms & Conditions is also acceptance of the Supporting Agreements defined herein, and in reference
                                        to this Agreement the term “Terms & Conditions” also includes other Supporting Documentation previously mentioned.</h5>
                                </li>
                                <li>
                                    <h5>In the event of any inconsistency or discrepancy between these Terms & Conditions and the 	User Terms & Conditions,
                                        these Terms & Conditions shall take priority and prevail.</h5>
                                </li>

                            </ol>
                        </li>
                        <li><h3>Notification of Changes</h3>
                            <ol>
                                <li>
                                    <h5>All Terms & Conditions & Supporting Agreements of BSL may be changed by BSL at any time and without Notice to any other party.</h5>
                                </li>
                                <li>
                                    <h5>It is your sole responsibility to periodically check our Terms & Conditions which are 	current as of the date they appear on our Website.
                                        Continuation of the use of our Site and Services by you, deem you to have accepted our Terms & Conditions as changed.</h5>
                                </li>

                                    <ol>
                                        <li>
                                            <h5>In the event that you do not agree with any of our Terms & Conditions, you must cease to use our Services immediately and
                                                notify us in writing as to the reason for your objection or issue(s) thereof. This event does not automatically render your Agreement
                                                void and cancelled permitting you to receive a refund. All decisions relating to the outcome of your objection and whether a refund
                                                of any unused pro-rata fee for the term is warranted, is at our sole discretion</h5>
                                        </li>
                                        <li>
                                            <h5>BSL reserve the right to withdraw, change or upgrade our Website or App, products or services ( including format, design, content, name, scope, etc)
                                                at any time. We will use reasonable endeavours to notify you in advance of any proposed changes that we make to BSL.
                                                If we make material changes, that disable the use of BSL by your Agency, you may terminate this Agreement by giving us written notice within 14 days of the change.
                                                Failure to terminate this Agreement within this period will constitute acceptance of the changes(s).</h5>
                                        </li>

                                    </ol>

                                </li>

                            </ol>
                        </li>
                        <li><h3>Provision of Service</h3>
                            <ol>
                                <li>
                                    <h5>Subject to these Terms & Conditions, and in consideration of the payment of Fees when due, 	BSL,
                                        agree to provide the service to you that allows a direct synergy and communication between users and you for the purpose
                                        of real estate services in connection with buying, selling and leasing of property, that are undertaken in the ordinary
                                        course of your everyday business. Each physical addressed office requires its own individual subscription.</h5>
                                </li>
                                <li>
                                    <h5>You may not use BSL services except as expressly stated in these Terms & Conditions</h5>
                                </li>
                                <li>
                                    <h5>You must upload and display on your Agent Profile on your Agency’s  website, audio visual electronic window display and e-brochures,
                                        any material that we forward you from time to time, to market and create awareness of Buy Sell Lease.</h5>
                                </li>
                                <li>
                                    <h5>You agree that:</h5>
                                    <ul class="sublist">
                                        <li>You will ensure and maintain that access to your subscription and the services provided by BSL will remain secure and confidential with
                                            only you and any assistants that you have in your personal/corporate employ.</li>
                                        <li>Your use of BSL is limited to providing information & professional services to users with regard to the purchase,
                                            sale and/or leasing of real estate.</li>
                                        <li>You may only promote real estate services and property that are currently available for sale and/or lease and that you have written
                                            Authority to act in the sale and/or lease of that property as prescribed by Property, Stock and Business Agents Act, 2002 and any other
                                            relevant Act and/or Regulation in your State/Territory.</li>
                                        <li>Any type of Registration and/or Licencing that is required in your State/Territory or within Australia that enables you to provide
                                            the service that you set out to perform     ( Real Estate and Other Real Estate Professional Services) is current at the time of this
                                            Agreement and will be kept renewed by you at all material times that you use the services of BSL.</li>
                                        <li>You will comply with the applicable laws , Codes,  Acts & Regulations, of your Industry profession, in relation to this Agreement,
                                            including and ensuring that any fee
                                            that you may elect to charge a client  as an administration fee as a result of this service is capped at a fixed fee and no more than:
                                            <ul class="sub4">
                                                <li>Properties for Sale - $110.00 Including GST in total/per property per annum</li>
                                                <li>Properties for Lease - $55.00 Including GST in total per property per annum</li>
                                            </ul>

                                        </li>
                                        <li>You agree not to reproduce, modify, distribute, display or otherwise provide access to, create derivative works from, decompile,
                                            disassemble or reverse engineer any portion of the Services.</li>
                                        <li>You agree not to distribute or post spam, chain letters, pyramid schemes, or similar communications through the services of BSL</li>
                                        <li>You agree not to impersonate another person and/or business/company or misrepresent your affiliation with another person or entity.</li>
                                        <li>Except as expressly stated herein, these Terms & Conditions do not provide you a licence to use, reproduce, distribute,
                                            display or provide access to any portion of the Services on third-party Web sites or otherwise.</li>
                                        <li>You may not share your BSL Account with others. You are responsible for all actions taken via your account.
                                            BSL will treat your use of the Services in accordance with our Privacy Policy.</li>
                                        <li>You agree to disclose these Terms & Conditions to the Agency in which you are employed or Contract to,
                                            as well as any person(s) such as all team members, administration and assistants, within your team.</li>
                                        <li>You agree to provide professional and accurate material, to be used through the service of BSL, to users, at all times.</li>
                                        <li>You agree to consider, respond and treat all user leads in a professional manner and in good faith.</li>
                                        <li>You and the Agency  in which you are employed or contracted to, if requested to by BSL, will use your best endeavours to actively promote
                                            BSL on your website and pages and in promotional and marketing material distributed by you.</li>
                                        <li>You agree that BSL may share & publish data such as number of properties listed in any particular area, sales effected, etc.
                                            to formulate relevant industry data for the use of BSL or other industry data creator, for the purpose of providing industry knowledge and background.
                                            This information, is information only disseminated from information that you make available through the use of BSL,
                                            and in so doing, you agree for that information to be utilised for the purposes herein.</li>
                                        <li>You agree that all data and traffic statistics and information with regard to the use of BSL,
                                            remains the sole property and right of BSL. Your use of BSL, allows the rights of use, only for the period of your Agreement,
                                            and no proprietary right is advanced to you.</li>
                                        <li>You agree that BSL has the right to Suspend or Terminate your Service Agreement with BSL, immediately,
                                            as a result of any poor or unacceptable conduct or representations, you and/or your Agency may make in your use of BSL,
                                            that we consider detrimental to our Brand, your competitors name and/or brand, or used in any other inappropriate and unprofessional manner,
                                            and that this may be without Notice to you.</li>
                                        <li>You agree that BSL may seek punitive damages as against you, for breaches or damage caused to BSL as governed by State and Federal Laws and Jurisdictions.</li>
                                        <li>You agree that the place of any adjudication, mediation, or legal proceedings, is in the City and State of BSL, where this Agreement is made, which is Sydney, NSW Australia.</li>
                                        <li>You may not create a link to any page of BSL’s Website without our prior written consent.
                                            If you do create a link to any page of our website, you do so at your own risk and the exclusions and limitations
                                            set out above will apply to your use of this website linking to it.</li>
                                    </ul>
                                </li>
                                <li>
                                    <h5>We reserve the right ( but have no obligation) to review, modify, reformat, reject or remove (or direct you to perform such actions),
                                        any material that you make available through the services of BSL.</h5>
                                </li>
                            </ol>
                        </li>
                        <li><h3>Fees & Payments</h3>
                            <ol>
                                <li>
                                    <h5>Unless otherwise agreed by BSL in writing, a set up fee will apply for new Agreements and in the event that a renewal of an
                                        Agreement is sought after the period of the renewal has expired, which is the termination date of any current Agreement</h5>
                                </li>
                                <li><h5>All Fees are paid in advance annually and in full before the commencement of any activity or service being provided by BSL.
                                        Receipt of any payment is further evidence of Acceptance of our Terms & Conditions by you.</h5></li>
                                <li><h5>BSL reserve the right to vary any payment terms with any Real Estate Agent, other Real Estate Professional, or Advertiser,
                                        agreed with in writing, which does not alter or provide any rights of equivalence for any other Agreement with any other client.</h5></li>
                                <li><h5>If any Agreement is made on Account, payment must be within 7 days of date of the invoice.</h5></li>
                                <li><h5>All Tax Invoices issued will contain full particulars including client details, service provided as well as
                                        Payment details for electronic payment and/or cheque details. Payment is only accepted as received in full when there is confirmation
                                        from our financial institution of cleared funds in our account. All Tax Invoices will set out the amount of the GST payable.</h5></li>
                                <li><h5>You must pay GST on a Taxable Supply made to You under this Agreement. You must account and pay the GST
                                        in the same manner and at the same time You pay the consideration for the Taxable Supply to which the GST relates.</h5></li>
                                <li><h5>Price Guarantee	Buy Sell Lease may offer you the following Price Guarantee to subscribers of our real estate services,
                                        only on the basis, that each subsequent renewal of subscription is completed, received and paid for, prior to the expiration
                                        of the Initial and First Service Agreement that is current at that time, for five consecutive years.</h5>
                                        <h5><b>For real estate Agents</b></h5>
                                        <p>Each year in the 5 year period	Subscription	<b>$5,000 + GST p.a. per office</b></p>
                                        <h5><b>For other real estate Professionals</b></h5>
                                        <p>Each year in the 5 year period	Subscription	$3,000 + GST p.a. per office</p>
                                </li>

                                <li><h5>In the event that a renewal of the subscription of Buy Sell Lease is not completed, received and paid for,
                                        prior to the expiration of the Initial and First Service Agreement, Buy Sell Lease will charge Subscription Fees relevant to
                                        New Subscribers at the time of the renewal request beyond the Initial and First Service Agreement period.
                                    </h5>
                                </li>

                            </ol>
                        </li>
                        <li><h3>Breaches and Failure to pay</h3>
                            <ol>
                                <li><h5>If you breach any of these Terms & Conditions relating to BSL, including failure to pay for a Service provided,
                                    or suffer an Act of Insolvency (as defined in clause 6.2), BSL may at our sole discretion and without limitation:</h5>
                                    <ul class="sub3">
                                        <li>Cancel any Agreement and related service to you, without notice to you.</li>
                                        <li>Amend our terms of payment that may include a variation to your periodic payment frequency, periodic payment amount, and the period of advanced payment – if any</li>
                                        <li>Charge interest on all overdue amounts at the rate of 2% above OUR financial institution Overdraft Base Rate</li>
                                        <li>Charge an administration and collection fee of $220 for each default of payment</li>
                                        <li>In addition, Charge an amount of $55.00 for each Cheque that is presented to BSL and Dishonoured by our financial institution,
                                            to cover our banking fees and administrative costs. In an instance of a second Dishonoured Cheque, we reserve the right to terminate the
                                            Agreement, and, if agreed to, BSL will insist on future payments of cash or direct deposits only.</li>
                                        <li>Take proceedings against you for any outstanding amounts</li>
                                        <li>Recover BSL’s costs including mercantile agency and legal costs on a full indemnity basis</li>
                                        <li>Exercise any other rights at law.</li>
                                        <li>In the event of non-payment, we reserve the right to take such action to recover the entire or outstanding amount representative of your Agreement with BSL.</li>
                                    </ul>
                                </li>
                                <li><h5>You suffer an “Insolvency Event”, if:</h5>
                                    <ul class="sub3">
                                        <li>You are a natural person and commit an act of bankruptcy; or</li>
                                        <li>You are a Body Corporate and cannot pay your debts as and when they fall due or enter into an agreement with your
                                            creditors other than in the ordinary course of business or pass a resolution for administration, winding up or liquidation,
                                            or has a receiver, manager, liquidator or administrator appointed to any of its property or assets or has a petition presented for its winding up.</li>
                                        <li>A written statement of debt signed by an authorised employee of BSL is evidence of the amount owed by you to BSL.</li>

                                    </ul>
                                </li>
                            </ol>
                        </li>
                        <li><h3>Term of the Agreement</h3>
                            <ol>
                                <li><h5>All Agreements are for a period of twelve (12) months, unless otherwise agreed to by BSL in writing.</h5>
                                </li>
                                <li><h5>The commencement of your twelve (12) month service period will commence on the same day that you have been notified by email.</h5>
                                </li>
                                <li><h5>BSL will not be held liable for any time taken for your Webmaster to activate the redirect code. Any delay in this process is a matter between you,
                                        the Agency you work with, your Head Office  and the relevant Webmaster. There may be a fee associated and charged for this service by the Webmaster and even the Agency.
                                        We will not be held liable or responsible for any fees or work associated with insertion of a redirect code(s).</h5>
                                </li>

                            </ol>
                        </li>
                        <li><h3>Renewal</h3>
                            <ol>
                                <li><h5>BSL will forward you, a Renewal Notice, eight (8) weeks prior to the expiration of your current Agreement.</h5>
                                </li>
                                <li><h5>Your Renewal Agreement and Fee must be received by us in compliance with the Agreement and Tax Invoice, in full,
                                        two (2) weeks prior to the expiration of your 	current Agreement.</h5>
                                </li>
                                <li><h5>In the event that BSL has not received what is referred to in Clause 8.2, BSL reserve the right to terminate your service and synergy of BSL,
                                        without further notice, at midnight on the last day of your Agreement. This means ALL activity that BSL has/have/will provide to you,
                                        will be suspended and/or terminated.</h5>
                                </li>

                            </ol>
                        </li>
                        <li><h3>Copyright Notice, Trademark, Patent</h3>
                            <ol>
                                <li><h5>Copyright and other relevant intellectual property rights exist on all text relating to BSL’s services and the full content of our Website and App.</h5>
                                </li>
                                <li><h5>BSL logo and name is a registered Trademark in Australia and other countries, and is owned by Integral Property Australia Pty Ltd. and exclusively licenced to myrealestateapp Pty Ltd.</h5>
                                </li>
                                <li><h5>The Innovation and Concept of BSL is registered as a Patent in Australia and other countries, and is owned by
                                        Integral Property Australia Pty Ltd. and exclusively licenced to myrealestateapp Pty Ltd.</h5>
                                </li>

                            </ol>
                        </li>
                        <li><h3>Acknowledgement</h3>
                            <ol>
                                <li><h5>You acknowledge and agree that you are solely responsible for the information, data and other content, you allow users to access through BSL.</h5>
                                </li>
                                <li><h5>You acknowledge that you have the authority to offer the services, products and information, through BSL, from relevant third parties with a proprietary interest.</h5>
                                </li>
                                <li><h5>You acknowledge that each provision of the Terms & Conditions are individually severable. If any provision becomes illegal,
                                        unenforceable or invalid in any jurisdiction, it is to be treated as being severed from this Agreement in the relevant jurisdiction,
                                        but the rest of this Agreement will not be affected. The legality, validity and enforceability of the provision in any other jurisdiction will not be affected.</h5>
                                </li>
                                <li><h5>You acknowledge that BSL may need to terminate or replace our third party contractor from time to time, at our discretion,
                                        which may be on short notice. BSL will use all reasonable endeavours to ensure the continuity of your service by BSL,
                                        however, we do not make, nor imply, any guarantee that your service will continue or will continue uninterrupted.</h5>
                                </li>
                                <li><h5>You acknowledge that BSL is provided to you on an “as is” basis and, to the extent permitted by law, we ( and our contractor),
                                        exclude all warranties and representations in relation to BSL, and our contractor excludes and limits its liability on the same terms.</h5>
                                </li>
                                <li><h5>You acknowledge that this Agreement may not be assigned to any other party including your own Body Corporate and person.</h5>
                                </li>
                            </ol>
                        </li>
                        <li><h3>Transfer & Suspension</h3>
                            <ol>
                                <li><h5>Neither you nor the Agency may assign, sub-licence or transfer (or attempt to assign, sub-licence or transfer) its rights or obligations under this Agreement
                                        ( including the right to receive the Service of BSL) to any other party ( including a related Entity) without the prior written consent of BSL.
                                        Any assignment or sub-licence without the prior written consent of BSL shall be null and void. This Agreement and
                                        Supporting Agreements may be assigned by BSL without the consent of you or the Agency</h5>
                                </li>
                            </ol>
                        </li>
                        <li><h3>Indemnity, Disclaimer of Warranties and Limitation of Liability</h3>
                            <ol>
                                <li><h5>To the extent permitted by law, BSL exclude all conditions and warranties that our site and App Synergy will be uninterrupted or error free.
                                        To the extent that our liability for breach of any implied warranty or condition cannot be excluded by law our liability will be limited to the re-supply of those services.
                                        BSL is not liable for any damages or loss from any Internet or Telco failure.</h5></li>
                                <li><h5>In no circumstances, will BSL be liable to you for any indirect, incidental, special and/or consequential loss or damages
                                        (inc. loss of profits, goodwill, data or opportunity). For products or services supplied by us, our liability to you will be
                                        limited to the amount paid by you in respect of those products or services.</h5></li>
                                <li><h5>You agree to indemnify and hold BSL and our contractor(s), harmless against any expenses, costs, loss or damage that we may suffer
                                        or incur as a result of or in connection with your use or conduct on connections with BSL or this Agreement, including any breach of this Agreement by you.</h5></li>
                                <li><h5>You will continually indemnify BSL against any claim or proceeding that is made, threatened or commenced, and against any liability, loss, damage,
                                        cost or expense (including our legal and associated costs on a Full Indemnity Basis) that BSL incurs or suffers as a result of:
                                        (a) any claims brought about by or on behalf of any third party relating to information or content provided by you and displayed through BSL
                                        ( including claims in respect of misleading and deceptive conduct); and (b) any wilful, unlawful or negligent act or omission by you in the material that you display through BSL.</h5></li>
                                <li><h5>You warrant to BSL, that the publication of any material byyou through the services of BSL, does not breach or infringe:</h5>
                                    <ul class="sublist">
                                        <li>The Competition and Consumer Act, 2010 or equivalent State legislation;</li>
                                        <li>The Property Stock, Business Agents Act, 2002;</li>
                                        <li>Any Applicable law, Code ( including any common law, statute, delegated legislation, rule or ordinance of the Commonwealth, State or Territory)
                                            that applies to the Industry within which YOU are engaged;</li>
                                        <li>Any copyright, trademark, obligation of confidentiality or other personal or proprietary right, and you have received such written
                                            permission to expressly use the interest and rights of any third party body corporate, including their business name,
                                            logo and any registered and protected interests;</li>
                                        <li>Any law of defamation, obscenity or contempt of any court, tribunal or Royal Commission;</li>
                                        <li>State, Territory or Commonwealth Privacy Legislation or Anti-Discrimination Legislation;</li>
                                    </ul>
                                    <p><br />AND</p>
                                    <ul class="sublist">
                                        <li>You hold the relevant and applicable registration and/or licencing permitting you to trade within the industry profession of which you represent on BSL;</li>
                                        <li>You will not infringe upon any third party’s Intellectual Property Rights;</li>
                                        <li>You have the right to represent the third party Body Corporate or Business of which you say you are employed or contracted to, and that you are authorised to do so;</li>
                                        <li>You will not expressly state or imply any relationship or affiliation with BSL or endorsement by BSL except as expressly permitted by these Terms & Conditions;</li>
                                        <li>You will not expressly state or imply any relationship or affiliation with BSL or endorsement by BSL except as expressly permitted by these Terms & Conditions;</li>
                                        <li>You will not do anything that has, or is likely to have ( directly or indirectly), the effect of defaming,
                                            disparaging or adversely affecting the integrity or reputation of BSL, including by directly or indirectly placing or
                                            allowing the placement of offending content through the use of BSL.</li>
                                    </ul>
                                </li>
                                <li><h5>You acknowledge that You do not rely on any representation or advice made by or on behalf of BSL in connection with the services BSL provide.</h5></li>
                                <li><h5>You acknowledge and accept the risk that any communication to, from and through BSL, may be intercepted, used or modified by third parties;</h5></li>
                                <li><h5>BSL does not warrant that access to our Site and Service will be uninterrupted or error free or that BSL or any material accessible through
                                        BSL is free from errors or viruses, worms, Trojan horses, time bomb, cancelbot or other harmful components.</h5></li>
                                <li><h5>Except in relation to any of your material uploaded and administered through BSL,
                                        which you grant us a licence to deal with, you do not have any right, title or interest in or relation to our site.
                                        You may not use any material on BSL to establish, maintain or provide your own publications
                                        (including marketing or promotional material) or Internet Site. Nothing in this Agreement should be construed as granting any right of use in relation to any
                                        material or trademark displayed on the Site without the express written consent of the relevant owner.</h5></li>
                                <li><h5>BSL is made available to you for the exclusive purpose of advertising your listings directly to users and also using BSL as a business generating
                                        tool in “Connecting People with Property”. You must not use the software for any other purpose.</h5></li>
                                <li><h5>All database material remains the property of BSL. Subscription will allow the use of the synergy for you, only for the period that your Agreement is valid.</h5></li>

                            </ol>
                        </li>
                        <li><h3>Confidentiality & Privacy Statement</h3>
                            <ol>
                                <li><h5>You shall treat as confidential all information regarding BSL business and affairs that may come into your possession as a result of or in the performance of this Agreement.
                                        You must not disclose our confidential information to any third party without our written permission, unless required to do so by law.</h5></li>
                                <li><h5>BSL collects your personal information to provide the service to you and for invoicing purposes.
                                        BSL may disclose this personal information to its related bodies corporate, to credit reporting agencies and other third
                                        parties as part of the provision of the BSL service and for overdue accounts, to debt collection agencies to recover amounts outstanding and owing.</h5></li>
                                <li><h5>You may gain access to your personal information by writing to the Privacy Officer, Buy Sell Lease , P.O. Box 133, Double Bay NSW 2028..
                                        BSL Privacy Policy is available on www.buyselllease.com.au.</h5></li>
                                <li><h5>Each party will treat as confidential, and will procure that its employees, employer, contractors, treat as confidential and will not disclose,
                                        unless disclosure is required by law:</h5>
                                    <ul class="sub3">
                                        <li>The terms of this Agreement( including terms relating to Pricing, Payment Arrangements)</li>
                                        <li>Information generated for the performance of this Agreement that includes all data, reports and the like with regard to the performance of BSL.</li>
                                        <li>Any other information that ought to in good faith to be treated as confidential given the circumstances of disclosure of the nature of the information;</li>
                                        <li>Any information derived wholly or in part for any information referred to in (a) to (c) above.</li>

                                    </ul>
                                </li>
                                <li><h5>Each party agrees to take all reasonable precautions to prevent any unauthorised use, disclosure, publication or dissemination of the
                                        confidential information by or on behalf of itself or any third party.</h5>
                                </li>
                            </ol>
                        </li>
                    <li><h3>Termination</h3>
                        <h5>Without limiting our other rights and remedies at law, BSL may terminate this Agreement immediately:</h5>
                        <ul class="num-list">
                            <li><h5>At any time and for any reason by giving you 28 days notice in writing;</h5></li>
                            <li><h5>If you breach this Agreement and fail to rectify that breach within 7 days notice;</h5></li>
                            <li><h5>If you are bankrupt, insolvent, enter into liquidation, administration or receivership, or a receiver, or a receiver or manager is appointed over any or all of your assets; or</h5></li>
                            <li><h5>If you die, your partnership is dissolved, or your company is deregistered (as applicable).</h5></li>
                        </ul>
                    </li>
                    <li><h3>Definitions</h3>
                        <h5>AGENCY	means an Employer or Body Corporate that you are employed or contracted to, which can include a Real Estate Agency Business or Business described in Other Real Estate Professional.</h5>
                        <h5>Body Corporate	The term ‘body corporate’ is considered to cover any artificial legal entity having a separate legal personality. These entities have perpetual succesion, they also have the power to act, hold property, enter into legal contracts, sue and be sued in their own name, just as a natural person can.
                            The types of entities falling into these categories are broad, and include:</h5>
                        <ul class="sublist">
                            <li>trading and non-trading</li>
                            <li>profit and non-profit making organisations</li>
                            <li>government-controlled entities</li>
                            <li>other entities with less or no government control or involvement.</li>
                        </ul>
                        <h5>Included in the definition of body corporate are entities created by:</h5>
                        <ul class="sublist">
                            <li>common law (such as a corporation sole and corporation aggregate)</li>
                            <li>statute (such as the Australian Securities & Investments Commission)</li>
                            <li>registration pursuant to statute (such as a company, building society, credit union, trade union, and incorporated association).<br /><br />
                                <p>If an entity is not established under an Act of Parliament, or under a statutory procedure of registration, such as the Corporations Law or an Incorporation Act,
                                it is generally not a body corporate.</p>
                                <p><b>Other Real Estate Professional </b> means anyone, entity or Body Corporate engaged in business that relates to the real estate industry, and may include, but is not limited to:
                                    Legal Practitioners, Valuers, Insurance Agents, Strata Agents, Pest Report Businesses, Building Report Businesses, Depreciation report Businesses, Finance Agents, Accountants, and the like
                                </p>
                                <p><b>BSL </b>  means Buy Sell Lease and the Company that is licenced to Use and Trade under its name and with all of its Intellectual Property, known as Myrealestateapp Pty Ltd ( A.C.N 159 653 504)
                                </p>
                                <p><b>Real Estate Services </b>  means any act or business related to the Buying. Selling and Leasing of Real Estate including but not limited to those described in AGENCY and OTHER REAL ESTATE PROFESSIONAL.
                                </p>
                                <p><b>Service Agreement </b>  means this document which relates to Terms and Conditions for Real Estate Agents and Other Real Estate Professionals.
                                </p>
                                <p><b>Supporting Agreements </b>  means the collective Agreements of BSL being: Privacy Policy, User Terms & Conditions, Service Agreement – Terms & Conditions Real Estate Agent,
                                    Advertising Policy, and any other Policy that may be introduced to our website at any time, and without notice to anyone.
                                </p>
                                <p><b>USER </b>  means a consumer that is utilising the services of BSL for the purpose of Buying, Selling and/or Leasing real estate, commonly described,
                                    within the real estate industry as Vendors, Purchasers, Landlords and Tenants.
                                </p>
                                <p><b>YOU </b>  means a person and or a legal entity or Body Corporate, that is engaged in the real estate industry as described in Real Estate Services,
                                    for and on behalf of an Employer in the capacity of an Employee or a Contractor.
                                </p>
                            </li>
                        </ul>
                    </li>

                    </ol>

                </div>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/faq-bg-agents.jpg') }}" width="" 100%" ></section>



@endsection