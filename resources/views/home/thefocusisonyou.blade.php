@extends('layouts.default.master')
@section('body-class', 'focus-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">

                <div class="pict-col col-lg-5 col-md-6 col-sm-12 col-xs-12">
                    <div class="title-row"><h1>THE FOCUS <br />IS ON YOU</h1></div>
                    <div class="default-transparent">
                        <p>Whether you are a consumer, real estate agent or real estate professional – the focus is on you. Real Estate shares the commodity of real estate, but the delivery and inception are with
                            people.</p>

                        <p>For consumers, we provide the most in depth and time efficient platform to find a property,
                            real estate professional and/or a real estate service. The process takes mere minutes, and
                            you can view the results at your leisure. We want you to know who you are dealing with.</p>

                        <p>For real estate agents, we provide you with the most direct and efficient form of real estate
                            marketing, to a real estate market, at a fraction of the cost of any other form of marketing.</p>
                        <p>
                            With Buy Sell Lease, we ensure that your marketing and branding is not diluted. Your
                            property, people and services are delivered directly from your very own website, without
                            any third party display advertisements or banner advertisements. We direct consumers into
                            your website – the Focus is on You.</p>

                        <p>For real estate professionals, we provide direct marketing of your services to people within
                            your demographic, who are requiring the services that you perform. </p>
                        <p>The opportunities provided to you and your business are far more direct and cost efficient than any other form
                            of marketing. Direct Marketing of your business and services to a real estate based
                            market…for your industry, that’s a first.</p>

                    </div>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-focus-is-on-you.jpg') }}" width="100%" ></section>
@stop
