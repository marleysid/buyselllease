@extends('layouts.default.master')
@section('body-class', 'termsofuse-body')
@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>User Terms & Conditions</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 sidebar">
                <ul>
                    <li class="active"><i class="glyphicon glyphicon-arrow-right"></i><a href="terms-of-use">USERS</a></li>
                    <li><a href="terms-subscribers">SUBSCRIBERS</a></li>
                </ul>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 faq-content">
                <h2>Terms of Use</h2>

                <p>Unless you are a Real Estate Agent or other Real Estate Professional acting in your professional capacity, you agree to use BSL for your personal use,
                    and your commercial use is limited to transactions done on your own behalf.<br/>
                    The commercial use of real estate agents and other real estate professionals is limited to providing information to consumers via BSL or, where
                    authorised,
                    taking actions on behalf of a consumer client.</p>

                <p>You agree not to reproduce, modify, distribute, display or otherwise provide access to, create derivatives from, decompile, disassemble or reverse
                    engineer any portion of the services of BSL.
                    Your access to and use of the service of BSL, is conditional upon your Acceptance of our Terms and Conditions, Policies & Disclaimers
                    contained on our website, <a href="http://www.buyselllease.com.au">www.buyselllease.com.au</a>.</p>

                <p>Your access to our site constitutes your agreement to be bound by our Terms and Conditions, Policies and Disclaimers, collectively known as Supporting
                    Agreements.
                    BSL may only be used for lawful purposes.</p>

                <h3>Intellectual Property & Copyright?</h3>

                <p>Buy Sell Lease is both a Registered Trademark and Patent, both Nationally and Internationally. The content displayed on our site, including but not
                    limited to,
                    all information, text, materials, images, graphics, software, tools, logos and trademarks, are protected by Trade Mark, Copyright and Patent
                    Protection laws.</p>

                <p>You must not modify, copy, reproduce, republish, frame, download onto a computer, upload to a third party, post, transmit, share or distribute this
                    content in any way.</p>

                <h3>Third Party Links and Advertising</h3>

                <p>The service provided by BSL includes links and direct synergy to third party real estate professionals, services and Web sites, as well as materials
                    provided by third parties.
                    BSL does not endorse, and takes no responsibility for such products, services, events, Web sites, and materials. You understand that BSL has no
                    obligation to, and generally does not,
                    approve or monitor materials provided by third parties through the services of BSL.
                    Your dealings with any third party arising in connection with the services of BSL are solely between you and such third parties,
                    and BSL takes no responsibility for any damages or costs of any type arising out of or in any way connected with your dealings with these third
                    parties.</p>

                <p>No guarantee or promise is given that the real estate agents or other real estate professionals hold a current licence, registration or any other
                    accreditation,
                    required by law, to perform the services that they say are doing. You should rely on your enquiries about, and with, these third parties.</p>

                <p>Linking to our Website and BSL service without written permission or authorisation is prohibited.</p>

                <h3>Content</h3>

                <p>BSL may at any time, without notice and liability to you, remove, alter or terminate access to any or all of your Content,
                    in our sole discretion. Without limitation, BSL may remove, alter or terminate access to any or all of your content, if we consider:</p>
                <ul>
                    <li>The content is in breach of any law or legislation;</li>
                    <li>The content infringes and impedes the intellectual property rights of BSL or any third party;</li>
                    <li>We are required to do so by a regulatory body or any relevant authority in accordance with any notice;</li>
                    <li>The content is:
                        <ol>
                            <li>Misleading and/or deceptive;</li>
                            <li>Inappropriate material in respect of the services of BSL and the real estate industry;</li>
                            <li>Likely to cause any offence;</li>
                            <li>Baseless and incorrect in the matter;</li>
                            <li>Insulting, Rude or obscene;</li>
                            <li>Defamatory;</li>
                            <li>Likely or will cause corruption as a result of a virus or other disabling code;</li>
                            <li>Otherwise unlawful</li>
                        </ol>
                    </li>
                </ul>

                <p>You represent that the information you enter and provide through the services of BSL is, true, accurate and correct, at all material times that you use
                    our services.</p>

                <p>You agree that you make sure that you keep your username and password private and secure at all times, and warrant that you accept all liability for
                    any unauthorised
                    access and use of your username and password. You grant and permit BSL, a National and an International, non-exclusive right to display and
                    communicate your content to third parties,
                    with regard to the services of BSL and the real estate industry, in any medium, whether in existence or currently utilised or not. In this grant and
                    permission,
                    BSL may treat your Content in any manner as aforementioned, without liability or compensation, of any nature to you.</p>

                <p>In using the services of BSL, you acknowledge and consent to third parties, such as real estate agents and other real estate professionals, receiving
                    your Content
                    and sending you information in relation to real estate services, and as such indemnify and release BSL from any claims that you may make in relation
                    to any third party synergy and communication.
                    BSL does not monitor the Content of real estate agents and other real estate professionals and accordingly, we make no warranty or representation,
                    expressed or implied with regard to any third party content. This synergy and communication is completely at your own risk.</p>

                <p>You should only use the service of BSL and provide the information requested at your own risk & decision, knowing that this information will be
                    forwarded to real estate
                    agents and other real estate professionals.</p>

                <h3>Acceptable Use</h3>

                <p>You agree not to use BSL in any way that is unlawful, or harms BSL, its service providers, advertisers and any other user.
                    You agree not to distribute or post spam, chain letters, pyramid schemes, or similar communications through the Services.
                    You agree not to impersonate another person or misrepresent your affiliation with another person or entity. <br/>
                    Except as expressly stated herein, these Terms of Use, do not provide you a licence to use, reproduce, distribute,
                    display or provide access to any portion of the Services on third party web sites or otherwise.</p>

                <p>BSL reserves the right, at our sole discretion, to pursue all legal avenues, including but not limited to, deletion of your USER account
                    and immediate termination of your services with BSL, upon any breach by you of any of our Terms & Conditions.</p>

                <h3>Role of BSL</h3>

                <p>Buy Sell Lease is not a real estate agency. BSL does not act as a real estate agent for you or any other user.
                    BSL does not sell, buy or lease real property. BSL does not receive fees on a lead basis from the real estate agents or other real estate
                    professionals who subscribe and use BSL.
                    All fees are on an Annual Subscription basis.</p>

                <p></p>BSL’s role through our uniquely designed synergy is to create a direct link and synergy between Consumers and Real Estate Agents and Other Real
                Estate Professionals,
                for the purpose of enquiring about selling, leasing and buying real estate in Australia.
                All enquiries are direct between you and real estate professionals, and not in the open through any website.
                All information provided by Real Estate Agents and Other Real Estate Professionals is not information provided through our website,
                unless it appears as a general advertisement promoting their services.</p>

                <h3>Indemnity</h3>

                <p>At all material times, you agree to fully indemnify BSL, against any loss, liability, claim or demand, as a result of your use of BSL</p>

                <h3>Jurisdiction</h3>

                <p>Your use of BSL and all of our legal notices will be governed by and construed in accordance with the laws of NSW, Australia,
                    and by using the services of BSL, you irrevocably and unconditionally submit to the jurisdiction of the courts of that State.</p>

                <h3>BSL Limitation and Liability</h3>

                <p>BSL cannot and does not confirm that each User, Real Estate Agent or Other Real Estate Professional is who they claim to be.
                    BSL do not, and cannot be, involved in your communication with third parties, or control the behaviour of all those who use the service of BSL, and
                    accordingly,
                    in the event of a dispute between you and any third parties, you release BSL ( and our employees) from claims, demands, costs ( including legal
                    costs),
                    liability and damages, actual and consequential, direct or indirect, of any kind and nature, known and unknown.</p>

                <p>To the extent permitted by law, our liability to you, for breach of these Terms & Conditions, or in negligence or in any other tort
                    of for any other common law or statutory cause of action arising in relation to these Terms and Conditions or the service of BSL,
                    is limited to the following, to be applied at our sole discretion:</p>
                <ul>
                    <li>The supply of the services again; or</li>
                    <li>The payment of the cost of having the services supplied again</li>
                </ul>

                <h3>Severability</h3>

                <p>If any provisions of the Terms & Conditions is deemed invalid by a court of competent jurisdiction, the invalidity of such provision shall not affect
                    the validity
                    of the remaining provisions of the Conditions, which shall remain in full force and effect.</p>

                <h3>Privacy</h3>

                <p>We are committed to protecting Your Privacy in accordance with our Privacy Policy. By using the services of BSL, you agree to the terms of our Privacy
                    Policy.</p>

                <h3>Definitions</h3>

                <p><b>AGENCY</b> means an Employer or Body Corporate that you are employed or contracted to, which can include a Real Estate Agency Business or Business
                    described
                    in Other Real Estate Professional.</p>

                <p><b>Body Corporate</b> The term ‘body corporate’ is considered to cover any artificial legal entity having a separate legal personality.
                    These entities have perpetual succesion, they also have the power to act, hold property, enter into legal contracts,
                    sue and be sued in their own name, just as a natural person can.</p>

                <p>The types of entities falling into these categories are broad, and include:
                <ul>
                    <li>trading and non-trading</li>
                    <li>profit and non-profit making organisations</li>
                    <li>government-controlled entities</li>
                    <li>other entities with less or no government control or involvement.</li>
                </ul>
                </p>
                <p>Included in the definition of body corporate are entities created by:
                <ul>
                    <li>common law (such as a corporation sole and corporation aggregate)</li>
                    <li>statute (such as the Australian Securities & Investments Commission)</li>
                    <li>registration pursuant to statute (such as a company, building society, credit union, trade union, and incorporated association).<br/>
                        If an entity is not established under an Act of Parliament, or under a statutory procedure of registration, such as the Corporations Law or an
                        Incorporation Act,
                        it is generally not a body corporate.
                    </li>
                    <li></li>
                </ul>
                </p>

                <p><b>Other Real Estate Professional</b> means anyone, entity or Body Corporate engaged in business that relates to the real estate industry, and may
                    include, but is not limited to:
                    Legal Practitioners, Valuers, Insurance Agents, Strata Agents, Pest Report Businesses,
                    Building Report Businesses, Depreciation report Businesses, Finance Agents, Accountants, and the like</p>

                <p><b>BSL and Services of BSL</b> means Buy Sell Lease and the Company that is licenced to Use and Trade under its name and with all of its Intellectual
                    Property, known as myrealestateapp Pty Ltd
                    ( A.C.N 159 653 504), including our Website, Social Media, Synergy, Advertising, App.</p>

                <p><b>Real Estate Services</b> means any act or business related to the Buying. Selling and Leasing of Real Estate including but not limited to those
                    described in
                    AGENCY and OTHER REAL ESTATE PROFESSIONAL.</p>

                <p><b>Supporting Agreements</b> means the collective Agreements of BSL being: Privacy Policy, User Terms & Conditions,
                    Subscriber Service Agreement – Terms & Conditions, Advertising Policy, and any other Policy
                    that may be introduced to our website at any time, and without notice to anyone.</p>

                <p><b>USER<b></b> means a consumer that is utilising the services of BSL for the purpose of Buying,
                        Selling and/or Leasing real estate, commonly described, within the real estate industry as Vendors, Purchasers, Landlords and Tenants.</p>

                <p><b>YOU</b> means a person and or a legal entity or Body Corporate, that is engaged in the real estate industry as described in Real Estate Services,
                    for and on behalf of an Employer in the capacity of an Employee or a Contractor.

                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/faq-bg-agents.jpg') }}" width="" 100%" ></section>



@endsection