
@extends('layouts.default.master')
@section('body-class', 'alerts-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

                </div>
                <div class="pict-col col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="title-row"><h1>GET INSTANT ALERTS</h1></div>
                    <div class="default-transparent">
                        <p>Buy Sell Lease has been created as a result of our Managing Director’s 20 year career within the real estate industry,
                            as well as personal experiences searching for property, people and real estate professionals.
                            Get up to date property so you are informed of every new listing.</p>
                        <p>
                            With Buy Sell Lease, you are constantly alerted with new listings and other material that you
                            are searching for. You may elect to receive email or sms alerts, and these updates are stored
                            in your profile setting for you to view at your leisure.</p>

                        <p>You can also elect to stop any further alerts after you have found what you are looking for.</p>

                        <p>We want to keep you informed of all new changes to your search. Buy Sell Lease is your
                            Personal Assistant, updating you on every new listing to assist you in your property
                            endeavours.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-get-instant-alerts.jpg') }}" width="100%" ></section>
@stop
