@extends('layouts.default.master')

@section('content-body')
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 head-1-2">

            <h1>How It Works</h1>

            <h2 class="red-head">Building Direct Relationships</h2>

        </div>
        <div class="row extra-cols">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="col-md-12 head3">
                    <h3>Buy Sell Lease is a portal that builds instant direct relationships with people wanting to buy,
                        sell and/or lease all types of property – residential, commercial, retail, investment, industrial, development sites, land, rural,
                        etc – One Portal for all of your property listings.</h3>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-l">
                    <p>
                        Buy Sell Lease is vastly different from other portals. We are not a visual directory of property, but rather a direct line of communication,
                        where the focus is on you, your brand, your properties and your team, and at all times providing a User with the fastest and most efficient way
                        to find a property and a real estate agent.</p>

                    <p> A User, described as a Vendor, Purchaser, Landlord or Tenant, can register and enter their search criteria and perform a property
                        and real estate based service search within minutes.</p>

                    <p>Search results include property they are searching, the personnel of your real estate agency for both sales and property management,
                        as well as real estate based services such as a property lawyer, financier, insurance provider, surveyor, pest report, depreciation report,
                        building report, and the like. Buy Sell Lease is a portal that provides a User with all the information
                        required in their pursuit of their real estate dealings.</p>

                    <p>Search results are matched with your own website content – returning results directly from your webpages, including property listings,
                        and individual profiles. Within these results, Users are, in an instant, in your actual website, displaying your content as you intended.
                        At all times – your brand, your properties and your services are the main focus, with content only displaying your brand.
                        As these pages are from your website, Buy Sell Lease branding is not displayed on the returned content.
                        The returned content is also void of third party banner advertisements. It’s all about you.</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-r">
                    <h4>COST</h4>

                    <p>No extra charge for</p>
                    <ul>
                        <li>Logos</li>
                        <li>Team profiles</li>
                        <li>Video</li>
                        <li>Priority placement</li>
                        <li>Data-basing and prospecting</li>
                        <li>Reports</li>
                        <li>No extra charges at ALL</li>
                    </ul>
                    <p>&nbsp;</p>
                    <h4>GPS TRACKING</h4>

                    <p>Buy Sell Lease connects Users with your properties and your office through Google maps.
                        User’s may click on the property and/or your office for GPS directions on their mobile device.</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col2">
                <h4>Prospecting <br/>and data-basing</h4>

                <p>
                    Prospecting and Data-basing has reached new heights in real estate marketing. Real Estate Agents are provided the email and phone
                    details of people searching and receiving your properties and team services for sale and lease.<br />
                    Buy Sell Lease sends you these reports for exporting into your CRM System to provide reports on search results
                    for your properties and team for your client presentations as well as tracking your own service search results.
                    You may also export this data for your prospecting and databasing. </p>
                    <p>With Buy Sell Lease, you are able to track the marketing impact of your properties,
                    match them with your inspection list, to determine “hot/cold” feedback, report to your Vendor, Landlord about current enquiry levels,
                    and communicate directly with people who are viewing your properties and services.

                     </p>

                <p>
                    The delivery is electronic and your target market ARE people wanting to buy, sell or lease property. The cost is covered in your flat annual subscription..
                </p>
                <p>&nbsp;</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4>Finding a real <br/>estate agent</h4>

                <p>The Real Estate industry is built on people. The commodity is property.
                    Buy Sell Lease connects people through property and provides the platform for new opportunities and new relationships to be borne.</p>

                <p>Finding an Agent has never had the depth and level of service that Buy Sell Lease offers. Users are directed to your team profiles,
                    enabling them to read your team profiles, explore their sales and leasing history, read their testimonials,
                    and make informed decisions of who they want to deal with.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4> Finding a real <br/>estate professional</h4>

                <p>Buy Sell Lease offers User’s the opportunity to find other real estate professionals to assist them in their
                    conveyancing of their sale, purchase and/or their lease, finance, insurance, survey, pest report, building report, depreciation report and the
                    like.</p>

                <p>We provide the full User experience, providing every resource and opportunity to assist the process of buying, selling and leasing.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4>MAKE BSL PART OF <br/>YOUR Business plan</h4>

                <p>Buy Sell Lease should be part of your business plan – improving the marketability of all of your properties and your brand and team,
                    and providing you with unique data-basing, prospecting and sales lead opportunities - for one fixed annual fee.</p>

                <p>One portal for EVERY property that you are selling or leasing. One portal to promote your brand, your people and your services
                    One portal to data-base, prospect and provide you with sales leads.</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4>Patent protected <br/>INNOVATION</h4>

                <p>The delivery of Buy Sell Lease is a unique innovation that is Patent Protected – ensuring originality in its delivery
                    and ensuring its exclusiveness and supremacy as a marketing portal.</p>

                <p>We are not just another real estate portal. With Buy Sell Lease, we not only market your properties,
                    we promote your brand, your people and your services, as well as allow you to develop and build your business
                    through prospecting and data-basing directly to people engaged in real estate.</p>
            </div>

        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/hiw-banner-bottom.jpg') }}" width="" 100%" ></section>

@stop
