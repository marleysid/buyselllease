
@extends('layouts.default.master')
@section('body-class', 'agent-info-body')

@section('content-body')
    <iframe src="//player.vimeo.com/video/124590621?portrait=0&color=aeaeae" width="100%" height="600px" style="margin-top: -20px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

    <a href="{{ route('home.agent-signup') }}" target="_self">
        <div class="navabar">
            <div class="container">
                <div class="row">

                    <div id="slideshow-mini">
                        <div>
                            <img class="img-responsive" src="{{ asset('/img/subscribe-empty.png') }}" >
                        </div>
                        <div>
                            <img class="img-responsive" src="{{ asset('/img/subscribe-now.png') }}" >
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </a>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script>
    $(function() {

        $("#slideshow-mini > div:gt(0)").hide();

        setInterval(function() {
            $('#slideshow-mini > div:first')
                    .fadeOut(1000)
                    .next()
                    .fadeIn(1000)
                    .end()
                    .appendTo('#slideshow-mini');
        },  15000);

    });
</script>


