@extends('layouts.default.master')
@section('body-class', 'sales-and-leasing-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">

                <div class="pict-col col-lg-5 col-md-8 col-sm-12 col-xs-12">
                    <div class="title-row"><h1>SALES AND LEASING</h1></div>
                    <div class="default-transparent">
                        <p>Buy Sell Lease is a real estate portal for all types of property - Residential, Commercial, Industrial, Retail, Investment, Rural, Development Sites and the like.</p>

                        <p>Whether you are looking to Buy or Lease, Sell or Lease, Buy Sell Lease allows you to find the property, find the real estate agent and find
                            the real estate professional to assist you in your real estate endeavours.</p>

                        <p>For the real estate industry, Buy Sell Lease is one portal for the marketing of ALL of your property for Sale and Lease, as well as your team that delivers these services.</p>

                        <h5>WE’VE GOT IT ALL!</h5>
                        <ul>
                            <li><p>Residential</p></li>
                            <li><p>Commercial</p></li>
                            <li><p>Industrial</p></li>
                            <li><p>Retail</p></li>
                            <li><p>Investment</p></li>
                            <li><p>Rural</p></li>
                            <li><p>Development Sites</p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-4 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-sales-and-leasing.jpg') }}" width="100%" ></section>
@stop
