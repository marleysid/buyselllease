@extends('layouts.home.master')

@section('header-style')
    <link href="{{ asset('/css/search.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/iosCheckbox.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/jquery.range.css') }}" rel="stylesheet">
@stop

@section('header-script')
    <script type="text/javascript" src="{{ asset('/dist/iosCheckbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/dist/jquery.range.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
@stop

@section('content-main')

    <div class="container">
        <div class="container main-slogan">
            <div class="row">
                <div class="col-md-12">
                    <h1>“Connecting you through property”</h1>
                </div>
                <div class="col-md-8">
                    <p>We are about building direct relationships through property. You receive properties for sale and lease directly from the websites of real estate agents – displaying properties as they intended – with full description, a complete array of photo’s, video and floorplans, as well as receiving in depth information of people in the real estate industry, such as real estate agents, financiers, conveyancers, insurance brokers, valuers, surveyors and the like, to help you make informed decisions of who you may wish to deal with. The process of performing your search takes minutes, your searches are forwarded to you instantly.</p>
                </div>

                @if(!Auth::check() || (Auth::check() && (Auth::user()->role != '2'  && Auth::user()->role!='3' &&  Auth::user()->role !='4')))

                <div class="col-md-4">

                    <p><a href="{{ route('home.agent-info') }}"  type="button" class="btn btn-secondary btn-big">Agent Info</a></p>
                    <p><a href="{{ URL::to('agent-signup') }}"  type="button" class="btn btn-primary btn-big">Agent Signup</a></p>
                    <p><a href="{{ route('professionals-signup') }}"  type="button" class="btn btn-primary btn-big">Professionals Signup<br><span>Other real estate industries</span></a></p>
                </div>

                    @endif
            </div>
        </div>

        <div class="row points-icons" >
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <a href="{{ route('home.directrelationships') }}"><img src="{{ asset('img/icon-relationships.png') }}"  alt="Relationships Icon" class="img-responsive">
                    <h4>Direct Relationships</h4></a>
                <p>Real Estate is an industry built on property. <br/>The delivery of which is through direct relationships between people. <a href="{{ route('home.directrelationships') }}">more...</a> </p>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <a href="{{ route('home.getinstantalerts') }}"><img src="{{ asset('img/icon-instant-alerts.png') }}"  alt="Alert Bell Icon" class="img-responsive">
                    <h4>GET INSTANT ALERTS</h4></a>
                <p>Get up to date property so you are informed of every new listing. With BSL, you are constantly alerted with new listings and <a href="{{ route('home.getinstantalerts') }}">more...</a></p>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <a href="{{ route('home.thefocusisonyou') }}"><img src="{{ asset('img/icon-focus-on-you.png') }}"  alt="Person Icon" class="img-responsive">
                    <h4>THE FOCUS IS ON YOU</h4></a>
                <p>Whether you are a consumer, real estate agent or real estate professional – the focus is on you. Real Estate shares the commodity <a href="{{ route('home.thefocusisonyou') }}">more...</a></p>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <a href="{{ route('home.costeffective') }}"><img src="{{ asset('img/icon-cost-effective.png') }}"  alt="Dollar Sign Icon" class="img-responsive">
                    <h4>Cost Effective</h4></a>
                <p>Naturally, for Consumers, the service of Buy Sell Lease is FREE! <br />
                    For real estate agents, BSL promotes your entire website content. <a href="{{ route('home.costeffective') }}">more...</a></p>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <a href="{{ route('home.salesandleasing') }}"><img src="{{ asset('img/icon-sales-and-leasing.png') }}"  alt="Key Icon" class="img-responsive">
                    <h4>Sales and Leasing</h4></a>
                <p>Buy Sell Lease is a real estate portal for all types of property - Residential, Commercial, Industrial, Retail, Investment and many <a href="{{ route('home.salesandleasing') }}">more...</a>
                </p>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <a href="{{ route('home.findservices') }}"><img src="{{ asset('img/icon-find-services.png') }}"  alt="Freight Trolley Icon" class="img-responsive">
                    <h4>Find Services</h4></a>
                <p>Real Estate is an arduous process that involves the services of many professionals. BSL will help you in finding practical support. <a href="{{ route('home.findservices') }}">more...</a></p>
            </div>
        </div>
    </div>
@stop

@section('footer-script')
    <script type="text/javascript">
        function search_next(stepName, isFinal) {
            $.ajax({
                method: "GET",
                url: '{{ url('/search') }}/' + stepName,
                data: $('#search-form').serialize(),
                async: false
            })
            .success(function(msg) {
                $('#search-widget').html(msg);
                $('.ios').iosCheckbox();
            });

            if(isFinal) {

                var is_only_porf =$('#search-widget').find($('#only_professional')).val();
                if(is_only_porf =='yes') {
                    window.location = '{{ route('dashboard.professionals') }}';
                } else {
                    window.location = '{{ route('dashboard.search') }}';
                }



            }
        }

        function search_start(track) {
            search_next(track + '')
        }
    </script>
@stop
