@extends('layouts.default.master')
@section('body-class', 'faq-body')
@section('content-body')
    <div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <h1>Frequently Asked Questions</h1>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidebar">
            <ul>
                <li><a href="customers">FOR CUSTOMERS</a></li>
                <li class="active"><i class="glyphicon glyphicon-arrow-right"></i><a href="agents">FOR REAL ESTATE AGENTS</a></li>
                <li><a href="services">SERVICES</a></li>
                <li><a href="marketing">MARKETING</a></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 faq-content">
            <h2>For Real Estate Agents</h2>
            <h4>What is the cost of marketing through Buy Sell Lease?</h4>

            <p>Buy Sell Lease is offered at one fixed annual fee of $5,000 + GST per individual office.</p>

            <p>For less than $100 + GST per week, you are marketing every property, every team member, your
                brand and your business – directly to a real estate focused market.</p>

            <h4>Are there any other additional costs?</h4>

            <p>There are no additional fees of Buy Sell Lease. We do not offer Premium or Deluxe packages as our
                system provides a fair and equitable approach in the delivery of your content to consumers. As all
                real
                estate content is administered directly from your website, there are no pricing attached to Video,
                the
                amount of photo’s, time based campaigns, floorplans, and the extent of your content. Your
                subscription covers your website content being marketed for one (1) year.</p>

            <h4>Can I recover my subscription fee to Buy Sell Lease?</h4>

            <p>Recovery of your Subscription Fee is at your own discretion. As an example, if your office performs
                500 real estate transactions of leasing and sales per year, a fee of $10 + GST attached to your
                Leasing/Management Agreements and Sales Agreements, will recover the entire amount of your
                Subscription.</p>

            <p>The dollar value of your fee is entirely your decision, but keep in mind that your competitors are
                also
                competing for the same business and your pricing should remain competitive.</p>

            <p>As Buy Sell Lease is a fixed fee, a fixed fee attributed to your Agency Agreements, saves time and
                effort for reconciling your marketing with Buy Sell Lease at the end of every Month as this fee will
                be
                fixed and built into your client Agreements.</p>

            <h4>How does Buy Sell Lease benefit the role of my Sales and Leasing Team?</h4>

            <p>Buy Sell Lease is a fixed fee. If you attribute a fixed fee to every Agency Agreement, your end of
                month reconciliation is simple and efficient.</p>

            <p>As Buy Sell Lease is based on a yearly Subscription, your Leasing and Sales team, need not request
                further marketing dollars if a property has not sold or leased within a given period, e.g 30 days.
                The
                sales and leasing listing presentation is no longer focused about the marketing dollars and
                investment
                of your client, or even the topping up of advertising dollars, but is focused on the performance of
                your team, allowing them to place more energy selling and leasing, rather than acting as an
                Advertising Agent.</p>

            <h4>Are subscriptions based on individual offices?</h4>

            <p>Buy Sell Lease is a subscription service for each and every physical office, regardless of whether a
                principle owns one office or three offices.</p>

            <p>Each office is recognised as a catchment area within a specific area and Buy Sell Lease works on the
                principle of location.</p>

            <h4>What guarantee does Buy Sell Lease provide in pricing?</h4>

            <p>For fully paid subscribers, as an introductory rate* Buy Sell Lease provides a Price Freeze Guarantee for Subscriptions that
                roll over continuously for a period of five (5) years and provide that our fixed annual fee will
                remain
                at $5,000 plus GST per annum for the period of the continuous rollover.</p>

            <p>Buy Sell Lease is committed to providing the best service of marketing and opportunity for the real
                estate industry.</p>

            <h4>What is the pricing structure for real estate agents outside of your public marketing area?</h4>

            <p>For all real estate agents, outside of our public marketing area, the subscription to Buy Sell Lease
                is
                completely – FREE!
                When Buy Sell Lease has committed to marketing in your State and Territory, real estate agents
                within these areas will be emailed and invited to convert their subscription into a full
                subscription.</p>

            <p>We are committed to Buy Sell Lease being a household real estate marketing brand, and offer our free
                subscription to real estate agents outside of our marketing area, to increase presence outside of
                our
                immediate area and provide awareness of our product and service to both consumers and the real
                estate industry.</p>
            <p><br /><b>* </b> <span style="font-size: 11px; color: #878787;">We reserve the right to withdraw the introductory rate at any time, without notice, which will only apply to non- executed Subscriptions</span></p>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- Full width Image -->
<!-- Image backgrounds are set within the full-width-->
<section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/faq-bg-agents.jpg') }}" width="" 100%" ></section>



@endsection
