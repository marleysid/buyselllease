@extends('layouts.default.master')
@section('body-class', 'faq-body')
@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>Frequently Asked Questions</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidebar">
                <ul>
                    <li><a href="customers">FOR CUSTOMERS</a></li>
                    <li><a href="agents">FOR REAL ESTATE AGENTS</a></li>
                    <li><a href="services">SERVICES</a></li>
                    <li class="active"><i class="glyphicon glyphicon-arrow-right"></i><a href="marketing">MARKETING</a></li>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 faq-content">
                <h2>Marketing</h2>

                <h4>What is your marketing plan?</h4>

                <p>The first stage of our marketing is focused in New South Wales in creating consumer traffic as well
                    as real estate content that serves consumers as well as the real estate industry. In this stage, Buy
                    Sell
                    Lease will engage in public marketing. Other States and Territories will follow thereafter.</p>

                <h4>Where will you market Buy Sell Lease?</h4>

                <p>Buy Sell Lease will engage in marketing on free to air Television, Radio, Billboards, Bus Advertising
                    as well as online strategy. Our strategy may change from time to time to reflect market or campaign
                    changes as well as marketing to increase our brand awareness.</p>

                <p>As Buy Sell Lease is a real estate portal that delivers the most innovative and cost effective means
                    of
                    marketing not only your property, but also your brand, your people and your services, we will seek
                    the marketing of Buy Sell Lease within the day to day business of your office, to

                    ensure the market is aware of Buy Sell Lease as Buy Sell Lease provides so many

                    great opportunities for your business and industry.</p>

                <h4>How can I use Special Deals for my Business?</h4>

                <p>Robert Oatley Vineyards are offering discounted wine directly to you, with the opportunity of
                    rebranding the wine with your corporate image to provide to your clients as a ‘thank you” for an
                    Appraisal, Sale or Leasing transaction. The opportunity and choice is yours. You simply click on the
                    Robert Oatley Vineyards icon and enter their website and complete your order with your redemption
                    code for delivery to your door. It’s as simple and easy as that.</p>

                <p>Buy Sell Lease is partnering with companies that will provide special deals to both our Consumers and our Subscribers
                    in creating our very own loyalty program to ensure constant re-engagement with Buy Sell Lease,
                    even after they have completed their real estate transaction. These Deals may be in the form of holidays,
                    retail shopping and the like that benefit our consumers as well as providing rewards to our Subscribers,
                    at a discounted level, for a job well done. These deals will be updated from time to time to enhance
                    and reward people for their real estate experience.</p>

                <p>Buy Sell Lease will not permit marketing of any real estate brand in offering any special deals for
                    marketing purposes.</p>

                </br>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/faq-bg-marceting.jpg') }}" width="" 100%" ></section>



@endsection

