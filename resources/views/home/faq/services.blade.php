@extends('layouts.default.master')
@section('body-class', 'faq-body')
@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>Frequently Asked Questions</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidebar">
                <ul>
                    <li><a href="customers">FOR CUSTOMERS</a></li>
                    <li><a href="agents">FOR REAL ESTATE AGENTS</a></li>
                    <li class="active"><i class="glyphicon glyphicon-arrow-right"></i><a href="services">SERVICES</a></li>
                    <li><a href="marketing">MARKETING</a></li>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 faq-content">
                <h2>Services</h2>

                <h4>How do I promote my properties, people and services through Buy Sell Lease?</h4>

                <p>Buy Sell Lease matches a consumer search with your own data from your website. If I were looking
                    for a particular property, price, suburb, it will match with your data and return these webpages to
                    the
                    consumer. The consumer clicks on the summary page and enters your website at that webpage as if
                    they typed that URL into their own search bar. The same search methodology is used for looking for
                    your personnel as if I am looking to sell or lease my property. Profiles will be returned to me, and
                    with a
                    click I am reading their profile, looking at their photo, contact details, testimonials, and
                    current/past
                    sales and leasing details.</p>

                <p>Buy Sell Lease is the most effective and direct means of marketing property, people and services to
                    consumers engaged in real estate. No other marketing can compete in terms of the cost or marketing
                    and the people you are directly marketing to, without even lifting a finger. More effective than
                    cold
                    calling and letterbox drops as these consumers are actually wanting to deal in real estate. You are
                    dealing with Hot Prospects.</p>

                <h4>Will third party real estate agents appear on the webpages of Buy Sell Lease or our own
                    content?</h4>

                <p>Buy Sell Lease will not permit any real estate brand or person appearing on our webpages for their
                    own branding and marketing of their business. Our business structure and model is to ensure a fair
                    and equitable real estate portal for the real estate industry. Real estate marketing is through your
                    own
                    Subscription. In the delivery of content from your website, as it is from your own website, it is
                    only
                    your content that is displayed exactly from your website, with branding, colours, logo and content that
                    you have.</p>

                <p>There is no ability, nor desire, to reduce or dilute your branding with our logo “Buy Sell Lease”,
                    nor
                    is there any ability or desire to cheekily market your competitors brand and promotions as a banner
                    advert alongside what you are promoting.</p>

                <h4>If I upload a property on my own website, when will it appear through Buy Sell Lease?</h4>

                <p>That very same day. It’s as simple and as easy as that..</p>

                <h4>What marketing opportunities do you offer us?</h4>

                <p>Buy Sell Lease directs consumers to your website to view property, people and services that you are
                    marketing, free of any time period marketing, free of any limit on content such as photos, video and
                    content, as well as presenting your material in your corporate branding – as you intended.</p>

                <p>As Consumers are directed to your website, in direct communication, rather than in an open forum,
                    opportunity exists, when a consumer is searching for a sales or leasing agent, to create a webpage
                    promotion in your staff profiles as a direct means of marketing, to increase your sales and leasing
                    opportunity.</p>

                <p>Examples may include – offering a Spring Auction themed campaign, an Investment orientated
                    campaign, An apartment Auction Campaign, a Free Property Management Guarantee to win new
                    Property Management.</p>

                <p>The opportunities are endless and cater to the creativity of your own agency and team. As these
                    marketing representations do not appear on a directory, but rather through consumer search results,
                    there is a reduced chance for your competition to view and match what your are offering.</p>

                <h4>How do we benefit from sales leads and prospects?</h4>

                <p>Consumers using Buy Sell Lease, register to use our service. In returning searches from your web
                    content, you will receive a regular report displaying the property, and/or person searched together
                    with email details of the consumer, that you may export into your own CRM System.</p>

                <p>If a consumer is looking for a real estate agent to sell or lease their property and view your team
                    profiles, you know that you are now dealing with a Hot Prospect. It’s now up to you to convert this
                    Prospect into a listing. What other portal provides you with real leads to help you make money,
                    build
                    your database and improve your business.</p>

                <h4>How do we manage our data that appears through Buy Sell Lease?</h4>

                <p>As a Subscriber, you will provide the feed that we require to enable your website content to be
                    viewed
                    through Buy Sell Lease. Essentially, all this means is that you only need to manage and update your
                    own Website. Any content and changes will automatically be viewed through Buy Sell Lease.</p>

                <h4>How do search results get delivered to consumers?</h4>

                <p>Other real estate portals deliver content in pages. It is common knowledge that many consumers drop
                    off dramatically after Page 2 of any search. To compensate themselves for the deliverance of the
                    most
                    number of hits, they provide you with upgraded packages to appear on these first two pages, that
                    expire within a certain time period, e.g. 30 days.</p>

                <p>Buy Sell Lease has created a fair and equitable system to ensure that our pricing remains fixed,
                    competitive and takes away the need for you to act as an advertising sales agent.</p>

                <p>We deliver search results in a random fashion. If 10 people were to perform the exact same search,
                    the
                    results will be randomised prior to their delivery to ensure that every agent may appear at the top
                    of
                    the list, as well as the middle and also the end. For each search result, the delivery is different.
                    This
                    ensures that we need not change our pricing model, and also ensures a greater number of properties
                    within any particular search, receive a greater number of consumers viewing the listings.</p>

                <h4>What content from our website is searchable through Buy Sell Lease?</h4>

                <p>In a nutshell, if it’s on your website, it’s on Buy Sell Lease. Your properties, whether you have
                    paid
                    advertising or not from your clients, your brand, your people and your services.</p>

                <p>Buy Sell Lease directs consumer’s into your very own website, to initially view the property or
                    personnel match and navigate to any page within your site that they wish to.</p>

                <p>With Buy Sell Lease, all you need to ensure is that your Website is not only up to date, but presents
                    your business in the best possible light. Your website is your front door to your business. We are
                    bringing people to your front door.<br></p>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/faq-bg-services.jpg') }}" width="" 100%" ></section>



@endsection

