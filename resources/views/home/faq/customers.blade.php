@extends('layouts.default.master')
@section('body-class', 'faq-body')
@section('content-body')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h1>Frequently Asked Questions</h1>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidebar">
                <ul>
                    <li class="active"><i class="glyphicon glyphicon-arrow-right"></i><a href="customers">FOR CUSTOMERS</a></li>
                    <li><a href="agents">FOR REAL ESTATE AGENTS</a></li>
                    <li><a href="services">SERVICES</a></li>
                    <li><a href="marketing">MARKETING</a></li>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 faq-content">
                <h2>For Customers</h2>

                <h4>What is the cost of using Buy Sell Lease?</h4>

                <p>Buy Sell Lease is a free service to Consumers in the pursuit of assisting you to Buy, Sell or Lease
                    Property.</p>

                <h4>Can I advertise my property without a real estate agent?</h4>

                <p>Buy Sell Lease is a service that is provided to real estate agents and other real estate
                    professionals.
                    Vendors and Landlords must market their properties through a real estate agent. Buy Sell Lease will
                    not accept Private Listings.</p>

                <h4>What is the cost of promoting my property through Buy Sell Lease?</h4>

                <p>Buy Sell Lease is offered to real estate agents on a fixed fee annual subscription. Advertising fees
                    for the promotion of your property through Buy Sell Lease is at the sole discretion of real estate
                    agents.</p>

                <h4>Are the real estate agents and other real estate professionals currently registered in their chosen
                    field?</h4>

                <p>Buy Sell Lease cannot and will not guarantee or provide any guarantee that an agent that
                    you choose or may choose to deal with is registered. We recommend that you enquire through the State and Territories
                    websites provided below. For other real estate professionals, please search online to determine if
                    they hold the appropriate registration for their chosen profession/service.
                    For real estate agents</p>

                <div class="row links-cols">
                    <div class="col-sm-4">
                        <ul>
                            <li>NEW SOUTH WALES<br /> <a href="http://www.fairtrading.nsw.gov.au"> www.fairtrading.nsw.gov.au</a></li>
                            <li>VICTORIA <br /><a href="http://www.consumer.vic.gov.au"> www.consumer.vic.gov.au</a></li>
                            <li>WESTERN AUSTRALIA <br /><a href="http://www.commerce.wa.gov.au"> www.commerce.wa.gov.au</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul>
                            <li>QUEENSLAND <br /><a href="http://www.qld.gov.au"> www.qld.gov.au</a></li>
                            <li>SOUTH AUSTRALIA<br /><a href="http://www.cbs.sa.gov.au"> www.cbs.sa.gov.au</a></li>
                            <li>NORTHERN TERRITORY <br /><a href="http://www.dob.nt.gov.au"> www.dob.nt.gov.au</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul>
                            <li>AUSTRALIAN CAPITAL TERRITORY<br /><a href="http://www.ors.act.gov.au"> www.ors.act.gov.au</a></li>
                            <li>TASMANIA<br /><a href="http://www.propertyagentsboard.com.au"> www.propertyagentsboard.com.au</a></li>
                        </ul>
                    </div>
                </div>

                <h4>What if an agent or other real estate professional asks me to pay?</h4>

                <p>As a Consumer and User of services of Buy Sell Lease, your registration and use of our service, is
                    completely free. Our services are for the promotion of real estate and services within the real
                    estate
                    industry. At no time, during your use of the services of Buy Sell Lease, should you be requested to
                    make any payment for any good or service offered by a real estate agent or real estate professional.
                    A
                    real estate agent may only request a marketing fee for the marketing of your property as part of you
                    entering into a Selling Agency or Leasing Agency Agreement.</p>

                <h4>What happens if I want to stop receiving a particular property search information?</h4>

                <p>At any time, you make may make a change to your current search or stop your current search, by
                    logging into your User Profile and viewing your current searches and selecting the “Stop Search” or
                    “Make a Change” icons.</p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/faq-bg-bottom.jpg') }}" width="" 100%" ></section>
@stop

