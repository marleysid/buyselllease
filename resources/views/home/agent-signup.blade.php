
@extends('layouts.default.master')

@section('header-script')
     <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
     <script src="{{ asset('js/phone/dist/jquery.inputmask.bundle.js?v='.time()) }}"></script>
     
@endsection

@section('body-class', 'agent-signup-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">

                <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 contact-form">
                    <h1>AGENT SIGN-UP</h1>

                      @include('flash::message')
                    <div class="col-md-12">
                        <div class="default-transparent">
                            <form role="form" method="post" action="{{ URL::to('agent-signup')}}" id="agent-signup-form">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                <p class="text-center">Each physical office address must have its own subscription.</p>

                                <div class="form-group">
                                    <span class="form-titles">Please enter your name in order (Firstname Lastname)</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('name',Input::old('name'),['class'=>'form-control', 'placeholder' => "Your Name *"]) !!}
                                            <label class="error">{!!$errors->first('name')!!}</label>
                                        </span>
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('company',Input::old('company'),['class'=>'form-control', 'placeholder' => "Company Name (If Applicable)"]) !!}
                                            <label class="error">{!!$errors->first('company')!!}</label>
                                        </span>
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('trading', Input::old('trading'), ['class' => 'form-control', 'placeholder' => "Company Trading Name *"]) !!}
                                            <label class="error">{!!$errors->first('trading')!!}</label>
                                        </span>
                                    </div>
                                     

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('abn',Input::old('abn'),['class'=>'form-control', 'placeholder' => "Company ABN"]) !!}
                                            <label class="error">{!!$errors->first('abn')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('acn',Input::old('acn'),['class'=>'form-control', 'placeholder' => "Company ACN"]) !!}
                                            <label class="error">{!!$errors->first('acn')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('email',Input::old('email'),['class'=>'form-control', 'placeholder' => "Your Email *"]) !!}
                                            <label class="error">{!!$errors->first('email')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                   <div class="form-group">
                                    <span class="form-titles">Select from drop down screen to make sure you pick the right suburb in the right state.</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                             {!! Form::text('suburb',Input::old('suburb'),['class'=>'form-control','id'=>"suburb", 'placeholder' => "operating suburb *"]) !!}
                                             <label class="error">{!!$errors->first('suburb')!!}</label>
                                        </span>
                                    </div>

                                     
                                     
                                </div>

                                <div class="form-group">
                                    <span class="form-titles">Please follow the example Url: http://www.example.com</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                           {!! Form::text('link',Input::old('link'),['class'=>'form-control' ,'placeholder' => "Your Website Address for company *"]) !!}
                                            <label class="error">{!!$errors->first('link')!!}</label>
                                        </span>
                                    </div>
                                     
                                     
                                </div>

                                <div class="form-group">
                                    <span class="form-titles">Please follow the example Url: http://www.example.com</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('staffURL',Input::old('staffURL'),['class'=>'form-control' ,'placeholder' => "Your Website Address for 'Meet the team' page *"]) !!}
                                            <label class="error">{!!$errors->first('staffURL')!!}</label>
                                        </span>
                                    </div>
                                     
                                     
                                </div>

                                

                                 <!-- your input  avatar  here -->
                                       <!-- your placeholder  AVATAR  here -->
                                          <div class="form-group">
                                             <div class="input-group">
                                                 <span class="input-group-addon">
                                                     <label style="float:left;"> Upload Logo * (Image with 180 X 180 pixels or same proportion.)</label>

                                            {!! Plupload::make('my_uploader_id_agent_logo', route('uploadAgentLogo'))
                                                                        ->setOptions([
                                                                         'unique_names'=>true,
                                                                         'multi_selection' => false,
                                                                        'filters' => [
                                                                        'max_file_size' => '2mb',
                                                                        'mime_types' => [
                                                                        ['title' => "Image files", 'extensions' => "jpg,jpeg,gif,png"],
                                                                        ],

                                                                        ],
                                                                        ])
                                                                        ->setAutoStart(true)->render() !!}
                                                    <span class="error uploadfilecls">{!!$errors->first('avatar')!!}</span>
                                                 </span>

                                             </div>
                                             
                                             


                                         </div>


                                         <!-- your input  avatar  here -->
                                       <!-- your placeholder  AVATAR  here -->
                                          <div class="form-group">
                                             <div class="input-group">
                                                 <span class="input-group-addon">
                                                     <label style="float:left;"> Profile Image (Image with 270 X 180 pixels or same proportion.)</label>

                                            {!! Plupload::make('my_uploader_id_agent_profile', route('uploadAgentProfile'))
                                                                        ->setOptions([
                                                                         'unique_names'=>true,
                                                                         'multi_selection' => false,
                                                                        'filters' => [
                                                                        'max_file_size' => '2mb',
                                                                        'mime_types' => [
                                                                        ['title' => "Image files", 'extensions' => "jpg,jpeg,gif,png"],
                                                                        ],

                                                                        ],
                                                                        ])
                                                                        ->setAutoStart(true)->render() !!}
                                                 </span>
                                             </div>
                                             <span class="error">{!!$errors->first('avatar')!!}</span>


                                         </div>




                                <!--ends here-->

                                <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('address',Input::old('address'),['class'=>'form-control', 'placeholder' => "Office Address *"]) !!}
                                            <label class="error">{!!$errors->first('address')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                   <!-- <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <select class="form-control" name="officetype">
                                                <option value ="">Choose Office type</option>
                                                @foreach($price as $pri)
                                                    <option value="{{$pri->office}}" {{ Input::old('officetype') == $pri->office?'selected':'' }}>I want to signup for {{$pri->office}} offices</option>
                                                @endforeach
                                            </select>
                                        </span>
                                    </div>
                                     <span class="error">{!!$errors->first('officetype')!!}</span>
                                </div> -->

                                <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('postcode',Input::old('postcode'),['class'=>'form-control', 'placeholder' => "Office Postcode *"]) !!}
                                            <label class="error">{!!$errors->first('postcode')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('mobile',Input::old('mobile'),['class'=>'form-control', 'id'=>'mobiletext', 'placeholder' => "Your Mobile Number"]) !!}
                                            <label class="error">{!!$errors->first('mobile')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                           {!! Form::text('phone',Input::old('phone'),['class'=>'form-control', 'id'=>'phonetext', 'placeholder' => "Work Phone Number *"]) !!}
                                            <label class="error">{!!$errors->first('phone')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>

                                <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('crm',Input::old('crm'),['class'=>'form-control', 'placeholder' => "Who is the company that uploads your property? *"]) !!}
                                            <label class="error">{!!$errors->first('crm')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>
                                <!--added fields -->
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                             {!! Form::text('importid',Input::old('importid'),['class'=>'form-control','id'=>"importid", 'placeholder' => "Agent ID"]) !!}
                                             <label class="error">{!!$errors->first('importid')!!}</label>
                                        </span>
                                    </div>
                                      
                                </div>

                                <!-- <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('industry',Input::old('industry'),['class'=>'form-control', 'placeholder' => "What is your Industry?"]) !!}
                                        </span>
                                    </div>
                                     <span class="error">{!!$errors->first('industry')!!}</span>
                                </div> -->

                                <!-- <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('service',Input::old('service'),['class'=>'form-control', 'placeholder' => "What service do you provide?"]) !!}
                                        </span>
                                    </div>
                                     <span class="error">{!!$errors->first('service')!!}</span>
                                </div> -->

                                <div class="form-group ">
                                    <span class="form-titles">A hundred word describing your business</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <textarea name="description" class="form-control" rows="8" cols="50" placeholder="description">{{ Input::old('description') }}</textarea>
                                            <label class="error">{!!$errors->first('description')!!}</label>
                                        </span>
                                    </div>
                                    
                                </div>


                                 <div class="form-group ">
                                    <span class="form-titles">Enter a password so you may edit your details in the future</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input id="password" type="password" name="password" class="form-control" placeholder="Password"/>
                                            <label class="error">{!!$errors->first('password')!!}</label>
                                        </span>
                                    </div>
                                     
                                     
                                </div>

                                  <div class="form-group ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input id="confirm_password" type="password" name="confirm_password" class="form-control" placeholder="Repeat password"/>
                                            <label class="error">{!!$errors->first('confirm_password')!!}</label>
                                        </span>
                                    </div>
                                     
                                </div>


                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('promo',Input::old('promo'),[],['id' => 'promo1']) !!}<span class="lbl padding-8">Have a Complimentary Payment?</span>
                                    </label>
                                    <span class="error">{!!$errors->first('promo')!!}</span>
                                </div>

                                  <div class="ans" id="ans" style="display: none;">
                                    <div class="input-group">
                                             <span class="input-group-addon">
                                                 <input type="text" name="promocode" id="promocode" class="form-control" placeholder="enter promotional code">

                                             </span>

                                    </div>
                                    <span class="error">{!!$errors->first('promocode')!!}</span>
                                </div>


                                <h3>Director Authorisation & Acknowledgement</h3>

                                <p>Director Authorisation “YOU” hereby certify that you are a Director of the aforementioned company and have the Authority to enter into
                                    such an arrangement with Buy Sell Lease for and/on behalf of YOUR AGENCY

                                <h3>Acknowledgement</h3>
                                <ul>
                                    <li>“YOU” hereby acknowledge and state that you have read through our “Service Agreement”, our “Terms & Conditions” and our “Privacy
                                        Policy” and have provided both true and accurate details within your Application Form and Agree to be bound by all of our
                                        aforementioned documents.
                                    </li>
                                    <li>YOU hereby acknowledge and consent to payment being made to BSL in accordance with the payment details and method that YOU provide
                                        below.
                                    </li>
                                    <li>You hereby acknowledge and agree to immediately notifying and authorising your CRM system provider in writing to provide a feed to
                                        your website for Buy Sell Lease and providing Buy Sell Lease with an email copy of the authorisation.
                                    </li>
                                </ul>
                                <div class="payment-info">
                                  <div class="col-sm-3">
                                    <img src="{{asset('/img/logo_main.png')}}" width="150">
                                  </div>
                                  <div class="col-sm-9">
                                    <h3 class="fontsmall">When making payments to Buy Sell Lease through the online form, your payment will appear on your statement as a payment to EZIDEBIT</h3>
                                  </div>
                                </div>

                                <div class="checkbox text-center">
                                    <label>
                                        {!! Form::checkbox('agree',Input::old('agree'),[]) !!}<span class="lbl padding-8 agreecon">I Agree</span>
                                        <label class="error">{!!$errors->first('agree')!!}</label>
                                    </label>
                                    
                                </div>

                                <h3>Introductory Offer</h3>
                                <p>As an Introductory Offer – Buy Sell Lease is offering New Subscribers a Fixed Price Guarantee for Five (5) Years from the date of the
                                    commencement of your Subscription with Buy Sell Lease. This is subject to five (5) continuous and unbroken years of subscription with
                                    Buy Sell Lease, from the date of commencement of your first year with Buy Sell Lease. This Introductory Offer may be withdrawn at any
                                    stage without Notice to you, and is only honoured on Agreements that have been entered into prior to the withdrawal of this offer.</p>





                                <div class="btn-row">
                                    <button type="submit" id="send-btn" class="btn btn-primary">Submit Registration</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>

                    <div class="col-md-12 address-box">
                        <div class="default-transparent">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h3>SALES</h3>
                                    <a href="mailto:sales@buyselllease.com.au">sales@buyselllease.com.au</a>

                                    <p>Tel 02 8960-7277</p>
                                </div>
                                <div class="col-sm-6">
                                    <h3>ADMIN</h3>
                                    <a href="mailto:admin@buyselllease.com.au">admin@buyselllease.com.au</a>

                                    <p>Tel 02 8960-7277<br><br></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix">&nbsp;</div>

                </div>

            </div>

            <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">

            </div>

        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-agent-signup-small.jpg') }}" width="100%" ></section>
    <script>
        $(function() {
            //Phone number masking
            $("#phonetext").unmask();
            $("#phonetext").on('change',function(){
                var phone_number_new = $("#phonetext").val();

                inputmask_phonenumber(phone_number_new,"#phonetext");
                
            });
            //Mobile number masking
            $("#mobiletext").on('change',function(){
                var mobile_number_new = $("#mobiletext").val();
                inputmask_phonenumber(mobile_number_new,"#mobiletext");
            });
            
             createUploader('my_uploader_id_agent_logo');
             createUploader('my_uploader_id_agent_profile');

             $('#send-btn').click(function (e) {
                  if($("#uploader-my_uploader_id_agent_logo-container").find('.filelist').is(":empty")){
                  $('.uploadfilecls').show();
                  $('.uploadfilecls').html('Logo field is required.');
                  $('body, html').animate({scrollTop:$('form').offset().top}, 'slow');
                  if($('#agent-signup-form').valid()){
                    return false;
                  }
              }
                  
              });
            //work phone number  formating
            $(":input").inputmask();

            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('suburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>

    <script>
        $(document).ready( function() {
            $('#promo1').change(function () {
                if (this.checked) {
                    $('#ans').fadeIn('slow');
                    $('#promocode').attr('required', true);
                } else {
                    $('#ans').fadeOut('slow');
                    $('#promocode').attr('required', false);
                }
            });

        });

    </script>
@stop

@section('header-script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[name=payment_type]').selected = false;
            showPaymentFields('none');
        });

        function showPaymentFields(paymentType) {
            $('.payment-item').css('display', 'none');
            $('.payment-' + paymentType).css('display', 'inline-block');
        }
    </script>

@stop
