
@extends('layouts.default.master')
@section('body-class', 'find-services-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">

                </div>
                <div class="pict-col col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="title-row"><h1>FIND SERVICES</h1></div>
                    <div class="default-transparent">
                        <p>Real Estate is an arduous process that involves the services of many professionals.</p>

                        <p>Buy Sell Lease provides you with the opportunity of finding real estate agents, conveyancers, finance brokers, insurance brokers, surveyors,
                            valuers, pest report, building report and the like.</p>

                        <p>With Buy Sell Lease, we provide you with every opportunity to find the professional to simplify your real estate experience and allow you to find
                            the right people quickly and efficiently.</p>


                        {{--<h4>Special Deals</h4>

                        <p>Offered exclusively and directly to our members and Subscribers of Buy Sell Lease ... </p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-find-services.jpg') }}" width="100%" ></section>
@stop


