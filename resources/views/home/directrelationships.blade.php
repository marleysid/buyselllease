@extends('layouts.default.master')
@section('body-class', 'relationships-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">

                <div class="pict-col col-lg-5 col-md-6 col-sm-12 col-xs-12">
                    <div class="title-row"><h1>DIRECT <br />RELATIONSHIPS</h1></div>
                    <div class="default-transparent">
                        <p>Real Estate is an industry built on property. The delivery of which is through direct relationships between people.</p>
                        <p>Buy Sell Lease provides a direct relationship between consumers and real estate agents and other real estate professionals.</p>
                        <p>Whether you are looking to Buy or Lease, or even Sell or Lease, Buy Sell Lease provides you with much more than generic property details. We provide you with the complete picture.

                            EVERY sales and rental listing that is offered by a real estate agent appears on their own
                            website. Property that appear on other real estate portals are mostly governed by time
                            period campaigns, as well as costings for the amount of photo’s, video, floorplans and other
                            content  that a real estate agent may upload.</p>

                        <p>Buy Sell Lease provides every property, every person and every service appearing on a real
                            estate agent of other real estate professionals website, to appear through Buy Sell Lease.
                            We do not place parameters with real estate groups of a time period campaign, the amount
                            of photos, video or floorplans, that increase the cost of real estate advertising.</p>

                        <p>In a nutshell, if it’s on a real estate agent or other real estate professionals website, it will
                            appear through Buy Sell Lease.</p>

                        <p>For the consumer, the depth of knowledge providing you with property, personnel and
                            services is unmatched. Within minutes, we perform your search and return the results you are looking for. We save
                            you valuable time and we save you in searching. Through our direct relationships, we
                            provide you with the most in depth information, directly to you.</p>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-direct-relationships.jpg') }}" width="100%" ></section>
@stop
