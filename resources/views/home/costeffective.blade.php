@extends('layouts.default.master')
@section('body-class', 'cost-effective-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">

                <div class="pict-col col-lg-5 col-md-8 col-sm-12 col-xs-12">
                    <div class="title-row"><h1>COST EFFECTIVE</h1></div>
                    <div class="default-transparent">
                        <p>Naturally, for Consumers, the service of Buy Sell Lease is FREE! </p>
                        <p>For real estate agents, Buy Sell Lease promotes your entire website content within your yearly subscription.
                            All your sale and lease properties, all your team, all your services and obviously,
                            your brand – within our one fixed price Annual Subscription of $5,000 + GST per annum for each office location.</p>
                        <h6>That’s less than $100 + GST per week.</h6>

                        <p>With Buy Sell Lease, there are no extra charges associated with time limit campaigns, priority placement, extra photos or video content. Simply, if it’s on your website, it’s on Buy Sell Lease.</p>

                        <p>For other real estate professionals engaged in the real estate industry offering legal, financial, insurance, pest,
                            building, surveying services and the like, the marketing of your business and your services directly from your own
                            website is offered at one fixed Annual Subscription of $3,000 + GST, marketing directly to consumers engaged in real estate. <br /><br /></p>
                        <h6>That’s less than $60 + GST per week.</h6>

                        <p>Direct Marketing to a real estate based market for an entire year.
                            Our low and attractive Annual Subscription rates – with no more to pay – ensure the delivery and marketing of your business
                            and personnel for the most attractive price of any marketing medium.<p>

                        <p>The most important and low cost investment that you can make to market and grow your business and services.</p>
                    </div>
                </div>
                <div class="col-lg-7 col-md-4 col-sm-12 col-xs-12">

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-cost-effective.jpg') }}" width="100%" ></section>
@stop
