@extends('emails.layouts.html')

@section('body')
<tr> <!-- header starts -->
        <td>
            <table style="height:60px; background:#323d45; padding:15px 40px;" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#fff; font-size:16px; font-weight:700;">Buy Sell Lease</td>
                    <td style="color:#fff; font-size:16px; font-weight:700; text-align:right;">New Professional</td>
                </tr>
            </table>
        </td>
    </tr> <!-- header ends -->
<tr> <!-- body container starts -->
    <td style="background:#fff; padding:20px;">
        <table style="background:#f7f7f7; margin:0 auto; padding:10px;" cellpadding="0" cellspacing="0" width="100%">
            
            <tr><td style="padding:30px 0 0 50px;"><h1 style="color:#323d45; font-size:20px; font-weight:700;">Hi BuySellLease Team,</h1></td></tr>
            <tr>
            	<td>
            		<p style="font-size:14px; color:#323d45; padding:0 40px 0 50px; line-height:25px;">
            			A Real Estate Professional has registered to join Buy Sell Lease. Please check Admin to Activate.
        	    	</p>
        	    	<p style="font-size:14px; color:#323d45; padding:0 40px 0 50px; line-height:25px;">
            			Professional Name:  {{ $name }} <br/>
                        Business Name: {{ $business_name }}  <br/>
                        Business Type: {{ GH::getCatName($business_type) }}  <br/>
        	    	</p>
	        	</td>
	    	</tr>
            <tr>
            	<td style="padding:10px 0 60px 50px;">
            		<a href="http://buyselllease.com.au/admin" style="font-size:16px; color:#f85352;">
            			Buy sell lease admin
            		</a>
            	</td>
            </tr>
        </table>
    </td>
</tr> <!-- body container ends -->
<tr> <!-- footer start -->
    <td style=" background:#f7f7f7; height:60px; border-top:1px solid #dcdcdc; padding:0 20px;">
    	<p style="font-size:12px; color:#757576;">
    		If you are not Buy Sell Lease, please disregard and destroy this information. This Information is Private and Confidential and is not to be used by any party other than Buy Sell Lease.
    	</p>
    </td>
</tr> <!-- footer ends -->
</table>
@stop