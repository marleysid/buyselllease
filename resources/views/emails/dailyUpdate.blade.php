@extends('emails.layouts.general')
@section('body')
    <div class="mail-area">
        <h4>Hi Buyselllease Admin,</h4>
        <div class="content">
            <p class="" style="text-align: justify;">
                This is auto generated reporting  for  property updates   on  our site from different  web hoster on {{date('Y-m-d')}}.
            </p>
        </div>

        @stop
        @section('footer')
            <div class="footer-area">
                <span class="clear:both;"> Many thanks</span><br>
                <a href="http://bsl.dev" class="float:left;clear:both;"><img src="{{ asset('img/logo-sm.png') }}"  height="100" width="100" alt="Buy Sell Lease"></a><br>
            <span>
        www.buyselllease.com.au<br>
        sales@buyselllease.com.au
                </span>
            </div>
    </div>
@stop