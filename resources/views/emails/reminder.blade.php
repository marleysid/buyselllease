@extends('emails.layouts.general')

@section('body')
<tr> <!-- header starts -->
        <td>
            <table style="height:60px; background:#323d45; padding:15px 40px;" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#fff; font-size:16px; font-weight:700;">Buy Sell Lease</td>
                    <td style="color:#fff; font-size:16px; font-weight:700; text-align:right;">Subscription Reminder</td>
                </tr>
            </table>
        </td>
    </tr> <!-- header ends -->
<tr> <!-- body container starts -->
    <td style="background:#fff; padding:20px;">
        <table style="background:#f7f7f7; margin:0 auto; padding:10px;" cellpadding="0" cellspacing="0" width="100%">
            
            <tr><td style="padding:30px 0 0 50px;"><h1 style="color:#323d45; font-size:20px; font-weight:700;">Hi {{$name}},</h1></td></tr>
            <tr>
                <td>
                    <p style="font-size:14px; color:#323d45; padding:0 40px 0 50px; line-height:25px;">
                        Your subscription will be expiring on three days. You can renew your subscription by <a href="{{$link}}">clicking here</a>.
                        If you donot want to renew your subscription, it will get deactivated from the system automatically. 
                    </p>
                    
                </td>
            </tr>
            <tr>
                <td style="padding:10px 0 60px 50px;">
                    
                    <p style="font-size:14px; color:#323d45; padding:0 40px 0 0px; line-height:25px;">
                        
                        Many Thanks <br />
                        <a href="http://buyselllease.com.au/" style="font-size:16px; color:#f85352;">
                            Buy sell lease Team
                        </a> <br />
                        <a href="mailto:sales@buyselllease.com.au">sales@buyselllease.com.au</a>

                    </p>
                </td>
            </tr>
        </table>
    </td>
</tr> <!-- body container ends -->
<tr> <!-- footer start -->
    <td style="background:#f7f7f7; height:60px; border-top:1px solid #dcdcdc; padding:0 20px;">
        <p style="font-size:12px; color:#757576;">
            If you are not {{ucfirst($name)}}, please disregard and destroy this information. This Information is Private and Confidential and is not to be used by any party other than Buy Sell Lease.
        </p>
    </td>
</tr> <!-- footer ends -->
</table>
@stop