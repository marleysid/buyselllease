@extends('emails.layouts.general')
@section('body')
    <div class="mail-area">
        <h4>Hi</h4>
        <div class="content">
            <span>Welcome to Buy Sell Lease!</span><br>

            Click here to reset your password: <a href="{{ url('password/reset/'.$token) }}">{{ url('password/reset/'.$token) }}</a>

        </div>


        @section('footer')
            <div class="footer-area">
                <span class="clear:both;"> Many thanks</span><br>
                <a href="http://bsl.dev" class="float:left;clear:both;"><img src="{{ asset('img/logo-sm.png') }}"  height="100" width="100" alt="Buy Sell Lease"></a><br>
            <span>
        www.buyselllease.com.au<br>
        sales@buyselllease.com.au
                </span>
            </div>
    </div>
@stop

