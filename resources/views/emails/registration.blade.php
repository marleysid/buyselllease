@extends('emails.layouts.html')

@section('body')
    <ul>
        <li>Name:       {{ $name }}</li>
        <li>Company:    {{ $company }}</li>
        <li>Trading:    {{ $trading }}</li>
        <li>ABN:        {{ $abn }}</li>
        <li>ACN:        {{ $acn }}</li>
        <li>Email:      {{ $email }}</li>
        <li>Website:    {{ $website }}</li>
        <li>Staff URL:  {{ $staffURL }}</li>
        <li>Address:    {{ $address }}</li>
        <li>Postcode:   {{ $postcode }}</li>
        <li>Mobile:     {{ $mobile }}</li>
        <li>Phone:      {{ $phone }}</li>
        <li>CRM:        {{ $crm }}</li>
        <li>Industry:   {{ $industry }}</li>
        <li>Service:    {{ $service }}</li>

    </ul>
@stop
