@extends('layouts.default.master')
@section('content-body')
    <div class="points-content" style="width:100%; height:100%; background:{{asset('img/bg-find-services.jpg')}};">
    <div class="container">
        <div class="row heading-row">
            <h2>
            Sorry, that page could not be found.</h2>

            Error 404: The page you requested does not exist, or is currently unavailable.
        </div>
    </div>
    </div>
    @endsection