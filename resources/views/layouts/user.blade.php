@extends('layouts.base')

@section('header-style')
    <link href="/css/user.css" rel="stylesheet">
@stop

@section('header-script')
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $('#forgot-button').on('click', function() {
                window.location = '{{{ URL::to('/users/forgot_password') }}}';
            });
        });
    </script>
@stop

@section('content-header')

@stop

@section('content-body')

<div class="container">
    @yield('content')
</div>

@stop

@section('content-footer')

@stop