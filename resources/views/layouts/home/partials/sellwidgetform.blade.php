<div id="sellForm">
    <div class="form_box">
        </br>
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group-lg">
                    <select class="form-control" style="width:100%" id="sell1">
                        <option value="" selected disabled>I WOULD LIKE TO SELL...</option>
                        <option value="1">MY NEW HOME</option>
                        <option value="2">A HOLIDAY PROPERTY</option>
                        <option value="3">AN INVESTMENT PROPERTY</option>
                        <option value="4">A RURAL PROPERTY</option>
                        <option value="5">A COMMERCIAL SPACE</option>
                        <option value="6">A COMMERCIAL INVESTMENT</option>
                        <option value="7">A DEVELOPMENT SITE</option>
                    </select>
                </div>
            </div>
        </div>
        </br>
        <div class="input-group-lg">
            <input type="text" class="form-control" style="width:100%" id="usr" placeholder="WHERE">
        </div>

        </br>
    </div>

</div>
<div id="sellForm2">
    <div id = "sellFormSelectors">
        <div class="form_box">
            </br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group-lg">
                        <select class="form-control" style="width:100%">
                            <option value="" selected disabled>TYPE OF PROPERTY</option>
                            <option>MY NEW HOME</option>
                            <option>A HOLIDAY PROPERTY</option>
                            <option>AN INVESTMENT PROPERTY</option>
                            <option>A RURAL PROPERTY</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="input-group-lg">
                        <select class="form-control" style="width:100%" id="sel1">
                            <option value="" selected disabled>BEDROOMS</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="input-group-lg">
                        <select class="form-control" style="width:100%" id="sel1">
                            <option value="" selected disabled>BATHROOMS</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>
                </div>
            </div>
            </br>
            </br>
        </div>
    </div>

    <div id = "sellFormSlider">
        <div class="form_box">
            <div class="row">


                <div class="col-lg-12" style="margin-left: 10px; margin-right: 20px;">

                    <input class="range-slider" type="hidden" value="2,8"/>


                </div>

            </div>

            <div class="row" style=" margin-right: 10px;">


                <div class="col-lg-12" style="margin-left: 20px; ">

                    <div id = "leftPrice" style="float: left"><h5><span>$10,000</span></h5></div>
                    <div id = "rightPrice" style="float: right"><h5><span>$1,000,000</span></h5></div>

                </div>

            </div>

        </div>


    </div>
</div>

<div id="sellForm3">
    <div class="form_box">
        <h4>I WOULD LIKE TO RECEIVE INFORMATION FROM...</h4>

        <div class="col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Solicitors - Conveyancers
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Accountant/Financial Adviser
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Painters
                </label>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Maintenance
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Gardner
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Rubbish removal
                </label>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Furniture hire
                </label>
            </div>
        </div>

        </br>
    </div>
</div>
