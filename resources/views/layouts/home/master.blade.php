@extends('layouts.base.master')

@section('content-header')
    <div id="home">
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <nav class="navbar" role="navigation" >
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                        <a href="{{ route('home.index') }}" /><img src="{{ asset('/img/logo-lg.png') }}"  alt="Buy Sell Lease"></a>
                    </div>
                </div>

                @include('partials.site-menu')

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 bsl-navigation" id="search-widget">
                    @include('search.partials.start')
                </div>
            </div>
        </nav>
    </div>

@stop

@section('content-main')

@stop

@section('content-body')
    <div class="container">
        <div id="content-main">
            @yield('content-main')
        </div>
    </div>
    <div class="container-fluid special-deals">
        <div class="container">
            <div class="row heading-row">
                <span class="special-header"><h3>OUR SPECIAL DEALS</h3></span> <span class="special-header"><p>Offered exclusively and directly to our members and subscribers of Buy Sell Lease.</p></span><!-- <p><a class="btn btn-secondary btn-sm" href="#" role="button">VIEW ALL</a></p> -->
            </div>
            <div class="row special-row">
                    @include('partials.deals-card')
            </div>
        </div>
    </div>

    <!-- layouts.home.partial.footer-script -->
    @include('layouts.home.partials.footer-script')

    <!-- footer-script -->
    @yield('footer-script')
@stop
