<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('meta-description')">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
</head>
<header>
    <script src="{{asset('js/vendor/jquery-1.11.1.min.js')}}"></script>
   <!--- <script src="{{asset('js/jquery.printPage.js')}}"></script>-->
    <script src="{{asset('js/jQuery.print.js')}}"></script>

    @yield('header')
</header>
<body id="whole-body" onLoad="printDiv();">
    @yield('body')
</body>

<footer>
    @yield('footer')
</footer>

</html>