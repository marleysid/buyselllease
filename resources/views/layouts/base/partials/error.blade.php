@if($errors->any())
<div class="alert alert-danger">

    <div class="row">
        <div class="col-md-1">
            <i class="icon fa fa-lg fa-warning"></i>
        </div>
        <div class="col-md-11">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon fa fa-lg fa-times-circle"></i></button>
            @foreach($errors->all() as $error)
                <div> {{ $error }}</div>
            @endforeach

        </div>
    </div>
</div>
@endif