<script src="{{ asset('/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>

<script src="{{ asset('/js/plugins.js?v='.time()) }}"></script>
<script src="{{ asset('js/jquery.maskedinput.js?v='.time()) }}"></script>
<script src="{{ asset('js/jquery.validate.min.js?v='.time()) }}"></script>
<script src="{{ asset('js/additional-methods.min.js?v='.time()) }}"></script>
<script src="{{ asset('/js/main.js?v='.time()) }}"></script>
<script src="{{ asset('/js/custom.js?v='.time()) }}"></script>
<script src="{{ asset('/js/plupload/plupload.full.min.js?v='.time()) }}"></script>
<script src="{{ asset('/js/plupload/upload.js?v='.time()) }}"></script>
<script src="{{ asset('/js/multiselect/multiselect.js?v='.time()) }}"></script>
<script src="{{ asset('js/bootbox.min.js?v='.time()) }}"></script>
<script src="{{ asset('js/ezidebit_2_0_0.min.js') }}"></script>




<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    var site_url = '{{ url() }}';
    var token = '{{ csrf_token() }}';
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='http://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
var endpoint = "https://api.demo.ezidebit.com.au/V3-5/public-rest";
	eziDebit.init(00899CD9-E5B3-4708-B04F-E02A4446E8B2, {
  // Extra parameters will go here between the curly braces
}, endpoint);
      
</script>