<script src="{{asset('js/subscribe.js')}}"></script>
<div id="footer" class="noprint">
    <div class="container">
        <div class="row links-row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                <h5>PROPERTY</h5>
                <div class="wrapper">
                    <ul>
                        <li><a href="{{url('/').'?search=buy'}}" onClick="search_next('buystep1', '')">Buy</a></li>
                        <li><a href="{{url('/').'?search=sell'}}" onClick="search_next('sellstep1', '')">Sell</a></li>
                        <li><a href="{{url('/').'?search=lease'}}"  onClick="search_next('leasestep1', '')">Lease</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-9">
                <h5>ABOUT US</h5>
                <div class="wrapper">
                    <ol>
                        <li><a href="{{ route('faq.customers') }}">faq</a></li>
                        <li><a href="{{ route('home.contactus') }}">contact us</a></li>
                        <li><a href="{{ route('home.termsofuse') }}">Terms of use</a></li>
                        <li><a href="{{ route('login') }}">Join now</a></li>
                        <li><a href="{{ route('home.privacypolicy') }}">Privacy policy</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>
                    </ol>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 newsletter">
                <img src="{{ asset('/img/bsl-news.png') }}"  alt="Buy Sell Lease">

                <!--subscibe form-->
                <form name="sub-form" id="subscribe">

                    <div class="input-group">
                        <input name="email" id="sub-email" type="text" class="form-control" placeholder="Enter your email">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" id="submit" type="submit">Subscribe</button>
                        </span>
                    </div>
                    <i class="fa fa-spinner" id="spin" style="display: none"></i>
                    <span class="sub-error error"></span>
                </form>
            <!--subscribe form ends here-->
            </div>
        </div>


        <script>
          var search_mode = '{{Input::get('search')}}';
          var current_url = '{{URL::full()}}';  
          var homepage = '{{url('/')}}'+'?search='+search_mode;
        
          if(search_mode !='' ){
            if(current_url == homepage){
                
                switch (search_mode) {
                    case 'buy':
                         search_next('buystep1', '');
                         break;
                    case 'sell':
                        search_next('sellstep1', '');
                         break;
                    case 'lease' :
                         search_next('leasestep1', '');
                         break;
                }
            }
          }
          
        </script>
        <div class="row copyright-row">
            <p>Copyright &copy;  {{ date('Y') }} Buy Sell Lease.</p>
        </div>
    </div>
</div>







