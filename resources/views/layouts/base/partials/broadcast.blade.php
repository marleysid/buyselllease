@if(env('BROADCAST_MESSAGE') != '')
    <div class="container-fluid text-center bg-success">
        <p class="text-uppercase text-success">{{ env('BROADCAST_MESSAGE') }}</p>
    </div>
@endif
