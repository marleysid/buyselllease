<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title','BuySellLease')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <!-- Additional Header Tags -->
        @yield('header-tags')
        <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
        <!-- JQuery -->
        @include('layouts.base.partials.header-jquery')

        <!-- BootStrap -->
        @include('layouts.base.partials.header-bootstrap')

        <!-- Global Style -->
        @include('layouts.base.partials.header-style')

        <!-- Custom Style -->
        @yield('header-style')

        <!-- Global Script -->
        @include('layouts.base.partials.header-script')

        <!-- Custom Script -->
        @yield('header-script')
    </head>
    <body class="@yield('body-class','')">
        @include('layouts.base.partials.broadcast')

        @yield('content-header')

        @yield('content-body')

        @include('layouts.base.partials.footer')
    </body>
    <script type="text/javascript">
        /*
         *function to automatically hide the alert message after 6 second
         */
        $(".alert").fadeTo(6000, 500).slideUp(500, function(){
            $(".alert").alert('close');
        });
</script>
</html>
