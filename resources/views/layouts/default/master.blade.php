@extends('layouts.base.master')

@section('content-header')
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo-sm">
                <a href="{{ route('home.index') }}" /><img src="{{ asset('/img/logo-sm.png') }}"  alt="Buy Sell Lease">
                </a>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        @include('partials.site-menu')
        @if (Auth::check())
            @if(Auth::user()->role != '1'  )
                @include('partials.user-menu')
            @endif

        @endif

    </div>

</nav>




@stop
