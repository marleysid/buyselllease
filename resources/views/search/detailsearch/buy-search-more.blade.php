<div class="col-md-12">
    <div class="row drop">
        <div class="col-md-12">
            <div class="input-group-md">
                <select name="category-action[]" id="category-action" class="form-control" multiple="multiple">
                    @foreach($categoryOptions  as $key=>$value)
                          <option value="{{$key}}" {{isset($searchProfileData['category-action'])?in_array($key,$searchProfileData['category-action'])?'selected':'':''}} >{{$value}}</option>
                        @endforeach
                </select>

                <span class="error">{!!$errors->first('category-action')!!}</span>
            </div>
        </div>
    </div>
</div>
{{--{{dd($searchProfileData)}}--}}
<div class="col-md-6 col-sm-6 row">
<div class="col-md-6">
    <div class="row drop">
        <div class="col-md-12">
            <div class="btn-group input-checks-button" data-toggle="buttons">
                <label class="btn btn-default disabled">
                    Bedrooms
                </label>
                @define $bedrooms = ['1', '2', '3', '4+']
                @foreach($bedrooms as $key=>$val)
                    <label class="btn btn-default {{isset($searchProfileData['bedrooms'])?in_array($val,$searchProfileData['bedrooms'])?'active':'':''}} ">
                        <input type="checkbox" name="bedrooms[]"   {{isset($searchProfileData['bedrooms'])?in_array($val,$searchProfileData['bedrooms'])?'checked':'':''}}  value="{{$val}}">{{$val}}
                    </label>

                @endforeach
            </div>
        </div>
    </div>

    <div class="row drop">
        <div class="col-md-12">
            <div class="btn-group input-checks-button" data-toggle="buttons">
                <label class="btn btn-default disabled">
                    Bathrooms
                </label>
                @define $bathrooms = ['1', '2', '3', '4+']
                @foreach($bathrooms as $key=>$val)
                    <label class="btn btn-default {{isset($searchProfileData['bathrooms'])?in_array($val,$searchProfileData['bathrooms'])?'active':'':''}}">
                        <input type="checkbox" name="bathrooms[]"  {{isset($searchProfileData['bathrooms'])?in_array($val,$searchProfileData['bathrooms'])?'checked':'':''}} value="{{$val}}">{{$val}}
                    </label>

                @endforeach
            </div>
        </div>
    </div>

    <div class="row  drop">
        <div class="col-md-12">
            <div class="btn-group input-checks-button" data-toggle="buttons">
                <label class="btn btn-default disabled">
                    Parking
                </label>
                @define $parking = ['1', '2', '3','4+']
                @foreach($parking as $key=>$val)
                    <label class="btn btn-default {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'active':'':''}}">
                        <input type="checkbox" name="parking[]"  {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'checked':'':''}} value="{{$key}}">{{$val}}
                    </label>

                @endforeach
            </div>
            <!-- </select> -->
        </div>
    </div>

    <!-- <div class="row  drop">
        <div class="col-md-12">

            <div class="btn-group input-checks-button" data-toggle="buttons">
                <label class="btn btn-default disabled">
                    Tenure
                </label>
                @define $tenure = ['1'=>'Vacant Possesion', '2'=>'Leases', '3'=>'All']
                @foreach($tenure as $key=>$val)

                    <label class="btn btn-default {{isset($searchProfileData['tenure'])?in_array($key,$searchProfileData['tenure'])?'active':'':''}}">
                        <input type="checkbox" title="asdasdasd" name="tenure[]" {{isset($searchProfileData['tenure'])?in_array($key,$searchProfileData['tenure'])?'checked':'':''}} value="{{$val}}">{{$val}}
                    </label>

                @endforeach
            </div>
        </div>
    </div> -->
    </div>
<div class="col-md-6">
    <div class="row add">
        <div class="col-md-12 col-sm-12">
            <div class="input-group-md">
                {!! Form::text('floor-area',   Input::old('floor-area') or isset($searchProfileData['floor-area'])?$searchProfileData['floor-area']:'', ['id' => 'floor-area', 'class'=>'form-control', 'placeholder' => 'FLOOR AREA']) !!}
            </div>
        </div>
         <div class="col-md-12 col-sm-12">
            <div class="input-group-md">
                {!! Form::text('land-area', Input::old('land-area') or isset($searchProfileData['land-area'])?$searchProfileData['land-area']:'', ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA (MIN)']) !!}
            </div>
        </div>
    </div>

    

    <div class="row ">
        <!-- <div class="col-md-12 col-sm-12">
            <div class="input-group-md">
                {!! Form::text('return', Input::old('return') or isset($searchProfileData['return'])?$searchProfileData['return']:'' , ['id' => 'return', 'class'=>'form-control', 'placeholder' => '% RETURN (P.A.)']) !!}
            </div>
        </div> -->
         <div class="col-md-12 col-sm-12">
            <div class="input-group-md">
                {!! Form::text('lettable-space', Input::old('lettable-space')  or isset($searchProfileData['lettable-space'])?$searchProfileData['lettable-space']:'', ['id' => 'lettable-space', 'class'=>'form-control', 'placeholder' => 'SIZE OF LETTABLE SPACE']) !!}
            </div>
        </div>
    </div>

</div>

    
    @define $mode = isset($searchProfileData['mode']) ? $searchProfileData['mode'] :'residential'
    @define $type ='buy'
<div class="col-md-12">
    <div class="row  drop">
        <div class="col-md-12">
            <!-- <div class="slider-draggable"> -->
                <div id="sliderContainer" class="col-xs-12">
                    <input class="range-slider" name="price-range" type="hidden" value="{{ $searchProfileData['price-range']  or '0,200'}}"/>
                </div>

                <div class="col-xs-6 text-left">
                    <div id="leftPrice" style="float: left"><h5><span></span></h5></div>
                </div>
                <div class="col-xs-6 text-right">
                    <div id="rightPrice" style="float: right"><h5><span></span></h5></div>
                </div>
            <!-- </div> -->
        </div>
    </div>
</div>
</div>

<div class="col-md-6 col-sm-6">

    
    <div class="row">
    
        <div class="col-md-6 col-sm-6">
        <span><strong>Property Types</strong></span>
            <!-- <label class="checkbox-inline">
                <input type="checkbox" value=""  name="propertytype[]">
                <span class="custom-checkbox"></span>
                <span>All Types</span>
            </label> --> <!-- <br> -->
                  
            @foreach($residentialActionOptions as $key=>$value)
                @if(($key !='I WOULD LIKE TO BUY...'))
                    <label class="checkbox-inline">
                        <input type="checkbox" value="{{$key}}" {{isset($searchProfileData['residential-action'])?in_array($key,$searchProfileData['residential-action'])?'checked':'':''}} name="residential-action[]">
                        <span class="custom-checkbox"></span>
                        <span>{{$value}}</span>
                    </label><br>
                @endif
                
            @endforeach
        </div>
       
        <div class="col-md-6 col-sm-6">
        <span><strong>Commercial Options</strong></span>
        <span style="display:none;">{{$j = 0}}</span>
            @foreach($commercialActionOptions as $key=>$value)
                @if(($key !='I WOULD LIKE TO BUY...') AND ($key < 10))
                    <label class="checkbox-inline">
                        <input type="checkbox" value="{{$key}}" {{isset($searchProfileData['commercial-action'])?in_array($key,$searchProfileData['commercial-action'])?'checked':'':''}} name="commercial-action[]">
                        <span class="custom-checkbox"></span>
                        <span>{{$value}}</span>
                    </label><br>
                @endif
               <span style="display:none;">{{$j++}}</span>
            @endforeach
        </div>

    </div>
</div>


<script>

    $(document).ready(function () {
        // if($(".agreement-type").is(":checked")){
        //     loadSlider();
        //     console.log("update");
        //  }
        
        // $(".search-more").click(function(){
        //     loadSlider();  
        //     console.log("update");  
        // });
    loadSlider();
    //$(".advance-form").hide();
    });


    function loadSlider() {
        var type = $('.agreement-type:checked').val();

        $('.range-slider').jRange({
            from: 0,
            to: 200,
            step: 1,
            format: '%s',
            width: '100%',
            showLabels: false,
            showScale: false,
            isRange: true,
            denominationLow: {{ \SH::getRangeMultiplier($type, $mode, 'low') }},
            denominationHigh: {{ \SH::getRangeMultiplier($type, $mode, 'high') }},
            onstatechange: function (value) {
                console.log(value);
                console.log(this.options);
            }
        });
    }


    $('#category-action').multiselect({
        buttonWidth: '100%',
        nonSelectedText: 'I WOULD LIKE TO  SEARCH OTHER REAL ESTATE RELATED SERVICES'
    });
</script>

