
<div class="col-md-12">
    <div class="row drop">
        <div class="col-md-12">
            <div class="input-group-md">
                {!! Form::select('action', $actionOptions, Input::old('action')?Input::old('action'):isset($searchProfileData['action'])?$searchProfileData['action']:null , ['id' => 'action', 'class'=>'form-control']) !!}
                <span class="error">{!!$errors->first('action')!!}</span>
            </div>
        </div>
    </div>
</div>



<div class="more-sell-search-options">

</div>


<script>

    $(document).ready(function () {
        //getProprtyTypeOptions();

        // $('#action').on('change', function () {
        //     var $this = $(this);
        //     var ptype = $this.val();

        //     if (ptype != 'I WOULD LIKE TO SELL...') {
        //         $.ajax({
        //             url: site_url + '/search/get-sell-more-option',
        //             type: 'post',
        //             dataType: 'html',
        //             data: {'ptype': ptype, '_token': '{{csrf_token()}}'},
        //             beforeSend: function () {
        //                 $('.more-sell-search-options').html("<p class='loading'>Loading...</p>");
        //             }
        //         }).done(function (html) {

        //             $('.more-sell-search-options').html(html);

        //         }).error(function (xhr, desc, err) {
        //             $('.tenant-or-landlord-options').html("<p class='loading'>" + err + "</p>");
        //         })
        //     }

        // });

    });


    function getProprtyTypeOptions() {
        var property_type = $('#action option:selected').val();

        if (property_type != 'I WOULD LIKE TO SELL...'){
            $.ajax({
                url: site_url + '/search/get-sell-more-option',
                type: 'post',
                dataType: 'html',
                data: {'ptype': property_type, '_token': '{{csrf_token()}}'},
                beforeSend: function () {
                    $('.more-sell-search-options').html("<p class='loading'>Loading...</p>");
                }
            }).done(function (html) {

                $('.more-sell-search-options').html(html);

            }).error(function (xhr, desc, err) {
                $('.tenant-or-landlord-options').html("<p class='loading'>" + err + "</p>");
            })
    }
    }

</script>