

<div class="col-md-12">
    <div class="row drop">
        <div class="col-md-12">
            <div class="input-group-md">
                <select name="category-action[]" id="category-action" class="form-control" multiple="multiple">
                @foreach($categoryOptions  as $key=>$value)
                    <option value="{{$key}}" {{isset($searchProfileData['category-action'])?in_array($key,$searchProfileData['category-action'])?'selected':'':''}} >{{$value}}</option>
                @endforeach
                    </select>
                <span class="error">{!!$errors->first('category-action')!!}</span>
            </div>
        </div>

    </div>
       {{-- {{dd($searchProfileData)}}--}}
    <div class="row drop">
        <div class="col-md-12">
            <div class="input-group-md">
                {!! Form::select('action', $actionOptions,  Input::old('action') ? Input::old('action') : isset($searchProfileData['action'])?$searchProfileData['action']:'', ['id' => 'action', 'class'=>'form-control']) !!}
                <span class="error">{!!$errors->first('action')!!}</span>
            </div>
        </div>
    </div>
</div>

<div class="more-sell-search-options">

</div>


<script>

    $(document).ready(function(){

        getPropertyTypeOptions();

        $('#action').on('change',function(){
           var $this = $(this);
           var ptype = $this.val();


            $.ajax({
                url:site_url+'/search/get-sell-more-option',
                type:'post',
                dataType:'html',
                data: {'ptype':ptype,'_token':'{{csrf_token()}}'},
                beforeSend:function(){
                    $('.more-sell-search-options').html("<p class='loading'>Loading...</p>");
                }
            }).done(function(html){

                $('.more-sell-search-options').html(html);

            }).error(function(xhr, desc, err){
                $('.more-sell-search-options').html("<p class='error'>"+err+"</p>");
            })

        });

    });


    function getPropertyTypeOptions() {
        var property_type = $('#action option:selected').val();
        console.log(property_type);
        if (property_type != 'I WOULD LIKE TO SELL...') {
            $.ajax({
                url: site_url + '/search/get-sell-more-option',
                type: 'post',
                dataType: 'html',
                data: {'ptype': property_type, '_token': '{{csrf_token()}}'},
                beforeSend: function () {
                    $('.more-sell-search-options').html("<p class='loading'>Loading...</p>");

                }
            }).done(function (html) {

                $('.more-sell-search-options').html(html);

            }).error(function (xhr, desc, err) {
                $('.more-sell-search-options').html("<p class='error'>" + err + "</p>");
            })
        }
    }

    $('#category-action').multiselect({
        buttonWidth: '100%',
        nonSelectedText: 'I WOULD LIKE TO  SEARCH OTHER REAL ESTATE RELATED SERVICES'
    });
</script>