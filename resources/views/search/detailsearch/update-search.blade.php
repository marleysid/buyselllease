<style>
.drop {
	padding-top: 10px;
}
#sliderContainer {
	padding-top: 20px;
}
.input-checks-button > .active {
	color: #fff;
	background-color: red;
}
.edit-search {
	border:2px solid red;
}
</style>
<div class="edit-search-wrap">
    <div class="row edit-search">
      <div class="col-md-12">
        <div class="row drop">
          <div class="col-md-12">
            <form action="{{url('dashboard/advance/search')}}" method="post" id="update-search">
              {!! Form::token() !!}
              <div class="row">
                <div class="col-md-12">
                  <div class="input-property-status"> <a href="javascript:void(0);" class="btn-clear"><i class="fa fa-cross"></i></a>
                    <div class="btn-group tab-like-radio" data-toggle="buttons">
                      <label class="btn btn-default {{$searchProfile['agreement-type'] =='buy'?'active':'' }}">
                        <input name="agreement-type" value="buy" class="agreement-type {{$searchProfile['agreement-type'] =='buy'?'active':'' }}" {{$searchProfile['agreement-type'] =='buy'?'checked':'' }} type="radio">
                        BUY </label>
                      <label class="btn btn-default {{$searchProfile['agreement-type'] =='sell'?'active':'' }}">
                        <input name="agreement-type" value="sell"  class="agreement-type {{$searchProfile['agreement-type'] =='sell'?'active':'' }}" {{$searchProfile['agreement-type'] =='sell'?'checked':'' }} type="radio">
                        SELL </label>
                      <label class="btn btn-default {{$searchProfile['agreement-type'] =='lease'?'active':'' }}">
                        <input name="agreement-type" value="lease" class="agreement-type {{$searchProfile['agreement-type'] =='lease'?'active':'' }}" {{$searchProfile['agreement-type'] =='lease'?'checked':'' }}  type="radio">
                        LEASE </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="input-group input-name"> {!! Form::text('suburb',Input::old('suburb')?Input::old('suburb'):$searchProfile['suburb'],['id' => 'suburb', 'class'=>'form-control', 'placeholder' => "Suburb"]) !!} <span class="clear-btn">clear</span> <span class="errorss">{!!$errors->first('suburb')!!}</span> <span class="input-group-btn">
                    <button class="btn  btn-primary  search_bn"><i class="fa fa-search fa-2x"></i></button>
                    </span> </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 input-include">
                  <div class="row">
                    <div class="col-sm-6">
                      <label class="checkbox-inline">
                        <input type="checkbox" id="inc-surrounding" value="1"  {{ isset($searchProfile['inc-surrounding']) && $searchProfile['inc-surrounding']?'checked':'' }} name="inc-surrounding">

                        <span class="custom-checkbox"></span> <span>Include surrounding suburbs</span> </label>
                    </div>
                    
                    <!--5 km, 10km-->
                    <?php 
                      if(array_key_exists('cover_distance', $searchProfile)){
                        $variable_distance = $searchProfile['cover_distance'];
                      }else{
                        $variable_distance = "";
                      }

                    ?>
                    <div class="col-sm-6 distancekm" style="{{ isset($searchProfile['inc-surrounding']) && $searchProfile['inc-surrounding']?'display:block;':'display:none;' }}">
                      <div class="radio radio-box-holder-wrap">
                        <div class="check-box-wrap-hold"> {!! Form::radio('cover_distance', '5', ($variable_distance==5 ? "checked": ""), ['id'=>'5km','class' => '']) !!}
                          <label for="5km" class="check-box-area">5 km</label>
                        </div>
                        <div class="check-box-wrap-hold"> {!! Form::radio('cover_distance', '10', ($variable_distance == 10 ? "checked" : ""), ['id'=>'10km','class' => '']) !!}
                          <label for="10km" class="check-box-area">10 km</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 option-switch"> 
                  <a data-toggle="collapse" data-target="#collapseOne" class="search-more pull-right"> 
                    <span>Advance Search</span> 
                    <span class="up-down fa fa-caret-down"></span> 
                  </a> 
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="row">
                <div class="more-filter" id="collapseOne">
                  <div class="advance-form" style="position: absolute; top: -9999px;">
                    <div class='more-filter-fields'> </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="btn" style="float: right;">
                      <button class="btn pull-right btn-primary"> Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript">


    // add autocomplete for suburb field
    if (document.getElementById('suburb') != null) {

        new google.maps.places.Autocomplete(
                document.getElementById("suburb"),
                {

                    'componentRestrictions': {
                        'country': 'au'
                    },
                    'types': ['(cities)']
                });
    }

    $(document).ready(function () {


        //loadSlider();

        $('#inc-surrounding').on('change', function () {
            if ($(this).is(":checked")) {
                $('.distancekm').show();
                $("#5km").prop('checked', true);
            } else {
                $(".distancekm").hide();
                $("#10km").prop('checked', false);
                $("#5km").prop('checked', false);
            }
        });


        $('.search-more').on('click',function(){
           
            if($(this).find('.up-down').hasClass('fa-caret-down')) {
                $(this).find('.up-down').removeClass('fa-caret-down');
                $(this).find('.up-down').addClass('fa-caret-up');

                $('.advance-form').css({"position": "relative", "top": "0", "left": "0"});
            } else {
                $(this).find('.up-down').addClass('fa-caret-down');
                $(this).find('.up-down').removeClass('fa-caret-up');

                 //$('.advance-form').css({'display','none'});
            }
        });


        $('.agreement-type').on('change', function () {
            var $this = $(this);
            var agreement_type = $this.val();

            $.ajax({
                        url: site_url + "/search/get-more-fields",
                        type: 'POST',
                        dataType: 'html',
                        data: {'agreement_type': agreement_type, '_token': '{{ csrf_token() }}'},
                        beforeSend: function () {
                            $('.more-filter-fields').html("<p class='loading'>Loading...</p>");

                        }

                    })
                    .done(function (html) {

                        $('.more-filter-fields').html(html);
                        
                    })
                    .error(function (xhr, desc, err) {
                        $('.tenant-or-landlord-options').html("<p class='loading'>"+err+"</p>");
                    });
        });


        //load advance search options  on load
        triggerChangeSearchMode();


    });

    function triggerChangeSearchMode() {
        var search_type = $('.agreement-type:checked').val();

        $.ajax({
                    url: site_url + "/search/get-more-fields",
                    type: 'POST',
                    dataType: 'html',
                    data: {'agreement_type': search_type, '_token': '{{ csrf_token() }}'},
                    beforeSend: function () {
                        $('.more-filter-fields').html("<p class='loading'>Loading...</p>");

                    }

                })
                .done(function (html) {

                    $('.more-filter-fields').html(html);
                    //$('.range-slider').jRange('valuesToPrc', '0,200');
                   //console.log($( ".more-filter-fields" ).find( ".range-slider" ));

                })
                .error(function (xhr, desc, err) {
                    $('.tenant-or-landlord-options').html("<p class='loading'>"+err+"</p>");
                });
    }
</script>