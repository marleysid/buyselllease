

    <div class="col-md-6 col-sm-6">

        @if($mode == 1)
            <div class="row drop">
                <div class="col-md-6">
                    <div class="btn-group input-checks-button" data-toggle="buttons">
                        <label class="btn btn-default disabled">
                            Bedrooms
                        </label>
                        @define $bedrooms = ['1', '2', '3', '4+']
                        @foreach($bedrooms as $key=>$val)
                        <label class="btn btn-default {{isset($searchProfileData['bedrooms'])?in_array($val,$searchProfileData['bedrooms'])?'active':'':''}}">
                            <input type="checkbox" name="bedrooms[]"  {{isset($searchProfileData['bedrooms'])?in_array($val,$searchProfileData['bedrooms'])?'checked':'':''}} value="{{$val}}">{{$val}}
                        </label>

                            @endforeach
                    </div>


                </div>
            </div>

            <div class="row drop">
                <div class="col-md-6">

                    <div class="btn-group input-checks-button" data-toggle="buttons">
                        <label class="btn btn-default disabled">
                            Bathrooms
                        </label>
                        @define $bathrooms = ['1', '2', '3', '4+']
                        @foreach($bathrooms as $key=>$val)
                            <label class="btn btn-default {{isset($searchProfileData['bathrooms'])?in_array($val,$searchProfileData['bathrooms'])?'active':'':''}}">
                                <input type="checkbox" name="bathrooms[]"  {{isset($searchProfileData['bathrooms'])?in_array($val,$searchProfileData['bathrooms'])?'checked':'':''}} value="{{$key}}">{{$val}}
                            </label>

                        @endforeach
                    </div>

                </div>
            </div>

            <div class="row drop">
                <div class="col-md-6">
                    <div class="btn-group input-checks-button" data-toggle="buttons">
                        <label class="btn btn-default disabled">
                            Parking
                        </label>
                        @define $parking = ['1', '2', '3' , '4+']
                        @foreach($parking as $key=>$val)
                            <label class="btn btn-default {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'active':'':''}}">
                                <input type="checkbox" name="parking[]" {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'checked':'':''}} value="{{$key}}">{{$val}}
                            </label>

                        @endforeach
                    </div>

                </div>
            </div>


            <div class="row drop">
                <div class="col-md-6">
                    <div class="input-group-md">
                        {!! Form::text('land-area', Input::old('land-area') ? Input::old('land-area') : isset($searchProfileData['land-area'])?$searchProfileData['land-area']:'', ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA']) !!}
                    </div>

                </div>
            </div>
        @elseif($mode == 2)
            <div class="row ">
                <div class="col-md-6 col-sm-12">
                    <div class="input-group-md">
                        {!! Form::text('floor-area', Input::old('floor-area') ? Input::old('floor-area') :isset($searchProfileData['floor-area'])?$searchProfileData['floor-area']:'', ['id' => 'floor-area', 'class'=>'form-control', 'placeholder' => 'FLOOR AREA']) !!}
                    </div>
                </div>
                 <div class="col-md-6 col-sm-12">
                    <div class="input-group-md">
                            {!! Form::text('land-area', Input::old('land-area') ? Input::old('land-area') : isset($searchProfileData['land-area'])?$searchProfileData['land-area']:'', ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA (MIN)']) !!}
                        </div>
                </div>
                </div>


                <!--<div class="row drop">
                    <div class="col-md-12">

                        <div class="btn-group input-checks-button" data-toggle="buttons">
                            <label class="btn btn-default disabled">
                                Tenure
                            </label>
                            @define $tenure = ['TENURE', 'VACANT POSSESSION', 'LEASES', 'ALL']
                            @foreach($tenure as $key=>$val)
                                <label class="btn btn-default {{isset($searchProfileData['tenure'])?in_array($val,$searchProfileData['tenure'])?'active':'':''}}">
                                    <input type="checkbox" name="tenure[]"  {{isset($searchProfileData['tenure'])?in_array($val,$searchProfileData['tenure'])?'checked':'':''}} value="{{$key}}">{{$val}}
                                </label>

                            @endforeach
                        </div>

                    </div>
                </div>-->

                <div class="row add02">
                    <div class="col-md-6">
                        <div class="btn-group input-checks-button" data-toggle="buttons">
                            <label class="btn btn-default disabled">
                                Parking
                            </label>
                            @define $parking = ['1', '2', '3' , '4+']
                            @foreach($parking as $key=>$val)
                                <label class="btn btn-default {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'active':'':''}}">
                                    <input type="checkbox" name="parking[]"  {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'checked':'':''}}value="{{$key}}">{{$val}}
                                </label>

                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row drop">
                    <div class="col-md-6 col-sm-12">
                        <div class="input-group-md">
                            {!! Form::text('return', Input::old('return') ? Input::old('return'):isset($searchProfileData['return'])?$searchProfileData['return']:'', ['id' => 'return', 'class'=>'form-control', 'placeholder' => '% RETURN (P.A.)']) !!}
                        </div>
                    </div>
                     <div class="col-md-6 col-sm-12">
                         <div class="input-group-md">
                            {!! Form::text('lettable-space', Input::old('lettable-space')?Input::old('lettable-space'):isset($searchProfileData['lettable-space'])?$searchProfileData['lettable-space']:'', ['id' => 'lettable-space', 'class'=>'form-control', 'placeholder' => 'SIZE OF LETTABLE SPACE']) !!}
                        </div>
                    </div>
                </div>

                @elseif($mode == 3)
                    <div class="row ">
                        <div class="col-md-6 col-sm-12">
                            <div class="input-group-md">
                                {!! Form::text('floor-area', Input::old('floor-area')?Input::old('floor-ara'):isset($searchProfileData['floor-area'])?$searchProfileData['floor-area']:'', ['id' => 'floor-area', 'class'=>'form-control', 'placeholder' => 'FLOOR AREA']) !!}
                            </div>
                         </div>
                          <div class="col-md-6 col-sm-12">
                            <div class="input-group-md">
                                {!! Form::text('land-area', Input::old('land-area')?Input::old('land-area'):isset($searchProfileData['land-area'])?$searchProfileData['land-area']:'', ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA (MIN)']) !!}
                            </div>
                        </div>
                    </div>


                    <!--<div class="row drop">
                        <div class="col-md-12">
                            <div class="btn-group input-checks-button" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    Tenure
                                </label>
                                @define $tenure = ['TENURE', 'VACANT POSSESSION', 'LEASES', 'ALL']
                                @foreach($tenure as $key=>$val)
                                    <label class="btn btn-default {{isset($searchProfileData['tenure'])?in_array($val,$searchProfileData['tenure'])?'active':'':''}}">
                                        <input type="checkbox" name="tenure[]" {{isset($searchProfileData['tenure'])?in_array($val,$searchProfileData['tenure'])?'checked':'':''}} value="{{$key}}">{{$val}}
                                    </label>

                                @endforeach
                            </div>
                        </div>
                    </div>-->

                    <div class="row add02">
                        <div class="col-md-6">
                            <div class="btn-group input-checks-button" data-toggle="buttons">
                                <label class="btn btn-default disabled">
                                    Parking
                                </label>
                                @define $parking = ['1', '2', '3', '4+']
                                @foreach($parking as $key=>$val)
                                    <label class="btn btn-default {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'active':'':''}}">
                                        <input type="checkbox" name="parking[]" {{isset($searchProfileData['parking'])?in_array($val,$searchProfileData['parking'])?'checked':'':''}} value="{{$key}}">{{$val}}
                                    </label>

                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="row drop">
                        <div class="col-md-6 col-sm-12">
                            <div class="input-group-md">
                                {!! Form::text('return', Input::old('return')?Imput::old('return'):isset($searchProfileData['return'])?$searchProfileData['return']:'', ['id' => 'return', 'class'=>'form-control', 'placeholder' => '% RETURN (P.A.)']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row drop">
                        <div class="col-md-6">
                            <div class="input-group-md">
                                {!! Form::checkbox('da-approved', Input::old('da-approved'),  Input::old('da-approved',isset($searchProfileData['da-approved'])?$searchProfileData['da-approved']:null) =='on', ['id' => 'da-approved', 'class'=>'ios']) !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>




            @if(count($typeOptions)>0)
        <div class="col-md-6 col-sm-6">

            <span><strong>Property Types</strong></span>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                   <!--  <label class="checkbox-inline">
                        <input type="checkbox" value=""  name="propertytype[]">
                        <span class="custom-checkbox"></span>
                        <span>All Types</span>
                    </label> <br> -->

                    @foreach($typeOptions as $key=>$value)
                          @if($key !='')
                              <label class="checkbox-inline">
                                  <input type="checkbox" value="{{$key}}"  {{isset($searchProfileData['property-type'])?in_array($key,$searchProfileData['property-type'])?'checked':'':''}} name="residential-action[]">
                                  <span class="custom-checkbox"></span>
                                  <span>{{$value}}</span>
                              </label><br>
                          @endif
                      @endforeach


            </div>
        </div>
            </div>
            @endif
    <script>
        $('.ios').iosCheckbox();
    </script>

