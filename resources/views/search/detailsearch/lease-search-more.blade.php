<div class="col-md-12">
    <div class="row drop">
        <div class="col-md-12">
            <div class="input-group-md">
                <select name="category-action[]" id="category-action" class="form-control" multiple="multiple">
                    @foreach($categoryOptions  as $key=>$value)
                        <option value="{{$key}}" {{isset($searchProfileData['category-action'])?in_array($key,$searchProfileData['category-action'])?'selected':'':''}} >{{$value}}</option>
                    @endforeach
                </select>
                <span class="error">{!!$errors->first('category-action')!!}</span>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="row drop">
        <div class="col-xs-12 col-lg-6 col-sm-6">
            <div class="radio radio-box-holder-wrap lease-wrap">
                <div class="check-box-wrap-hold">
                    <?php 

                       $checked_var = "";
                       if(Input::old('tenant') ) {
                          $checked_var = Input::old('tenant');
                       } else if(!Input::old('tenant') && isset($searchProfileData['tenant'])) {
                            $checked_var = $searchProfileData['tenant'];
                       }
                        
                    ?>
                   
                    {!! Form::radio('tenant', 'tenant', $checked_var == 'tenant', ['id'=>'tenant','class' => 'tenant-landlord']) !!}
                    <label for="tenant" class="check-box-area">I am a Tenant</label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6 col-sm-6">
            <div class="radio radio-box-holder-wrap lease-wrap">

                <div class="check-box-wrap-hold">
                    {!! Form::radio('tenant', 'landlord', $checked_var == 'landlord', ['id'=>'landlord','class' => 'tenant-landlord']) !!}
                    <label for="landlord" class="check-box-area">I am a Landlord</label>

                </div>
            </div>
        </div>


    </div>
</div>

<div class="tenant-or-landlord-options">

</div>


<script>

    $(document).ready(function () {

        triggerLandlordGetMoreOptions();

        $('.tenant-landlord').on('change', function () {
            var $this = $(this);
            var tenant_landlrd = $this.val();

            $.ajax({
                url:site_url+'/search/get-tenant-landlord-search-options',
                dataType:'html',
                data:{'tenant_landlrd':tenant_landlrd,'_token':'{{csrf_token()}}'},
                type:'POST',
                beforeSend:function() {
                    $('.tenant-or-landlord-options').html("<p class='loading'>Loading...</p>");
                }

            }).done(function(html){
                $('.tenant-or-landlord-options').html(html);
            }).error(function(xhr, desc, err){
                $('.tenant-or-landlord-options').html("<p class='loading'>"+err+"</p>");
            });

        })
    });


    $('#category-action').multiselect({
        buttonWidth: '100%',
        nonSelectedText: 'I WOULD LIKE TO  SEARCH OTHER REAL ESTATE RELATED SERVICES'
    });


    function triggerLandlordGetMoreOptions(){

        var tenant_landlrd = $('.tenant-landlord:checked').val();
            $.ajax({
                url:site_url+'/search/get-tenant-landlord-search-options',
                dataType:'html',
                data:{'tenant_landlrd':tenant_landlrd,'_token':'{{csrf_token()}}'},
                type:'POST',
                beforeSend:function() {
                    $('.tenant-or-landlord-options').html("<p class='loading'>Loading...</p>");
                }

            }).done(function(html){
                $('.tenant-or-landlord-options').html(html);
                
            }).error(function(xhr, desc, err){
                $('.tenant-or-landlord-options').html("<p class='loading'>"+err+"</p>");
            });


    }
</script>

