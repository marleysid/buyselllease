<div id="buyForm">
    <input type="hidden" name="agreement-type" value="buy">
    <div class="form_box">
        </br>
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group-lg">
                    <select class="form-control" id="buy1">
                        <option selected disabled>I WOULD LIKE TO BUY...</option>
                        <option>My new home</option>
                        <option>A holiday property</option>
                        <option>A residential investment property</option>
                        <option>A rural property</option>
                        <option>A commercial space</option>
                        <option>A retail space</option>
                        <option>An industrial property</option>
                        <option>A commercial investment property</option>
                        <option>A development site</option>
                    </select>
                </div>
            </div>
        </div>
        </br>
        <div class="input-group-lg">
            <input type="text" class="form-control"  id="usr" placeholder="WHERE" name="suburb">
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" class="ios" name="inc-surrounding"> Include surrounding suburbs
            </label>
        </div>
        </br>
    </div>
</div>
<div id="buyForm2">
    <div id = "buyFormSelectors">
        <div class="form_box">
            </br>

            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group-lg">
                        <select class="form-control" id="b2" name="property-type">
                            <option value="" selected disabled>TYPE OF PROPERTY a</option>
                            <option>House</option>
                            <option>Townhouse/Villa</option>
                            <option>Apartment</option>
                            <option>New Apartment</option>
                            <option>New Townhouse</option>
                            <option>Garage</option>
                            <option>Retirement Living</option>
                            <option>Rural *</option>
                            <option>Choose Acres/Hectares</option>
                            <option>Block of apartments</option>
                        </select>
                    </div>
                </div>
                <div class = buyPropertySelector1>
                    <div class="col-lg-3">
                        <div class="input-group-lg">
                            <select class="form-control"  id="sel1" name="bedrooms">
                                <option value="" selected disabled>BEDROOMS</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group-lg">
                            <select class="form-control" id="sel1" name="bathrooms">
                                <option value="" selected disabled>BATHROOMS</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class = buyPropertySelector2>
                    <div class="col-lg-6">
                        <div class="input-group-lg">
                            <select class="form-control"  id="sel1" name="area">
                                <option value="" selected disabled>AREA</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class = buyPropertySelector3>
                    <div class="col-lg-3">
                        <div class="input-group-lg">
                            <select class="form-control" id="sel1" name="area">
                                <option value="" selected disabled>LANDAREA</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group-lg">
                            <select class="form-control"  id="sel1" name="da-approved">
                                <option value="" selected disabled>DA APPROVED</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            </br>
            </br>
        </div>
    </div>

    <div id = "buyFormSlider">
        <div class="form_box">
            <div class="row">

                <div class="col-lg-12" >

                    <input class="range-slider" type="hidden" value="2,8" name="price-range"/>

                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">

                    <div id = "leftPrice" style="float: left"><h5><span>$10,000</span></h5></div>
                    <div id = "rightPrice" style="float: right"><h5><span>$1,000,000</span></h5></div>

                </div>

            </div>

        </div>

    </div>

</div>

<div id="buyForm3">
    <div class="form_box">

        <h4>I WOULD LIKE TO RECEIVE INFORMATION FROM...</h4>

        <div class="col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[solicitor]"> Solicitors - Conveyancers
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[accountant]"> Accountant/Financial Adviser
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[finance-broker]"> Finance Broker
                </label>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[insurance-broker]"> Insurance Broker
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[surveyor]"> Surveyor
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[pest-reporter]"> Pest Reporter
                </label>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[building-inspector]"> Building Inspector
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[valuer]"> Valuer
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="subscribe[buyers-agent]"> Buyers Agent
                </label>
            </div>
        </div>

        </br>
    </div>
</div>
