@include('flash::message')
<h1>Looking to buy, sell or lease a property?</h1>
<h4>We offer a new innovative way to search for a property and find a service that will assist you in your property transactions, directly between you and the real estate professionals.</h4>
<div class="btn-group btn-group-justified bsl-buttons">
    <button class="btn btn-primary btn-big" onClick="search_next('buystep1', '')">buy</button>
    <button class="btn btn-primary btn-big" onClick="search_next('sellstep1', '')">sell</button>
    <button class="btn btn-primary btn-big" onClick="search_next('leasestep1', '')">lease</button>
</div>
