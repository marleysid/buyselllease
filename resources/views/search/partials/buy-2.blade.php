
<form id="search-form">
    <div class="row">
@if($mode == 'residential')
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::select('bedrooms[]', ['BEDROOMS' => 'BEDROOMS', 1 => '1+', 2 => '2+', 3 => '3+', 4 => '4+', 5 => '5+'], Input::old('bedrooms'), ['id' => 'bedrooms', 'class'=>'form-control styled']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::select('bathrooms[]', ['BATHROOMS' => 'BATHROOMS', 1 => '1+', 2 => '2+', 3 => '3+'], Input::old('bathrooms'), ['id' => 'bathrooms', 'class'=>'form-control styled']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::select('parking[]', ['PARKING' => 'PARKING', 1 => '1+', 2 => '2+', 3 => '3+'], Input::old('parking'), ['id' => 'parking', 'class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::text('land-area', Input::old('land-area'), ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA']) !!}
            </div>
        </div>
@elseif($mode == 'commercial')
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::text('floor-area', Input::old('floor-area'), ['id' => 'floor-area', 'class'=>'form-control', 'placeholder' => 'FLOOR AREA']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::text('land-area', Input::old('land-area'), ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA (MIN)']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::select('tenure[]', ['TENURE', 'VACANT POSSESSION', 'LEASES', 'ALL'], Input::old('tenure'), ['id' => 'tenure', 'class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::select('parking[]', ['PARKING', 1, 2, 3, 4, 5], Input::old('parking'), ['id' => 'parking', 'class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::text('return', Input::old('return'), ['id' => 'return', 'class'=>'form-control', 'placeholder' => '% RETURN (P.A.)']) !!}
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::text('lettable-space', Input::old('lettable-space'), ['id' => 'lettable-space', 'class'=>'form-control', 'placeholder' => 'SIZE OF LETTABLE SPACE']) !!}
            </div>
        </div>
@endif
        <div id="sliderContainer" class="col-xs-12" >
            <input class="range-slider" name="price-range" type="hidden" value="0,200"/>
        </div>
        
        <div class="col-xs-6 text-left">
            <div id="leftPrice" style="float: left"><h5><span></span></h5></div>
        </div>
        <div class="col-xs-6 text-right">
            <div id = "rightPrice" style="float: right"><h5><span></span></h5></div>
        </div>
       
    </div>

</form>

<script type="text/javascript">
    function loadSlider() {
        $('.range-slider').jRange({
            from: 0,
            to: 200,
            step: 1,
            format: '%s',
            width: '100%',
            showLabels: false,
            showScale: false,
            isRange: true,
            denominationLow: {{ \SH::getRangeMultiplier('buy', $mode, 'low') }},
            denominationHigh: {{ \SH::getRangeMultiplier('buy', $mode, 'high') }},
            onstatechange: function(value) {
                console.log(value);
                console.log(this.options);
            }
        });
    }

    $(document).ready(function() {
       loadSlider();
	   


    });
</script>
@include('search.partials.transition-controls', $transition_params)
