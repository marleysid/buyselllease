<div class="row">
    <h4>I WOULD LIKE TO RECEIVE INFORMATION FROM...</h4>
    <div class="col-sm-4">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[solicitor]"> Solicitors - Conveyancers
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[accountant]"> Accountant/Financial Adviser
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[finance-broker]"> Finance Broker
            </label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[insurance-broker]"> Insurance Broker
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[surveyor]"> Surveyor
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[pest-reporter]" id="subscribe"> Pest Reporter
            </label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[building-inspector]"> Building Inspector
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[valuer]"> Valuer
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="subscribe[buyers-agent]"> Buyers Agent
            </label>
        </div>
    </div>
</div>
@include('search.partials.transition-controls', $transition_params)
