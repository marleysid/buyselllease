<form id="search-form">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="input-group-md">
                {!! Form::select('property-type[]', $typeOptions, Input::old('property-type'), ['id' => 'property-type', 'class'=>'form-control']) !!}
            </div>
        </div>

        @if($mode == 1)
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('bedrooms[]', ['BEDROOMS', 1, 2, 3, 4, 5], Input::old('bedrooms'), ['id' => 'bedrooms', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('bathrooms[]', ['BATHROOMS', 1, 2, 3, 4, 5], Input::old('bathrooms'), ['id' => 'bathrooms', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('parking[]', ['PARKING', 1, 2, 3, 4, 5], Input::old('parking'), ['id' => 'parking', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('land-area', Input::old('land-area'), ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA']) !!}
                </div>
            </div>
        @elseif($mode == 2)
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('floor-area', Input::old('floor-area'), ['id' => 'floor-area', 'class'=>'form-control', 'placeholder' => 'FLOOR AREA']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('land-area', Input::old('land-area'), ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA (MIN)']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('tenure[]', ['TENURE', 'VACANT POSSESSION', 'LEASES', 'ALL'], Input::old('tenure'), ['id' => 'tenure', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('parking[]', ['PARKING', 1, 2, 3, 4, 5], Input::old('parking'), ['id' => 'parking', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('return', Input::old('return'), ['id' => 'return', 'class'=>'form-control', 'placeholder' => '% RETURN (P.A.)']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('lettable-space', Input::old('lettable-space'), ['id' => 'lettable-space', 'class'=>'form-control', 'placeholder' => 'SIZE OF LETTABLE SPACE']) !!}
                </div>
            </div>
        @elseif($mode == 3)
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('floor-area', Input::old('floor-area'), ['id' => 'floor-area', 'class'=>'form-control', 'placeholder' => 'FLOOR AREA']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('land-area', Input::old('land-area'), ['id' => 'land-area', 'class'=>'form-control', 'placeholder' => 'LAND AREA (MIN)']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('tenure[]', ['Tenure', 'Vacant Possession', 'Leases', 'All'], Input::old('tenure'), ['id' => 'tenure', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::select('parking[]', ['PARKING', 1, 2, 3, 4, 5], Input::old('parking'), ['id' => 'parking', 'class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::text('return', Input::old('return'), ['id' => 'return', 'class'=>'form-control', 'placeholder' => '% RETURN (P.A.)']) !!}
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="input-group-md">
                    {!! Form::checkbox('da-approved', Input::old('da-approved'), false, ['id' => 'da-approved', 'class'=>'ios']) !!}
                </div>
            </div>
        @endif
    </div>
</form>
@include('search.partials.transition-controls', $transition_params)
