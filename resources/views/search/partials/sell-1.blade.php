<form id="search-form">
    {!! Form::hidden('agreement-type', 'sell') !!}
    {!! Form::hidden('only_professional', 'no',['id'=>'only_professional']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="input-group-md">
                {!! Form::select('action', $actionOptions, Input::old('action'),['id' => 'action', 'class'=>'form-control']) !!}
              <span class="error">{!!$errors->first('action')!!}</span>

            </div>
        </div>
       <div class="col-xs-12">
            <div class="input-group-md">
                {!! Form::select('category-action[]', $categoryOptions, Input::old('category-action[]'),['multiple'=>'multiple', 'id' => 'category-action', 'class'=>'form-control']) !!}
                <span class="error">{!!$errors->first('category-action')!!}</span>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="input-group-md">
                {!! Form::text('suburb',Input::old('suburb'),['id' => 'suburb', 'class'=>'form-control', 'placeholder' => "Where"]) !!}
                <span class="error">{!!$errors->first('suburb')!!}</span>
            </div>
        </div>
        <div>
            <div class="col-md-4">
            <div class="checkbox pull-left">
                <label>
                    {!! Form::checkbox('inc-surrounding', Input::old('inc-surrounding'), false, ['id' => 'inc-surrounding']) !!} <span>Include surrounding suburbs</span>

                </label>

            </div>

      </div>
            <!--5 km, 10km-->
            <div class="col-xs-6 5-10-km" style="display:none;">


                <div class="radio radio-box-holder-wrap">
                    <div class="check-box-wrap-hold">
                        {!! Form::radio('cover_distance', '5', null, ['id'=>'male','class' => '']) !!}
                        <label for="male" class="check-box-area">5 km</label>

                    </div>

                    <div class="check-box-wrap-hold">
                        {!! Form::radio('cover_distance', '10', null, ['id'=>'female','class' => '']) !!}
                        <label for="female" class="check-box-area">10 km</label>

                    </div>
                </div>

            </div>
        </div>
    </div>

</form>
@include('search.partials.transition-controls', $transition_params)

