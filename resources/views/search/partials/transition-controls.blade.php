<div class="bsl-buttons bsl-search-nav">
	<div class="row">
        <div class="col-xs-6 text-left">
            <button class="btn btn-primary btn-big" id="back" onClick="search_next('{{ $prevStep }}', false)">Back</button>
        </div>
        <div class="col-xs-6 text-right">
    @if($currentStep != $totalSteps)
            <button class="btn btn-primary btn-big search_btn" id="next" onClick="search_next('{{ $nextStep }}', false)">Next</button>
    @else
            <button class="btn btn-primary btn-big search_btn" onClick="search_next('{{ $nextStep }}', true)">Search</button>
    @endif
        </div>
    </div>
</div>
<div id="search-progress" class="text-center">
    <ul class="progress-dots">
@for($i = 1; $i <= $totalSteps; $i++)
        <li class="{{ $i == $currentStep ? 'current' : '' }}"></li>
@endfor
    </ul>
</div>
<script type="text/javascript">

    var type_of_lease = '';

    $(document).ready(function() {
        $('#search-select').on('change', function() {
            search_start($(this).val());
        });

        $('#search-widget SELECT OPTION:first-child').each(function(index) {

            $(this).attr('disabled', 'disabled');
        });

        $('#search-widget #category-action OPTION:first-child').each(function(index) {

            $(this).attr('disabled', false);
        });

        $('#search-select').on('change', function() {
            search_start($(this).val());
        });





        // add autocomplete for suburb field
        if (document.getElementById('suburb') != null) {
            
            new google.maps.places.Autocomplete(
                    document.getElementById("suburb"),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        }




        //multiselect


        $('#category-action').multiselect({
            buttonWidth: '100%',
            nonSelectedText:'I WOULD LIKE TO  SEARCH OTHER REAL ESTATE RELATED SERVICES'
        });
           /* $('#btnSelected').click(function () {
                var selected = $("#lstFruits option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
            });*/

        $('#residential-action').on('change',function(){
            $('#category-action').trigger('change');
        });

        $('#commercial-action').on('change',function(){
            $('#category-action').trigger('change');
        });

        $('#inc-surrounding').on('change',function(){
            if($(this).is(":checked")){
                $('.5-10-km').show();
                $("#male").prop('checked',true);
            } else {
                $('.5-10-km').hide();
                $("#male").prop('checked',false);
                $("#female").prop('checked',false);
            }
        });



        $('#category-action').on('change',function(){
            var agreement_type = $('input[name=agreement-type]').val();
            var category = $(this).val();
            if(category) {
                if (agreement_type == 'sell') {
                    var action = $('#action option:selected').val();

                    if (action == 'I WOULD LIKE TO SELL...') {
                        var search = "'search'";
                        $('.search_btn').text('Search');
                        $('.search_btn').attr('onClick','search_next('+search+', true)');
                        $('input[name=only_professional]').val('yes');

                    } else {

                        var step = "'sellstep2'";
                        $('.search_btn').text('Next');
                        $('.search_btn').attr('onClick','search_next('+step+', false)');
                        $('input[name=only_professional]').val('no');
                    }

                } else if(agreement_type == 'buy') {
                    var commercial_action = $('#commercial-action option:selected').val();
                    var residential_action = $('#residential-action option:selected').val();
                    if(commercial_action =='I WOULD LIKE TO BUY...' && residential_action=='I WOULD LIKE TO BUY...') {
                        var search = "'search'";
                        $('.search_btn').text('Search');
                        $('.search_btn').attr('onClick','search_next('+search+', true)');
                        $('input[name=only_professional]').val('yes');
                    } else {
                        var step = "'buystep2'";
                        $('.search_btn').text('Next');
                        $('.search_btn').attr('onClick','search_next('+step+', false)');
                        $('input[name=only_professional]').val('no');
                    }


                } else {
                    var commercial_action = $('#commercial-action option:selected').val();
                    var residential_action = $('#residential-action option:selected').val();


                    console.log(commercial_action);
                    console.log(residential_action);

                    if(type_of_lease =='landlord') {
                        var action = $('#action option:selected').val();

                        if (action == 'I WOULD LIKE TO SELL...') {

                            var search = "'search'";
                            $('.search_btn').text('Search');
                            $('.search_btn').attr('onClick', 'search_next(' + search + ', true)');
                            $('input[name=only_professional]').val('yes');
                        } else {
                            var step = "'leasestep2'";
                            $('.search_btn').text('Next');
                            $('.search_btn').attr('onClick', 'search_next(' + step + ', false)');
                            $('input[name=only_professional]').val('no');
                        }
                    } else {
                        if (commercial_action == 'I WOULD LIKE TO LEASE...' && residential_action == 'I WOULD LIKE TO LEASE...') {
                            var search = "'search'";
                            $('.search_btn').text('Search');
                            $('.search_btn').attr('onClick', 'search_next(' + search + ', true)');
                            $('input[name=only_professional]').val('yes');
                        } else {
                            var step = "'leasestep2'";
                            $('.search_btn').text('Next');
                            $('.search_btn').attr('onClick', 'search_next(' + step + ', false)');
                            $('input[name=only_professional]').val('no');
                        }
                    }
                }
            } else {
                if (agreement_type == 'sell') {
                    var step = "'sellstep2'";
                    $('.search_btn').text('Next');
                    $('.search_btn').attr('onClick','search_next('+step+', false)');
                    $('input[name=only_professional]').val('no');
                } else if(agreement_type == 'buy'){
                    var step = "'buystep2'";
                    $('.search_btn').text('Next');
                    $('.search_btn').attr('onClick','search_next('+step+', false)');
                    $('input[name=only_professional]').val('no');
                } else {
                    var step = "'leasestep2'";
                    $('.search_btn').text('Next');
                    $('.search_btn').attr('onClick','search_next('+step+', false)');
                    $('input[name=only_professional]').val('no');
                }

            }
        })

});



    $('.tenant-landlord').on('change',function () {
        var  lan_ten = $(this).val();
        $('.error').text('');
        if (this.id == "tenant") {
             type_of_lease = 'tenant';
            $("#residential-action, #commercial-action, .bullet-icon").show();
            $('.two-actions').show();
            $("#landlordopt").hide();
        } else {
            type_of_lease = 'landlord';
            $("#residential-action, #commercial-action, .bullet-icon").hide();
            $('.two-actions').hide();
            $("#landlordopt").show();
        }

    });



</script>


<div class="modal fade" id="searchNoticeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                @include('search.partials.development-user-notice')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
