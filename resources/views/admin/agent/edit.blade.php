@extends('admin.layouts.master')

@section('css')
    <style>
        .error{
            color: red;
            font-size: 12px;
        }
    </style>
   
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
    <link href="{{asset('js/colorpicker/dist/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('js/colorpicker/src/css/docs.css')}}" rel="stylesheet" rel="stylesheet"/>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/Agenttools') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to Agent List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Edit Agent Detail (Required fields are marked as * Please fill them.)</h4>
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel-body">
              
             <form class="form-horizontal" method="post" action="{{ URL::to('admin/agent/update/'.$users->user_id) }}" enctype="multipart/form-data"> 
                     


                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="agent-name" class="col-sm-2 control-label">Agent Name <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="agent_name" value="{{Input::old('agent_name') ? Input::old('agent_name') : $users->agent_name}}" class="form-control" id="agent-name"
                                       placeholder="Agent Name">
                                <span class="error">{!!$errors->first('agent_name')!!}</span>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="company-mame" class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="company_name" value="{{(Input::old('company_name')) ? Input::old('company_name') : $users->company_name}}" class="form-control" id="company-name"
                                       placeholder="Company Name">
                                <span class="error">{!!$errors->first('company_name')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="trading-name" class="col-sm-2 control-label">Trading Name <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="trading_name" value="{{Input::old('trading_name') ? Input::old('trading_name') : $users->trading_name}}" class="form-control" id="trading_name"
                                       placeholder="Trading Name">
                                <span class="error">{!!$errors->first('trading_name')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="email" value="{{Input::old('email') ? Input::old('email') : $users->email}}" class="form-control" id="email"
                                       placeholder="Email">
                                <span class="error">{!!$errors->first('email')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="import-id" class="col-sm-2 control-label">Agent Id</label>
                            <div class="col-sm-10">
                                <input type="text" name="import_id" value="{{Input::old('import_id') ? Input::old('import_id') : $users->import_id}}" class="form-control" id="import-id"
                                       placeholder="Agent ID">
                                <span class="error">{!!$errors->first('import_id')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="suburb" class="col-sm-2 control-label">Suburb <span class="required-field">*</span></label>

                            <div class="col-sm-10">
                                <span class="msg">Select from drop down screen to make sure they pick the right suburb in the right state.</span>
                                <input type="text" name="suburb" value="{{Input::old('suburb') ? Input::old('suburb') : $users->suburb}}" class="form-control" id="suburb"
                                       placeholder="Suburb">
                                <span class="error">{!!$errors->first('suburb')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Office Address <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="address" value="{{Input::old('address') ? Input::old('address') : $users->address}}" class="form-control" id="address"
                                       placeholder="Office Address">
                                <span class="error">{!!$errors->first('address')!!}</span>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="office-type" class="col-sm-2 control-label">Office Type <span class="required-field">*</span></label>
                            <div class="col-sm-10">

                                <select class="form-control" name="office_type" id="office-type">
                                    <option value="">Choose Office type</option>
                                    @foreach($price as $pri)
                                        <option value="{{$pri->office}}" {{ Input::old('office_type') ? Input::old('office_type') : $users->offices == $pri->office?'selected':'' }}>
                                            I want to signup for {{$pri->office}} offices
                                        </option>
                                    @endforeach
                                </select>

                                <span class="error">{!!$errors->first('office_type')!!}</span>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label for="abn" class="col-sm-2 control-label">ABN</label>
                            <div class="col-sm-10">
                                <input type="text" name="abn" value="{{Input::old('abn') ? Input::old('abn') : $users->abn}}" class="form-control" id="abn" placeholder="abn">
                                <span class="error">{!!$errors->first('abn')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="acn" class="col-sm-2 control-label">ACN</label>
                            <div class="col-sm-10">
                                <input type="text" name="acn" value="{{Input::old('acn') ? Input::old('acn') : $users->acn}}" class="form-control" id="acn" placeholder="acn">
                                <span class="error">{!!$errors->first('acn')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                            <div class="col-sm-10">
                                <input type="text" name="mobile" value="{{Input::old('mobile') ? Input::old('mobile') : $users->mobile}}" class="form-control" id="mobile"
                                       placeholder="Mobile" data-inputmask="&#039;mask&#039;: &#039;9999-999-999&#039;">
                                <span class="error">{!!$errors->first('mobile')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Phone No <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="phone" value="{{Input::old('phone') ? Input::old('phone') : $users->phone}}" class="form-control" id="phone"
                                       placeholder="Phone" data-inputmask="&#039;mask&#039;: &#039;99-9999-9999&#039;">
                                <span class="error">{!!$errors->first('phone')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="company-website" class="col-sm-2 control-label">Company Website <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <span class="msg"> Office Website URL Address</span>
                                <input type="text" name="company_website" value="{{Input::old('company_website') ? Input::old('company_website') : $users->listing_base_url}}" class="form-control"
                                       id="company-website" placeholder="Company website">
                                <span class="error">{!!$errors->first('company_website')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="company-profile-url" class="col-sm-2 control-label">Meet the team Website <span class="required-field">*</span></label>

                            <div class="col-sm-10">
                                <span class="msg">Website URL Address for "Meet the team”</span>
                                <input type="text" name="meet_the_team_url" value="{{Input::old('meet_the_team_url') ? Input::old('meet_the_team_url') : $users->listing_staff_url}}" class="form-control"
                                       id="company-profile-url" placeholder="Meet the Team Url">
                                <span class="error">{!!$errors->first('meet_the_team_url')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post-code" class="col-sm-2 control-label">Post Code <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="postcode" value="{{Input::old('postcode') ? Input::old('postcode') : $users->postcode}}" class="form-control" id="post-code"
                                       placeholder="Postcode">
                                <span class="error">{!!$errors->first('postcode')!!}</span>
                            </div>
                        </div>


                        


                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <span class="msg">A hundred word describing your business</span>
                                <textarea name="description" class="form-control" id="description"
                                          placeholder="Description">{{Input::old('description') ? Input::old('description') : $users->description}}</textarea>
                                <span class="error">{!!$errors->first('description')!!}</span>
                            </div>
                        </div>


                        <div class="form-group">


                           
                        
                            <label for="logo" class="col-sm-2 control-label">Logo <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <span class="msg">You can enter image url, if your image is on other sites. Image should be 180 X 180 pixels or same proportion to appear properly.</span>
                                <img src="{{ $users->logo_url }}" alt="logo" style="width: 250px; clear:both;" class="img-thumbnail">
                            
                                
                                <input type="ifle" name="logo" value="{{Input::old('logo') ? Input::old('logo') : $users->logo_url}}" class="form-control" id="logo"
                                       placeholder="Logo Url">
                                <span class="error">{!!$errors->first('logo')!!}</span><br>

                                <strong>or</strong>
                                <span class="msg">The Profile image should be uploaded in square size no less than 180 X 180 pixels or same proportion to appear properly</span>
                                <input type="file" name="logo_upload" class="form-control" id="logo-upload">


                            </div>
                        </div>


                        <div class="form-group">

                            
                            <label for="profile-url" class="col-sm-2 control-label">Profile image</label>
                            <div class="col-sm-10">
                                <span class="msg">You can enter image url, if your image are on other sites. Image should be 270 X 180 pixels or same proportion to appear properly.</span>
                                <img src="{{ $users->profile_image }}" alt="logo" style="width: 250px; clear:both;" class="img-thumbnail">                  
              
                                <input type="text" name="profile_image" value="{{Input::old('profile_image') ? Input::old('profile_image') : $users->profile_image}}" class="form-control" id="profile-image"
                                       placeholder="Profile Image Url">
                                <span class="error">{!!$errors->first('profile_image')!!}</span><br>
                                <strong>or</strong>
                                <span class="msg">The profile image should be uploaded no less than 270 X 180 pixels or same proportion to appear properly</span>
                                <input type="file" name="profile_upload" class="form-control" id="profile-upload">


                            </div>
                        </div>





                        <div class="form-group">
                            <label for="primary-color" class="col-sm-2 control-label">Primary Color</label>
                            <div class="col-sm-10 ">
                                <div class="input-group demo2">
                                    <input type="text" id="primary-color"  value="{{Input::old('primary_color')?Input::old('primary_color'):($users->primary_colour)? $users->primary_colour:'#C90000'}}" name="primary_color"
                                           class="form-control"/>
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="Web-hoster" class="col-sm-2 control-label">Who is the company that uploads your property? <span class="required-field">*</span></label>
                            <div class="col-sm-10">

                                <select class="form-control" name="web_hoster" id="Web-hoster">
                                    <option value="">Choose Web Hoster</option>
                                    @foreach($web_hoster as $hoster)
                                        <option value="{{'/'.$hoster->directory}}" {{ Input::old('web_hoster') ? Input::old('web_hoster') : $users->ftp_home_directory == '/'.$hoster->directory?'selected':'' }}> {{$hoster->name}}</option>
                                    @endforeach
                                </select>

                                <span class="error">{!!$errors->first('web_hoster')!!}</span>
                            </div>
                        </div>





                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm">Update Details</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    
@section('js')

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script src="{{asset('js/colorpicker/dist/js/bootstrap-colorpicker.js')}}"></script>
    <script src="{{asset('js/colorpicker/src/js/docs.js')}}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script src="{{ asset('js/phone/dist/jquery.inputmask.bundle.js') }}"></script>

    <script>
        $(function () {
            $(":input").inputmask();
            $('.demo2').colorpicker({});

            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('suburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>

@endsection

