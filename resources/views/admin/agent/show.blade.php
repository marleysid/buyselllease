@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/Agenttools') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to Agent List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Agent Detail
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel-body">
                    <div class="table-responsive">
                   
                        <table class="table table-hover" >
                           <tr>
                            <th>Profile Image</th>
                           <td><img src="{!! $agent->profile_image !!}" width="280" height="170" /></td>
                           </tr>

                            <tr>
                            <th>Logo</th>
                           <td><img src="{!! $agent->logo_url !!}" width="180" height="180" /></td>
                           </tr>
                            <tr>
                                <th> Name</th>
                                <td>{!! $agent->user->name !!}</td>
                            </tr>

                            <tr>
                                <th> Company Name</th>
                                <td>{!! $agent->name !!}</td>
                            </tr>

                            <tr>
                                <th> Trading Name</th>
                                <td>{!! $agent->trading_name !!}</td>
                            </tr> 

                            <tr>
                                <th>Email</th>
                                <td>{!! $agent->user->email !!}</td>
                            </tr>

                             <tr>
                                <th>Import ID</th>
                                <td>{!! $agent->import_id !!}</td>
                            </tr>

                            <tr>
                                <th>Office(s)</th>
                                <td>{!! $agent->offices !!}</td>
                            </tr>


                            <tr>
                                <th>Suburb</th>
                                <td>{!! $agent->suburb !!}</td>
                            </tr>


                            <tr>
                                <th>Address</th>
                                <td>{!! $agent->address !!}</td>
                            </tr>

                            <tr>
                                <th>Website</th>
                                <td><a href="{!! $agent->listing_base_url !!}" target="_blank">{!! $agent->listing_base_url !!}</a></td>
                            </tr>

                             <tr>
                                <th>Meet the team</th>
                                <td><a href="{!! $agent->listing_staff_url !!}" target="_blank">{!! $agent->listing_staff_url !!}</a></td>
                            </tr>


                            <tr>
                                <th>ABN</th>
                                <td>{!! $agent->abn !!}</td>
                            </tr>

                             <tr>
                                <th>ACN</th>
                                <td>{!! $agent->acn !!}</td>
                            </tr> 

                            <tr>
                                <th>Mobile No</th>
                                <td>{!! $agent->mobile !!}</td>
                            </tr>

                            <tr>
                                <th>Phone</th>
                                <td>{!! $agent->phone !!}</td>
                            </tr>

                            <tr>
                                <th>Postcode</th>
                                <td>{!! $agent->postcode !!}</td>
                            </tr>

                            

                             <tr>
                                <th>Primary Color</th>
                                <td><div style="background:{!! $agent->primary_colour  or '#C90000'!!};height: 50px;width: 50px;">{!! $agent->primary_color !!}</div></td>
                            </tr>

                            <tr>
                                <th>Web Hoster</th>
                                <td>{!! $agent->ftp_home_directory !!}</td>
                            </tr>

                            <tr>
                                <th>Description</th>
                                <td>{!! nl2br($agent->description) !!}</td>
                            </tr>



                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection

