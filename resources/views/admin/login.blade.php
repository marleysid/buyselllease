
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Buyselllease|Admin</title>
    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
    <!-- BOOTSTRAP STYLES-->
    <link href="{{asset('adminassets')}}/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- CUSTOM STYLES-->
    <link href="{{asset('adminassets')}}/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body>
<div class="container">
    <div class="row text-center ">
        <div class="col-md-12">
            <br><br>
            <h2> Buyselllease:Admin Login</h2>
            <br>
        </div>
    </div>
    <div class="row ">

        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>   Enter Details To Login </strong>
                </div>
                @include('flash::message')
                <div class="panel-body">
                    {!!Form::open(['url'=>url('admin/login'),'id'=>'login-form','class'=>'form-signin','method'=>'POST'])!!}
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                            <input type="text" name="name" class="form-control"  autofocus placeholder="Your Username " />

                        </div>
                    @if ($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                            <input type="password" name="password" class="form-control"  placeholder="Your Password" />

                        </div>
                    @if ($errors->has('password')) <div class="error">{{ $errors->first('password') }}</div> @endif
                        <div class="form-group">

                                            {{--<span class="pull-right">
                                                   <a href="#" >Forget password ? </a>
                                            </span>--}}
                        </div>

                        <button class="btn btn-primary ">Login Now</button>
                       {{-- Not register ? <a href="registeration.html" >click here </a>--}}
                    {!! Form::close() !!}
                </div>

            </div>
        </div>


    </div>
</div>
<script type="text/javascript">
    /*
     *function to automatically hide the alert message after 6 second
     */
    $(".alert").fadeTo(6000, 500).slideUp(500, function(){
        $(".alert").alert('close');
    });
</script>

<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{asset('adminassets')}}/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{asset('adminassets')}}/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{asset('adminassets')}}/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="{{asset('adminassets')}}/js/custom.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-38955291-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
