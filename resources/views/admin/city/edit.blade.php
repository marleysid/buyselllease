@extends('admin.layouts.master')




@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }

    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/citylist') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to State List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Create New Region</h3>
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/updatecity/'.$city->id)}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Region</label>
                            <div class="col-sm-10">
                              <!--   <input type="text" name="region" class="form-control" placeholder="Enter your region" value="{{$city->getRegion->name}}" disabled /> -->
                              <select class="form-control" name="region">
                                  @foreach($region as $reg)
                                    <option value="{{$reg->id}}" {{($reg->id == $city->region_id) ? 'selected' : ''}}>{{$reg->name}}</option>
                                  @endforeach
                              </select>
                                <span class="error">{!!$errors->first('region')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Suburb Name</label>
                                <div class="col-sm-10">
                                    <span class="msg">Select from drop down screen to make sure they pick the right suburb in the right state.</span>
                                    <input type="text" id="name" name="name" class="form-control suburb" id="inputPassword3" placeholder=" Enter Suburb Name" value="{{$city->name}}" required>
                                    <span class="error">{!!$errors->first('name')!!}</span>
                                </div>
                        </div>
                      


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

    <script type="text/javascript">
        new google.maps.places.Autocomplete(
                    document.getElementById('name'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });

    </script>

@endsection
