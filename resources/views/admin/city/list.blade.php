@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection

@section('content')
    <style>
        td{
            white-space: nowrap;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h3>State List</h3>
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

<a style="margin-top: 10px; margin-left: 5px;" href="{{ URL::to('admin/createnewcity') }}" class="btn btn-default">Create New <i class="fa fa-plus"></i> </a>
               <!--adding new city form ends here-->

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" id="users-table">
                            <thead>
                            <tr>
                                <th>State</th>
                                <th>Suburb</th>
                                <th>Operations</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($region as $key => $val)
                                    <tr>
                                        <td>{{$val->name}}</td>
                                        <td>
                                            @if (count($val->getAllSuburb) != "0")
                                            <ul>
                                            @foreach($val->getAllSuburb as $s)
                                                
                                                <li>{{$s->name}}&nbsp;<a href="{{url('admin/city/edit/'.$s->id)}}">Edit</a>&nbsp; <a href="{{url('admin/city/delete/'.$s->id)}}" onclick="return confirm('Are you sure want to delete this suburb?');">Delete</a></li>
                                                
                                            @endforeach
                                            
                                                
                                            @endif
                                            <li><a href="{{ URL::to('admin/createnewcity/'.$val->id) }}">Add New</a></li>
                                            </ul>
                                        </td>
                                        <td>
                                            <a href="{{url('admin/region/edit/'.$val->id)}}" class="btn btn-default btn-sm">Edit</a>
                                            <a href="{{url('admin/region/delete/'.$val->id)}}" onclick="return confirm('Are you sure want to delete this Region? If you delete this suburb inside this Region will also delete.');" class="btn btn-default btn-sm">Delete</a>
                                        </td>
                                        <!-- <td>{{$val->lon}}</td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script>
        function confirmation(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function active(){
            if (! confirm('Are you sure want to continue?')) return false;
        }
    </script>
@endsection

