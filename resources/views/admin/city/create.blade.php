@extends('admin.layouts.master')




@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }

    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/citylist') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to State List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Create New Region</h3>
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/storecity')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">State</label>
                            <div class="col-sm-10">
                              @if(!empty($selected))
                               <select name="region" id="region" class="form-control">
                                    @foreach($region as $reg)
                                        <option value="{{$reg->id}}" {{ (($selected == $reg->id) ? "selected" : "") }}>{{$reg->name}}</option>
                                    @endforeach
                                </select>
                                @else
                                 <input type="text" name="newregion" id="customeregion" class="form-control" placeholder="Enter State Name" />
                                @endif
                               <!--  <input type="text" name="region" class="form-control" placeholder="Enter your region" required />
                                <span class="error">{!!$errors->first('region')!!}</span> -->
                               <!--  <a style="float: right; cursor: pointer;" id="newregion">add New Region</a> -->
                               <!-- <span style="float: right;">Add New region<input type="checkbox" id="mynewregion" style="width: 25px;
    height: 20px;"></span> -->
                            </div>
                        </div>


                        <!-- <div class="form-group regionname" style="display: none;">
                         <div class="col-sm-offset-2 col-sm-10">
                             <input type="text" name="newregion" id="customeregion" class="form-control" placeholder="Enter your region" />
                         </div>
                        </div> -->

                        <div class="form-group addthis all">
                            <label for="inputPassword3" class="col-sm-2 control-label">Suburb Name</label>
                                <div class="col-sm-10">
                                    <span class="msg">Select from drop down screen to make sure they pick the right suburb in the right state.</span>
                                    <input type="text" id="name" name="name[]" class="form-control suburb" id="inputPassword3" placeholder=" Enter Suburb Name" required>
                                    <span class="error">{!!$errors->first('name')!!}</span>
                                </div>
                        </div>
                      
                        <a style="float:right; cursor: pointer;" class="add-btn">Add another</a>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

    <script type="text/javascript">
       /* new google.maps.places.Autocomplete(
                    document.getElementById('name'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });*/
          var input = document.getElementsByClassName('suburb');
          //console.log(input);
           var options = {
                 'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
             };

             for (i = 0; i < input.length; i++) {
                 autocomplete = new google.maps.places.Autocomplete(input[i], options);
             }


    </script>


    <script type="text/javascript">
        $('.add-btn').off('click').on('click', function(){
            //clone div here
            $('.addthis.all').clone()
                        .show()
                        .removeClass('all')
                        .insertAfter(".addthis:last")
                        .find("input:text").val("").end();
            for (i = 0; i < input.length; i++) {
                autocomplete = new google.maps.places.Autocomplete(input[i], options);
            }
        });
    </script>


    <script type="text/javascript">
      $("#mynewregion").change(function() {
            if(this.checked) {
              $('.regionname').show();
              $('#customeregion').prop("required", 'true');
              $('#region').attr('disabled', 'true');
              $('#region').prop("required", 'false');
            } else{
                 $('#region').removeAttr('disabled');
                   $('#customeregion').prop("required", 'false');
                     $('#region').prop("required", 'true');
                   $('.regionname').hide();
            }
        });
    </script>
@endsection
