@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />


@endsection



@section('content')
    <style>
        .error{
            color: red;
            font-size: 12px;
        }

    </style>

    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
                <a href="{{URL::to('admin/advertisement')}}" class="btn btn-primary btn-sm">{{ '<<' }} Back to Advertisement List</a>
            <div class="panel panel-default" style="margin-top: 10px;">
                <div class="panel-heading">
                    Edit Advertisement
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/advertisement/update').'/'.$editad->id }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Image Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="title"  class="form-control" value="{{ $editad->title }}" id="inputEmail3" placeholder="Enter Image title">
                                <span class="error">{!!$errors->first('title')!!}</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea name="description"  rows="8" class="form-control" id="inputEmail3" placeholder="Enter Image Description">{{ $editad->description }}</textarea>
                                <span class="error">{!!$errors->first('description')!!}</span>
                            </div>
                        </div>

                        <div class="form-group" style="padding-left: 50%">
                            @if(file_exists(public_path('uploads/admin/advertisement').'/'.$editad->image))
                                <img src="{{ asset('uploads/admin/advertisement').'/'.$editad->image }}" style="width: 250px;" class="img-thumbnail">
                            @else
                                <img src="{{ asset('uploads').'/'.'noimage.png' }}" style="width: 250px;" class="img-thumbnail">
                            @endif
                         </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Change Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="userfile"  class="form-control" id="inputEmail3">
                                <span class="error">{!!$errors->first('userfile')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Image Link</label>
                            <div class="col-sm-10">
                                <input type="text" name="link"  class="form-control" value="{{ $editad->link }}" id="inputEmail3">
                                <span class="error">{!!$errors->first('link')!!}</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Choose Status</label>
                            <div class="col-sm-10">
                                <select name="status" id="status" class="form-control">
                                    <option value="{{ $editad->status }} selected">
                                        @if ($editad->status == 1)
                                                Active
                                            @else
                                                 Suspended
                                        @endif
                                    </option>

                                    @if ($editad->status == 1)
                                        <option value="2">Suspended</option>
                                    @else
                                        <option value="1">Active</option>
                                    @endif
                                </select>
                                <span class="error">{!!$errors->first('status')!!}</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure want to edit advertisement?')">Update Ad</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')


            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection
