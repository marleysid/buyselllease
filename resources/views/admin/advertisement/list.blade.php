@extends('admin.layouts.master')
@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <style>
        th{
            white-space: nowrap;
        }
    </style>
@endsection
@section('pagetitle')
    Advertisment Management
@endsection
@section('pagedesctiption')
    <a href="{{route('admin.advertisement.create')}}" class="btn btn-primary btn-sm">Add Advertisement</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Advertisment list
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        @if (Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        <table class="table table-condensed table-hover" id="advertisement-table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Link</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Change Status</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                        </table>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
@endsection
@section('js')

    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminassets/js/advertisement.js')}}"></script>


    <script>
        function suspend(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function activate(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function deleteThis(){
            if (! confirm('Are you sure want to continue?')) return false;
        }
    </script>
@endsection


