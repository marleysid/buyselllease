@extends('admin.layouts.master')
@section('css')
@endsection
@section('pagetitle')
    Advertisment Add
@endsection
@section('pagedesctiption')

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                @if($action['form']=='add')
                    @include('admin.advertisement.add')
                @elseif($action['form'] =='edit')
                    @include('admin.advertisement.edit')
                @endif



            </div>
        </div>
    </div>



@endsection

@section('js')

@endsection