@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }

    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/emailtemplate') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to Template List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                  Edit Email Template - {{ $temp->type }}
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/updatetemplate'. '/'. $temp->id)}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">




                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Email Content</label>
                            <div class="col-sm-10">
                                <textarea name="text" class="form-control" rows="8">{{ $temp->text }}</textarea>
                                <span class="error">{!!$errors->first('text')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure want to edit this template ?')">Update</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
@endsection
@section('js')
    <script>
        function hello(){
            var type = document.getElementById("type");
            var selectedText = type.options[type.selectedIndex].text;

            if (selectedText == 'Discount'){
                $("#percent").show();
                $("#percentage").prop('required',true);
            } else {
                $("#percent").hide();
            }
        }

    </script>

    <script>
    CKEDITOR.replace( 'text', {
            uiColor: '#9AB8F3',
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,
            autoParagraph: false
        });
    </script>

    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection
