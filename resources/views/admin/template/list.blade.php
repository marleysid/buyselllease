@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection

@section('content')
    <style>
        td{
            white-space: nowrap;
        }
    </style>


    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                   Template List
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" id="users-table">
                            <thead>
                            <tr>
                                <th>Email Type</th>
                                <th>Contents</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($template as $temp)

                                <tr>
                                    <td>{{ $temp->type }}</td>
                                    <td>{{ str_limit(strip_tags(htmlspecialchars_decode($temp->text),20)) }}</td>
                                    <td><a href="{{ URL::to('admin/templateedit'.'/'.$temp->id) }}" class="btn btn-default btn-sm">Edit Template</a></td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script>
        function confirmation(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function active(){
            if (! confirm('Are you sure want to continue?')) return false;
        }
    </script>
@endsection

