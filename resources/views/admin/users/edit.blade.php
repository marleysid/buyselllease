@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }
    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/users') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to User List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Users Detail
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

           
                
                <div class="panel-body">
                    <form class="form-horizontal" id="updateprofile" method="post" action="{{ URL::to('admin/users/update/'.$users->userid) }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">First Name <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="firstname" value="{{Input::old('firstname') ? Input::old('firstname') : $users->firstname}}" class="form-control" id="inputEmail3" placeholder="first name">
                                <span class="error">{!!$errors->first('firstname')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Last Name <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="lastname" value="{{ Input::old('lastname') ? Input::old('lastname') : $users->lastname }}" class="form-control" id="inputPassword3" placeholder="last name">
                                <span class="error">{!!$errors->first('lastname')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Email <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="email" value="{{ Input::old('email') ? Input::old('email') : $users->email }}" class="form-control" id="inputPassword3" placeholder="email">
                                <span class="error">{!!$errors->first('email')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Street Address <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="street_address" value="{{ Input::old('street_address') ? Input::old('street_address') : $users->street_address }}" class="form-control" id="inputPassword3" placeholder="street address">
                                <span class="error">{!!$errors->first('street_address')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                         <label for="inputPassword3" class="col-sm-2 control-label"></label>
                            <div class="checkbox col-sm-10">
                                    <label>
                                        <input type="radio" id="mysuburb" name="searchtype" value="suburb" {{($users->type == 'suburb') ? 'checked' : ''}}>
                                         <span class="lbl padding-8">Suburb (10km Radius) </span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>
                                    

                                    <label>
                                        <input type="radio" id="region" name="searchtype" value="region" {{($users->type == 'region') ? 'checked' : ''}}> 
                                         <span class="lbl padding-8">State</span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>
                                
                                  <label>
                                        <input type="radio" id="nationwide" name="searchtype" value="nationwide" {{($users->type == 'nationwide') ? 'checked' : ''}}>
                                         <span class="lbl padding-8">Nationwide</span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>


                                </div> 
                        </div>

                       
                            <div class="form-group dsuburb" style=display:{{ ($users->type == 'suburb') ? 'block' : 'none' }}>
                                <label for="inputPassword3" class="col-sm-2 control-label">Suburb <span class="required-field">*</span></label>
                                <div class="col-sm-10">
                                    <span class="msg">Select from drop down screen to make sure they pick the right suburb in the right state.</span>
                                    <input type="text" name="suburb" value="{{ Input::old('suburb') ? Input::old('suburb') : $users->suburb }}" class="form-control" id="suburb" placeholder="suburb">
                                    <span class="error">{!!$errors->first('suburb')!!}</span>
                                </div>
                            </div>
                      

                            <div class="form-group dregion" style=display:{{ ($users->type == 'region') ? 'block' : 'none' }}>
                            <label for="inputPassword3" class="col-sm-2 control-label"> </label>
                            <div class="col-sm-10">
                                @if(!empty($region))
                                   @foreach($region as $region)
                                                      <label>
                                                          <input type="checkbox" id="nationwide" name="region[]" value="{{$region->id}}" {{in_array($region->id, $col) ? 'checked' : ''}}>
                                                           <span class="lbl padding-8">{{$region->name}} </span>
                                                      </label>
                                                   @endforeach
                                @endif
                                <span class="error">{!!$errors->first('region')!!}</span>
                            </div>
                        </div>
                      

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Business Name </label>
                            <div class="col-sm-10">
                                <input type="text" name="business_name" value="{{ Input::old('business_name') ? Input::old('business_name') : $users->business_name }}" class="form-control" id="inputPassword3" placeholder="Business Name">
                                <span class="error">{!!$errors->first('business_name')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Your position in the business <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="business_position" value="{{ Input::old('business_position') ? Input::old('business_position') : $users->business_position }}" class="form-control" id="inputPassword3" placeholder="Business Position">
                                <span class="error">{!!$errors->first('business_position')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="company_name" value="{{ Input::old('company_name') ? Input::old('company_name') : $users->company_name }}" class="form-control" id="inputPassword3" placeholder="company name">
                                <span class="error">{!!$errors->first('company_name')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Edit Logo <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <span class="msg">You can enter image url, if your image is on other sites. Image should be 180 X 180 pixels or same proportion.</span>
                                
                                <img src="{{asset('uploads/professionals').'/'.$users->logo}}" style="width: 100px;" class="img-thumbnail"/>

                                <input type="file" name="logo" class="form-control" id="inputPassword3">
                                <span class="error">{!!$errors->first('logo')!!}</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Your Industry </label>
                            <div class="col-sm-10">
                               <select name="category" class="form-control">
                                   <option value="{{ $users->id }}">{{ $users->title }}</option>
                                   @if(!empty($pro))
                                   @foreach($pro as $prof)
                                       <option value="{{ $prof->id }}">{{ $prof->title }}</option>
                                   @endforeach
                                   @endif
                               </select>
                                <span class="error">{!!$errors->first('category')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Mobile</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ Input::old('mobile') ? Input::old('mobile') : $users->mobile_no }}" name="mobile" class="form-control" id="inputPassword3" placeholder="Mobile">
                                <span class="error">{!!$errors->first('mobile')!!}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Phone <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ Input::old('contact') ? Input::old('contact') : $users->contact }}" name="contact" class="form-control" id="inputPassword3" placeholder="Phone number">
                                <span class="error">{!!$errors->first('contact')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">URL <span class="required-field">*</span></label>

                            <div class="col-sm-10">
                                <span class="msg">Your website address for consumers to be directed to </span>
                                <input type="text" value="{{ Input::old('url') ? Input::old('url') : $users->url }}" name="url" class="form-control" id="inputPassword3" placeholder="URL">
                                <span class="error">{!!$errors->first('url')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Description <span class="required-field">*</span></label>
                            <div class="col-sm-10">
                                <span class="msg">A hundred word describing your business</span>
                               <textarea name="description" class="form-control" rows="6">{{ Input::old('description') ? Input::old('description') : $users->description}}</textarea>
                                <span class="error">{!!$errors->first('description')!!}</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm">Update Details</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script src="{{ asset('js/phone/dist/jquery.inputmask.bundle.js') }}"></script>

    <script>
        $(function () {
            //$(":input").inputmask();
            //$('.demo2').colorpicker({});

            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('suburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>



    <script type="text/javascript">
         $('input[name=searchtype]').change(function () {
               if (document.getElementById('nationwide').checked){
                       $('.dsuburb').hide();
                       $('.dregion').hide();
                       $('#suburb').prop('required', false);
                       $('#selectedregion').prop('required', false);
                   //disable the validation for other field
               } else if(document.getElementById('mysuburb').checked){
                       $('.dsuburb').show();
                       $('.dregion').hide();
                       $('#suburb').prop('required', true);
                   $('#selectedregion').prop('required', false);
               } else if(document.getElementById('region').checked){
                      $('.dsuburb').hide();
                      $('.dregion').show();
                      $('#selectedregion').prop('required', true);
                      $('#suburb').prop('required', false);
                  //do other parts 
               }
            });
    </script>
@endsection
