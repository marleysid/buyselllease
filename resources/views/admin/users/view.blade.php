@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/users') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to User List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Users Detail
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-hover" >

                          <tr>
                              <th>Full Name</th>
                              <td>{{ $users->firstname }} {{$users->lastname}}</td>
                          </tr>

                          <tr>
                              <th>Email</th>
                              <td>{{ $users->email }} </td>
                          </tr>

                          <tr>
                              <th>Street Address</th>
                              <td>{{ $users->street_address }} </td>
                          </tr>

                          <tr>
                              <th>Professional Location</th>
                              <td>
                                @if($users->type == 'nationwide')
                                  Nationwide
                                @elseif($users->type == 'suburb')
                                  {{$users->suburb}}
                                @else
                                 {{$users->regionname}}
                                @endif
                              </td>
                          </tr>

                          <tr>
                              <th>Business Name</th>
                              <td>{{ $users->business_name }} </td>
                          </tr>

                          <tr>
                              <th>Company Logo</th>
                              <td><img src="{{asset('uploads/professionals').'/'.$users->logo}}" style="width: 100px;" class="img-thumbnail"/></td>
                          </tr>

                          <tr>
                              <th>Company Name</th>
                              <td>{{ $users->company_name }} </td>
                          </tr>

                          <tr>
                              <th>Business Postion</th>
                              <td>{{ $users->business_position }} </td>
                          </tr>
                          <tr>
                              <th>Mobile</th>
                              <td>{{ $users->mobile_no }} </td>
                          </tr>
                          <tr>
                              <th>Phone Number</th>
                              <td>{{ $users->contact }} </td>
                          </tr>

                          <tr>
                              <th>Profession</th>
                              <td>{{ $users->title }} </td>
                          </tr>

                          <tr>
                              <th>Joined Date</th>
                              <td>{{ $users->created_at }} </td>
                          </tr>
                          <tr>
                              <th>Description</th>
                              <td>{{ $users->description }} </td>
                          </tr>

                      </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection