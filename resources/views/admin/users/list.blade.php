@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection
@section('pagetitle')
    User Management

@endsection

@section('content')
    <style>
        td{
            white-space: nowrap;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Users list
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" id="users-table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th style="white-space: nowrap">User Type</th>
                                <th>Status</th>
                                <th>Joined Date</th>
                                <th style="white-space: nowrap">Change Status</th>
                                <th>Operations</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
@endsection
@section('js')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminassets/js/user.js?v='.time())}}"></script>

    <script>
        function confirmation(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function active(){
            if (! confirm('Are you sure want to continue?')) return false;
        }
    </script>
@endsection

