@section('head')
    @include('admin.partials.head')
@show()


@section('header')
    @include('admin.partials.header')
@show()


@section('siderbar')
    @include('admin.partials.sidebar')
@show()


@section('breadcum')
    @include('admin.partials.breadcum')
@show()


@yield('content')


@section('footer')
    @include('admin.partials.footer')
@show()











