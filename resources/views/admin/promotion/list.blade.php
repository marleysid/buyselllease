@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection

@section('content')
    <style>
        td{
            white-space: nowrap;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                   Promotional Code List
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                <a style="margin-top: 10px; margin-left: 5px;" href="{{ URL::to('admin/createnewpromocode') }}" class="btn btn-default">Create New <i class="fa fa-plus"></i> </a>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" id="users-table">
                            <thead>
                            <tr>
                                <th>Promo Code</th>
                                <th>Code Type</th>
                                <th>Discount Percentage</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($code as $promo)
                                <tr>
                                    <td>{{$promo->code}}</td>
                                    <td>{{ucfirst($promo->type)}}</td>
                                    <td>
                                        @if ($promo->percentage != '')
                                            {{$promo->percentage }} %
                                        @else
                                             N/A
                                        @endif
                                    </td>
                                    <td>{{substr($promo->created_at, 0, 10)}}</td>
                                    <td>
                                        <a href="{{ URL::to('admin/deletepromo/'. $promo->id) }}" class="btn btn-default btn-sm" onclick="return confirm('Are you sure want to continue?')">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script>
        function confirmation(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function active(){
            if (! confirm('Are you sure want to continue?')) return false;
        }
    </script>
@endsection

