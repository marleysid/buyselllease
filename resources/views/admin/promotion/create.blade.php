@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }

    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/promotionalcodelist') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to Promo List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create New Promotional Code
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/storepromocode')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Promo Code</label>
                            <div class="col-sm-10">
                                <input type="text" name="code"  class="form-control" id="inputEmail3" placeholder="Code" value="{{ str_random(8) }}" readonly>
                                <span class="error">{!!$errors->first('code')!!}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Promo Type</label>
                            <div class="col-sm-10">
                                <select name="type" id="type" class="form-control" onchange="hello()">
                                    <option value="">Choose One</option>
                                    <option value="discount">Discount</option>
                                    <option value="free">Free</option>
                                </select>
                                <span class="error">{!!$errors->first('type')!!}</span>
                            </div>
                        </div>

                        <div id="percent" style="display: none;" class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Discount Percent(%)</label>
                            <div class="col-sm-10">
                                <input type="text" id="percentage" name="percentage" class="form-control" id="inputPassword3" placeholder="Specify Discount Percent">
                                <span class="error">{!!$errors->first('percentage')!!}</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure want to create new promo code ?')">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
    <script>
        function hello(){
            var type = document.getElementById("type");
            var selectedText = type.options[type.selectedIndex].text;

            if (selectedText == 'Discount'){
                $("#percent").show();
                $("#percentage").prop('required',true);
            } else {
                $("#percent").hide();
            }
        }
    </script>

            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection
