@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
@endsection

@section('content')
    <style>
        td{
            white-space: nowrap;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                  Agent Pricing List
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif

                <a style="margin-top: 10px; margin-left: 5px;" href="{{ URL::to('admin/createpricing') }}" class="btn btn-default">Create Pricing &nbsp;<i class="fa fa-plus"></i> </a>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" id="users-table">
                            <thead>
                            <tr>
                                <th>Number of Offices</th>
                                <th>prices</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($price as $prices)
                                <tr>
                                    <td>{{$prices->office}}</td>
                                    <td>{{$prices->price}}</td>
                                     <td><a href="{{URL::to('admin/editprice/'. $prices->id )}}" class="btn btn-default btn-sm">Edit Price</a></td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script>
        function confirmation(){
            if (! confirm('Are you sure want to continue?')) return false;
        }

        function active(){
            if (! confirm('Are you sure want to continue?')) return false;
        }
    </script>
@endsection

