@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }

    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/agentpricing') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to Pricing List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create New Pricing
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/storepricing')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">No. of offices</label>
                            <div class="col-sm-10">
                                <select name="office" id="type" class="form-control" onchange="hello()">
                                    <option value= ''>Choose One</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="10">15</option>
                                    <option value="10">20</option>
                                    <option value="">Add your own</option>
                                </select>
                                <span class="error">{!!$errors->first('office')!!}</span>
                            </div>
                        </div>

                        <div id="custom" style="display: none;" class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <input type="text" id="office" name="office" class="form-control" id="inputPassword3" placeholder="No. of Office">
                                <span class="error">{!!$errors->first('office')!!}</span>
                            </div>
                        </div>


                           <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Price($)</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="price" placeholder="Add Price">
                                <span class="error">{!!$errors->first('price')!!}</span>
                            </div>
                        </div>

                    
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure want to create new pricing ?')">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
    <script>
        function hello(){
            var type = document.getElementById("type");
            var selectedText = type.options[type.selectedIndex].text;

            if (selectedText == 'Add your own'){
                $("#custom").show();
                $("#office").prop('required',true);
                $("#type").prop("disabled", true);
            } else {
                $("#custom").hide();
            }
        }
    </script>
            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection
