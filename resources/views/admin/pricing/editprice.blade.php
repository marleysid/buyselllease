@extends('admin.layouts.master')

@section('css')

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
        .error{
            color:red;
            padding-top: 5px;
            font-size: 12px;
        }

    </style>
@endsection
@section('pagetitle')
    <a href="{{ URL::to('admin/agentpricing') }}" class="btn btn-primary btn-sm"> {{ '<<' }} Back to Pricing List</a>
@endsection


@section('content')
    <style>

    </style>
    <div class="row">
        <div class="col-md-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create New Pricing
                </div>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ URL::to('admin/updatepricing/'.$editprice->id)}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">No. of offices</label>
                            <div class="col-sm-10">
                                <select name="office" id="type" class="form-control" onchange="hello()" readonly>
                                   <option value ="{{$editprice->office}}" selected>{{$editprice->office}}</option>
                                </select>
                                <span class="error">{!!$errors->first('office')!!}</span>
                            </div>
                        </div>


                           <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Price($)</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="price" placeholder="Add Price" value="{{$editprice->price}}">
                                <span class="error">{!!$errors->first('price')!!}</span>
                            </div>
                        </div>

                    
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure want to update pricing ?')">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End  Kitchen Sink -->
        </div>
    </div>
    @endsection
    @section('js')
            <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection
