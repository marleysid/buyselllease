</div>
<!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{asset('adminassets')}}/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{asset('adminassets')}}/js/bootstrap.min.js"></script>

<!-- METISMENU SCRIPTS -->
<!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> -->
<script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>
<script src="{{asset('adminassets')}}/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->


@yield('js')

<script type="text/javascript">
    /*
     *function to automatically hide the alert message after 6 second
     */
    $(".alert").fadeTo(6000, 500).slideUp(500, function(){
        $(".alert").alert('close');
    });
</script>
</body>
</html>
