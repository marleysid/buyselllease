<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Buyselllease | @yield('title')</title>
    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">
    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- CUSTOM STYLES-->
    <link href="{{asset('adminassets')}}/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

   @yield('css')
</head>
<body>