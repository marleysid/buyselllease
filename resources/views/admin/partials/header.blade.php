<script>
    var  site_url = '{{url()}}'
</script>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('admin.dashboard')}}" style='position:relative;'><strong style="float:left;">Buyselllease</strong></a>
        </div>

        <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> Logged in at {{Carbon\Carbon::parse(Auth::user()->updated_at)->format('Y M d | H:i:s')}} &nbsp; <a href="{{route('admin.logout')}}" class="btn btn-danger square-btn-adjust">Logout</a> </div>
    </nav>
