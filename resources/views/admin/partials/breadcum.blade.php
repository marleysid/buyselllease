<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>@yield('pagetitle')</h2>
                <h5>@yield('pagedesctiption')</h5>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr />