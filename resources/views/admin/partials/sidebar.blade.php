<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
           <li class="text-center">
                <img src="{{asset('img/logo-lg.png')}}" class="user-image img-responsive"/>
            </li>


            <li>
                <a  class="{{GH::activeSidebar(route('admin.dashboard'))}}" href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
            </li>

            <li>
                <a  class="{{GH::activeSidebar(route('admin.users'))}}" href="{{route('admin.users')}}"><i class="fa fa-users fa-3x"></i>Users Management</a>
            </li>

            <li>
                <a class="{{GH::activeSidebar(route('admin.payment'))}}"  href="{{route('admin.payment')}}"><i class="fa fa-dollar fa-3x"></i> Payment Settings</a>
            </li>

            <li>
                <a  class="{{GH::activeSidebar(URL::to('admin/promotionalcodelist'))}}" href="{{ URL::to('admin/promotionalcodelist') }}"><i class="fa fa-filter fa-3x"></i>Manage Promotional Code</a>
            </li>

             <li>
                <a  class="{{GH::activeSidebar(URL::to('admin/agentpricing'))}}"  href="{{ URL::to('admin/agentpricing') }}"><i class="fa fa-dollar fa-3x"></i>Agent Pricing Management</a>
            </li>

            <li >
                <a  class="{{GH::activeSidebar(route('admin.advertisement'))}}" href="{{route('admin.advertisement')}}"><i class="fa fa-buysellads fa-3x"></i>Advertisement Management</a>
            </li>

             <li>
                <a class="{{GH::activeSidebar(URL::to('admin/citylist'))}}" href="{{ URL::to('admin/citylist') }}"><i class="fa fa-building-o fa-3x"></i>State </a>
            </li>
           
            <li>
                <a   class="{{GH::activeSidebar(route('admin.agenttools'))}}"  href="{{route('admin.agenttools')}}"><i class="fa fa-gears fa-3x"></i>Agent Details </a>
            </li>

            <li>
                <a class="{{GH::activeSidebar(URL::to('admin/emailtemplate'))}}" href="{{ URL::to('admin/emailtemplate') }}"><i class="fa fa-envelope-o fa-3x"></i>Email Template </a>
            </li>

            


      <!--<li>
                <a href="#"><i class="fa fa-sitemap fa-3x"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Link</a>
                    </li>
                    <li>
                        <a href="#">Second Level Link</a>
                    </li>
                    <li>
                        <a href="#">Second Level Link<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>

                        </ul>

                    </li>
                </ul>
            </li>
           <li  >
                 <a class="active-menu"  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
             </li>-->
        </ul>

    </div>

</nav>
<!-- /. NAV SIDE  -->