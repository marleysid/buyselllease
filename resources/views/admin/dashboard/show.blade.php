@extends('admin.layouts.master')

@section('css')
    <style>
        .error{
            color: red;
            font-size: 12px;
        }
    </style>
   
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="{{asset('adminassets')}}/js/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
    <link href="{{asset('js/colorpicker/dist/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('js/colorpicker/src/css/docs.css')}}" rel="stylesheet" rel="stylesheet"/>
@endsection
@section('pagetitle')
    Dashboard
@endsection
@section('content')
<div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-user"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">{{$users}} Users</p>
                    <p class="text-muted"></p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-briefcase"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">{{$agents}} Agents</p>
                    <p class="text-muted"></p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-graduation-cap"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">{{$professionals}} Professionals</p>
                    <p class="text-muted"></p>
                </div>
             </div>
		     </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-home"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">{{$properties}} Properties</p>
                    <p class="text-muted"></p>
                </div>
             </div>
		     </div>
			</div>
                 <!-- /. ROW  -->
                <hr />  

 @endsection