@extends('layouts.default.master')
@section('header-script')
    <script type="text/javascript" src="{{ asset('/dist/iosCheckbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/dist/jquery.range.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script src="{{ asset('js/phone/dist/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/custom_map.js') }}"></script>

@endsection
@section('body-class', 'agent-signup-body')
@section('content-body')

    <div class="points-content">

        <div class="container">

            <div class="row">

                <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 contact-form">
                    <h1>PROFESSIONALS SIGN UP</h1>

                    @include('flash::message')

                    <div class="col-md-12">
                        <div class="default-transparent">
                            <form role="form" id="professional-signup-form" method="post" action="{{url('professionals-signup')}}" file='true'>
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                <!-- <div class="form-group">
                                  <span class="form-titles">Your Username *</span>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('user_name',Input::old('name'),['class'=>'form-control', 'placeholder' => "Your User Name *"]) !!}

                                        </span>

                                    </div>
                                    <span class="error">{!!$errors->first('user_name')!!}</span>
                                </div> -->

                                 <!-- your input  firstname  here -->
                                   <!-- your placeholder  FIRSTNAME  here -->
                                      <div class="form-group">
                                        <!-- <span class="form-titles">First Name *</span> -->
                                         <div class="input-group">
                                             <span class="input-group-addon">
                                                 {!! Form::text('first_name',Input::old('firstname'),['class'=>'form-control', 'placeholder' => "Your First Name *"]) !!}
                                                 <label class="error">{!!$errors->first('first_name')!!}</label>
                                             </span>

                                         </div>
                                         
                                     </div>

                                 <!-- your input  lastname  here -->
                                   <!-- your placeholder  LASTNAME  here -->
                                      <div class="form-group">
                                        <!-- <span class="form-titles">Last name *</span> -->
                                         <div class="input-group">

                                             <span class="input-group-addon">
                                                 {!! Form::text('last_name',Input::old('lastname'),['class'=>'form-control', 'placeholder' => "Last Name *"]) !!}
                                                 <label class="error">{!!$errors->first('last_name')!!}</label>
                                             </span>

                                         </div>
                                         
                                     </div>

                                  <!-- your input  street_address  here -->
                                    <!-- your placeholder  Street Address  here -->
                                       <div class="form-group">
                                        <!-- <span class="form-titles">Street Address  *</span> -->
                                          <div class="input-group">
                                              <span class="input-group-addon">
                                                  {!! Form::text('street_address',Input::old('street_address'),['class'=>'form-control', 'placeholder' => "Street Address *"]) !!}
                                                   <label class="error">{!!$errors->first('street_address')!!}</label>
                                              </span>

                                          </div>
                                         
                                      </div>
                                <span class="form-titles"> Operation Range</span>
                                 <div class="checkbox">
                                  
                                
                                  <label>
                                        <input type="radio" id="mysuburb" name="searchtype" value="{{Input::old('suburb')}}">
                                         <span class="lbl padding-8">Suburb (10km Radius)</span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>


                               
                                <label>
                                        <input type="radio" id="region" name="searchtype" value="{{Input::old('region')}}">
                                         <span class="lbl padding-8">State </span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>

                                    <label>
                                        <input type="radio" id="nationwide" name="searchtype" value="{{Input::old('nationwide')}}">
                                         <span class="lbl padding-8">Nationwide </span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>
                                    <h3 class="fontsmall">Buy Sell Lease reserve the right to review your Subscription to determine if you are registered and paid for a particular Region Selection and change your settings in accordance with your Agreement, without notice to you.</h3>

                                </div> 



                                   <!-- your input  suburb  here -->
                                     <!-- your placeholder SUBURB    here -->
                                        <div class="form-group suburb-block" style="{{!Input::old('suburb')?'display:block;':'display:none'}}">
                                          <!-- <span class="form-titles">Suburb of your business *</span> -->
                                          <span class="form-titles">Select from drop down screen to make sure you pick the right suburb in the right state.</span>
                                           <div class="input-group">
                                               <span class="input-group-addon">
                                                   <!-- {!! Form::text('suburb',Input::old('suburb'),['id'=>'suburb', 'class'=>'form-control', 'placeholder' => "Suburb of your business *"]) !!} -->
                                                   <input name="suburb" type="text" value="{{Input::old('suburb')}}" class="form-control" placeholder="Suburb of your business" id="suburb"></input>
                                                   <label class="error">{!!$errors->first('suburb')!!}</label>
                                               </span>

                                           </div>
                                           

                                       </div>


                                      <!--region block here-->
                                      <div class="form-group region-block" style="{{'display:none'}}">
                                           <div class="input-group lasterr">
                                                  <span class="input-group-addon ">
                                                   
                                                   @foreach($region as $region)
                                                      <label>
                                                          <input type="checkbox" id="selectedregion" name="region[]" value="{{$region->id}}">
                                                           <span class="lbl padding-8">{{$region->name}} </span>
                                                      </label>
                                                   @endforeach
                                                        

                                                         
                                                        <!--end of state options-->
                                                       <label class="error">{!!$errors->first('region')!!}</label>
                                                  </span>

                                              </div>

                                              <div class="input-group mycity">
                                                  <span class="input-group-addon city-block">
                                                        
                                                  </span>
                                              </div>




                                             
                                       </div>





                                      <!-- your input  business_name  here -->
                                        <!-- your placeholder  Name of your Business  here -->
                                           <div class="form-group">
                                            <!-- <span class="form-titles">Name of your Business *</span> -->
                                              <div class="input-group">
                                                  <span class="input-group-addon">
                                                      {!! Form::text('business_name',Input::old('business_name'),['class'=>'form-control', 'placeholder' => "Name of your Business *"]) !!}
                                                       <label class="error">{!!$errors->first('business_name')!!}</label>
                                                  </span>

                                              </div>
                                             
                                          </div>

                                         <!-- your input  company_name  here -->
                                           <!-- your placeholder  Name of  Your Company  here -->
                                              <div class="form-group">
                                                <!-- <span class="form-titles">Name of  Your Company *</span> -->
                                                 <div class="input-group">
                                                     <span class="input-group-addon">
                                                         {!! Form::text('company_name',Input::old('company_name'),['class'=>'form-control', 'placeholder' => "Name of  Your Company"]) !!}
                                                         <label class="error">{!!$errors->first('company_name')!!}</label>
                                                     </span>

                                                 </div>
                                                 
                                             </div>



                                     <!-- your input  avatar  here -->
                                       <!-- your placeholder  AVATAR  here -->
                                          <div class="form-group">

                                             <div class="input-group">
                                                 <span class="input-group-addon">
                                                     <label style="float:left;"> Upload Logo * (Image with 180 X 180 pixels or same proportion.)</label>

                                            {!! Plupload::make('my_uploader_id', url('uploadImage'))
                                                                        ->setOptions([
                                                                         'unique_names'=>true,
                                                                         'multi_selection' => false,
                                                                        'filters' => [
                                                                        'max_file_size' => '2mb',
                                                                        'mime_types' => [
                                                                        ['title' => "Image files", 'extensions' => "jpg,jpeg,gif,png"],
                                                                        ],

                                                                        ],
                                                                        ])
                                                                        ->setAutoStart(true)->render() !!}
                                                    <span class="error uploadfilecls"></span>
                                                    <label class="error">{!!$errors->first('avatar')!!}</label>
                                                 </span>
                                             </div>
                                             


                                         </div>










                                         <div class="form-group">
                                           <!-- <span class="form-titles">Email *</span> -->
                                             <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('email',Input::old('email'),['class'=>'form-control', 'placeholder' => "Email *"]) !!}
                                            <label class="error">{!!$errors->first('email')!!}</label>
                                        </span>

                                             </div>
                                             
                                         </div>

                                         <!-- your input  business_position  here -->
                                         <!-- your placeholder  Business Position  here -->
                                         <div class="form-group">
                                           <!-- <span class="form-titles">Your position in the business *</span> -->
                                             <div class="input-group">
                                             <span class="input-group-addon">
                                                 {!! Form::text('business_position',Input::old('business_position'),['class'=>'form-control', 'placeholder' => "Your position in the business *"]) !!}
                                                 <label class="error">{!!$errors->first('business_position')!!}</label>
                                             </span>

                                             </div>
                                             
                                         </div>

                                

                                         <!-- your input  mobile_number  here -->
                                         <!-- your placeholder  Mobile Number  here -->
                                         <div class="form-group">
                                          <!-- <span class="form-titles">Your Mobile Number </span> -->
                                             <div class="input-group">
                                                   <span class="input-group-addon">
                                                       {!! Form::text('mobile_number',Input::old('mobile_no'),['class'=>'form-control', 'id'=>'mobiletext', 'placeholder' => "Your Mobile Number"]) !!}
                                                       <label class="error">{!!$errors->first('mobile_number')!!}</label>
                                                   </span>

                                             </div>
                                             
                                         </div>


                                      <div class="form-group" id="work">
                                           <!-- <span class="form-titles">Your Phone Number </span> -->
                                             <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::text('work_phone_number',Input::old('contact'),['class'=>'form-control', 'id'=>'phonetext', 'placeholder' => "Work Phone Number *"]) !!}
                                            <label class="error">{!!$errors->first('work_phone_number')!!}</label>
                                        </span>

                                             </div>
                                             
                                         </div>


                                         <div class="form-group">
                                           <!-- <span class="form-titles">Category </span> -->
                                             <div class="input-group">
                                        <span class="input-group-addon">
                                            {!! Form::select('category', $professions, Input::old('category') ,['class'=>'form-control', 'id'=>'category', 'placeholder' => 'Category ']) !!}
                                          <label class="error">{!!$errors->first('category')!!}</label>
                                        </span>

                                             </div>
                                             
                                         </div>



                                         <!-- your input  link  here -->
                                         <!-- your placeholder  LINK  here -->
                                         <div class="form-group">
                                          <span class="form-titles">Web site address should be started from <i>http://</i> </span>
                                             <div class="input-group">
                                             <span class="input-group-addon">
                                                 {!! Form::text('link',Input::old('link'),['class'=>'form-control' ,'placeholder' => "Your Website Address For Consumers To Be  Directed To *"]) !!}
                                                 <label class="error">{!!$errors->first('link')!!}</label>
                                             </span>

                                             </div>
                                             
                                         </div>

                                         <!-- your input  description  here -->
                                         <!-- your placeholder  description  here -->
                                         <div class="form-group">
                                          <!-- <span class="form-titles">Description of what your business does *</span> -->
                                          <span class="form-titles">A hundred word describing your business</span>
                                             <div class="input-group">
                                               <span class="input-group-addon">
                                                   {{--{!! Form::text('description',Input::old('description'),['class'=>'form-control', 'placeholder' => "description"]) !!}--}}
                                                   {!! Form::textarea('description',Input::old('description'), ['class' => 'form-control','placeholder' => "Description of what your business does. *"]) !!}
                                                  <label class="error">{!!$errors->first('description')!!}</label>
                                               </span>

                                             </div>
                                             
                                         </div>





                                  <!-- your input  confirm_password  here -->
                                    <!-- your placeholder  CONFIRM PASSWORD  here -->
                                       <div class="form-group">
                                        <!-- <span class="form-titles">Password *</span> -->
                                        <span class="form-titles">Enter a password so you may edit your details in the future</span>
                                          <div class="input-group">
                                              <span class="input-group-addon">
                                                  {!! Form::password('password',['class'=>'form-control','id'=>'password', 'placeholder' => "Password *"]) !!}
                                                  <label class="error">{!!$errors->first('password')!!}</label>
                                              </span>

                                          </div>
                                          
                                      </div>


                                <div class="form-group">
                                  <!-- <span class="form-titles">Confirm Password *</span> -->
                                    <div class="input-group">
                                              <span class="input-group-addon">
                                                  {!! Form::password('confirm_password',['class'=>'form-control', 'placeholder' => "Confirm Password *"]) !!}
                                                  <label class="error">{!!$errors->first('confirm_password')!!}</label>
                                              </span>

                                    </div>
                                    
                                </div>

                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('promo',Input::old('promo'),[],['id' => 'promo']) !!}<span class="lbl padding-8">Have a Complimentary Payment?</span>
                                    </label>
                                    <span class="error">{!!$errors->first('promo')!!}</span>
                                </div>



                                <div class="ans" id="ans" style="display: none;">
                                    <div class="input-group">
                                             <span class="input-group-addon">
                                                 <input type="text" name="promocode" id="promocode" class="form-control" placeholder="enter promotional code">

                                             </span>

                                    </div>
                                    <span class="error">{!!$errors->first('promocode')!!}</span>
                                </div>




                                <h3>Director Authorisation & Acknowledgement</h3>

                                <p>YOU hereby certify that you are a Director of the aforementioned company and have the authority to enter into such an arrangement with Buy Sell Lease for/and on behalf of your business and/or company </p>

                                <h3>Acknowledgement</h3>
                                <ul>
                                    <li>YOU hereby acknowledge and state that you have read through our “Service Agreement”, our “Terms & Conditions” and our “Privacy
                                        Policy” and have provided both true and accurate details within your Application Form and Agree to be bound by all of our
                                        aforementioned documents.
                                    </li>
                                    <li>YOU hereby acknowledge and consent to payment being made to BSL in accordance with the payment details and method that YOU provide
                                        below.
                                    </li>
                                    <li>YOU hereby acknowledge and agree that  you in entering into  the arrangement with BSL, are not infringing upon any
                                        Intellectual Property Rights of any third party, and will keep BSL fully indemnified from any consequences or actions relating to any contravention therof.
                                    </li>
                                </ul>
                                <div class="payment-info">
                                  <div class="col-sm-3">
                                    <img src="{{asset('/img/logo_main.png')}}" width="150">
                                  </div>
                                  <div class="col-sm-9">
                                    <h3 class="fontsmall">When making payments to Buy Sell Lease through the online form, your payment will appear on your statement as a payment to EZIDEBIT</h3>
                                  </div>
                                </div>
                                <div class="checkbox text-center">
                                    <label>
                                        {!! Form::checkbox('agree',Input::old('agree'),[]) !!}<span class="lbl padding-8 agreecon">I Agree *</span>
                                        <label class="error">{!!$errors->first('agree')!!}</label>
                                    </label>
                                    
                                </div>







                                <div class="btn-row">
                                    <button type="submit" id="send-btn" class="btn btn-primary">Submit & pay</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>

                    <div class="col-md-12 address-box">
                        <div class="default-transparent">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h3>SALES</h3>
                                    <a href="mailto:sales@buyselllease.com.au">sales@buyselllease.com.au</a>

                                    <p>Tel 02 8960-7277</p>
                                </div>
                                <div class="col-sm-6">
                                    <h3>ADMIN</h3>
                                    <a href="mailto:admin@buyselllease.com.au">admin@buyselllease.com.au</a>

                                    <p>Tel 02 8960-7277<br><br></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix">&nbsp;</div>

                </div>

            </div>

            <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12">

            </div>

        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Full width Image ==== visible ONLY in mobile devices  -->
    <!-- Image backgrounds are set within the full-width-->
    <section class="image-bg-fixed-height"><img class="img-responsive" src="{{ asset('/img/bg-agent-signup-small.jpg') }}" width="100%" ></section>
    <script>
        $(function() {
            //Phone number masking
            $("#phonetext").unmask();
            $("#phonetext").on('change',function(){
                var phone_number_new = $("#phonetext").val();

                inputmask_phonenumber(phone_number_new,"#phonetext");
                
            });
            //Mobile number masking
            $("#mobiletext").on('change',function(){
                var mobile_number_new = $("#mobiletext").val();
                inputmask_phonenumber(mobile_number_new,"#mobiletext");
            });

            createUploader('my_uploader_id');
            
            $('#send-btn').click(function (e) {
                if($("#uploader-my_uploader_id-container").find('.filelist').is(":empty")){
                  $('.uploadfilecls').show();
                  $('.uploadfilecls').html('Logo field is required.');
                  $('body, html').animate({scrollTop:$('form').offset().top}, 'slow');
                  if($('#professional-signup-form').valid()){
                    return false;
                  }

                }   
                  
              });

            //work phone number  formating
            $(":input").inputmask();

            
            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('suburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
            
            
        });
    </script>


    <script>
        $(document).ready( function() {
        
            $('#promo').change(function () {
                if (this.checked) {
                    $('#ans').fadeIn('slow');
                    $('#promocode').attr('required', true);
                } else {
                    $('#ans').fadeOut('slow');
                    $('#promocode').attr('required', false);
                }
            });

        });

    </script>


    <script type="text/javascript">
        $('input[name=searchtype]').change(function () {
               if (document.getElementById('nationwide').checked){
                   $('.suburb-block').hide();
                   $('.region-block').hide();
                   $('#suburb').prop('required', false);
                   $('#selectedregion').prop('required', false);
                   //disable the validation for other field
               } else if(document.getElementById('mysuburb').checked){
                   $('.suburb-block').show();
                   $('.region-block').hide();
                   $('#suburb').prop('required', true);
                   $('#selectedregion').prop('required', false);
               } else if(document.getElementById('region').checked){
                  $('.suburb-block').hide();
                  $('.region-block').show();
                  $('#selectedregion').prop('required', true);
                  $('#suburb').prop('required', false);
               }
            });
    </script>

    <script>
        $(document).ready( function() {
           $('input[name=agree]').prop('checked', false);
            $('#mysuburb').prop('checked', true);
            $('#suburb').prop('required', true);
        });
    </script>



@endsection
@endsection
@endsection