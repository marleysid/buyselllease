@extends('layouts.default.master')

@section('header-style')
    <link href="{{ asset('/css/search.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/iosCheckbox.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/jquery.range.css') }}" rel="stylesheet">
    <style>
        .infoBox .boxImage {
            width: 150px;
            height: 115px;
            float: left;
        }
    </style>
@endsection


@section('header-script')
    <script type="text/javascript" src="{{ asset('/dist/iosCheckbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/dist/jquery.range.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <!--<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>-->
    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer.js"></script>
    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
    <script src="{{asset('js/googlemap/professional.script.js')}}"></script>
    <script>
        var base_url = '{{url('')}}';
        var bed_img = '{{asset('/img/icon-bed-red.png')}}';
        var bath_img = '{{asset('/img/icon-shower-red.png')}}';
        var car_img = '{{asset('/img/icon-car-red.png')}}';
    </script>
@endsection



@section('content-body')
    <div class="container items-list">

        <!-- LIST HEADER ROW -->
        @include('search.detailsearch.update-search')
        <div class="row head-row">
            <div class="col-md-12">
                <h2>{{  $professionals->total()  }} real estate related services found</h2>

                <p>Your search has been automatically saved and you will be notified if new Professional match your criteria. </p>

                <ul class="disp-icons nav nav-tabs">
                    <li class="disp-btn"><a  data-toggle="tab" href="#grid-view" id="btn-view-grid"><span class="glyphicon glyphicon-th-list active"></span></a></li>
                    <li class="disp-btn"><a data-toggle="tab" href="#view-map" id="btn-view-map"><span class="glyphicon glyphicon-map-marker"></span></a>
                    </li>
                </ul>
            </div>

            <!--search criteria starts here-->
            <div class="col-lg-12 col-md-12">
                <hr/>

                @if(!empty($searchProfile))

                    <div class="col-lg-12 col-md-12">
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Where</div>
                            <div class="search-profile-summary-value">{{ explode(',', $searchProfile['suburb'])[0] }} {{ !empty($searchProfile['inc-surrounding']) ? 'and surrounding suburbs' : '' }}</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Professional Type</div>
                            <div class="search-profile-summary-value">{{ isset($searchProfile['category-action']) ? GH::getProfessionName($searchProfile['category-action']):'All' }}</div>
                        </div>
                        @if(!empty($searchProfile['price-range']) && isset($searchProfile['mode']))
                            <?php list($min, $max) = explode(',', SH::getCalculatedRange($searchProfile['agreement-type'], $searchProfile['mode'], $searchProfile['price-range'])); ?>
                            <div class="col-xs-3">
                                <div class="search-profile-summary-label">Price</div>
                                <div class="search-profile-summary-value">${{ number_format((int)$min, 0, '.', ',') }} -
                                    ${{ number_format((int)$max, 0, '.', ',') }}</div>
                            </div>
                        @endif

                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Within</div>
                            <div class="search-profile-summary-value">{{ (isset($searchProfile['cover_distance']) && !empty($searchProfile['cover_distance']) ? $searchProfile['cover_distance'] : "0") }}km</div>
                        </div>
                    </div>

                @endif
            </div>
            <!--search criteria ends here-->


            <div class="col-md-12 clearfix"></div>
            <!-- SORT ROW -->

            <div class="col-md-12 list-sort">
                <hr>
                <div class="share-block">
                    <span class="inline"><h6>SHARE</h6></span>
                    <?php
                    $segment4 = Request::segment(4);
                    if($segment4){
                        $url_append = '';
                    } else {
                        $url_append = '/'.$searchprofileid;
                    }
                    ?>
                    <a href="{{ SocShare::facebook(['picture' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/fb-icon.png') }}"></a>
                    <a href="{{ SocShare::pinterest(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/pin-it-icon.png') }}"></a>
                    <a href="{{ SocShare::gplus([['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists']])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/g-pls-icon.png') }}"></a>
                    <a href="{{ SocShare::twitter(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/tw-icon.png') }}"></a>
                    <a href="{{ SocShare::linkedin(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/link-in-icon.png') }}"></a>
                    {{-- <img src="{{ asset('img/social-icons.png') }}">--}}

                    <div class="see-pro"><a class="btn btn-secondary btn-sm" href="{{url('dashboard/search')}}" role="button">{{$redirect_url_text}}</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-content">

            <div class="tab-pane active" id="grid-view">
                <!-- START LIST -->
                @forelse($professionals as $professional)

                    <div class="row grey-background-holder">

                        @if($professional->logo != '' && file_exists(public_path().'/uploads/professionals/'.$professional->logo))
                            <div class="col-md-3 col-sm-12 col-xs-12"><div class="img-wrap-holder row imagelist" style="background: url('{{asset('uploads/professionals').'/'.$professional->logo}}')"><!-- <img src="{{asset('uploads/professionals').'/'.$professional->logo}}"  alt="Photo of" class="img-responsive"> --></div></div>
                        @else
                            <div class="col-md-3 col-sm-12 col-xs-12"><div class="img-wrap-holder row imagelist" style="background: url('{{asset('uploads/noimage.png')}}')"><!-- <img src="{{asset('uploads/noimage.png')}}"  alt="Photo of" class="img-responsive"> --></div></div>
                        @endif
                        


                        <div class="col-md-5 col-sm-12 col-xs-12"><div class="detail-holder"><h4>{!! GH::limit_word($professional->business_name,4) !!} <small></small>
                                </h4><span class="location-line"><span class="glyphicon glyphicon-map-marker"> </span> 
                                        @if ($professional->type == 'nationwide')
                                            Nationwide
                                        @elseif ($professional->type == 'suburb')
                                            {{ $professional->suburb }}
                                        @elseif($professional->type == 'region')
                                            {{ implode(', ', GH::getRegionName($professional->lat) )}}
                                        @else 
                                            Not specified
                                        @endif
                                        </span>
                                        <p>{!! GH::limit_word($professional->description,50) !!}</p>

                                        </div>

                            </div>
                       
                    <!--problem here-->
              
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="logo-area-holder">
                            @if (!empty($professional->category->image_accessor))
                                <img src="{{SH::getImageService($professional->category->image_accessor)}}"  alt="Photo of" class="img-responsive">
                            @else
                                 <img src="" alt="No Image" class="img-responsive">
                            @endif
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="business-detail-wrapper-holder">
                                <strong>
                                @if(!empty($professional->category->title))
                                {{$professional->category->title}}
                                @else
                                 Unspecified Industry
                                @endif
                                </strong>
                                <p><i class="fa fa-phone"></i>&nbsp; 
                                @if(!empty($professional->contact))
                                {{GH::contact_format($professional->contact)}}
                                @else
                                  'Contact Not available'
                                @endif
                                </p>
                                <a target="_blank" href="{{ GH::pritify_url($professional->url) }}" class="view-professional-btn">VIEW PROFESSIONAL</a>
                            </div>
                        </div>
              
                    <!--problem here-->
                    </div>


                @empty
                    No Related Real Estate Services Found
                @endforelse


                            <!-- pagination area -->
                    {!! $professionals->render() !!}
            </div>

            <div class="tab-pane" id="view-map">

                <div id="gMap" style="height:500px;width:100%;">


                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        var searchprofileid = '{{$searchprofileid}}';
        goMap.init({
            container: "gMap",
            options: {
                map: {
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new google.maps.LatLng("-25", "134")
                },
                marker: {
                    icon: '{{asset("/img/houseMarker.png")}}',
                    infobox: {
                        boxClass: 'infoBox'
                        , maxWidth: 0
                        , pixelOffset: new google.maps.Size(-170, -40)
                        , zIndex: null
                        , closeBoxURL: ""
                        , infoBoxClearance: new google.maps.Size(20, 20)
                        , pane: "floatPane"
                        , enableEventPropagation: false
                        , alignBottom: true
                    }
                },
                cluster: {
                    event: {
                        mouseover: {
                            icon: '{{asset("img/icon-map-circle2.png")}}'
                        },
                        mouseout: {
                            icon: '{{asset("/img/icon-map-circle.png")}}'
                        }
                    },
                    style: {
                        textColor: "#ffffff",
                        fontFamily: "Arial",
                        textSize: 14,
                        height: 56,
                        width: 60,
                        url: '{{asset("/img/icon-map-circle.png")}}'
                    }
                }
            }
        });


        /* $('#btn-view-map').on('click', function () {
         goMap.data = [];
         goMap.render();

         ajaxMap(1);
         });*/


        $('#btn-view-map').on('click', function () {
            $('#view-map').addClass('active');
            $(this).find('span').addClass('active');

            $('#grid-view').removeClass('active');
            $("#btn-view-grid").find('span').removeClass('active');
            goMap.data = [];
            goMap.render();
            ajaxMap(1);
        });

        $("#btn-view-grid").click(function(){
            $('#grid-view').addClass('active');
            $(this).find('span').addClass('active');

            $('#view-map').removeClass('active');
            $('#btn-view-map').find('span').removeClass('active');
        });

        var ajaxMap = function (i) {

            $.ajax({
                url: base_url+"/get-map-data-professional/" + searchprofileid,
                type: 'GET',
                async: true,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    goMap.mergeData(data.map_data);
                    goMap.render();
                }
            });
        }
    </script>

@endsection