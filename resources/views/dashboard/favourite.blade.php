@extends('layouts.default.master')

@section('content-body')
    <script>
        var base_url = '{{url('')}}';
        var bed_img = '{{asset('/img/icon-bed-red.png')}}';
        var bath_img = '{{asset('/img/icon-shower-red.png')}}';
        var car_img = '{{asset('/img/icon-car-red.png')}}';
    </script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer.js"></script>
    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>

    <script src="{{asset('js/googlemap/map.script.js')}}"></script>
    <style>
        .infoBox .boxImage {
            width: 150px;
            height: 115px;
            float: left;
        }
    </style>
    <div class="container items-list">
        @include('flash::message')
                <!-- LIST HEADER ROW -->
        <div class="row head-row">
            <div class="col-md-12">

                <h2>{{ $favouriteproperties->total() }}   favourite properties found</h2>

               <!-- <p>Your search has been automatically saved and you will be notified if new properties match your
                    criteria. </p> -->





                <ul class="disp-icons nav nav-tabs">
                    <li class="disp-btn"><a  data-toggle="tab" href="#grid-view" id="btn-view-grid"><span class="glyphicon glyphicon-th-list active"></span></a></li>
                    <li class="disp-btn"><a data-toggle="tab" href="#view-map" id="btn-view-map"><span class="glyphicon glyphicon-map-marker"></span></a>
                    </li>
                </ul>
            </div>
           <!-- <div class="col-lg-12 col-md-12 list-labels">
                <hr/>
                @if(!empty($searchProfile))
                    <div class="col-lg-12 col-md-12">
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Where</div>
                            <div class="search-profile-summary-value">{{ explode(',', $searchProfile['suburb'])[0] }} {{ !empty($searchProfile['inc-surrounding']) ? 'and surrounding suburbs' : 'All' }}</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Type</div>
                            <div class="search-profile-summary-value" style="white-space: nowrap;">{{ $searchProfile['residential-action'] or "All" }} {{ !empty($searchProfile['commercial-action']) ? ",".$searchProfile['commercial-action'] : "" }}</div>
                        </div>


                        @if(!empty($searchProfile['price-range']))
                            <?php list($min, $max) = explode(',', SH::getCalculatedRange($searchProfile['agreement-type'], $searchProfile['mode'], $searchProfile['price-range'])); ?>
                            <div class="col-xs-3">
                                <div class="search-profile-summary-label">Price</div>
                                <div class="search-profile-summary-value">${{ number_format((int)$min, 0, '.', ',') }} -
                                    ${{ number_format((int)$max, 0, '.', ',') }}</div>
                            </div>
                        @endif

                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Within</div>
                            <div class="search-profile-summary-value">{{ $searchProfile['cover_disatance'] or '5' }}km</div>
                        </div>


                    </div>
               // @endif
              //  @if(Auth::check() && Auth::user()->role !='1')
                    <div class="col-lg-5 col-md-5 pull-right">
                        <div class="lab-on-off">
                            <div class="switch">
                                <label class="checkbox-inline">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch"  @if(Auth::user()->notification == 1) checked @endif class="onoffswitch-checkbox"
                                               id="notification"  value="">
                                        <label class="onoffswitch-label" for="notification">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="lab-notif"><h5>NOTIFICATIONS</h5>

                            <p>Email me if new properties are added to my search</p></div>
                    </div>
              //  @endif
            </div>
            <div class="col-md-12 clearfix"></div>
            <!-- SORT ROW -->
            <div class="col-md-12 list-sort">
                <hr>

                <div class="col-md-5 col-sm-12 col-xs-12 left">
                    <div class="lab-where">
                        <h6>SORT</h6>
                    </div>

                    <div class="sort-icons">
                        <span class="inline sort-btn">


                            <a href="{{ GH::get_sort_urls(Request::url(),'source_updated',Input::get('order'),Input::get('page'))}}"
                               class="btn btn-secondary {{Input::get('sort') == 'source_updated'?'btn-primary':'' }}"><img
                                        src="{{ asset('img/icon-sort1.png') }}"></a></span>
                        <span class="inline sort-btn"><a
                                    href="{{ GH::get_sort_urls(Request::url(),'price',Input::get('order'),Input::get('page'))}}"
                                    class="btn btn-secondary {{Input::get('sort') == 'price'?'btn-primary':'' }}"><img
                                        src="{{ asset('img/icon-sort2.png') }}"></a></span>
                        <span class="inline sort-btn"><a
                                    href="{{ GH::get_sort_urls(Request::url(),'bedrooms',Input::get('order'),Input::get('page'))}}"
                                    class="btn btn-secondary {{Input::get('sort') == 'bedrooms'?'btn-primary':'' }}"><img
                                        src="{{ asset('img/icon-sort3.png') }}"></a></span>
                        <span class="inline sort-btn"><a
                                    href="{{ GH::get_sort_urls(Request::url(),'bathrooms',Input::get('order'),Input::get('page'))}}"
                                    class="btn btn-secondary {{Input::get('sort') == 'bathrooms'?'btn-primary':'' }}"><img
                                        src="{{ asset('img/icon-sort4.png') }}"></a></span>
                        <span class="inline sort-btn"><a
                                    href="{{ GH::get_sort_urls(Request::url(),'parking',Input::get('order'),Input::get('page'))}}"
                                    class="btn btn-secondary {{Input::get('sort') == 'parking'?'btn-primary':'' }}"><img
                                        src="{{ asset('img/icon-sort5.png') }}"></a></span>
                        <span class="inline sort-btn"><a
                                    href="{{ GH::get_sort_urls(Request::url(),'favourite',Input::get('order'),Input::get('page'))}}"
                                    class="btn btn-secondary {{Input::get('sort') == 'favourite'?'btn-primary':'' }}"><img
                                        src="{{ asset('img/icon-sort6.png') }}"></a></span>
                    </div>
                </div>

                <div class="col-md-7 col-sm-12 col-xs-12 right">
                    <div class="share-block">
                        <span class="inline"><h6>SHARE</h6></span>
                        <?php
                        $segment4 = Request::segment(4);
                        if ($segment4) {
                            $url_append = '';
                        } else {
                            $url_append = '/'.\Auth::user()->id;
                        }
                        ?>

                        <a href="{{ SocShare::facebook(['picture' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}"
                           target="_blank"><img src="{{ asset('img/fb-icon.png') }}"></a>
                        <a href="{{ SocShare::pinterest(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}"
                           target="_blank"><img src="{{ asset('img/pin-it-icon.png') }}"></a>
                        <a href="{{ SocShare::gplus([['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists']])->getUrl().$url_append }}"
                           target="_blank"><img src="{{ asset('img/g-pls-icon.png') }}"></a>
                        <a href="{{ SocShare::twitter(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}"
                           target="_blank"><img src="{{ asset('img/tw-icon.png') }}"></a>
                        <a href="{{ SocShare::linkedin(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}"
                           target="_blank"><img src="{{ asset('img/link-in-icon.png') }}"></a>


                    </div>
                </div>

            </div>
        </div>
        <div class="tab-content">
            <div class="row tab-pane active" id="grid-view">
                <!-- START LIST -->
                <div class="list-areas">


                    @foreach ($favouriteproperties as $property)
                        @include('partials.property-list-item', array('property'=> $property))
                    @endforeach
                </div>


                {!! $favouriteproperties->appends(['sort' => Input::get('sort'),'order'=>Input::get('order')])->render() !!}



                        <!-- PAGINATION -->
                <!-- MAP AREA -->
            </div>

            <div class="tab-pane" id="view-map">

                <div id="gMap" style="height:500px;width:100%;">

                </div>
            </div>

        </div>

    </div>



    <script type="text/javascript">
        var searchprofileid = '{{\Auth::user()->id}}';
        goMap.init({
            container: "gMap",
            options: {
                map: {
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new google.maps.LatLng("-25", "134")
                },
                marker: {
                    icon: '{{asset("/img/houseMarker.png")}}',
                    infobox: {
                        boxClass: 'infoBox'
                        , maxWidth: 0
                        , pixelOffset: new google.maps.Size(-170, -40)
                        , zIndex: null
                        , closeBoxURL: ""
                        , infoBoxClearance: new google.maps.Size(20, 20)
                        , pane: "floatPane"
                        , enableEventPropagation: false
                        , alignBottom: true
                    }
                },
                cluster: {
                    event: {
                        mouseover: {
                            icon: '{{asset("img/icon-map-circle2.png")}}'
                        },
                        mouseout: {
                            icon: '{{asset("/img/icon-map-circle.png")}}'
                        }
                    },
                    style: {
                        textColor: "#ffffff",
                        fontFamily: "Arial",
                        textSize: 14,
                        height: 56,
                        width: 60,
                        url: '{{asset("/img/icon-map-circle.png")}}'
                    }
                }
            }
        });


        $('#btn-view-map').on('click', function () {
            $('#view-map').addClass('active');
            $(this).find('span').addClass('active');

            $('#grid-view').removeClass('active');
            $("#btn-view-grid").find('span').removeClass('active');
            goMap.data = [];
            goMap.render();
            ajaxMap(1);
        });

        $("#btn-view-grid").click(function(){
            $('#grid-view').addClass('active');
            $(this).find('span').addClass('active');

            $('#view-map').removeClass('active');
            $('#btn-view-map').find('span').removeClass('active');
        });


        var ajaxMap = function (i) {

            $.ajax({
                url: base_url+"/get-map-data-favourite",
                type: 'GET',
                async: true,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    goMap.mergeData(data.map_data);
                    goMap.render();
                }
            });
        }
    </script>

@stop
