<html>
<head>
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

    <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js" type="text/javascript"></script>
    <script src="data.json" type="text/javascript"></script>
</head>
<body onload="initializeMaps()">
<div id="map_canvas" style="width:100%;height:300px"></div>
</body>
</html>

<script>
    var data = {
        "markers":[
            {
                "lat": "13.0839",
                "lng": "80.2700"
            },
            {
                "lat": "13.0654",
                "lng": "80.2326"
            },
            {
                "lat": "13.0342",
                "lng": "80.2301"
            },
            {
                "lat": "12.9300",
                "lng": "80.1100"
            },
            {
                "lat": "13.0826",
                "lng": "80.2750"
            }
        ]
    };

    mcOptions = {
        styles: [
            {
                height: 53,
                url: "/img//img/icon-map-pin.png",
                width: 53
            },
            {
                height: 56,
                url: "/img/icon-map-circle2.png",
                width: 56
            },
            {
                height: 66,
                url: "/img/icon-map-circle.png",
                width: 66
            },
            {
                height: 78,
                url: "/img/icon-map-circle.png",
                width: 78
            },
            {
                height: 90,
                url: "/img/icon-map-circle.png",
                width: 90
            }
        ]
    };
    function initializeMaps() {
        var latlng = new google.maps.LatLng("-25", "134");
        var myOptions = {
            zoom: 2,
            center:latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
       // var mc = new MarkerClusterer(map, data, mcOptions);
        var mc = new MarkerClusterer(map);
        var marker, i;
        for (i = 0; i < data.markers.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(data.markers[i].lat, data.markers[i].lng),
                map: map
            });
            mc.addMarker(marker);
        }




    }
</script>



http://a32.me/2012/09/marker-clusters-on-google-map-with-two-data-sources/