@extends('layouts.default.master')


@section('header-style')
    <link href="{{ asset('/css/search.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/iosCheckbox.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/jquery.range.css') }}" rel="stylesheet">
    <style>
        .infoBox .boxImage {
            width: 150px;
            height: 115px;
            float: left;
        }
    </style>
@endsection

@section('header-script')
    <script type="text/javascript" src="{{ asset('/dist/iosCheckbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/dist/jquery.range.js') }}"></script>
   <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <!--<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>-->
    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer.js"></script>
    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
    <script src="{{asset('js/googlemap/agent.script.js')}}"></script>
    <script>
    var base_url = '{{url('')}}';
    var bed_img = '{{asset('/img/icon-bed-red.png')}}';
    var bath_img = '{{asset('/img/icon-shower-red.png')}}';
    var car_img = '{{asset('/img/icon-car-red.png')}}';
    </script>
@endsection
@section('content-body')

    <div class="container items-list">
<!-- LIST HEADER ROW -->
        
        @include('search.detailsearch.update-search')
      
        <div class="row head-row">
            <div class="col-md-12">
                <h2>{{ $agents->total() }} agents found</h2>
                <p>Your search has been automatically saved and you will be notified if new properties match your criteria. </p>
                <ul class="disp-icons nav nav-tabs">
                    <li class="disp-btn"><a  data-toggle="tab" href="#grid-view" id="btn-view-grid"><span class="glyphicon glyphicon-th-list active"></span></a></li>
                    <li class="disp-btn"><a data-toggle="tab" href="#view-map" id="btn-view-map"><span class="glyphicon glyphicon-map-marker"></span></a>
                    </li>
                </ul>
            </div>

            <!--search criteria starts here-->
                        <div class="col-lg-12 col-md-12 list-labels">
                <hr/>
                @if(!empty($searchProfile))
                    <div class="col-lg-7 col-md-7">
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Where</div>
                            <div class="search-profile-summary-value">{{ explode(',', $searchProfile['suburb'])[0] }} {{ !empty($searchProfile['inc-surrounding']) ? 'and surrounding suburbs' : '' }}</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Type</div>
                            <div class="search-profile-summary-value">{{ $searchProfile['action'] or 'All' }}</div>
                        </div>
                        @if(!empty($searchProfile['price-range']) && isset($searchProfile['mode']))
                            <?php list($min, $max) = explode(',', SH::getCalculatedRange($searchProfile['agreement-type'], $searchProfile['mode'], $searchProfile['price-range'])); ?>
                            <div class="col-xs-3">
                                <div class="search-profile-summary-label">Price</div>
                                <div class="search-profile-summary-value">${{ number_format((int)$min, 0, '.', ',') }} -
                                    ${{ number_format((int)$max, 0, '.', ',') }}</div>
                            </div>
                        @endif
                        <?php 
                              if(array_key_exists('cover_distance', $searchProfile)){
                                $variable_distance = $searchProfile['cover_distance'];
                              }else{
                                $variable_distance = "";
                              }
                            ?>
                        <div class="col-xs-3">
                            <div class="search-profile-summary-label">Within</div>

                            <div class="search-profile-summary-value">{{ $variable_distance? $variable_distance : 0 }}km</div>
                        </div>
                    </div>
                @endif

                @if(Auth::check() && Auth::user()->role !='1')
                <div class="col-lg-5 col-md-5 pull-right">
                    <div class="lab-on-off">
                        <div class="switch">
                            <label class="checkbox-inline">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch"  @if(Auth::user()->notification == 1) checked @endif class="onoffswitch-checkbox" id="notification"  value="">
                                    <label class="onoffswitch-label" for="notification">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="lab-notif"><h5>NOTIFICATIONS</h5>
                        <p>Email me if new properties are added to my search</p></div>
                </div>
                @endif
            </div>
            <!--search criteria ends here-->


            <div class="col-md-12 clearfix"></div>
<!-- SORT ROW -->
            <div class="col-md-12 list-sort">
                <hr>
                <div class="share-block">
                    <span class="inline"><h6>SHARE</h6></span>
                    <?php
$segment4 = Request::segment(4);
if ($segment4) {
	$url_append = '';
} else {
	$url_append = '/' . $searchprofileid;
}
?>
                    <a href="{{ SocShare::facebook(['picture' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/fb-icon.png') }}"></a>
                    <a href="{{ SocShare::pinterest(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/pin-it-icon.png') }}"></a>
                    <a href="{{ SocShare::gplus([['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists']])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/g-pls-icon.png') }}"></a>
                    <a href="{{ SocShare::twitter(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/tw-icon.png') }}"></a>
                    <a href="{{ SocShare::linkedin(['media' => asset('/img/logo-sm.png'), 'description' => 'Realestate property lists'])->getUrl().$url_append }}" target="_blank"><img src="{{ asset('img/link-in-icon.png') }}"></a>
                    <div class="see-pro"><a class="btn btn-secondary btn-sm" href="{{url('dashboard/search/professionals')}}" role="button">See Real Estate Related Services</a></div>
                </div>
            </div>
        </div>
<!-- START LIST -->
    <div class="tab-content">
        <div class="tab-pane active" id="grid-view">
             @foreach ($agents as $agent)
                @include('partials.agent-list-item', array('agent'=> $agent))
            @endforeach

             {!! $agents->render() !!}
    </div>

    <div class="tab-pane" id="view-map">

                <div id="gMap" style="height:500px;width:100%;">

                </div>
            </div>

        </div>

    </div>

   <script type="text/javascript">
        var searchprofileid = '{{$searchprofileid}}';
        goMap.init({
            container: "gMap",
            options: {
                map: {
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new google.maps.LatLng("-25", "134")
                },
                marker: {
                    icon: '{{asset("/img/houseMarker.png")}}',
                    infobox: {
                        boxClass: 'infoBox'
                        , maxWidth: 0
                        , pixelOffset: new google.maps.Size(-170, -40)
                        , zIndex: null
                        , closeBoxURL: ""
                        , infoBoxClearance: new google.maps.Size(20, 20)
                        , pane: "floatPane"
                        , enableEventPropagation: false
                        , alignBottom: true
                    }
                },
                cluster: {
                    event: {
                        mouseover: {
                            icon: '{{asset("img/icon-map-circle2.png")}}'
                        },
                        mouseout: {
                            icon: '{{asset("/img/icon-map-circle.png")}}'
                        }
                    },
                    style: {
                        textColor: "#ffffff",
                        fontFamily: "Arial",
                        textSize: 14,
                        height: 56,
                        width: 60,
                        url: '{{asset("/img/icon-map-circle.png")}}'
                    }
                }
            }
        });


        $('#btn-view-map').on('click', function () {
            $('#view-map').addClass('active');
            $(this).find('span').addClass('active');

            $('#grid-view').removeClass('active');
            $("#btn-view-grid").find('span').removeClass('active');
            goMap.data = [];
            goMap.render();
            ajaxMap(1);
        });

        $("#btn-view-grid").click(function(){
            $('#grid-view').addClass('active');
            $(this).find('span').addClass('active');

            $('#view-map').removeClass('active');
            $('#btn-view-map').find('span').removeClass('active');
        });


        var ajaxMap = function (i) {

            $.ajax({
                url: base_url+"/get-map-data-agent/" + searchprofileid,
                type: 'GET',
                async: true,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    goMap.mergeData(data.map_data);
                    goMap.render();
                }
            });
        }


    </script>

@stop
