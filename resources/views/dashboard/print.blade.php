@extends('layouts.print.print')

@section('title')
    {{ \App\Models\PropertyHelper::getData($propertyDetails,'headline') }}
@endsection

@section('header')
        <link href="{{asset('css/print.css')}}" rel="stylesheet" media="all">
        <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" media="all">
        <link rel="stylesheet" href="{{asset('css/bootstrap-theme.min.css')}}" media="all" >
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" media="all">

@endsection

@section('body')

    <div class="container print-area"  id="output">
    <a href="javascript;" id="print" style="display:none;">print</a>
        <div class="">
                <div class="title-sec">
                    <div class="title-details">
                    {{ \App\Models\PropertyHelper::getData($propertyDetails,'headline') }}<br>
                        {{  GH::removeCommaFromLastOfString($propertyDetails->address)}}<br>
                        {{ \App\Models\PropertyHelper::getData($propertyDetails,'category') }}
                    </div>
                    <div class="bed-bath-parking">
                        <span><img src="{{ asset('img/icon-bed-grey.png') }}" />{{$propertyDetails->bedrooms}}</span>
                        <span><img src="{{ asset('img/bath-room-icon-grey.png') }}" />{{$propertyDetails->bathrooms}}</span>
                        <span><img src="{{ asset('img/car-icon-grey.png') }}" />{{$propertyDetails->parking}}</span>
                    </div>
                    <div class="agency-logo">
                        <img src="{{ $otherDetails->logo_url }}" alt="{{ $otherDetails->name }}"/>
                    </div>
                </div>
                <div class="images">
                    <div class="main-image">
                        @foreach($images as $key => $img)
                            @if($key =='1')
                              <img   src="{{$img}}" />
                            @endif

                        @endforeach

                    </div>
                    <div class="related-images">
                        <div class="row">
                        @foreach($images as $key => $img)
                            @if($key !='1')
                                    <div class="col-xs-3">
                                    <img src="{{$img}}" class="img-thumbnail" alt="Thumbnail Image">
                                        </div>
                            @endif

                        @endforeach
                            </div>

                    </div>

                </div>

            <div class="property-description ">


                <div class="desc-content"><p>{!! nl2br(e(\App\Models\PropertyHelper::getData($propertyDetails,'description'))) !!}</p></div>
                <div class="">
                
                
                    </div>
                    
                    <div class="">
						
                
	                </div>
                    <div class="agent-details">
                  

                    <div class="property-detail-features-wrapper">
                        <div class="general-features-wrapper">
                            <h3>General Features</h3>
                            <ul>
                                <li>Property type: <b>{{ \App\Models\PropertyHelper::getData($propertyDetails,'category') }}</b></li>
                                <li>Bedrooms: <b>{{ $propertyDetails->bedrooms }}</b></li>
                                <li>Bathrooms: <b>{{ $propertyDetails->bathrooms }}</b></li>
                                <li>Land size: <b>
                                        @if(\App\Models\PropertyHelper::getData($propertyDetails,'landDetails.area') != "")
                                            {{ \App\Models\PropertyHelper::getData($propertyDetails,'landDetails.area')  }} square meter (approx)
                                        @else
                                            N/A
                                        @endif
                                    </b></li>
                            </ul>
                        </div>
                        <div class="outdoor-features-wrapper">
                            <h3>Outdoor Features</h3>
                            <ul>
                                <li>Garage spaces: <b> {{ $propertyDetails->parking }}</b></li>
                            </ul>
                        </div>
                    </div>
                    <div class="inspections">
                    <div class="right-text-wrap-holder">
                        <div class="times-box-wrapper-holder">
                            <strong>Inspection time</strong>
                            @if(!empty($inspectionsTimes))
                                @foreach($inspectionsTimes as $inspection)

                                    <span><i class="fa fa-calendar"></i>&nbsp;{{$inspection}}</span><br>
                                @endforeach
                            @else
                                <span>No inspections are currently scheduled.</span>
                            @endif

                        </div>
                    </div>
                </div>
                      <h3>Agents</h3>
                    <p>
                        <span><i class="fa fa-user"></i>&nbsp; {{ \App\Models\PropertyHelper::getData($propertyDetails,'listingAgent.name') }}</span><br>
                        <span><i class="fa fa-envelope-o"></i> {{ \App\Models\PropertyHelper::getData($propertyDetails,'listingAgent.email') }}</span><br>
                        <span><i class="fa fa-phone"></i>&nbsp; {{ \App\Models\PropertyHelper::getData($propertyDetails,'listingAgent.telephone') }}</span><br>
                    </p>
                </div>
            </div>
  @if(\App\Models\PropertyHelper::getData($propertyDetails,'floorplan_1'))
            <div class="floor-plan-images ">
                <h3>Floor Plans</h3>
                <img src="{{ \App\Models\PropertyHelper::getData($propertyDetails,'floorplan_1') }}" alt="florplan "/>
            </div>
            @endif
            <div class="agnecy-details">
                <div class="goodyer-wrapper-holder">
                   
                    <p> <b>About {{ $otherDetails->trading_name }}</b><br/></p>
                     @if($otherDetails->profile_image && $otherDetails->profile_image !='')
                     <img class="img-fixed-sizing-holder-wrap" src="{{ $otherDetails->profile_image }}" />
                     @endif
                    <p>{{ $otherDetails->description  }}</p>
                    <p>  <b>Address:</b> {{$otherDetails->address or 'N/A'}}</p>
                    <p>  <b>Contact:</b> {{$otherDetails->phone or 'N/A'}}</p>

                </div>
                <div class="map">
                    <!-- map here-->
                </div>
            </div>

            <div class="footer-area ">

            </div>



        </div>
    </div>

    <script>
        function printDiv() {
            $.print("#whole-body");
        }
    </script>
@endsection

@section('footer')


@endsection

@section('footer-scripts')


@endsection

