@extends('layouts.default.master')

@section('content-body')

 <style>
        #bxslider-pager{
            height: 100% !important;
        }

        #bxslider-pager > li > img {
            height: 101px !important;
        }

        .pager-images{
            height: 101px !important;
            width: 150px !important;
        }

        .bxslider li img {
            height:625px !important;
        }

        #bxslider-pager > li {
            width: 150px !important;
        }

        .pager-active{
            border:2px solid #333 ;
        }
        #googleMap{
            width:100%; height: 300px; border: 1px solid #0f0f0f;
        }


    </style>


    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>

    <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer.js"></script>
   <script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox_packed.js"></script>
    <div class="container items-list">

        <script type="text/javascript" src="{{asset('js/bxslider/jquery.bxslider.min.js')}}"></script>

        <link href="{{asset('js/bxslider/jquery.bxslider.css')}}" rel="stylesheet">



        <!-- LIST HEADER ROW -->
        <div>&nbsp;</div>
        <div class="marign-left-rignt-mbl mb-10">
            <a href="{{URL::previous() }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Back</a>
        </div>
        <div class="row detail-holder-wrapper-">
            <div class="detail-banner-wrap">
              <div class="row">
                    <div class="">
						<ul class="bxslider" id="bxslider">
								@foreach($images as $key => $img)
									<li><img src="{{$img}}"   /></li>

								@endforeach
						</ul>
                        </div>
                    <div class="bxslider-pager-sec no-print">

                <div id="bx-pager">
                    <ul id="bxslider-pager">
                    @foreach($images as $key => $img)
                            <li data-slideIndex="{{$key}}" class="{{($key==0)?'pager-active':''}}"><img  class="pager-images" src="{{$img}}" /></li>
                    @endforeach
                    </ul>
                </div>



                        </div>
						 <div class="outside">

                    <p><span id="slider-prev" class="slider-prev"></span><span id="slider-next" class="slider-next"></span></p>
                </div>

			</div>
            </div>
            <!--end banner wrapper here -->
            <div class="property-detail-page-wrapper-holder row">
                <div class="col-md-8 col-sm-12">
                    <div class="left-section-detail-wrapper">
                        <div class="top-property-detail-wrapper-holder">
                            
                           
                            <h3>{{  GH::removeCommaFromLastOfString($propertyDetails->address)}}</h3>
                            

                            @if(!in_array("auction",explode(' ',strtolower(\App\Models\PropertyHelper::getData($propertyDetails,'priceView')))) && (\App\Models\PropertyHelper::getData($propertyDetails,'price')))
                            <span>$ {{ number_format(\App\Models\PropertyHelper::getData($propertyDetails,'price'),  2, '.', ',') }}</span>
                            @endif
                            <span class="small">{{ \App\Models\PropertyHelper::getData($propertyDetails,'category') }}</span>

                            @if(\App\Models\PropertyHelper::getData($propertyDetails,'floorplan_1'))
                            <span class="small"><img src="{{ asset('img/floorplan.png') }}" /><a href="#" data-toggle="modal" data-target="#myModal" src="{{ \App\Models\PropertyHelper::getData($propertyDetails,'floorplan_1') }}" >Floorplan</a></span>
                            @endif


                            <div class="icon-holder-wrapp-right">
                                <span><img src="{{ asset('img/icon-bed-grey.png') }}" />{{$propertyDetails->bedrooms}}</span>
                                <span><img src="{{ asset('img/bath-room-icon-grey.png') }}" />{{$propertyDetails->bathrooms}}</span>
                                <span><img src="{{ asset('img/car-icon-grey.png') }}" />{{$propertyDetails->parking}}</span>
                            </div>
                        </div>


                     <!--i added this-->
                        <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                     <h3 class="agent-logo-here-heading-wrapper"><img src="{{ $otherDetails->logo_url }}" alt="{{ $otherDetails->name }}"></h3>
                                        <hr/>
                                        <p>
                                        <span><i class="fa fa-user"></i>&nbsp; {{ \App\Models\PropertyHelper::getData($propertyDetails,'listingAgent.name') }}</span><br>
                                        <span><i class="fa fa-envelope-o"></i> {{ \App\Models\PropertyHelper::getData($propertyDetails,'listingAgent.email') }}</span><br>
                                        <span><i class="fa fa-phone"></i>&nbsp; {{ \App\Models\PropertyHelper::getData($propertyDetails,'listingAgent.telephone') }}</span><br>

                                        <span><a class="view-agent" target="_blank" href="{{ $otherDetails->listing_staff_url }}">Meet the  Team</a></span>
                                        </p>
                                    </div>
                                </div>
                        </div>
                        <!--i added this-->


                        <div class="property-content-detail-wrapper-holder">
                            <div class="heading-wrapper-holder-title">
                                <div class="left-text-wrap-holder pull-left">
                                    <h3>{{ GH::removeCommaFromLastOfString($propertyDetails->address)}}</h3>
                                    <h4>{{ \App\Models\PropertyHelper::getData($propertyDetails,'headline') }}</h4>
                                </div>
                                <div class="right-text-wrap-holder pull-right">
                                    <div class="times-box-wrapper-holder">
                                        <strong>Inspection time</strong>
                                        @if(!empty($inspectionsTimes))
                                        @foreach($inspectionsTimes as $inspection)

                                                <span><i class="fa fa-calendar"></i>&nbsp;{{$inspection}}</span><br>
                                            @endforeach
                                            @else
                                        <span>No inspections are currently scheduled.</span>
                                        @endif

                                    </div>
                                </div>
                                <p>{!! nl2br(e(\App\Models\PropertyHelper::getData($propertyDetails,'description'))) !!}</p>
                            </div>


                        </div>
						<div class="property-detail-features-wrapper">
                                <div class="general-features-wrapper">
                                    <h3>General Features</h3>
                                    <ul>
                                        <li>Property type: <b>{{ \App\Models\PropertyHelper::getData($propertyDetails,'category') }}</b></li>
                                        <li>Bedrooms: <b>{{ $propertyDetails->bedrooms }}</b></li>
                                        <li>Bathrooms: <b>{{ $propertyDetails->bathrooms }}</b></li>
                                        <li>Land size: <b>
                                                @if(\App\Models\PropertyHelper::getData($propertyDetails,'landDetails.area') != "")
                                                    {{ \App\Models\PropertyHelper::getData($propertyDetails,'landDetails.area')  }} square meter (approx)
                                                @else
                                                    N/A
                                                @endif
                                            </b></li>
                                    </ul>
                                </div>
                                <div class="outdoor-features-wrapper">
                                    <h3>Outdoor Features</h3>
                                    <ul>
                                        <li>Garage spaces: <b> {{ $propertyDetails->parking }}</b></li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                </div>

                <!--right sidebar start here-->
                <div class="col-md-4 col-sm-12 print-margin">
                    <div class="right-section-detail-wrapper">
                        <div class="right-top-wrapper-holder">
                            <a class="print no-print" target="_new" href="{{url('print/property-detail/'.$propertyDetails->property_code)}}"><span class="glyphicon glyphicon-print"></span>print page</a>
                            @if(Auth::check() && Auth::user()->role !='1')
                        <span class="save"><a href="javascript:;"  pcode="{{$propertyDetails->property_code}}" class="{{GH::isFavourite($propertyDetails->property_code)?'remove-favourite-property':'add-favourite-property'}}"><span class="glyphicon glyphicon-heart-full glyphicon-heart-empty {{ GH::isFavourite($propertyDetails->property_code)  ? 'make-red' : 'heart-deafult-color' }}"></span></a>Save</span>
                                @endif
                        </div>
                        <!--map start here-->
                        <div class="map-wrapper-holder">
                            <div id="googleMap"></div>
                        </div>

                        <div class="goodyer-wrapper-holder">
                        <h3> <img src="{{ $otherDetails->logo_url }}"  alt="{{ $otherDetails->name }}"></h3>
                           	<img class="img-fixed-sizing-holder-wrap" src="{{ $otherDetails->profile_image }}" />
                            <p> <b>About {{ $otherDetails->trading_name }}</b><br/></p>
                            <p>{{ $otherDetails->description  }}</p>
                             <p>  <b>Address:</b> {{$otherDetails->address or 'N/A'}}</p>
                             <p>  <b>Contact:</b> {{$otherDetails->phone or 'N/A'}}</p>
                            <p><a href="{{ $otherDetails->listing_base_url }}" class="view-agent" target="_blank">View agency profile</a></p>
                        </div>

                    </div>



                </div>
            </div>
        </div>
    </div>
    </div>
    <p style="display: none" class="lat" id="lat">{{$lat = $propertyDetails->lat}}</p>
    <p style="display: none" class="lng" id="lng">{{$propertyDetails->lng}}</p>





    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel-2">Floorplan</h4>
                </div>
                <div class="modal-body">
                    <img src="{{ \App\Models\PropertyHelper::getData($propertyDetails,'floorplan_1') }}" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

   <script>

        var iconBase = '{{asset("/img/icon-map-pin.png")}}';
        function initialize() {
            var lat = document.getElementById("lat").innerHTML;
            var lng = document.getElementById("lng").innerHTML;
            var myLatlng = new google.maps.LatLng(lat,lng);
            var myOptions = {
                center:new google.maps.LatLng(lat,lng),
                zoom:13,
                mapTypeId:google.maps.MapTypeId.ROADMAP,
                streetViewControl:true

            };
            var map=new google.maps.Map(document.getElementById("googleMap"), myOptions);

           var marker = new google.maps.Marker({
                position: myLatlng,
                title:"Property Location!",
                icon: iconBase
            });
            marker.setMap(map);
        }
     google.maps.event.addDomListener(window, 'load', initialize);




        var realSlider= $("ul#bxslider").bxSlider({
            speed:1000,
            pager:false,
            adaptiveHeight: false,
            mode: 'fade',
            infiniteLoop:true,
            hideControlOnEnd:true,
            nextSelector: '.slider-next',
            prevSelector: '.slider-prev',
            nextText: '<img src = "{{ asset('img/next-arrow.png') }}">',
            prevText: '<img src = "{{ asset('img/prv-arrow.png') }}">',
            onSlideBefore:function($slideElement, oldIndex, newIndex){
                changeRealThumb(realThumbSlider,newIndex);

            }

        });

        var realThumbSlider=$("ul#bxslider-pager").bxSlider({
            mode: 'vertical',
            minSlides: 5,
            maxSlides: 5,
            slideWidth: 276,
            slideMargin: 12,
            moveSlides: 1,
            pager:false,
            speed:1000,
            infiniteLoop:true,
            hideControlOnEnd:true,
            nextSelector: '.slider-next hidethis',
            prevSelector: '.slider-prev hidethis',
            nextText: '<img src = "{{ asset('img/next-arrow.png') }}">',
            prevText: '<img src = "{{ asset('img/prv-arrow.png') }}">',
            onSlideBefore:function($slideElement, oldIndex, newIndex){
                $("#sliderThumbReal ul .pager-active").removeClass("pager-active");
                 $slideElement.addClass("pager-active");

            }
        });

        linkRealSliders(realSlider,realThumbSlider);

        if($("#bxslider-pager li").length<5){
            $("#bxslider-pager .bx-next").hide();
        }

        // sincronizza sliders realizzazioni
        function linkRealSliders(bigS,thumbS){

            $("ul#bxslider-pager").on("click","li",function(event){
                event.preventDefault();
                var newIndex=$(this).attr("data-slideIndex");
                bigS.goToSlide(newIndex);
            });
        }

        //slider!=$thumbSlider. slider is the realslider
        function changeRealThumb(slider,newIndex){

            var $thumbS=$("#bxslider-pager");
            $thumbS.find('.pager-active').removeClass("pager-active");
            $thumbS.find('li[data-slideIndex="'+newIndex+'"]').addClass("pager-active");

            if(slider.getSlideCount()-newIndex>=4)slider.goToSlide(newIndex);
            else slider.goToSlide(slider.getSlideCount()-4);

        }
    </script>

@endsection