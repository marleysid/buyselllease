
@extends('layouts.default.master')
@section('header-script')
    <script type="text/javascript" src="{{ asset('/dist/iosCheckbox.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
     <script src="{{ asset('js/phone/dist/jquery.inputmask.bundle.js') }}"></script>
@endsection
@section('body-class', 'login-body')

@section('content-body')


    <div class="user-login points-content">

        <div class="container"> <!-- container-fluid -->

            <div class="row">
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12">
                    <div class="col-sm-12 login-box">
                        <div class="panel-body">
                            <h1>Edit Profile - Professional Account</h1>


                            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('professional/editprofile') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="username" value="{{ $details->name }}"
                                                   placeholder="username">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('username')!!}</span>

                                </div>


                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="firstname"
                                                   placeholder="firstname" value="{{ $details->firstname }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('firstname')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="lastname"
                                                   placeholder="lastname" value="{{ $details->lastname }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('lastname')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="streetaddress"
                                                   placeholder="street address" value="{{ $details->street_address }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('streetaddress')!!}</span>

                                </div>

                            <!--operation range-->
                                            <span class="form-titles"> Operation Range</span>
                                 <div class="checkbox">
                                  
                                    <label>
                                        <input type="radio" id="nationwide" name="searchtype" value="nationwide" {{($details->type == 'nationwide' ? 'checked' : '')}}>
                                         <span class="lbl padding-8">Nationwide </span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>

                                
                                  <label>
                                        <input type="radio" id="mysuburb" name="searchtype" value="suburb" {{($details->type == 'suburb' ? 'checked' : '')}}>
                                         <span class="lbl padding-8">Suburb </span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>


                               
                                <label>
                                        <input type="radio" id="region" name="searchtype" value="region" {{($details->type == 'region' ? 'checked' : '')}}>
                                         <span class="lbl padding-8">Region </span>
                                    </label>
                                    <span class="error">{!!$errors->first('searchtype')!!}</span>


                                </div> 

                            <!--operation range-->

                                

                            <!--block options-->
                                <div class="form-group suburb-block" style="display: {{($details->type == 'suburb' ? 'block' : 'none')}}">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="suburb" id="suburb"
                                                   placeholder="suburb of your business" value="{{ $details->suburb }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('suburb')!!}</span>

                                </div>

                                            <!--region block here-->
                                      <div class="form-group region-block" style="display: {{($details->type == 'region' ? 'block' : 'none')}}">
                                           <div class="input-group">
                                                  @foreach($region as $region)
                                                      <label>
                                                          <input type="checkbox" id="nationwide" name="region[]" value="{{$region->id}}" {{in_array($region->id, $col) ? 'checked' : ''}}>
                                                           <span class="lbl padding-8">{{$region->name}} </span>
                                                      </label>
                                                   @endforeach

                                              </div>

                                              <div class="input-group mycity">
                                                  <span class="input-group-addon city-block">
                                                        
                                                  </span>
                                              </div>




                                             
                                       </div>
                            <!--end of block options-->





                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="business"
                                                   placeholder="NAME OF YOUR BUSINESS" value="{{ $details->business_name }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('business')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="company"
                                                   placeholder="COMPANY NAME" value="{{ $details->company_name }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('company')!!}</span>

                                </div>

                                <!-- your input  avatar  here -->
                                <!-- your placeholder  AVATAR  here -->
                                <div class="form-group">
                                    @if($details->logo != '' && file_exists(public_path().'/uploads/professionals/'.$details->logo))
                                    <img src="{{asset('uploads/professionals').'/'.$details->logo}}" style="width: 100px;" class="img-thumbnail"/>
                                    @else
                                        <img src="{{asset('uploads/noimage.png')}}" style="width: 100px;" class="img-thumbnail"/>
                                        @endif
                                    <div class="input-group">
                                                 <span class="input-group-addon">
                                                     <label for="files" style="float:left; color:black;"> Edit Logo</label>
                                                     <input type="file" name="userfile" class=" form-control">
                                                 </span>
                                    </div>
                                    <span class="error">{!!$errors->first('logo')!!}</span>
                                </div>


                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="email"
                                                   placeholder="email" value="{{ $details->email }}" readonly>
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('email')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="position"
                                                   placeholder="your position in business" value="{{ $details->business_position }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('position')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" data-inputmask="'mask': '99-9999-9999'" name="work" value="{{ $details->contact }}"
                                                   placeholder="Work phone number">
                                            <i class="glyphicon glyphicon-earphone"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('work')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="mobile"
                                                   placeholder="mobile number" data-inputmask="'mask': '9999-999-999'" value="{{ $details->mobile_no }}">
                                            <i class="glyphicon glyphicon-phone"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('mobile')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <select class="form-control" name="category">

                                                    @foreach($profession as $pro)
                                                        <option value="{{ $pro->id }}" @if ($pro->id == $details->profession) selected @endif>{{ $pro->title }}</option>
                                                    @endforeach
                                            </select>
                                        </span>

                                    </div>
                                    <span class="error">{!!$errors->first('category')!!}</span>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="text" class="form-control" name="address"
                                                   placeholder="web address" value="{{ $details->url }}">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('address')!!}</span>

                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <textarea type="text" class="form-control" name="description"
                                                   placeholder="company description" rows="6">{{ $details->description }}</textarea>

                                    </div>
                                    <span class="error">{!!$errors->first('description')!!}</span>

                                </div>



                                <div class="row">
                                    <button type="submit" class="btn btn-primary btn-login" onclick=" return confirm('Do you sure want to update the information?')">Update</button>
                                </div>
                                <div class="col-sm-6">

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 space-v-h"></div>
                <!-- EMPTY col-1 -->


                <p>&nbsp;</p>

            </div>
        </div>
    </div>

    </div>
        <script>
        $(function() {
            //work phone number  formating
            $(":input").inputmask();
            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('suburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>



    <script type="text/javascript">
        $('input[name=searchtype]').change(function () {
               if (document.getElementById('nationwide').checked){
                   $('.suburb-block').hide();
                   $('.region-block').hide();
                   $('#suburb').prop('required', false);
                   $('#selectedregion').prop('required', false);
                   //disable the validation for other field
               } else if(document.getElementById('mysuburb').checked){
                   $('.suburb-block').show();
                   $('.region-block').hide();
                   $('#suburb').prop('required', true);
                   $('#selectedregion').prop('required', false);
               } else if(document.getElementById('region').checked){
                  $('.suburb-block').hide();
                  $('.region-block').show();
                  $('#selectedregion').prop('required', true);
                  $('#suburb').prop('required', false);
               }
            });
    </script>



@endsection
@endsection


