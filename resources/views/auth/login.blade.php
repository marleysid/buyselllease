@extends('layouts.default.master')
@section('body-class', 'login-body')

@section('content-body')

    <div class="user-login">

        <div class="container"> <!-- container-fluid -->
            @include('flash::message')
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="col-sm-12 login-box">
                        <div class="panel-body">
                            <h1>LOGIN</h1>

                            <h3>Have an account already?<br/>
                                Log in and start searching!</h3>

                            <form class="form-horizontal" role="form" method="POST" action="{{ url('auth/login') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="facebook-btn"><a href="{{ Route('facebook.login') }}"
                                                             class="btn btn-primary"><i class="fa fa-facebook"></i>
                                        Login with
                                        Facebook</a></div>
                                <div class="gplus-btn"><a href="{{ Route('google-plus.login') }}"
                                                          class="btn btn-primary"><i class="fa fa-google-plus"></i>
                                        Login with
                                        Google+</a>
                                </div>
                                <div class="col-sm-12">
                                    <div class="divider1">
                                        <h6>Or</h6>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" placeholder="E-Mail Address">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('email')!!}</span>        

                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="Your Password">
                                            <i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('password')!!}</span>
                                </div>
                                <div class="row">
                                    <button type="submit" class="btn btn-primary btn-login">Login</button>
                                </div>
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"><span class="lbl padding-8">Remember me</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your
                                        Password?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 space-v-h"></div>
                <!-- EMPTY col-1 -->

                <!-- JOIN US Cection -->
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 join-box">
                    <div class="col-sm-12">
                        <h1>JOIN US</h1>

                        <h3>Don’t have an account?<br/>
                            Register now, it is free and only takes few minutes!</h3>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ url('auth/register') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="text" class="form-control" name="signup_email"
                                                   value="{{ old('signup_email') }}" placeholder="Your Email">
                                            <i class="glyphicon glyphicon-user"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('signup_email')!!}</span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="password" class="form-control" name="signup_password"
                                                   placeholder="Password">
                                            <i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('signup_password')!!}</span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="password" class="form-control" name="password_confirmation"
                                                   placeholder="Retype Password">
                                            <i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('password_confirmation')!!}</span>
                                </div>
                                <div class="row">
                                    <button type="submit" class="btn btn-primary btn-signup">Sign up</button>
                                </div>

                            </form>
                        </div>

                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 buttons-col">

                        <div class="row">
                            <h4>BUILDING DIRECT RELATIONSHIPS</h4>
                            <h6><br/>Buy Sell Lease is a portal that builds instant direct relationships with people
                                wanting to buy,
                                sell and/or lease all types of property – residential, commercial, retail, investment,
                                industrial, development sites,
                                land, rural, etc – One Portal for all of your property listings.<br/><br/>

                            </h6>
                        </div>

                        <div class="row">
                            <a class="btn btn-secondary" href="{{ route('home.contactus') }}">Contact us</a>
                            <a class="btn btn-secondary" href="{{ route('home.howitworks') }}">Learn more</a>
                        </div>


                    </div>


                </div>
                <!-- end SIGN UP col-7 -->

                <p>&nbsp;</p>

            </div>
        </div>
    </div>

    </div>

@endsection
@endsection

