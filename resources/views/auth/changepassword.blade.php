@extends('layouts.default.master')
@section('body-class', 'login-body')

@section('content-body')

    <div class="user-login">

        <div class="container"> <!-- container-fluid -->

            <div class="row">
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="col-sm-12 login-box">
                        <div class="panel-body">
                            <h1>CHANGE PASSWORD</h1>


                            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('changepassword') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="password" class="form-control" name="oldpassword"
                                                    placeholder="Old Password">
                                            <i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('oldpassword')!!}</span>

                                </div>


                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="password" class="form-control" name="newpassword"
                                                   placeholder="New Password">
                                            <i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('newpassword')!!}</span>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                            <input type="password" class="form-control" name="confirmnewpassword"
                                                   placeholder="Retype New Password">
                                            <i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                    <span class="error">{!!$errors->first('confirmnewpassword')!!}</span>
                                </div>



                                <div class="row">
                                    <button type="submit" class="btn btn-primary btn-login">Confirm</button>
                                </div>
                                <div class="col-sm-6">

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 space-v-h"></div>
                <!-- EMPTY col-1 -->


                <p>&nbsp;</p>

            </div>
        </div>
    </div>

    </div>
@endsection
@endsection

